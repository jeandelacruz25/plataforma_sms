<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->unsignedInteger('id_tipo_identidad');
            $table->unsignedBigInteger('num_identidad');
            $table->date('fecha_nacimiento');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('avatar')->default('img/avatars/user_default.png');
            $table->string('locale')->default(config('app.locale'));
            $table->string('session_id', 100)->nullable();
            $table->unsignedInteger('id_status');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_tipo_identidad')
                ->references('id')
                ->on('tipo_identidad');

            $table->foreign('id_status')
                ->references('id')
                ->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
