<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->call(RoleTableSeeder::class);
        $this->call(TypeIdentitySeeder::class);
        $this->call(StatusTableSeeder::class);
	    $this->call(MenuTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
