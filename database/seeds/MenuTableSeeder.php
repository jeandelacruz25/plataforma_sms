<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu();
        $menu->name = 'Pagina en Blanco';
        $menu->parent = 0;
        $menu->order = 0;
        $menu->vue_name = 'blankPage';
        $menu->icon = 'feather feather-airplay';
        $menu->enabled = 1;
        $menu->created_at = \Carbon\Carbon::now();
        $menu->updated_at = \Carbon\Carbon::now();
        $menu->save();

        $menu = new Menu();
        $menu->name = 'Gestión';
        $menu->parent = 0;
        $menu->order = 1;
        $menu->vue_name = 'gestion';
        $menu->icon = 'feather feather-folder';
        $menu->enabled = 1;
        $menu->created_at = \Carbon\Carbon::now();
        $menu->updated_at = \Carbon\Carbon::now();
        $menu->save();

        $menu = new Menu();
        $menu->name = 'Roles';
        $menu->parent = 2;
        $menu->order = 0;
        $menu->vue_name = 'roles';
        $menu->icon = 'feather feather-users';
        $menu->enabled = 1;
        $menu->created_at = \Carbon\Carbon::now();
        $menu->updated_at = \Carbon\Carbon::now();
        $menu->save();

        $menu = new Menu();
        $menu->name = 'Usuarios';
        $menu->parent = 2;
        $menu->order = 1;
        $menu->vue_name = 'users';
        $menu->icon = 'feather feather-user';
        $menu->enabled = 1;
        $menu->created_at = \Carbon\Carbon::now();
        $menu->updated_at = \Carbon\Carbon::now();
        $menu->save();
    }
}
