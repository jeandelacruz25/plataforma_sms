<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $user = new User();
        $user->nombres = 'User';
        $user->apellidos = 'Second';
        $user->id_tipo_identidad = '1';
        $user->num_identidad = '76797459';
        $user->fecha_nacimiento = '1994-08-12';
        $user->email = 'user@example.com';
        $user->username = 'test';
        $user->password = bcrypt('12345');
        $user->id_status = 1;
        $user->save();
        $user->roles()->attach($role_user);

        $role_admin = Role::where('name', 'admin')->first();
        $admin = new User();
        $admin->nombres = 'Admin';
        $admin->apellidos = 'Last';
        $admin->id_tipo_identidad = '1';
        $admin->num_identidad = '76797459';
        $admin->fecha_nacimiento = '1994-08-12';
        $admin->email = 'admin@example.com';
        $admin->username = 'admin';
        $admin->password = bcrypt('12345');
        $admin->id_status = 1;
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
