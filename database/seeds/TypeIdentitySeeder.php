<?php

use Illuminate\Database\Seeder;
use App\Models\TipoIdentidad;

class TypeIdentitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_identidad = new TipoIdentidad();
        $tipo_identidad->tipo_identidad = 'DNI';
        $tipo_identidad->save();
    }
}
