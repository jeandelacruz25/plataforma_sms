const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* Estilo General */

mix.sass('resources/assets/bonvue/scss/login.scss', 'public/css');
mix.sass('resources/assets/bonvue/scss/front.scss', 'public/css');
mix.sass('resources/assets/bonvue/scss/pace.scss', 'public/css');

/* Fin Estilo General*/

/* Estilos para el Login */

mix.styles([
    'resources/assets/bonvue/vendors/material-icons/material-icons.css',
    'resources/assets/bonvue/vendors/linea-icons/styles.css',
    'resources/assets/bonvue/vendors/mono-social-icons/monosocialiconsfont.css',
    'resources/assets/bonvue/vendors/feather-icons/feather.css',
    'resources/utilities/css/icon_moon.css',
    'resources/utilities/css/login.css'
], 'public/css/securitec_login.css');

/* Fin Login */

/* Estilos para el Front */

mix.styles([
    'resources/assets/bonvue/vendors/jquery-ui/jquery-ui.css',
    'resources/assets/bonvue/vendors/material-icons/material-icons.css',
    'resources/assets/bonvue/vendors/linea-icons/styles.css',
    'resources/assets/bonvue/vendors/mono-social-icons/monosocialiconsfont.css',
    'resources/assets/bonvue/vendors/feather-icons/feather.css',
    'node_modules/mediaelement/src/css/mediaelementplayer.css',
    'node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.css',
    'node_modules/datatables/media/css/jquery.dataTables.css',
    'node_modules/vue2-animate/dist/vue2-animate.css',
    'node_modules/datedropper/datedropper.css',
    'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
    'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
    'resources/assets/bonvue/vendors/bootstrap-select/css/bootstrap-select.css',
    'resources/assets/bonvue/vendors/multi-select/css/multi-select.css',
    'resources/assets/bonvue/vendors/bootstrap-tagsinput/jquery.tag-editor.css',
    'resources/assets/bonvue/vendors/dropzone/dropzone.css',
    'node_modules/emojionearea/dist/emojionearea.css',
    'node_modules/skeleton-placeholder/dist/bone.min.css',
    'resources/utilities/js/datatables/plugins/responsive/responsive.bootstrap4.css',
    'resources/utilities/css/icon_moon.css',
    'resources/utilities/css/front.css',
    'resources/utilities/css/switcher.css',
    'node_modules/timeout-dialog-bootstrap/dist/timeout-dialog-bootstrap.css',
    'node_modules/flatpickr/dist/flatpickr.css'
], 'public/css/securitec_front.css');

/* Fin Front */

/* Javacript para el Login */

mix.babel([
    'node_modules/pace-js/pace.js'
], 'public/js/node_login.js');

mix.babel([
    'resources/utilities/js/helper_login.js'
], 'public/js/helper_login.js');

/* Fin Login */

/* Javacript para el front */

mix.js([
    'resources/assets/js/app.js'
], 'public/js/app_score.js');

mix.babel([
    'node_modules/socket.io-client/socket.io.min.js',
    'node_modules/pace-js/pace.js',
    'node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.js',
    'node_modules/jquery-form-validator/form-validator/jquery.form-validator.js',
    'node_modules/datedropper/datedropper.js',
    'node_modules/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js',
    'resources/assets/bonvue/vendors/bootstrap-select/js/bootstrap-select.js',
    'resources/assets/bonvue/vendors/bootstrap-select/js/i18n/defaults-en_US.js',
    'resources/assets/bonvue/vendors/bootstrap-select/js/i18n/defaults-es_ES.js',
    'resources/assets/bonvue/vendors/bootstrap-filestyle/bootstrap-filestyle.js',
    'resources/assets/bonvue/vendors/multi-select/js/jquery.multi-select.js',
    'resources/assets/bonvue/vendors/bootstrap-tagsinput/jquery.caret.min.js',
    'resources/assets/bonvue/vendors/bootstrap-tagsinput/jquery.tag-editor.js',
    'node_modules/jquery.quicksearch/dist/jquery.quicksearch.js',
    'node_modules/ion-sound/js/ion.sound.js',
    'resources/assets/bonvue/vendors/dropzone/dropzone.js',
    'node_modules/timeout-dialog-bootstrap/dist/timeout-dialog-bootstrap.js',
    'node_modules/flatpickr/dist/flatpickr.js'
], 'public/js/node_front.js');

mix.babel([
    'resources/utilities/js/cookie.js',
    'resources/utilities/js/helper_front.js'
], 'public/js/helper_front.js');

mix.babel([
    'resources/utilities/js/datatables/plugins/responsive/dataTables.responsive.min.js',
    'resources/utilities/js/datatables/plugins/responsive/responsive.bootstrap4.min.js',
    'node_modules/datatables.net-scroller/js/dataTables.scroller.js',
    'resources/utilities/js/datatables/language.js',
    'resources/utilities/js/datatables/columns.js',
    'resources/utilities/js/datatables.js'
], 'public/js/score_datatables.js');

mix.babel([
    '.env.js',
    'resources/utilities/js/nodejs/initScore.js',
    'resources/utilities/js/nodejs/onSocket.js'
], 'public/js/scoreNode.js').version();

/* Fin Front */

mix.copy('resources/utilities/js/vue'                              , 'public/js/vue');
mix.copy('resources/assets/fonts'                                  , 'public/fonts');
mix.copy('resources/assets/bonvue/vendors'                         , 'public/assets/utilities');
mix.copy('node_modules/datedropper/dd-icon'                        , 'public/css/dd-icon');
mix.copy('resources/utilities/js/form'                             , 'public/js/form');
mix.copy('resources/utilities/favicon.png'                         , 'public/favicon.png');
mix.copy('resources/assets/img'                                    , 'public/img');
mix.copy('resources/assets/images'                                 , 'public/images');
mix.copy('resources/assets/sounds'                                 , 'public/sounds');
mix.copy('resources/assets/examples'                               , 'public/examples');
mix.copy('resources/utilities/home'                                , 'public');
mix.copy('resources/utilities/js/utils'                            , 'public/js');
