
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.swal = require('sweetalert2');
window.moment = require('moment');
window.magnificPopup = require('magnific-popup');
window.DataTable = require('datatables');
window.datepicker = require('bootstrap-datepicker');
window.daterangepicker = require('daterangepicker');
window.io = require('socket.io-client');
window.emojioneArea = require('emojionearea');
window.Push = require('push.js');
window.download = require('downloadjs');
window.utf8 = require('utf8');
window.bootbox = require('bootbox');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const VueBar = require('vuebar');
const VueGoogleMaps = require('vue2-google-maps');
const VueCountTo = require('vue-count-to');

Vue.use(VueBar);
Vue.use(VueCountTo);
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyCKRc1ehX5BFBNHyLhP6WtQBqbwT42bmvg",
        libraries: "places" // necessary for places input
    }
});

Vue.component('pagination', require('./components/PaginationCampaignUser.vue'));

/*const app = new Vue({
    el: '#app'
});
*/