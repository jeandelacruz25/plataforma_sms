/**
 * Created by Carlos on 15/12/2017.
 *
 * [columnsDatatable description]
 * @routes El ID de la tabla
 * @return Devuelve las columnas a tomarse
 */
const columnsDatatable = (route) => {
    let columns = ''
    if (route === 'listRoles') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'status', name: 'status', className: 'text-center'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listUsers') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'avatar', name: 'avatar', orderable: false, searchable: false, width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'clientes', name: 'clientes', visible: false},
            {data: 'status', name: 'status', className: 'text-center'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listClientes') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'avatar', name: 'avatar', orderable: false, searchable: false, width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'ruc', name: 'ruc'},
            {data: 'status', name: 'status', className: 'text-center'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listCampaign') {
        columns = [
            {data: 'id', name: 'id', width: '20px', searchable: false},
            {data: 'name', name: 'name', orderable: false, searchable: false},
            {data: 'nameCampana', name: 'nameCampana', visible: false},
            {data: 'clientes', name: 'clientes', searchable: false, visible: false},
            {data: 'estado_sms', name: 'estado_sms', searchable: false},
            {data: 'telefonos', name: 'telefonos', className: 'text-center', searchable: false},
            {data: 'proceso_envio', name: 'proceso_envio', className: 'text-center', searchable: false},
            {data: 'fecha_envio', name: 'fecha_envio', className: 'text-center', searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listSMSIndividual') {
        columns = [
            { data: 'id', name: 'id', width: '20px' },
            { data: 'bulkid', name: 'bulkid' },
            { data: 'clientes', name: 'clientes', visible: false},
            { data: 'fecha_envio', name: 'fecha_envio' },
            { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center' }
        ]
    }
    if (route === 'listKeyword') {
        columns = [
            { data: 'id', name: 'id', width: '20px' },
            {data: 'name', name: 'name', orderable: false, searchable: false},
            {data: 'nameKeyword', name: 'nameKeyword', visible: false},
            {data: 'clientes', name: 'clientes', visible: false},
            {data: 'estado_servicio', name: 'estado_servicio', className: 'text-center'},
            {data: 'estado_error', name: 'estado_error', className: 'text-center'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listSendKeyword') {
        columns = [
            { data: 'id', name: 'id', width: '20px' },
            { data: 'bulkid', name: 'bulkid' },
            { data: 'clientes', name: 'clientes', visible: false },
            { data: 'keyword', name: 'keyword' },
            { data: 'fecha_envio', name: 'fecha_envio' },
            { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center' }
        ]
    }
    if (route === 'listBlackList') {
        columns = [
            { data: 'id', name: 'id', width: '20px' },
            { data: 'telefono', name: 'telefono' },
            { data: 'clientes', name: 'clientes', visible: false },
            { data: 'fecha_registro', name: 'fecha_envio' },
            { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center' }
        ]
    }
    return columns
}