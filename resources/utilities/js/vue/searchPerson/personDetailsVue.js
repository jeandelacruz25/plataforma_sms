'use strict'

var objPersonDetails = {
    numDNI: '',
    dataPerson: [],
    dataTelefonos: [],
    dataSugerenciaFamiliar: [],
    dataAddress: [],
    dataJobs: [],
    dataAutos: [],
    dataFamiliares: [],
    dataSunat: [],
    dataReporteSBS: [],
    initializeDataTelefonos: false,
    initializeDataSugerenciasFamilia: false,
    initializeDataAddress: false,
    initializeGoogleMaps: false,
    initializeDataJob: false,
    initializeDataAutos: false,
    initializeDataFamiliares: false,
    initializeDataSunat: false,
    initializeDataReporteSBS: false,
    initializeDataDetalleSBS: false,
    detalleSBS: {
        loadDetalleSBS: true,
        dataDetalleSBS: [],
        codigoSBS: ''
    },
    personPhoto: '',
    googleMaps: {
        zoom: 12,
        center: {},
        marker: {}
    },
    initFormValidacion: true
}

var vmPersonDetails = new Vue({
    el: '#personDetailsVue',
    data: objPersonDetails,
    computed: {
        namePersona() {
            return `${this.dataPerson.NOMBRES} ${this.dataPerson.APELLIDO_PAT} ${this.dataPerson.APELLIDO_MAT}`
        },
        dateBirthday() {
            return moment(this.dataPerson.FECHA_NAC).format('DD/MM/YYYY')
        },
        yearsPersona() {
            let dateBirthday = moment(this.dataPerson.FECHA_NAC)
            let dateActually = moment()

            return dateActually.diff(dateBirthday, 'years')
        },
        formatDateTimeValidacion() {
            if(this.dataTelefonos.length > 0) {
                return this.dataTelefonos.map(function (item) {
                    let numTelefono = item.TELEFONO
                    let dateTime = item.FECHAEVAL
                    if(numTelefono.length === 9){
                        return dateTime ? moment(dateTime).format('DD/MM/YYYY hh:mm:ss a') : '-'
                    }else{
                        return '-'
                    }
                })
            }
        },
        statusValidacion() {
            if(this.dataTelefonos.length > 0) {
                return this.dataTelefonos.map(function (item) {
                    let numTelefono = item.TELEFONO
                    let idStatus = parseInt(item.ESTADO)
                    if(numTelefono.length === 9){
                        return getStatusValidacion(idStatus)
                    }else{
                        return '-'
                    }
                })
            }
        },
        nameSugerenciaFamiliar() {
            return this.dataSugerenciaFamiliar.map(function(item) {
                return `${item.NOMBRES} ${item.APELLIDO_PAT} ${item.APELLIDO_MAT}`
            })
        },
        distritoAddress() {
            return this.dataAddress.map(function(item) {
                if(item['DISTRITO']){
                    return item['DISTRITO']
                }else{
                    return '-'
                }
            })
        },
        situationJob() {
            if(this.dataJobs.length > 0){
                return this.dataJobs.map(function(item) {
                    let statusJob = item['SITUACION']
                    let statusSend = String(statusJob)
                    let nameStatus = ''
                    switch (statusSend){
                        case 'A':
                            nameStatus = 'Activo'
                            break
                        case 'C':
                            nameStatus = 'Cesado'
                            break
                        default:
                            nameStatus = '-'
                            break
                    }
                    return nameStatus
                })
            }
        },
        dateJob() {
            return this.dataJobs.map(function (item) {
                let dateReport = item['PERIODO']
                let yearDateReport = dateReport.substring(0, 4)
                let monthDateReport = dateReport.substring(4, 6)
                moment.locale(vmFront.userInformation.locale)
                return `${ucwords(moment(monthDateReport).format('MMMM'))} del ${yearDateReport}`
            })
        },
        dateBirthdayFamily() {
            return this.dataFamiliares.map(function (item) {
                return moment(item['FECHANAC_F']).isValid() ? moment(item['FECHANAC_F']).format('DD/MM/YYYY') : '-'
            })
        },
        yearsFamily() {
            return this.dataFamiliares.map(function (item) {
                let dateBirthday = moment(item['FECHANAC_F'])
                let dateActually = moment()

                return moment(item['FECHANAC_F']).isValid() ? dateActually.diff(dateBirthday, 'years') : '-'
            })
        },
        typeFamily() {
            return this.dataFamiliares.map(function (item) {
                return item['PARENTESCO']
            })
        },
        dateFormatReportSBS() {
            return this.dataReporteSBS.map(function (item) {
                let dateReport = item.FECHAREPORTE
                let yearDateReport = dateReport.substring(0, 4)
                let monthDateReport = dateReport.substring(4, 6)
                let dayDateReport = dateReport.substring(6, 8)
                return `${dayDateReport}/${monthDateReport}/${yearDateReport}`
            })
        },
        numberEntidadesReportSBS() {
            return this.dataReporteSBS.map(function (item) {
                let numberEntidades = parseInt(item.CANTIDADEMPRESAS, 10)
                return `${numberEntidades}`
            })
        },
        calificationClassDetalleSBS() {
            return this.detalleSBS.dataDetalleSBS.map(function (item) {
                let calificacion = parseInt(item.CLASIFICACION)
                let colorCalificacion = ''
                switch (calificacion){
                    case 0:
                        colorCalificacion = 'bg-nor'
                        break
                    case 1:
                        colorCalificacion = 'bg-cpp'
                        break
                    case 2:
                        colorCalificacion = 'bg-def'
                        break
                    case 3:
                        colorCalificacion = 'bg-dud'
                        break
                    case 4:
                        colorCalificacion = 'bg-per'
                        break
                    default:
                        colorCalificacion = 'bg-scal'
                        break
                }

                return colorCalificacion
            })
        },
        calificationDetalleSBS() {
            return this.detalleSBS.dataDetalleSBS.map(function (item) {
                let calificacion = parseInt(item.CLASIFICACION)
                let statusCalificacion = ''
                switch (calificacion){
                    case 0:
                        statusCalificacion = 'NOR'
                        break
                    case 1:
                        statusCalificacion = 'CPP'
                        break
                    case 2:
                        statusCalificacion = 'DEF'
                        break
                    case 3:
                        statusCalificacion = 'DUD'
                        break
                    case 4:
                        statusCalificacion = 'PER'
                        break
                    default:
                        statusCalificacion = 'SCAL'
                        break
                }

                return statusCalificacion
            })
        },
        nameEmpresaSBSDetalle() {
            return this.detalleSBS.dataDetalleSBS.map(function (item) {
                let nameEmpresa = item.EMPRESA
                return `${nameEmpresa}`
            })
        },
        tipoProductoSBS() {
            return this.detalleSBS.dataDetalleSBS.map(function (item) {
                let tipoProducto = ''
                let tipoCredito = item.TIPOCREDITO
                if(parseInt(tipoCredito)  % 1 === 0){
                    tipoProducto = '-'
                }else{
                    tipoProducto = tipoCredito
                }
                return `${tipoProducto}`
            })
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getDataPerson: async function (){
            const dataPerson = await this.sendUrlRequest('/getDataPersona', {
                params: {
                    dniPersona: this.numDNI
                }
            }, 'get')
            this.dataPerson = dataPerson.datosPersonales
            this.dataTelefonos = dataPerson.datosTelefonos
            this.dataAddress = dataPerson.datosDirecciones
            this.dataFamiliares = dataPerson.datosFamiliares
            this.dataAutos = dataPerson.datosAutos
            this.dataJobs = dataPerson.datosLaborales
            let photoPerson = (dataPerson.datosPersonales.NRODOC).replace(/ /g,'')
            this.personPhoto = `images/faces/${photoPerson}.jpg`
            this.initializeDataTelefonos = true
            this.initializeDataAddress = true
            this.initializeDataFamiliares = true
            this.initializeDataAutos = true
            this.initializeDataJob = true

            if((vmFront.menuCliente).indexOf('validacion_telefonos') <= 0 && (vmFront.menuUsuario).indexOf('validacion_telefonos') >= 0 || (vmFront.menuCliente).indexOf('validacion_telefonos') <= 0 && (vmFront.menuUsuario).indexOf('validacion_telefonos') <= 0 || (vmFront.menuCliente).indexOf('validacion_telefonos') >= 0 && (vmFront.menuUsuario).indexOf('validacion_telefonos') <= 0) this.initFormValidacion = false

            if(this.dataAddress.length > 0){
                await this.getDataGoogleMaps(`${this.dataAddress[0]['DIRECCION']}, ${this.dataAddress[0]['DISTRITO']} / ${this.dataAddress[0]['DISTRITO']}`)
            }else{
                this.googleMaps.zoom = 10
                this.googleMaps.center = { lat: -12.03851, lng: -77.050904 }
                this.googleMaps.marker = { lat: -12.03851, lng: -77.050904 }
                this.initializeGoogleMaps = true
            }
        },
        getDataGoogleMaps: async function(dataAddress) {
            this.initializeGoogleMaps = false
            const response = await this.sendUrlRequest('/api/getCoordinateMaps', {
                params: {
                    dataAddress: `${dataAddress}`
                }
            }, 'get')

            this.googleMaps.zoom = 12
            this.googleMaps.center = { lat: response.latitude, lng: response.longitude }
            this.googleMaps.marker = { lat: response.latitude, lng: response.longitude }
            this.initializeGoogleMaps = true
        },
        getDataPersonTelefonos: async function() {
            this.initializeDataTelefonos = false
            const dataTelefonos = await this.sendUrlRequest('/getDataTelefonos', {
                params: {
                    dniPersona: this.numDNI
                }
            }, 'get')

            this.dataTelefonos = dataTelefonos
            this.initializeDataTelefonos = true
        },
        getDataPersonSunat: async function () {
            const dataRUC = await this.sendUrlRequest('/getDataPersonSunat', {
                params: {
                    dniPersona: this.numDNI
                }
            }, 'get')
            if(dataRUC.success){
                this.dataSunat = dataRUC.result
            }
            this.initializeDataSunat = true
        },
        getDataPersonReporteSBS: async function () {
            const dataReporteSBS = await this.sendUrlRequest('/getDataPersonReporteSBS', {
                params: {
                    dniPersona: this.numDNI
                }
            }, 'get')
            this.dataReporteSBS = dataReporteSBS
            this.initializeDataReporteSBS = true
            if(this.dataReporteSBS.length > 0){
                await this.getDataPersonDetalleSBS(this.dataReporteSBS[0].CODIGOSBS)
            }
        },
        getDataPersonDetalleSBS: async function (codigoSBS) {
            this.initializeDataDetalleSBS = true
            this.detalleSBS.loadDetalleSBS = false
            const dataDetalleSBS = await this.sendUrlRequest('/getDataPersonDetalleSBS', {
                params: {
                    codigoSBS: codigoSBS
                }
            }, 'get')
            this.detalleSBS.dataDetalleSBS = dataDetalleSBS
            this.detalleSBS.loadDetalleSBS = true
        },
        getDataAll: async function (){
            await this.getDataPerson()
            await this.getDataPersonSunat()
            await this.getDataPersonReporteSBS()
        },
        imageLoadError () {
            this.personPhoto = `img/avatars/user_default.png`
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        }
    }
})