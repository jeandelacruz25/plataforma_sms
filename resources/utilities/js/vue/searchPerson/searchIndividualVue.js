'use strict'

var objSearchPerson = {
    dataPerson: [],
    filterPerson: {
        departamentos: [],
        provincias: [],
        distritos: []
    },
    dataAutos: [],
    dataSunat: [],
    pagination: {
        current_page: 1
    },
    paginationAutos: {
        current_page: 1
    },
    initializeSugerencias: false,
    initializeSugerenciasAuto: false,
    initializeSearchDNI: false,
    initializeSearchRUC: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    formData: {
        numDNI: '',
        apePaterno: '',
        apeMaterno: '',
        nomPersona: '',
        placaAuto:''
    }
}

var vmSearchPerson = new Vue({
    el: '#searchIndividualVue',
    data: objSearchPerson,
    computed: {
        tipoDoc() {
            if (this.dataPerson.length > 0) {
                return this.dataPerson.map(function (item) {
                    let tipoDoc = item.TIPODOC
                    let nameDoc = ''
                    switch(tipoDoc){
                        case '00':
                            nameDoc = 'OTROS'
                            break
                        case '01':
                            nameDoc = 'DNI'
                            break
                        case '04':
                            nameDoc = 'CARNET DE EXTRANJERIA'
                            break
                        case '06':
                            nameDoc = 'RUC'
                            break
                        case '07':
                            nameDoc = 'PASAPORTE'
                            break
                        case '11':
                            nameDoc = 'PART. DE NACIMIENTO IDENTIDAD'
                            break
                    }
                    return nameDoc
                })
            }
        },
        namePerson() {
            if (this.dataPerson.length > 0) {
                return this.dataPerson.map(function (item) {
                    return `${item.APELLIDO_PAT} ${item.APELLIDO_MAT} ${item.NOMBRES}`
                })
            }
        },
        dateBirthday() {
            if (this.dataPerson.length > 0) {
                return this.dataPerson.map(function (item) {
                    return moment(item.FECHA_NAC).format('DD/MM/YYYY')
                })
            }
        },
        yearsPersona() {
            if (this.dataPerson.length > 0) {
                return this.dataPerson.map(function (item) {
                    let dateBirthday = moment(item.FECHA_NAC)
                    let dateActually = moment()
                    return dateActually.diff(dateBirthday, 'years')
                })
            }
        }
    },
    mounted() {
        this.initSearchPerson()
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        initSearchPerson: async function (){
            if(localStorage.getItem('objSearchPerson')){
                let objPerson = localStorage.getItem('objSearchPerson')
                let objData = JSON.parse(objPerson)

                if(objData.dniPersona){
                    this.formData.numDNI = objData.dniPersona
                }else if(objData.apeMaterno || objData.apePaterno || objData.nomPersona){
                    this.formData.apePaterno = objData.apePaterno
                    this.formData.apeMaterno = objData.apeMaterno
                    this.formData.nomPersona = objData.nomPersona
                    this.pagination = { current_page: objData.page }
                }else{
                    this.formData.numPlaca = objData.numPlaca
                    this.paginationAutos = { current_page: objData.page }
                }
            }
        },
        searchPerson: async function (){
            changeButtonForm('btnForm','btnLoad')
            $('#chartHTMLSunat').html('')
            $('#chartScriptSunat').html('')
            try {
                this.initializeSugerencias = false
                this.initializeSugerenciasAuto = false
                this.initializeSearchDNI = false
                this.initializeLoadPaginate = false
                this.dataPerson = []
                this.dataAutos =[]
                this.pagination = { current_page: 1 }
                this.paginationAutos = { current_page: 1 }
                const response = await axios['post']('/getDataFormIndividualSearch', {
                    dniPersona: this.formData.numDNI,
                    numPlacaAuto: this.formData.placaAuto,
                    apePatPersona: this.formData.apePaterno,
                    apeMatPersona: this.formData.apeMaterno,
                    nombrePersona: this.formData.nomPersona
                })
                if(response.data.message === 'Success'){
                    if(response.data.viewRuc){
                        await this.searchRUC()
                    }else if(response.data.numDNI){
                        await this.searchDNI()
                    }else{
                        await this.loadTableSugerencias()
                    }
                }else if(response.data.message === 'SuccessAuto'){
                    await this.loadTableAutoSugerencias()
                }
            } catch (error) {
                changeButtonForm('btnLoad','btnForm')
                showErrorForm(error.response.data, '.formIndividualError')
                this.dataPerson = []
                this.dataAutos = []
            }
        },
        searchDNI: async function (){
            this.initializeSugerencias = false
            this.initializeSugerenciasAuto = false
            this.initializeSearchDNI = false
            this.initializeLoadPaginate = false
            this.dataPerson = []
            this.dataAutos = []
            this.dataRUC = []
            this.pagination = { current_page: 1 }
            this.paginationAutos = { current_page: 1 }
            this.initializeSearchDNI = true
            const response = await this.sendUrlRequest('/getPersonDNI', {
                params: {
                    dniPersona: this.formData.numDNI
                }
            }, 'get')
            if(Object.keys(response).length > 0){
                this.initDataPagination = false
                let numDNI = this.formData.numDNI
                this.clickLoadOption(numDNI)
                localStorage.setItem('objSearchPerson', JSON.stringify({
                    dniPersona: this.formData.numDNI
                }))
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.numDNI, found: true }, 'post')
            }else{
                this.initializeSearchDNI = false
                this.initDataPagination = true
                changeButtonForm('btnLoad','btnForm')
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.numDNI, found: false }, 'post')
            }
        },
        searchRUC: async function () {
            this.initializeSugerencias = false
            this.initializeSugerenciasAuto = false
            this.initializeSearchDNI = false
            this.initializeSearchRUC = false
            this.initializeLoadPaginate = false
            this.dataPerson = []
            this.dataAutos = []
            this.dataRUC = []
            this.pagination = { current_page: 1 }
            this.paginationAutos = { current_page: 1 }
            this.initializeSearchRUC = true
            const dataRUC = await this.sendUrlRequest('/getDataPersonSunatGraph', {
                params: {
                    dniPersona: this.formData.numDNI
                }
            }, 'get')
            if(dataRUC.msg){
                this.initializeSearchRUC = false
                this.initDataPagination = true
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.numDNI, found: false }, 'post')
            }else{
                this.initDataPagination = false
                this.dataSunat = dataRUC.dataSunat.result
                let chartHTML = dataRUC.chartHTML
                let chartScript = dataRUC.chartScript
                if((this.dataSunat.cantidad_trabajadores).length > 0){
                    $('#chartHTMLSunat').html(`<h5 class="custom-title mt-0">Cantidad de Trabajadores y/o Prestadores de Servicios</h5><div class="border border-top-0 p-3">${chartHTML}</div>`)
                    $('#chartScriptSunat').html(chartScript)
                }
                this.initializeSearchRUC = false
                localStorage.setItem('objSearchPerson', JSON.stringify({
                    dniPersona: this.formData.numDNI
                }))
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.numDNI, found: true }, 'post')
            }
            changeButtonForm('btnLoad','btnForm')
        },
        loadTableSugerencias: async function (){
            if(this.dataPerson.length === 0){
                this.initializeSugerencias = true
                this.initializeSugerenciasAuto = false
                this.initializeSearchDNI = false
                this.paginationAutos = { current_page: 1 }
            }
            this.dataAutos = []
            this.dataRUC = []
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest('/paginationDataPersona', {
                params: {
                    page: this.pagination.current_page,
                    dniPersona: this.formData.numDNI,
                    nomPersona: this.formData.nomPersona,
                    apePaterno: this.formData.apePaterno,
                    apeMaterno: this.formData.apeMaterno,
                }
            } , 'get')

            this.dataPerson = response.data.data
            this.pagination = response.pagination

            if(this.dataPerson.length > 0){
                this.initDataPagination = false
                localStorage.setItem('objSearchPerson', JSON.stringify({
                    page: this.pagination.current_page,
                    nomPersona: this.formData.nomPersona,
                    apePaterno: this.formData.apePaterno,
                    apeMaterno: this.formData.apeMaterno
                }))
                this.sendUrlRequest('/baseEstadistica', { numDNI: `${this.formData.nomPersona} ${this.formData.apePaterno} ${this.formData.apeMaterno}`, found: true }, 'post')
            }else{
                this.initDataPagination = true
                this.sendUrlRequest('/baseEstadistica', { numDNI: `${this.formData.nomPersona} ${this.formData.apePaterno} ${this.formData.apeMaterno}`, found: false }, 'post')
            }

            this.initializeLoadPaginate = true
            this.initializeSugerencias = false
            this.initializeSugerenciasAuto = false
            this.initializeSearchDNI = false
            changeButtonForm('btnLoad','btnForm')
        },
        loadTableAutoSugerencias: async function (){
            if(this.dataAutos.length === 0){
                this.initializeSugerencias = false
                this.initializeSugerenciasAuto = true
                this.initializeSearchDNI = false
                this.pagination = { current_page: 1 }
            }
            this.dataPerson = []
            this.dataRUC = []
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest('/paginationDataAuto', {
                params: {
                    page: this.paginationAutos.current_page,
                    numPlaca: this.formData.placaAuto
                }
            } , 'get')
            this.dataAutos = response.data.data
            this.paginationAutos = response.pagination

            if(this.dataAutos.length > 0){
                this.initDataPagination = false
                localStorage.setItem('objSearchPerson', JSON.stringify({
                    page: this.paginationAutos.current_page,
                    numPlaca: this.formData.placaAuto
                }))
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.placaAuto, found: true }, 'post')
            }else{
                this.initDataPagination = true
                this.sendUrlRequest('/baseEstadistica', { numDNI: this.formData.placaAuto, found: false }, 'post')
            }

            this.initializeLoadPaginate = true
            this.initializeSugerencias = false
            this.initializeSugerenciasAuto = false
            this.initializeSearchDNI = false
            changeButtonForm('btnLoad','btnForm')
        },
        clickLoadOption: async function (numDNI){
            this.initializeLoadPaginate = false
            await vmFront.loadOptionMenu('personDetails', {
                numDNI: numDNI
            }, 'post')
            this.initializeLoadPaginate = true
        },
        imageLoadError: function () {
            return `img/avatars/default.png`
        },
        eraseFormData: function () {
            this.formData.numDNI = ''
            this.formData.apePaterno = ''
            this.formData.apeMaterno = ''
            this.formData.nomPersona = ''
            this.formData.placaAuto = ''
            localStorage.removeItem('objSearchPerson')
        }
    }
})