'use strict'

var objFormSearchMasivo = {
    formData: {
        numDNI: '',
        apePaterno: '',
        apeMaterno: '',
        nomPersona: '',
        placaAuto:''
    }
}

var vmFormSearchPerson = new Vue({
    el: '#searchMasivoFormVue',
    data: objFormSearchMasivo,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                const response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        }
    }
})