'use strict'

var objSMSReceivedReport = {
    selectClientes: '',
    selectCampana: '',
    optionsListEnvio: []
}

var vmSMSReceivedReport = new Vue({
    el: '#smsReceivedReportVue',
    data: objSMSReceivedReport,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                const response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        changeCliente: function(){
            setTimeout(function() {
                $('.selectpickerRecibido').selectpicker('refresh')
            }, 100)
            if(this.selectClientes != '') this.changeCampana()
            if(this.selectClientes === '') {
                this.selectCampana = ''
            }
        },
        changeCampana: async function (){
            if(this.selectClientes != '' && this.selectCampana != ''){
                let listCampana = await this.sendUrlRequest('/listReportCampana', {
                    tipoEnvio: this.selectCampana,
                    idCliente: this.selectClientes
                })
                this.optionsListEnvio = listCampana
                setTimeout(function() {
                    $('.selectListEnvioReceived').selectpicker({ style: 'btn-default btn-sm' })
                    $('.selectListEnvioReceived').selectpicker('refresh')
                }, 100)
            }
        }
    }
})