'use strict'

var objSMSReport = {
    selectClientes: '',
    selectCampana: '',
    optionsListEnvio: []
}

var vmSMSReport = new Vue({
    el: '#smsReportVue',
    data: objSMSReport,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                const response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        changeCliente: function(){
            setTimeout(function() {
                $('.selectpicker').selectpicker('refresh')
            }, 100)
            if(this.selectClientes != '') this.changeCampana()
            if(this.selectClientes === '') {
                this.selectCampana = ''
            }
        },
        changeCampana: async function (){
            if(this.selectClientes != '' && this.selectCampana != ''){
                let listCampana = await this.sendUrlRequest('/listReportCampana', {
                    tipoEnvio: this.selectCampana,
                    idCliente: this.selectClientes
                })
                this.optionsListEnvio = listCampana
                setTimeout(function() {
                    $('.selectListEnvio').selectpicker({ style: 'btn-default btn-sm' })
                    $('.selectListEnvio').selectpicker('refresh')
                    $('.filtroEnvio').selectpicker({ style: 'btn-default btn-sm' })
                    $('.filtroEnvio').selectpicker('refresh')
                }, 100)
            }
        }
    }
})