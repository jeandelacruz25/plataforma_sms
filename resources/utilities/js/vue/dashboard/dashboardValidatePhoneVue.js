'use strict'

var objDashboardValidatePhone = {
    boxCount: {
        prendido: 0,
        buzon: 0,
        nocontesta: 0,
        validados: 0,
        campanas: 0,
        campanassinlanzar: 0
    }
}

var vmDashboardValidatePhone = new Vue({
    el: '#dashboardValidatePhoneVue',
    data: objDashboardValidatePhone,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        chartDashboardValidateDay: async function (requestType, url, parametersChart = {}) {
            $('#dashboardValidateDayChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardValidateDayChartHTML').html(response.chartHTML)
            $('#dashboardValidateDayCharJS').html(response.chartScript)
        },
        chartDashboardNumbersValidate: async function (requestType, url, parametersChart = {}) {
            $('#dashboardNumbersValidateChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardNumbersValidateChartHTML').html(response.chartHTML)
            $('#dashboardNumbersValidateCharJS').html(response.chartScript)
        },
        chartDashboardValidateMonth: async function (requestType, url, parametersChart = {}) {
            $('#dashboardMonthValidateChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardMonthValidateChartHTML').html(response.chartHTML)
            $('#dashboardMonthValidateCharJS').html(response.chartScript)
        },
        loadDashboardValidatePhone: async function () {
            this.clearDashboardValidatePhone()
            await this.listEstadosValidatePhone()
            await this.chartDashboardValidateDay('post', '/dashboardValidateTelephoneDay', {})
            await this.chartDashboardNumbersValidate('post', '/dashboardNumbersValidate', {})
            await this.chartDashboardValidateMonth('post', '/dashboardValidatePhoneMonth', {})
        },
        clearDashboardValidatePhone: function() {
            this.boxCount.prendido = 0
            this.boxCount.buzon = 0
            this.boxCount.nocontesta = 0
            this.boxCount.validados = 0
            this.boxCount.campanas = 0
            this.boxCount.campanassinlanzar = 0
            $('#dashboardValidateDayChartHTML').html('<div class="cssload-loader"></div>')
            $('#dashboardNumbersValidateChartHTML').html('<div class="cssload-loader"></div>')
            $('#dashboardMonthValidateChartHTML').html('<div class="cssload-loader"></div>')
        },
        listEstadosValidatePhone: async function () {
            const response = await this.sendUrlRequest('/listEstadosValidatePhone', {}, 'post')
            if(Object.keys(response.estadosValidados).length > 0){
                if(response.estadosValidados.prendido) this.boxCount.prendido = response.estadosValidados.prendido
                if(response.estadosValidados.buzon_voz) this.boxCount.buzon = response.estadosValidados.buzon_voz
                if(response.estadosValidados.no_contesta) this.boxCount.nocontesta = response.estadosValidados.no_contesta
                if(response.numerosValidados) this.boxCount.validados = response.numerosValidados
                if(response.campanasCreadas) this.boxCount.campanas = response.campanasCreadas
                if(response.campanasSinLanzar) this.boxCount.campanassinlanzar = response.campanasSinLanzar
            }
        }
    }
})