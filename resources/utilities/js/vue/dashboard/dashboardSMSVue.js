'use strict'

var objDashboardSMS = {
    monthReceived: {
        totalSMS: 0
    },
    monthSend: {
        totalSMS: 0
    },
    boxCount: {
        sinEnviar: 0,
        pendiente: 0,
        noEntregado: 0,
        entregado: 0,
        expirado: 0,
        rechazado: 0,
        listaNegra: 0
    }
}

var vmDashboardSMS = new Vue({
    el: '#dashboardSMSVue',
    data: objDashboardSMS,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        chartDashboardSMSSend: async function (requestType, url, parametersChart = {}) {
            $('#dashboardSendChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardSendChartHTML').html(response.chartHTML)
            $('#dashboardSendCharJS').html(response.chartScript)
            this.monthSend.totalSMS = response.countSMS
        },
        chartDashboardSMSReceived: async function (requestType, url, parametersChart = {}) {
            $('#dashboardReceivedChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardReceivedChartHTML').html(response.chartHTML)
            $('#dashboardReceivedCharJS').html(response.chartScript)
            this.monthReceived.totalSMS = response.countSMS
        },
        chartDashboardMonth: async function (requestType, url, parametersChart = {}) {
            $('#dashboardMonthChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardMonthChartHTML').html(response.chartHTML)
            $('#dashboardMonthCharJS').html(response.chartScript)
        },
        loadDashboardSMS: async function () {
            this.clearDashboardSMS()
            await this.listEstadosSMS()
            await this.chartDashboardSMSSend('post', '/dashboardSMSSend', {})
            await this.chartDashboardSMSReceived('post', '/dashboardSMSReceived', {})
            await this.chartDashboardMonth('post', '/dashboardSMSMonth', {})
        },
        clearDashboardSMS: function() {
            this.boxCount.pendiente = 0
            this.boxCount.noEntregado = 0
            this.boxCount.entregado = 0
            this.boxCount.expirado = 0
            this.boxCount.rechazado = 0
            this.boxCount.listaNegra = 0
            this.monthSend.totalSMS = 0
            $('#dashboardSendChartHTML').html('<div class="cssload-loader"></div>')
            this.monthReceived.totalSMS = 0
            $('#dashboardReceivedChartHTML').html('<div class="cssload-loader"></div>')
            $('#dashboardMonthChartHTML').html('<div class="cssload-loader"></div>')
        },
        listEstadosSMS: async function () {
            const response = await this.sendUrlRequest('/listEstadosSMS', {}, 'post')
            if(Object.keys(response.smsSinEnviar).length > 0){
                this.boxCount.sinEnviar = response.smsSinEnviar.aun_sin_enviar
            }
            if(Object.keys(response.smsEnviados).length > 0){
                if(response.smsEnviados.pendiente) this.boxCount.pendiente = response.smsEnviados.pendiente
                if(response.smsEnviados.no_entregado) this.boxCount.noEntregado = response.smsEnviados.no_entregado
                if(response.smsEnviados.entregado) this.boxCount.entregado = response.smsEnviados.entregado
                if(response.smsEnviados.expirado) this.boxCount.expirado = response.smsEnviados.expirado
                if(response.smsEnviados.rechazado) this.boxCount.rechazado = response.smsEnviados.rechazado
                if(response.smsEnviados.lista_negra) this.boxCount.listaNegra = response.smsEnviados.lista_negra
            }
        }
    }
})