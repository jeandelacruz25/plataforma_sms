'use strict'

var objDashboardBases = {
    boxCount: {
        usuarios: 0,
        campanas: 0,
        campanassincruzar: 0,
        documentosbuscados: 0,
        documentosencontrados: 0,
        efectividadbusqueda: 0
    }
}

var vmDashboardBases = new Vue({
    el: '#dashboardBasesVue',
    data: objDashboardBases,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        dashboardTotalEncontrados: async function (requestType, url, parametersChart = {}) {
            $('#dashboardTotalEncontradosChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardTotalEncontradosChartHTML').html(response.chartHTML)
            $('#dashboardTotalEncontradosChartJS').html(response.chartScript)
        },
        dashboardBuscadosEncontrados: async function (requestType, url, parametersChart = {}) {
            $('#dashboardDiferenciadoChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardDiferenciadoChartHTML').html(response.chartHTML)
            $('#dashboardDiferenciadoChartJS').html(response.chartScript)
        },
        chartDashboardBasesMonth: async function (requestType, url, parametersChart = {}) {
            $('#dashboardMonthCrucesChartHTML').html('<div class="cssload-loader"></div>')
            const response = await this.sendUrlRequest(`${url}`, parametersChart, requestType)
            $('#dashboardMonthCrucesChartHTML').html(response.chartHTML)
            $('#dashboardMonthCrucesCharJS').html(response.chartScript)
        },
        loadDashboardBases: async function () {
            this.clearDashboardBases()
            await this.listBoxBases()
            await this.dashboardTotalEncontrados('post', '/dashboardTotalEncontrados', {})
            await this.dashboardBuscadosEncontrados('post', '/dashboardBuscadosEncontrados', {})
            await this.chartDashboardBasesMonth('post', '/dashboardBasesMonth', {})
        },
        clearDashboardBases: function() {
            this.boxCount.usuarios = 0
            this.boxCount.campanas = 0
            this.boxCount.campanassincruzar = 0
            this.boxCount.documentosbuscados = 0
            this.boxCount.documentosencontrados = 0
            this.boxCount.efectividadbusqueda = 0
            $('#dashboardTotalEncontradosChartHTML').html('<div class="cssload-loader"></div>')
            $('#dashboardDiferenciadoChartHTML').html('<div class="cssload-loader"></div>')
            $('#dashboardMonthCrucesChartHTML').html('<div class="cssload-loader"></div>')
        },
        listBoxBases: async function () {
            const response = await this.sendUrlRequest('/listBoxBases', {}, 'post')
            if(response.usuariosBases) this.boxCount.usuarios = parseInt(response.usuariosBases)
            if(response.campanasCreadas) this.boxCount.campanas = parseInt(response.campanasCreadas)
            if(response.campanasSinCruzar) this.boxCount.campanassincruzar = parseInt(response.campanasSinCruzar)
            if(response.campanasSinCruzar) this.boxCount.campanassincruzar = parseInt(response.campanasSinCruzar)
            if(Object.keys(response.queryBasesRaw[0]).length > 0){
                let queryBasesRaw = response.queryBasesRaw[0]
                if(queryBasesRaw.total) this.boxCount.documentosbuscados = parseInt(queryBasesRaw.total)
                if(queryBasesRaw.encontrados) this.boxCount.documentosencontrados = parseInt(queryBasesRaw.encontrados)
                if(queryBasesRaw.efectivos) this.boxCount.efectividadbusqueda = Math.round(queryBasesRaw.efectivos)
            }
        }
    }
})