'use strict'

var objFront = {
    userInformation: '',
    userInformationCliente: '',
    menuUsuario: [],
    menuCliente: [],
    utilitiesUser: {
        totalUsers: '',
        totalUsersMonth: '',
        lastUser: ''
    },
    nameRoute: '',
    nameMenu: '',
    chatSMSUbicationCampaign: '',
    chatSMSUbicationNotCampaign: '',
    listNotificationSMS: [],
    listNotification: []
}

var vmFront = new Vue({
    el: '#frontVue',
    data: objFront,
    computed: {
        avatarUser: function(){
            return `${this.userInformation.avatar}?version=${moment().format('YmdHis')}`
        },
        listFixNotificationSMS: function(){
            return multiDimensionalUnique(this.listNotificationSMS)
        },
        listFixNotification: function(){
            return this.listNotification.reverse()
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        loadAllMethods: async function() {
            await this.getUserInformation()
            await this.getClienteMenu()
            await this.getUserMenu()
            await this.loadOptionMenu('dashboard', {}, 'get')
            await getNotificationSMS()
        },
        loadOptionMenu: async function (routeMenu, parameters = {}, typeRequest = 'post') {
            $('#container').html(`<div class="row"><div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div></div>`)
            $('#container').html(await this.sendUrlRequest(`/${routeMenu}`, parameters, typeRequest))
            this.nameMenu = routeMenu
        },
        getUserInformation: async function (socketEmit = true) {
            const userInformation = await this.sendUrlRequest('/getUserInformation', {}, 'get')
            this.userInformation = userInformation[0]
            this.userInformationCliente = userInformation[0].clientes
            const nameCliente = (userInformation[0].clientes.palabra_clave).toLowerCase()
            if(socketEmit) socketNodejs.emit('createRoom', { nameRoom: nameCliente, userID: userInformation[0].id})
        },
        getUserMenu: async function () {
            const userMenu = await this.sendUrlRequest('/getUserMenu', {}, 'get')
            this.menuUsuario = []
            const menuUser = this.menuUsuario
            userMenu.forEach(function(value) {
                menuUser.push(value.vue_name)
            })
        },
        getClienteMenu: async function () {
            const clienteMenu = await this.sendUrlRequest('/getClienteMenu', {}, 'get')
            this.menuCliente = []
            const menuCliente = this.menuCliente
            clienteMenu.forEach(function(value) {
                menuCliente.push(value.vue_name)
            })
        },
        getReceivedSMSCampaign: async function(){
            const getSMSReceivedLogs = await this.sendUrlRequest('/api/getSMSReceivedLogs', {}, 'get')
            if (this.nameRoute === 'Chat SMS' && typeof vmSMSChatCampaign !== 'undefined' && this.chatSMSUbicationCampaign === 'smsCampana') vmSMSChatCampaign.reloadlistChatCampaign()
            if (this.nameRoute === 'Chat SMS' && typeof vmSMSChatNumbers !== 'undefined' && this.chatSMSUbicationCampaign === 'smsChatLit') vmSMSChatCampaign.reloadListChatNumbers(vmSMSChatCampaign.idBulkSMS)
            if (this.nameRoute === 'Chat SMS' && typeof vmSMSChatWindow !== 'undefined' && this.chatSMSUbicationCampaign === 'smsChatWindow') vmSMSChatNumbers.reloadListChatWindow(vmSMSChatNumbers.idBulkSMS, vmSMSChatNumbers.idFrom)
        },
        getReceivedSMSNotCampaign: async function(){
            const getSMSReceivedLogs = await this.sendUrlRequest('/api/getSMSReceivedLogs', {}, 'get')
            if (this.nameRoute === 'Chat SMS Sin Campaña' && typeof vmSMSChatNotCampaign !== 'undefined' && this.chatSMSUbicationNotCampaign === 'smsSinCampana') vmSMSChatNotCampaign.reloadlistChatNotCampaign()
            if (this.nameRoute === 'Chat SMS Sin Campaña' && typeof vmSMSChatNumbersNot !== 'undefined' && this.chatSMSUbicationNotCampaign === 'smsChatListNot') vmSMSChatNotCampaign.reloadListChatNotNumbers(vmSMSChatNotCampaign.idBulkSMS)
            if (this.nameRoute === 'Chat SMS Sin Campaña' && typeof vmSMSChatWindowNot !== 'undefined' && this.chatSMSUbicationNotCampaign === 'smsChatWindowNot') vmSMSChatNumbersNot.reloadListChatNotWindow(vmSMSChatNumbersNot.idBulkSMS, vmSMSChatNumbersNot.idFrom)
        },
        responseModal: async function (nameRoute, routeLoad, valID = {}) {
            try {
                const response = await axios['post'](`${routeLoad}`, { valueID: valID })
                $(nameRoute).html(response.data)
            } catch (error) { return error.response.data }
        },
        getDeliveryReports: async function (idBulk, tipoEnvio, nameRoute, routeLoad, routeApi, parametersApi = {}){
            let imageLoading = `<div class="cssload-loader"></div>`
            $(nameRoute).html(imageLoading)
            await this.responseModal(nameRoute, routeLoad, idBulk)
        },
        loadDivs: async function(divLoad, routeLoad, parameters = {}, typeRequest = 'post'){
            $(`${divLoad}`).html(`<div class="cssload-loader"></div>`).html(await this.sendUrlRequest(`/${routeLoad}`, parameters, typeRequest))
        },
        redirectNotification: async function (data){
            if(data.tipoSubida === 0 || data.tipoSubida === 1 || data.tipoSubida === 2){
                return await this.loadOptionMenu('sms', {}, 'get')
            }else if(data.tipoSubida === 3){
                return await this.loadOptionMenu('validacion_telefonos', {}, 'get')
            }
        },
        deleteIndexNotification: function (index){
            return this.listNotification.splice(index, 1)
        },
        getNotificationSMS: async function (){
            const response = await this.sendUrlRequest('/getNotificationSMS', {}, 'get')
            this.listNotificationSMS = multiDimensionalUnique(response)
        },
        setViewSMSDetalle: async function (typeCampaign, idBulk){
            await this.sendUrlRequest('/setViewSMSDetalle', {
                tipoCampana: typeCampaign,
                idBulk: idBulk
            })
        },
        chatViewNumberList: function (idCampana, idBulkSMS) {
            if(idCampana == 0){
                this.loadOptionMenu('chatViewNumbers', {
                    params: {
                        idNotCampana: idBulkSMS
                    }
                }, 'get')
            }else{
                this.loadOptionMenu('chatViewNumbers', {
                    params: {
                        idCampana: idCampana
                    }
                }, 'get')
            }
        }
    }
})