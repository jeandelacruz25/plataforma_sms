'use strict'

var objDataTableClientes = {
    dataClientes: [],
    pagination: {
        current_page: 1
    },
    initializeClientes: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchCliente: ''
}

var vmDataTableClientes = new Vue({
    el: '#dataTableClientesVue',
    data: objDataTableClientes,
    computed: {
        nameCliente() {
            if(this.dataClientes.length > 0){
                return this.dataClientes.map(function(item) {
                    let nameCliente = item.razon_social
                    return `<span class="avatar thumb-xss mr-b-0"><img src="${item.avatar}?version=${moment().format('YmdHis')}" onerror="this.src='img/clientes/default.png'"></span> ${nameCliente}`
                })
            }
        },
        statusCliente() {
            if(this.dataClientes.length > 0){
                return this.dataClientes.map(function(item) {
                    let statusClientes = item.status.color == 'bg-success' ? 'btn-green2' : 'btn-rose2'
                    return `<span class="px-3 bg-span ${statusClientes}">${item.status.nombre}</span>`
                })
            }
        },

    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchClientes: async function () {
            this.initializeClientes = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationClientes`, {
                params: {
                    page: this.pagination.current_page,
                    searchCliente: this.searchCliente
                }
            }, 'get')
            this.dataClientes = response.data.data
            this.pagination = response.pagination
            this.initializeClientes = false
        },
        paginationFetchClientes: async function () {
            if(this.dataClientes.length === 0){
                this.initializeClientes = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationClientes`, {
                params: {
                    page: this.pagination.current_page,
                    searchCliente: this.searchCliente
                }
            }, 'get')
            this.dataClientes = response.data.data
            this.pagination = response.pagination

            if(this.dataClientes.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeClientes = false
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        }
    }
})