'use strict'

var objDataTableKeywordSMSSend = {
    dataSMSKeywordSend: [],
    pagination: {
        current_page: 1
    },
    initializeSMSKeywordSend: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    selectCliente: '',
    selectKeyword: '',
    searchDateRange: `${moment().startOf('month').format('YYYY-MM-DD')} / ${moment().endOf('month').format('YYYY-MM-DD')}`
}

var vmDataTableSMSKeywordSend = new Vue({
    el: '#dataTableSMSKeywordSendVue',
    data: objDataTableKeywordSMSSend,
    computed: {
        dateSend(){
            if(this.dataSMSKeywordSend.length > 0){
                return this.dataSMSKeywordSend.map(function(item) {
                    let dateSend = item.fecha_envio ? moment(item.fecha_envio).format('DD/MM/YYYY HH:mm:ss a') : '-'
                    return dateSend
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            setTimeout(function() {
                initSelectPicker('.selectPickerKeyword', {
                    style: "btn-facebook text-white fs-12"
                })
            }, 100)
        },
        dateRangePicker: function() {
            setTimeout(function() {
                initDateRangePicker('.dateRangeKeyword', {
                    locale: {
                        format: 'YYYY-MM-DD',
                        customRangeLabel: 'Escoger Rango',
                        separator: ' / ',
                        applyLabel: 'Aplicar',
                        cancelLabel: 'Cerrar',
                    },
                    ranges: {
                        'Hoy': [moment(), moment()],
                        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Hace 7 Dias': [moment().subtract(6, 'days'), moment()],
                        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                        'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                        'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'left'
                })

                $('input[name="fechaFiltroKeyword"]').on('apply.daterangepicker', function(ev, picker) {
                    let dateRange = picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD')
                    $(this).val(dateRange)
                    vmDataTableSMSKeywordSend.searchDateRange = dateRange
                })

                $('input[name="fechaFiltroKeyword"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('')
                    vmDataTableSMSKeywordSend.searchDateRange = ''
                })
            }, 200)
        },
        fetchSMSKeywordSend: async function () {
            this.initializeSMSKeywordSend = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationSMSKeywordSend`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    selectKeyword: this.selectKeyword,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataSMSKeywordSend = response.data.data
            this.pagination = response.pagination
            this.initializeSMSKeywordSend = false
            this.selectPicker()
            this.dateRangePicker()
        },
        paginationFetchSMSKeywordSend: async function () {
            if(this.dataSMSKeywordSend.length === 0){
                this.initializeSMSKeywordSend = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationSMSKeywordSend`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    selectKeyword: this.selectKeyword,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataSMSKeywordSend = response.data.data
            this.pagination = response.pagination

            if(this.dataSMSKeywordSend.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeSMSKeywordSend = false
            this.selectPicker()
            this.dateRangePicker()
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        },
        clickDownload: function (routeDownload, parameteres = {}) {
            return ajaxDownload(routeDownload, parameteres)
        }
    }
})