'use strict'

var objDataTableCampanasValidacion = {
    dataValidacionCampanas: [],
    pagination: {
        current_page: 1
    },
    initializeValidacionCampanas: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    selectCliente: '',
    searchCampana: '',
    searchDateRange: `${moment().startOf('month').format('YYYY-MM-DD')} / ${moment().endOf('month').format('YYYY-MM-DD')}`
}

var vmDataTableValidacionCampanas = new Vue({
    el: '#dataTableValidacionCampanasVue',
    data: objDataTableCampanasValidacion,
    computed: {
        nameCampana() {
            if(this.dataValidacionCampanas.length > 0){
                return this.dataValidacionCampanas.map(function(item) {
                    let nameCampana = item.nombre_campana
                    let statusCampana = item.status.color == 'bg-success' ? 'text-success' : 'text-danger'
                    return `<i class="fa fa-circle fa-fw ${statusCampana}" aria-hidden="true" data-tooltip="tooltip" data-placement="bottom" title="${item.status.nombre}"></i> ${nameCampana}`
                })
            }
        },
        statusSend() {
            if(this.dataValidacionCampanas.length > 0){
                return this.dataValidacionCampanas.map(function(item) {
                    let statusSend = parseInt(item.estado_envio)
                    let nameStatus = ''
                    let colorStatus = ''
                    switch (statusSend){
                        case 0:
                            nameStatus = 'Pendiente a Validar'
                            colorStatus = 'btn-rose2'
                            break
                        case 1:
                            nameStatus = 'Validado'
                            colorStatus = 'btn-green2'
                            break
                        case 2:
                            nameStatus = '<i class="fa fa-spinner fa-spin"></i> Cargando Números'
                            colorStatus = 'btn-purple2'
                            break
                        case 3:
                            nameStatus = '<i class="fa fa-spinner fa-spin"></i> Validando Telefonos'
                            colorStatus = 'btn-yellow2'
                            break
                        case 4:
                            nameStatus = 'Validación Detenida'
                            colorStatus = 'btn-orange2'
                            break
                    }
                    return `<span class="px-3 bg-span ${colorStatus}">${nameStatus}</span>`
                })
            }
        },
        percentageValidation() {
            if(this.dataValidacionCampanas.length > 0){
                return this.dataValidacionCampanas.map(function(item) {
                    let totalSend = parseInt(item.detalle_count_count)
                    let percentageSend = parseInt(totalSend) === 0 || parseInt(item.total_count) === 0 ? parseInt(totalSend) : (parseInt(totalSend) * 100) / parseInt(item.total_count)
                    let colorPercentageSend = ''
                    switch (true){
                        case (percentageSend > 75):
                            colorPercentageSend = "success-light"
                            break
                        case (percentageSend > 50 && percentageSend <= 75):
                            colorPercentageSend = "percentage-50-75"
                            break
                        case (percentageSend > 25 && percentageSend <= 50):
                            colorPercentageSend = "percentage-25-50"
                            break
                        case (percentageSend > 10 && percentageSend <= 25):
                            colorPercentageSend = "percentage-10-25"
                            break
                        case (percentageSend > 0 && percentageSend <= 10):
                            colorPercentageSend = "percentage-0-10"
                            break
                        default:
                            colorPercentageSend = "percentage-0-10"
                            break
                    }

                    if(parseInt(item.estado_envio) === 3 && parseInt(item.detalle_count_count) === parseInt(item.total_count)){
                        axios['post'](`/finishValidationCampaign`, {campaignID: item.id})
                    }

                    return `<div class="progress progress-md" style="position: inherit !important;">
                                <div class="progress-bar bg-${colorPercentageSend}" style="width: ${parseInt(percentageSend)}%" role="progressbar">
                                    <b class="text-gray-800">${percentageSend.toFixed(0)}%</b>
                                </div>
                            </div>`
                })
            }
        },
        inicioValidacion(){
            if(this.dataValidacionCampanas.length > 0){
                return this.dataValidacionCampanas.map(function(item) {
                    let dateValidacion = item.inicio_validacion ? moment(item.inicio_validacion).format('DD/MM/YYYY HH:mm:ss a') : '-'
                    return dateValidacion
                })
            }
        },
        finValidacion(){
            if(this.dataValidacionCampanas.length > 0){
                return this.dataValidacionCampanas.map(function(item) {
                    let dateValidacion = item.fin_validacion ? moment(item.fin_validacion).format('DD/MM/YYYY HH:mm:ss a') : '-'
                    return dateValidacion
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            setTimeout(function() {
                initSelectPicker('.selectClientes', {
                    style: "btn-facebook text-white fs-12"
                })
            }, 100)
        },
        dateRangePicker: function() {
            setTimeout(function() {
                initDateRangePicker('.dateRangeValidacionCampania', {
                    locale: {
                        format: 'YYYY-MM-DD',
                        customRangeLabel: 'Escoger Rango',
                        separator: ' / ',
                        applyLabel: 'Aplicar',
                        cancelLabel: 'Cerrar',
                    },
                    ranges: {
                        'Hoy': [moment(), moment()],
                        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Hace 7 Dias': [moment().subtract(6, 'days'), moment()],
                        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                        'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                        'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'left'
                })

                $('input[name="fechaFiltroCampania"]').on('apply.daterangepicker', function(ev, picker) {
                    let dateRange = picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD')
                    $(this).val(dateRange)
                    vmDataTableValidacionCampanas.searchDateRange = dateRange
                })

                $('input[name="fechaFiltroCampania"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('')
                    vmDataTableValidacionCampanas.searchDateRange = ''
                })
            }, 200)
        },
        fetchValidacionCampanas: async function () {
            this.initializeValidacionCampanas = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationValidacionCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataValidacionCampanas = response.data.data
            this.pagination = response.pagination
            this.initializeValidacionCampanas = false
            this.selectPicker()
            this.dateRangePicker()
        },
        paginationFetchValidacionCampanas: async function () {
            if(this.dataValidacionCampanas.length === 0){
                this.initializeValidacionCampanas = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationValidacionCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataValidacionCampanas = response.data.data
            this.pagination = response.pagination

            if(this.dataValidacionCampanas.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeValidacionCampanas = false
            this.selectPicker()
            this.dateRangePicker()
        },
        nodeFetchValidacionCampanas: async function () {
            const response = await this.sendUrlRequest(`/paginationValidacionCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataValidacionCampanas = response.data.data
            this.pagination = response.pagination
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        },
        clickDownload: function (routeDownload, parameteres = {}) {
            return ajaxDownload(routeDownload, parameteres)
        },
        executeValidacion: function (idCampana, idEstado){
            const response = this.sendUrlRequest('/executeValidacion', {
                idCampana: idCampana,
                idEstado: idEstado
            }, 'post')
        }
    }
})