'use strict'

var objDataTableKeywordSMS = {
    dataSMSKeyword: [],
    pagination: {
        current_page: 1
    },
    initializeSMSKeyword: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    selectCliente: '',
    searchKeyword: ''
}

var vmDataTableSMSKeyword = new Vue({
    el: '#dataTableSMSKeywordVue',
    data: objDataTableKeywordSMS,
    computed: {
        nameKeyword() {
            if(this.dataSMSKeyword.length > 0){
                return this.dataSMSKeyword.map(function(item) {
                    let nameKeyword = item.nombre_keyword
                    let statusKeyword = item.status.color == 'bg-success' ? 'text-success' : 'text-danger'
                    return `<i class="fa fa-circle fa-fw ${statusKeyword}" aria-hidden="true" data-tooltip="tooltip" data-placement="bottom" title="${item.status.nombre}"></i> ${nameKeyword}`
                })
            }
        },
        statusService() {
            if(this.dataSMSKeyword.length > 0){
                return this.dataSMSKeyword.map(function(item) {
                    let statusService = parseInt(item.estado_servicio)
                    let nameService = ''
                    let colorService = ''
                    switch (statusService){
                        case 0:
                            nameService = 'Pendiente'
                            colorService = 'btn-yellow2'
                            break
                        case 1:
                            nameService = 'Confirmado'
                            colorService = 'btn-green2'
                            break
                    }
                    return `<span class="px-3 bg-span ${colorService}">${nameService}</span>`
                })
            }
        },
        statusError() {
            if(this.dataSMSKeyword.length > 0){
                return this.dataSMSKeyword.map(function(item) {
                    let statusError = parseInt(item.estado_error)
                    let nameError = ''
                    let colorError = ''
                    switch (statusError){
                        case 0:
                            nameError = 'Inactivo'
                            colorError = 'btn-rose2'
                            break
                        case 1:
                            nameError = 'Activo'
                            colorError = 'btn-green2'
                            break
                    }
                    return `<span class="px-3 bg-span ${colorError}">${nameError}</span>`
                })
            }
        },
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            setTimeout(function() {
                initSelectPicker('.selectClientes', {
                    style: "btn-facebook text-white fs-12"
                })
            }, 100)
        },
        fetchSMSKeyword: async function () {
            this.initializeSMSKeyword = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationSMSKeyword`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchKeyword: this.searchKeyword
                }
            }, 'get')
            this.dataSMSKeyword = response.data.data
            this.pagination = response.pagination
            this.initializeSMSKeyword = false
            this.selectPicker()
        },
        paginationFetchSMSKeyword: async function () {
            if(this.dataSMSKeyword.length === 0){
                this.initializeSMSKeyword = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationSMSKeyword`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchKeyword: this.searchKeyword
                }
            }, 'get')
            this.dataSMSKeyword = response.data.data
            this.pagination = response.pagination

            if(this.dataSMSKeyword.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeSMSKeyword = false
            this.selectPicker()
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        }
    }
})