'use strict'

var objDataTableUsuarios = {
    dataUsuarios: [],
    pagination: {
        current_page: 1
    },
    initializeUsuarios: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchUsuario: '',
    selectCliente: ''
}

var vmDataTableUsuarios = new Vue({
    el: '#dataTableUsuariosVue',
    data: objDataTableUsuarios,
    computed: {
        nameUsuario() {
            if(this.dataUsuarios.length > 0){
                return this.dataUsuarios.map(function(item) {
                    let nameUsuario = `${item.nombres} ${item.apellidos}`
                    let sessionUsuario = item.session_id ? 'user--online' : 'user--offline'
                    return `<span class="avatar thumb-xss mr-b-0 ${sessionUsuario}"><img class="rounded-circle" src="${item.avatar}?version=${moment().format('YmdHis')}" onerror="this.src='img/clientes/default.png'"></span> ${nameUsuario}`
                })
            }
        },
        statusUsuario() {
            if(this.dataUsuarios.length > 0){
                return this.dataUsuarios.map(function(item) {
                    let statusClientes = item.status.color == 'bg-success' ? 'btn-green2' : 'btn-rose2'
                    return `<span class="px-3 bg-span ${statusClientes}">${item.status.nombre}</span>`
                })
            }
        },

    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            setTimeout(function() {
                initSelectPicker('.selectClientes', {
                    style: "btn-facebook text-white fs-12"
                })
            }, 100)
        },
        fetchUsuarios: async function () {
            this.initializeUsuarios = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationUsuarios`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchUsuario: this.searchUsuario
                }
            }, 'get')
            this.dataUsuarios = response.data.data
            this.pagination = response.pagination
            this.initializeUsuarios = false
            this.selectPicker()
        },
        paginationFetchUsuarios: async function () {
            if(this.dataUsuarios.length === 0){
                this.initializeUsuarios = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationUsuarios`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchUsuario: this.searchUsuario
                }
            }, 'get')
            this.dataUsuarios = response.data.data
            this.pagination = response.pagination

            if(this.dataUsuarios.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeUsuarios = false
            this.selectPicker()
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        }
    }
})