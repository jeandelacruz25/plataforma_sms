'use strict'

var objDataTableCampanasBase = {
    dataBaseCampanas: [],
    pagination: {
        current_page: 1
    },
    initializeBaseCampanas: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    selectCliente: '',
    searchCampana: '',
    searchDateRange: `${moment().startOf('month').format('YYYY-MM-DD')} / ${moment().endOf('month').format('YYYY-MM-DD')}`
}

var vmDataTableBaseCampanas = new Vue({
    el: '#dataTableBaseCampanasVue',
    data: objDataTableCampanasBase,
    computed: {
        nameCampana() {
            if(this.dataBaseCampanas.length > 0){
                return this.dataBaseCampanas.map(function(item) {
                    let nameCampana = item.nombre_campana
                    let statusCampana = item.status.color == 'bg-success' ? 'text-success' : 'text-danger'
                    return `<i class="fa fa-circle fa-fw ${statusCampana}" aria-hidden="true" data-tooltip="tooltip" data-placement="bottom" title="${item.status.nombre}"></i> ${nameCampana}`
                })
            }
        },
        statusBase() {
            if(this.dataBaseCampanas.length > 0){
                return this.dataBaseCampanas.map(function(item) {
                    let statusBase = parseInt(item.estado_base)
                    let nameStatus = ''
                    let colorStatus = ''
                    switch (statusBase){
                        case 0:
                            nameStatus = 'Pendiente a cruzar'
                            colorStatus = 'btn-rose2'
                            break
                        case 1:
                            nameStatus = 'Cruzado'
                            colorStatus = 'btn-green2'
                            break
                        case 2:
                            nameStatus = '<i class="fa fa-spinner fa-spin"></i> Cargando Documentos'
                            colorStatus = 'btn-purple2'
                            break
                        case 3:
                            nameStatus = '<i class="fa fa-spinner fa-spin"></i> Cruzando Bases'
                            colorStatus = 'btn-yellow2'
                            break
                    }
                    return `<span class="px-3 bg-span ${colorStatus}">${nameStatus}</span>`
                })
            }
        },
        statusCruce(){
            if(this.dataBaseCampanas.length > 0){
                return this.dataBaseCampanas.map(function(item) {

                    if(item.criterios_count_count === item.criterios_count && parseInt(item.estado_base) === 3){
                        axios['post'](`/finishCruceBase`, {campaignID: item.id})
                    }

                    return `${item.criterios_count_count} / ${item.criterios_count}`
                })
            }
        },
        dateSend(){
            if(this.dataBaseCampanas.length > 0){
                return this.dataBaseCampanas.map(function(item) {
                    let dateSend = item.fecha_envio ? moment(item.fecha_envio).format('DD/MM/YYYY HH:mm:ss a') : '-'
                    return dateSend
                })
            }
        },
        dateFinish(){
            if(this.dataBaseCampanas.length > 0){
                return this.dataBaseCampanas.map(function(item) {
                    let dateFinish = item.fecha_fin ? moment(item.fecha_fin).format('DD/MM/YYYY HH:mm:ss a') : '-'
                    return dateFinish
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            setTimeout(function() {
                initSelectPicker('.selectClientes', {
                    style: "btn-facebook text-white fs-12"
                })
            }, 100)
        },
        dateRangePicker: function() {
            setTimeout(function() {
                initDateRangePicker('.dateRangeCampania', {
                    locale: {
                        format: 'YYYY-MM-DD',
                        customRangeLabel: 'Escoger Rango',
                        separator: ' / ',
                        applyLabel: 'Aplicar',
                        cancelLabel: 'Cerrar',
                    },
                    ranges: {
                        'Hoy': [moment(), moment()],
                        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Hace 7 Dias': [moment().subtract(6, 'days'), moment()],
                        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                        'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                        'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'left'
                })

                $('input[name="fechaFiltroCampania"]').on('apply.daterangepicker', function(ev, picker) {
                    let dateRange = picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD')
                    $(this).val(dateRange)
                    vmDataTableBaseCampanas.searchDateRange = dateRange
                })

                $('input[name="fechaFiltroCampania"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('')
                    vmDataTableBaseCampanas.searchDateRange = ''
                })
            }, 200)
        },
        fetchBaseCampanas: async function () {
            this.initializeBaseCampanas = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationBaseCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataBaseCampanas = response.data.data
            this.pagination = response.pagination
            this.initializeBaseCampanas = false
            this.selectPicker()
            this.dateRangePicker()
        },
        paginationFetchBaseCampanas: async function () {
            if(this.dataBaseCampanas.length === 0){
                this.initializeBaseCampanas = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationBaseCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataBaseCampanas = response.data.data
            this.pagination = response.pagination

            if(this.dataBaseCampanas.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeBaseCampanas = false
            this.selectPicker()
            this.dateRangePicker()
        },
        nodeFetchBaseCampanas: async function () {
            const response = await this.sendUrlRequest(`/paginationBaseCampanas`, {
                params: {
                    page: this.pagination.current_page,
                    selectCliente: this.selectCliente,
                    searchCampana: this.searchCampana,
                    searchDateRange: this.searchDateRange
                }
            }, 'get')
            this.dataBaseCampanas = response.data.data
            this.pagination = response.pagination
        },
        nameCriterio: function (idCriterio) {
            switch (parseInt(idCriterio)){
                case 1:
                    return 'Datos Personales'
                    break
                case 2:
                    return 'Telefonos'
                    break
                case 3:
                    return 'Direcciones'
                    break
                case 4:
                    return 'Familiares'
                    break
                case 5:
                    return 'Sueldos'
                    break
                case 6:
                    return 'Sunat'
                    break
                case 7:
                    return 'Financiero'
                    break
                case 8:
                    return 'Autos'
                    break
            }
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        },
        clickDownload: function (routeDownload, parameteres = {}) {
            return ajaxDownload(routeDownload, parameteres)
        }
    }
})