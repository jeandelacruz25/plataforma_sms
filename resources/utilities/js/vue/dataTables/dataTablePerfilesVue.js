'use strict'

var objDataTablePerfiles = {
    dataPerfiles: [],
    pagination: {
        current_page: 1
    },
    initializePerfiles: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchPerfil: ''
}

var vmDataTablePerfiles = new Vue({
    el: '#dataTablePerfilesVue',
    data: objDataTablePerfiles,
    computed: {
        statusPerfil() {
            if(this.dataPerfiles.length > 0){
                return this.dataPerfiles.map(function(item) {
                    let statusPerfiles = item.status.color == 'bg-success' ? 'btn-green2' : 'btn-rose2'
                    return `<span class="px-3 bg-span ${statusPerfiles}">${item.status.nombre}</span>`
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchPerfiles: async function () {
            this.initializePerfiles = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationPerfiles`, {
                params: {
                    page: this.pagination.current_page,
                    searchPerfil: this.searchPerfil
                }
            }, 'get')
            this.dataPerfiles = response.data.data
            this.pagination = response.pagination
            this.initializePerfiles = false
        },
        paginationFetchPerfiles: async function () {
            if(this.dataPerfiles.length === 0){
                this.initializePerfiles = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationPerfiles`, {
                params: {
                    page: this.pagination.current_page,
                    searchPerfil: this.searchPerfil
                }
            }, 'get')
            this.dataPerfiles = response.data.data
            this.pagination = response.pagination

            if(this.dataPerfiles.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializePerfiles = false
        },
        clickEvent: function (divModal, routeLoad, id) {
            return responseModal(divModal, routeLoad, id)
        }
    }
})