'use strict'

var objCampaignBaseUsers = {
    dataCampaignBase: [],
    pagination: {
        current_page: 1
    },
    id_base: '',
    initializeCampaignBase: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchTelephone: ''
}

var vmCampaignBaseUsers = new Vue({
    el: '#campaignUserVue',
    data: objCampaignBaseUsers,
    computed: {
        messageSMS() {
            if(this.dataCampaignBase.length > 0){
                return this.dataCampaignBase.map(function(item) {
                    let messageSMS = item.mensaje_usuario
                    return messageSMS.length > 58 ? messageSMS.substring(0,58)+'..' : messageSMS
                })
            }
        },
        formatDateTime() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let dateTime = item.fecha_recepcion
                    return dateTime ? moment(dateTime).format('DD/MM/YYYY hh:mm:ss a') : '-'
                })
            }
        },
        descriptionStatusSMS() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let nameStatus = item.estado_descripcion
                    return getDescriptionStatusSMS(nameStatus)
                })
            }
        },
        statusSMS() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let idStatus = parseInt(item.estado_sms)
                    return getStatusSMS(idStatus)
                })
            }
        },
        conceptStatusSMS() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let idStatus = parseInt(item.estado_sms)
                    return getConceptStatusSMS(idStatus)
                })
            }
        },
        countrySMS() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let mccmnc = String(item.numero_mccmnc)
                    let numberMCC = mccmnc ? parseInt(mccmnc.substring(0, 3)) : 0
                    return getCountryMCCMNC(numberMCC)
                })
            }
        },
        mobileNetworkSMS() {
            if(this.dataCampaignBase.length > 0) {
                return this.dataCampaignBase.map(function (item) {
                    let mccmnc = String(item.numero_mccmnc)
                    let operadorSMS = ''
                    if (mccmnc.length === 5) {
                        operadorSMS = mccmnc ? parseInt(mccmnc.substring(3, 6)) : 0
                    } else if (mccmnc.length === 6) {
                        operadorSMS = mccmnc ? parseInt(mccmnc.substring(3, 7)) : 0
                    } else {
                        operadorSMS = 0
                    }
                    return getOperatorMCCMNC(operadorSMS)
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchBaseCampaign: async function () {
            this.initializeCampaignBase = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationFormCampaignUsers`, {
                params: {
                    page: this.pagination.current_page,
                    valueID: this.id_base,
                    searchTelephone: this.searchTelephone
                }
            }, 'get')
            this.dataCampaignBase = response.data.data
            this.pagination = response.pagination
            this.initializeCampaignBase = false
        },
        paginationFetchBaseCampaign: async function () {
            if(this.dataCampaignBase.length === 0){
                this.initializeCampaignBase = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationFormCampaignUsers`, {
                params: {
                    page: this.pagination.current_page,
                    valueID: this.id_base,
                    searchTelephone: this.searchTelephone
                }
            }, 'get')
            this.dataCampaignBase = response.data.data
            this.pagination = response.pagination

            if(this.dataCampaignBase.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeCampaignBase = false
        },
        viewChats: function (telefono, idBulk){
            return vmFront.loadDivs('.chatWindow', 'chatSMSHistory', { numero: telefono, remitente: idBulk  }, 'post')
        }
    }
})