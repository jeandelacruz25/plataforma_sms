'use strict'

var objMiniDashboardTelefono = {
    idCampaign: '',
    idCliente: '',
    idStatus: '',
    initFormGenerar: false,
    initFormGenerarMarcadores: false,
    initFormGenerarGestiones: false,
    initFormGenerarInactivos: false,
    initFormCartera: false,
    initFormCarteraMarcadores: false,
    nameCartera: '',
    selectProveedores: [],
    selectCartera: [],
    selectedProveedor: '',
    selectedCartera: '',
    selectedMarcador: '',
    selectedBolsa: '1'
}

var vmMiniDashboardTelefono = new Vue({
    el: '#miniDashboardTelefonoVue',
    data: objMiniDashboardTelefono,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            this.selectedBolsa = '1'
            setTimeout(function() {
                initSelectPicker('.selectGenerar', {
                    style: 'btn-default text-black fs-13'
                })
                $('.selectGenerar').selectpicker('refresh')
            }, 100)
        },
        formGenerarMarcadores: async function (){
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormGenerarMarcadores = false
            this.initFormGenerarInactivos = false
            this.initFormCartera = false
            this.initFormCarteraMarcadores = false
            this.selectProveedores = []
            this.selectCartera = []
            this.loadForm = true
            const dataProveedores = await this.sendUrlRequest('/getProveedores', {
                params: {
                    idCliente: this.idCliente
                }
            }, 'get')
            this.selectProveedores = dataProveedores
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
            this.loadForm = false
            this.initFormGenerar = true
            this.initFormGenerarMarcadores = true
        },
        changeCarteraMarcadores: async function (){
            this.initFormCarteraMarcadores = false
            this.selectCartera = []
            const dataCartera = await this.sendUrlRequest('/getCarteras', {
                params: {
                    nameProveedor: this.selectedProveedor
                }
            }, 'get')
            this.selectCartera = dataCartera
            this.initFormCarteraMarcadores = true
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
        },
        formGenerarGestiones: async function (statusRequest){
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormGenerarMarcadores = false
            this.initFormGenerarInactivos = false
            this.initFormCartera = false
            this.initFormCarteraMarcadores = false
            this.selectProveedores = []
            this.selectCartera = []
            this.loadForm = true
            const dataProveedores = await this.sendUrlRequest('/getProveedores', {
                params: {
                    idCliente: this.idCliente
                }
            }, 'get')
            this.selectProveedores = dataProveedores
            this.idStatus = statusRequest
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
            this.loadForm = false
            this.initFormGenerar = true
            this.initFormGenerarGestiones = true
        },
        changeCartera: async function (){
            this.initFormCartera = false
            this.selectCartera = []
            const dataCartera = await this.sendUrlRequest('/getCarteras', {
                params: {
                    nameProveedor: this.selectedProveedor
                }
            }, 'get')
            this.selectCartera = dataCartera
            this.initFormCartera = true
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
        },
        formGenerarInactivos: async function (statusRequest){
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormGenerarMarcadores = false
            this.initFormGenerarInactivos = false
            this.initFormCartera = false
            this.initFormCarteraMarcadores = false
            this.selectProveedores = []
            this.selectCartera = []
            this.loadForm = true
            $('.selectGenerar').selectpicker('refresh')
            this.loadForm = false
            this.initFormGenerar = true
            this.initFormGenerarInactivos = true
        },
        generarMarcador: async function(){
            changeButtonForm('btnForm','btnLoad')
            alertaSimple('', '<i class="fa fa-spinner fa-spin"></i> Sincronizando...', null, null, false, false, false)
            const response = await this.sendUrlRequest('/generarMarcador', {
                tipoRequest: 'telefono',
                nameCartera: this.nameCartera,
                idCartera: this.selectedCartera,
                tipoMarcador: this.selectedMarcador,
                tipoBolsa: this.selectedBolsa,
                idCampaign: this.idCampaign
            }, 'post')
            swal.close()
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormGenerarMarcadores = false
            this.initFormGenerarInactivos = false
            alertaSimple('', 'Se generó una campaña de marcación con éxito.', 'success', 2000, false, false, false)
            changeButtonForm('btnLoad','btnForm')
            axiosChart('post', '/getDataMiniDashboardTelefono', {
                valueID: this.idCampaign
            })
        },
        generarGestion: async function(){
            changeButtonForm('btnForm','btnLoad')
            alertaSimple('', '<i class="fa fa-spinner fa-spin"></i> Sincronizando...', null, null, false, false, false)
            const response = await this.sendUrlRequest('/generarGestion', {
                tipoRequest: 'validacion',
                idCartera: this.selectedCartera,
                idStatus: this.idStatus,
                idCliente: this.idCliente,
                idCampaign: this.idCampaign
            }, 'post')

            swal.close()

            if(response === 'equivalencia'){
                changeButtonForm('btnLoad','btnForm')
                alertaSimple('', 'No cuenta con equivalencias para sincronizar.', 'error', 2000, false, false, false)
            }else{
                this.initFormGenerar = false
                this.initFormGenerarGestiones = false
                this.initFormGenerarMarcadores = false
                this.initFormGenerarInactivos = false
                alertaSimple('', 'Se sincronizaron los resultados con éxito.', 'success', 2000, false, false, false)
                changeButtonForm('btnLoad','btnForm')
                axiosChart('post', '/getDataMiniDashboardTelefono', {
                    valueID: this.idCampaign
                })
            }
        },
        generarInactivos: async function(){
            changeButtonForm('btnForm','btnLoad')
            alertaSimple('', '<i class="fa fa-spinner fa-spin"></i> Sincronizando...', null, null, false, false, false)
            const response = await this.sendUrlRequest('/generarInactivos', {
                tipoRequest: 'validacion',
                idStatus: this.idStatus,
                idCliente: this.idCliente,
                idCampaign: this.idCampaign
            }, 'post')

            swal.close()
            alertaSimple('', 'Se inactivaron teléfonos con éxito.', 'success', 2000, false, false, false)
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormGenerarMarcadores = false
            this.initFormGenerarInactivos = false
            changeButtonForm('btnLoad','btnForm')
            axiosChart('post', '/getDataMiniDashboardTelefono', {
                valueID: this.idCampaign
            })
        },
        clearFormGenerarMarcadores: function (){
            this.initFormGenerar = false
            this.initFormGenerarMarcadores = false
            this.initFormCartera = false
            this.initFormCarteraMarcadores = false
        },
        clearFormGenerarGestiones: function (){
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormCartera = false
            this.initFormCarteraMarcadores = false
        }
    }
})