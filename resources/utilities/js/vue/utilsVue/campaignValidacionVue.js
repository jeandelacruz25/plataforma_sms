'use strict'

var objCampaignBaseValidacion = {
    dataCampaignValidacionBase: [],
    pagination: {
        current_page: 1
    },
    id_base: '',
    initializeCampaignValidacionBase: false,
    initializeLoadPaginate: true,
    initDataPagination: false,
    searchTelephone: ''
}

var vmCampaignValidacionBase = new Vue({
    el: '#campaignValidacionVue',
    data: objCampaignBaseValidacion,
    computed: {
        formatDateTime() {
            if(this.dataCampaignValidacionBase.length > 0) {
                return this.dataCampaignValidacionBase.map(function (item) {
                    let dateTime = item.fecha_validacion
                    return dateTime ? moment(dateTime).format('DD/MM/YYYY hh:mm:ss a') : '-'
                })
            }
        },
        statusValidacion() {
            if(this.dataCampaignValidacionBase.length > 0) {
                return this.dataCampaignValidacionBase.map(function (item) {
                    let idStatus = parseInt(item.estado_validacion)
                    return getStatusValidacion(idStatus)
                })
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        fetchBaseCampaignValidacion: async function () {
            this.initializeCampaignValidacionBase = true
            this.pagination = { current_page: 1 }
            const response = await this.sendUrlRequest(`/paginationFormCampaignValidationTelefonos`, {
                params: {
                    page: this.pagination.current_page,
                    valueID: this.id_base,
                    searchTelephone: this.searchTelephone
                }
            }, 'get')
            this.dataCampaignValidacionBase = response.data.data
            this.pagination = response.pagination
            this.initializeCampaignValidacionBase = false
        },
        paginationFetchBaseCampaignValidacion: async function () {
            if(this.dataCampaignValidacionBase.length === 0){
                this.initializeCampaignValidacionBase = true
            }
            this.initializeLoadPaginate = false
            const response = await this.sendUrlRequest(`/paginationFormCampaignValidationTelefonos`, {
                params: {
                    page: this.pagination.current_page,
                    valueID: this.id_base,
                    searchTelephone: this.searchTelephone
                }
            }, 'get')
            this.dataCampaignValidacionBase = response.data.data
            this.pagination = response.pagination

            if(this.dataCampaignValidacionBase.length > 0){
                this.initDataPagination = false
            }else{
                this.initDataPagination = true
            }

            this.initializeLoadPaginate = true
            this.initializeCampaignValidacionBase = false
        },
        viewChats: function (telefono, idBulk){
            return vmFront.loadDivs('.chatWindow', 'chatSMSHistory', { numero: telefono, remitente: idBulk  }, 'post')
        }
    }
})