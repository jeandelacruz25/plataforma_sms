'use strict'

var objMiniDashboardSMS = {
    idCampaign: '',
    idBulkSMS: '',
    idCliente: '',
    idStatus: '',
    initFormGenerar: false,
    initFormGenerarGestiones: false,
    initFormCartera: false,
    nameCartera: '',
    selectProveedores: [],
    selectCartera: [],
    selectedProveedor: '',
    selectedCartera: '',
    selectedMarcador: '',
    selectedBolsa: '1'
}

var vmMiniDashboardSMS = new Vue({
    el: '#miniDashboardSMSVue',
    data: objMiniDashboardSMS,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) {
                return error.status
            }
        },
        selectPicker: function () {
            this.selectedBolsa = '1'
            setTimeout(function() {
                initSelectPicker('.selectGenerar', {
                    style: 'btn-default text-black fs-13'
                })
                $('.selectGenerar').selectpicker('refresh')
            }, 100)
        },
        formGenerarGestiones: async function (statusRequest){
            this.loadForm = true
            const dataProveedores = await this.sendUrlRequest('/getProveedores', {
                params: {
                    idCliente: this.idCliente
                }
            }, 'get')
            console.dir(dataProveedores)
            this.selectProveedores = dataProveedores
            this.idStatus = statusRequest
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
            this.loadForm = false
            this.initFormGenerar = true
            this.initFormGenerarGestiones = true
        },
        changeCartera: async function (){
            const dataCartera = await this.sendUrlRequest('/getCarteras', {
                params: {
                    nameProveedor: this.selectedProveedor
                }
            }, 'get')
            this.selectCartera = dataCartera
            $('.selectGenerar').selectpicker('refresh')
            this.selectPicker()
        },
        generarGestion: async function(){
            changeButtonForm('btnForm','btnLoad')
            alertaSimple('', '<i class="fa fa-spinner fa-spin"></i> Sincronizando...', null, null, false, false, false)
            const response = await this.sendUrlRequest('/generarGestion', {
                tipoRequest: 'sms',
                idCartera: this.selectedCartera,
                idStatus: this.idStatus,
                idCampaign: this.idCampaign
            }, 'post')
            changeButtonForm('btnLoad','btnForm')
            swal.close()

            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormCartera = false

            if((this.idCampaign).length > 0) {
                axiosChartSMS('post', '/getDataMiniDashboardSMS', {
                    campaignID: this.idCampaign
                })
            }else{
                axiosChartSMS('post', '/getDataMiniDashboardSMS', {
                    bulkID: this.bulkID
                })
            }
        },
        clearFormGenerarMarcadores: function (){
            this.initFormGenerar = false
            this.initFormGenerarGestiones = false
            this.initFormCartera = false
        }
    }
})