'use strict'

var objSMSChatNumbers = {
    listChatNumbers: [],
    idBulkSMS: 0,
    idFrom: '',
    selected: []
}

var vmSMSChatNumbers = new Vue({
    el: '#smsChatNumbersVue',
    data: objSMSChatNumbers,
    computed: {
        mensajeUsuario() {
            return this.listChatNumbers.map(function(item) {
                let mensajeUsuario = item.mensaje_usuario
                return mensajeUsuario.length > 45 ? mensajeUsuario.substring(0,45)+'..' : mensajeUsuario
            })
        },
        selectAll: {
            get: function () {
                let leidoSMS = []
                this.listChatNumbers.map(function(chatNumbers) {
                    if(chatNumbers.leido_sms === 1) leidoSMS.push(chatNumbers.id)
                })
                if(leidoSMS.length > 0){
                    return this.selected.length == leidoSMS.length
                }else{
                    return false
                }
            },
            set: function (value) {
                let selected = []
                if (value) {
                    this.listChatNumbers.forEach(function (chatNumbers) {
                        if(chatNumbers.leido_sms === 1) selected.push(chatNumbers.id)
                    })
                }
                this.selected = selected;
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getlistChatNumbers: async function (){
            this.listChatNumbers = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: this.idBulkSMS
            }, 'post')
        },
        getListChatWindow: async function (idBulkSMS, idFrom, nameLoad, typeRequest = 'post'){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            $(`${nameLoad}`).html(`<div class="cssload-loader"></div>`)
            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            $(`${nameLoad}`).html(await this.sendUrlRequest(`/chatWindow`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            }, typeRequest))

            vmSMSChatCampaign.listChatCampaign = await this.sendUrlRequest(`/listChatCampaign`, {
                params: {
                    idCliente: vmSMSChatCampaign.idCliente
                }
            }, 'get')

            vmSMSChatNumbers.listChatNumbers = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: vmSMSChatCampaign.idBulkSMS
            })
        },
        reloadListChatWindow: async function (idBulkSMS, idFrom){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            vmSMSChatWindow.listChatWindow = await this.sendUrlRequest(`/listChatWindow`,{
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            }, 'post')

            vmSMSChatCampaign.listChatCampaign = await this.sendUrlRequest(`/listChatCampaign`, {
                params: {
                    idCliente: vmSMSChatCampaign.idCliente
                }
            }, 'get')

            vmSMSChatNumbers.listChatNumbers = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: vmSMSChatCampaign.idBulkSMS
            })
        },
        getChatView: async function (idCampana, idBulkSMS, idFrom, nameLoad, typeRequest = 'post'){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            $(`${nameLoad}`).html(`<div class="cssload-loader"></div>`)

            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            vmSMSChatNumbers.listChatNumbers = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: idCampana
            })

            $(`${nameLoad}`).html(await this.sendUrlRequest(`/chatWindow`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            }, typeRequest))
        }
    }
})