'use strict'

var objSMSChatNotCampaign = {
    idCliente: '',
    listChatNotCampaign: [],
    idBulkSMS: 0
}

var vmSMSChatNotCampaign = new Vue({
    el: '#smsChatNotCampaignVue',
    data: objSMSChatNotCampaign,
    computed: {
        nameNotCampaign() {
            return this.listChatNotCampaign.map(function(item) {
                let nameBulkID = (item.id_bulk_sms).toUpperCase()
                return nameBulkID.length > 18 ? nameBulkID.substring(0,18)+'..' : nameBulkID
            })
        },
        countNotViewSMS(){
            return this.listChatNotCampaign.map(function(item) {
                return item.leido_sms
            })
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getlistChatNotCampaign: async function (idCliente, divPrimary = false, divSecondary = false){
            let parameters = {}
            if(idCliente != 'undefined') {
                this.idCliente = idCliente
                parameters = {
                    params: {
                        idCliente: this.idCliente
                    }
                }
            }
            this.listChatNotCampaign = await this.sendUrlRequest(`/listChatNotCampaign`, parameters, 'get')
            if(divPrimary && divSecondary){
                $(`${divPrimary}`).html('<div class="mail-inbox-header disabled"><div class="mail-inbox-tools d-flex align-items-center"><span class="checkbox checkbox-primary bw-3 heading-font-family fs-14 fw-semibold headings-color mail-inbox-select-all mr-r-20"><label><input type="checkbox"> <span class="label-text">Seleccionar todos</span></label></span><div class="btn-group"><a href="javascript:void(0)" class="btn btn-sm btn-link mr-2 text-muted"><i class="list-icon fs-18 feather feather-refresh-cw"></i></a><a href="javascript:void(0)" class="btn btn-sm btn-link mr-2 text-muted"><i class="list-icon fs-18 feather feather-eye"></i></a></div></div></div><div class="px-4"><div class="text-center mt-4 mb-4"><img alt="" src="img/select.png"></div><label class="text-center width-100">Seleccione BulkID</label></div>')
                $(`${divSecondary}`).html('<div class="px-4"></div>')
            }
        },
        getListChatNumbers: async function (idBulkSMS, nameLoad, divSecondary, typeRequest = 'post'){
            this.idBulkSMS = idBulkSMS
            $(`${nameLoad}`).html(`<div class="cssload-loader"></div>`)
            $(`${nameLoad}`).html(await this.sendUrlRequest(`/chatNumbers`, {
                idNotCampana: this.idBulkSMS
            }, typeRequest))
            this.getlistChatNotCampaign(this.idCliente)
            $(`${divSecondary}`).html('<div class="px-4 mr-t-100"><div class="text-center mt-4 mb-4"><img alt="" src="img/select.png"></div><label class="text-center width-100">Seleccione un Chat</label></div>')
            Object.keys(vmFront.listNotificationSMS).filter(key => vmFront.listNotificationSMS[key].id_bulk_sms == idBulkSMS).some((item, index) => { vmFront.listNotificationSMS.splice(item, 1) })
            vmFront.setViewSMSDetalle(1, idBulkSMS)
        },
        reloadlistChatNotCampaign: async function (idCliente){
            let parameters = {}
            if(idCliente != 'undefined') {
                this.idCliente = idCliente
                parameters = {
                    params: {
                        idCliente: this.idCliente
                    }
                }
            }
            this.listChatNotCampaign = await this.sendUrlRequest(`/listChatNotCampaign`, parameters, 'get')
        },
        reloadListChatNotNumbers: async function (idBulkSMS){
            this.idBulkSMS = idBulkSMS
            vmSMSChatNumbersNot.listChatNumbersNot = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: this.idBulkSMS
            }, 'post')
            this.reloadlistChatNotCampaign(this.idCliente)
        }
    }
})