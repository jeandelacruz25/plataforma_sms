'use strict'

var objSMSChatWindowNot = {
    listChatWindowNot: [],
    idBulkSMS: 0,
    mensajeChatNotCampaign: '',
    idFrom: ''
}

var vmSMSChatWindowNot = new Vue({
    el: '#smsChatWindowNotVue',
    data: objSMSChatWindowNot,
    computed: {
        isSystem() {
            return this.listChatWindowNot.map(function(item) {
                let replySMS = (item.envio_sms == '0' ? 'message reply media mb-0' : 'message media mb-0')
                return replySMS
            })
        },
        isAvatarSystem() {
            return this.listChatWindowNot.map(function(item) {
                let replyAvatarSMS = (item.envio_sms === 0 ? 'img/modules/user_sms.svg?version='+moment().format('YmdHis') : 'img/modules/user_sms_reply.svg?version='+moment().format('YmdHis'))
                return replyAvatarSMS
            })
        },
        isTimeChat() {
            return this.listChatWindowNot.map(function(item) {
                let response = ''
                let dateTimeChat = moment(item.fecha_sms)
                let dateTimeActually = moment()
                let diffYearDateTime = dateTimeActually.diff(dateTimeChat, 'years')
                let diffMonthDateTime = dateTimeActually.diff(dateTimeChat, 'months')
                let diffDaysDateTime = dateTimeActually.diff(dateTimeChat, 'days')
                let diffSecondsDateTime = dateTimeActually.diff(dateTimeChat, 'seconds')

                if(diffSecondsDateTime > 0){
                    let hours = Math.floor( diffSecondsDateTime / 3600 );
                    let minutes = Math.floor( (diffSecondsDateTime % 3600) / 60 );
                    let seconds = diffSecondsDateTime % 60;

                    if(diffYearDateTime > 0){
                        response += diffYearDateTime
                        response += diffYearDateTime === 1 ? " año " : " años "
                    } else if(diffMonthDateTime > 0) {
                        response += diffMonthDateTime
                        response += diffMonthDateTime === 1 ? " mes " : " meses "
                    } else if(diffDaysDateTime > 0) {
                        response += diffDaysDateTime
                        response += diffDaysDateTime === 1 ? " dia " : " dias "
                    } else {
                        if(hours > 0){
                            response += hours
                            response += hours === 1 ? " hora " : " horas "
                        } else if(minutes > 0) {
                            response += minutes
                            response += minutes === 1 ? " minuto " : " minutos "
                        } else if(seconds > 0){
                            response += seconds
                            response += seconds === 1 ? " segundo " : " segundos "
                        }
                    }
                }

                return 'Hace ' + response

            })
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getListChatWindow: async function (){
            this.listChatWindowNot = await this.sendUrlRequest(`/listChatWindow`,{
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            }, 'post')
            setTimeout(function() { $('#chatContainerNot').scrollTop($("#chatContainerNot")[0].scrollHeight) }, 1)
        }
    }
})