'use strict'

var objSMSChatNumbersNot = {
    listChatNumbersNot: [],
    idBulkSMS: 0,
    idFrom: '',
    selected: []
}

var vmSMSChatNumbersNot = new Vue({
    el: '#smsChatNumbersNotVue',
    data: objSMSChatNumbersNot,
    computed: {
        mensajeUsuario() {
            return this.listChatNumbersNot.map(function(item) {
                let mensajeUsuario = item.mensaje_usuario
                return mensajeUsuario.length > 45 ? mensajeUsuario.substring(0,45)+'..' : mensajeUsuario
            })
        },
        selectAll: {
            get: function () {
                let leidoSMS = []
                this.listChatNumbersNot.map(function(chatNumbers) {
                    if(chatNumbers.leido_sms === 1) leidoSMS.push(chatNumbers.id)
                })
                if(leidoSMS.length > 0){
                    return this.selected.length == leidoSMS.length
                }else{
                    return false
                }
            },
            set: function (value) {
                let selected = []
                if (value) {
                    this.listChatNumbersNot.forEach(function (chatNumbers) {
                        if(chatNumbers.leido_sms === 1) selected.push(chatNumbers.id)
                    })
                }
                this.selected = selected;
            }
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                const response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getlistChatNumbers: async function (){
            this.listChatNumbersNot = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: this.idBulkSMS
            }, 'post')
        },
        getListChatWindow: async function (idBulkSMS, idFrom, nameLoad, typeRequest = 'post'){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            $(`${nameLoad}`).html(`<div class="cssload-loader"></div>`)

            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            $(`${nameLoad}`).html(await this.sendUrlRequest(`/chatWindow`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom,
                windowNot: true
            }, typeRequest))

            vmSMSChatNotCampaign.listChatNotCampaign = await this.sendUrlRequest(`/listChatNotCampaign`, {
                params: {
                    idCliente: vmSMSChatNotCampaign.idCliente
                }
            }, 'get')

            vmSMSChatNumbersNot.listChatNumbersNot = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: vmSMSChatNotCampaign.idBulkSMS
            })
        },
        reloadListChatNotWindow: async function (idBulkSMS, idFrom){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            vmSMSChatWindowNot.listChatWindowNot = await this.sendUrlRequest(`/listChatWindow`,{
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            }, 'post')

            vmSMSChatNotCampaign.listChatNotCampaign = await this.sendUrlRequest(`/listChatNotCampaign`, {
                params: {
                    idCliente: vmSMSChatNotCampaign.idCliente
                }
            }, 'get')

            vmSMSChatNumbersNot.listChatNumbersNot = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: vmSMSChatNotCampaign.idBulkSMS
            })
        },
        getNotChatView: async function (idBulkSMS, idFrom, nameLoad, typeRequest = 'post'){
            this.idBulkSMS = idBulkSMS
            this.idFrom = idFrom

            $(`${nameLoad}`).html(`<div class="cssload-loader"></div>`)

            await this.sendUrlRequest(`/changeSeenChat`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom
            })

            vmSMSChatNumbersNot.listChatNumbersNot = await this.sendUrlRequest(`/listChatNumbers`, {
                idCampana: idBulkSMS
            })

            $(`${nameLoad}`).html(await this.sendUrlRequest(`/chatWindow`, {
                remitenteSMS: this.idBulkSMS,
                telefonoSMS: this.idFrom,
                windowNot: true
            }, typeRequest))
        }
    }
})