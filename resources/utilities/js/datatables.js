/**
 * Created by Carlos on 15/12/2017.
 *
 * [dataTables description]
 * @nombreDIV ID de la tabla
 * @routes Ruta de donde se tomara los datos
 * @return Estructura el Datatable ah tomarse
 */
const dataTables = (nombreDIV, routes, parameters = {}, typeRequest = 'POST', lengthChange = true, infoDatatable = true) => {
    // Creacion del DataTable
    $(`#${nombreDIV}`).DataTable({
        destroy: true,
        deferRender: true,
        processing: false,
        serverSide: true,
        lengthChange: lengthChange,
        info: infoDatatable,
        ajax: {
            url: routes,
            type: typeRequest,
            data: parameters
        },
        paging: true,
        pageLength: 10,
        lengthMenu: [10, 20, 40, 50, 100, 200, 300, 400, 500],
        language: dataTables_lang(),
        columns: columnsDatatable(nombreDIV)
    })
}

const dataTablesNotColumn = (nombreDIV, pageLength = 10, lengthChange = true, infoDatatable = true) => {
    // Creacion del DataTable
    $(`#${nombreDIV}`).DataTable({
        destroy: true,
        deferRender: true,
        processing: true,
        lengthChange: lengthChange,
        info: infoDatatable,
        paging: true,
        pageLength: pageLength,
        lengthMenu: [5, 10, 50, 100, 200, 300, 400, 500, 1000],
        language: dataTables_lang()
    })
}