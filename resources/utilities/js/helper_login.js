// Agrega por defecto el crsf token en las peticiones ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

// Función que muestra un boton de carga cuando se ejecuta un formulario
const changeButtonForm = (btnShow, btnHide) => {
    $('.' + btnHide).removeClass('d-none')
    $('.' + btnShow).addClass('d-none')
}

// Función que muestra los errores que emite el controlador en un DIV
const showErrorForm = (data, formDiv) => {
    let errors = ''
    for(datos in data){
        errors += '<label class="text-danger">' + data[datos] + '</label>'
    }
    $(formDiv).fadeIn().html(errors)
    $(formDiv).removeClass('d-none')
}

// Función para ocultar los errores cuando se ingresa en algun campo que se requeria data
const hideErrorForm = (formDiv) => {
    $("input[type=text],input[type=checkbox],input[type=password],input[type=file],div.emojionearea-editor,div.bootstrap-select,button[data-toggle=dropdown]").click(function() {
        $(formDiv).addClass('d-none').html('')
    })
}

const alertaSimple = (titleAlerta = '', textoAlerta = '', tipoAalerta = null, tiempoAlerta = 2000, outsideClick = false, escapeKey = false, enterKey = false) => {
    swal({
        title: titleAlerta,
        html: textoAlerta,
        type: tipoAalerta,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: outsideClick,
        allowEscapeKey: escapeKey,
        allowEnterKey: enterKey,
        timer: tiempoAlerta
    }).then(
        function () {},
        function (dismiss) { }
    )
}