socketNodejs.on('reloadManageUsers', function () {
    if(typeof vmDataTableUsuarios !== 'undefined') vmDataTableUsuarios.paginationFetchUsuarios()
    console.log('[socketNodejs] Se actualiza los usuarios')
})

socketNodejs.on('reloadMessageChat', function () {
    if(typeof vmChat !== 'undefined') vmChat.getChat()
    console.log('[socketNodejs] Se actualiza el chat')
})

socketNodejs.on('notificationSMS', function (data) {
    let arrayCliente = data.arrayCliente
    let nameCampana = arrayCliente.campana === null ? arrayCliente.id_bulk_sms : arrayCliente.campana.nombre_campana
    let filterCampana = arrayCliente.campana === null ? arrayCliente.id_bulk_sms : arrayCliente.id_campana

    let listNotificationSMS = vmFront.listNotificationSMS

    let pushNotification = listNotificationSMS.filter(item => {
        if(item.id_campana === filterCampana){
            return item.id_campana === filterCampana && moment(item.fecha_respuesta_sms).unix() === moment(arrayCliente.fecha_respuesta_sms).unix()
        }else if(item.id_bulk_sms === filterCampana){
            return item.id_bulk_sms === filterCampana && moment(item.fecha_respuesta_sms).unix() === moment(arrayCliente.fecha_respuesta_sms).unix()
        }
    })

    if(pushNotification.length === 0){
        Push.create("Nueva Respuesta de SMS", {
            body: 'Acabas de recibir una respuesta en tu campaña, ' + nameCampana,
            icon: arrayCliente.cliente.avatar,
            vibrate: 200,
            silent: false,
            tag: nameCampana,
            timeout: 5000,
            onClick: function onClick() {
                window.focus()
                this.close()
                //ion.sound.stop("notification_sms")
            },
            onShow: function onShow(){
                //ion.sound.play("notification_sms")
            },
            onClose: function onClose(){
                //ion.sound.stop("notification_sms")
            }
        })
    }

    vmFront.getNotificationSMS()

    if(arrayCliente.campana === null) { vmFront.getReceivedSMSNotCampaign() }else{ vmFront.getReceivedSMSCampaign() }

    console.log('[socketNodejs] Se recibe una respuesta de sus SMS')
})

socketNodejs.on('reloadDatatable', function (data) {
    if(data.nameDatatable == 'listCampaign'){
        setTimeout(function() {
            if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.nodeFetchSMSCampanas()
        }, 1200)
    }else if(data.nameDatatable == 'listCampaignValidacion'){
        setTimeout(function() {
            if (typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.nodeFetchValidacionCampanas()
        }, 1200)
    }else if(data.nameDatatable == 'listCampaignBase'){
        setTimeout(function() {
            if (typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.nodeFetchBaseCampanas()
        }, 1200)
    }else if(data.nameDatatable == 'dataPersonDetailsTelephone'){
        setTimeout(function() {
            if (typeof vmPersonDetails !== 'undefined') {
                vmPersonDetails.getDataPersonTelefonos()
                vmPersonDetails.initFormValidacion = true
            }
        }, 1200)
    }
    console.log('[socketNodejs] Se actualiza un datatable o una detalle de telefonos')
})

socketNodejs.on('notificationCampaign', function (data) {
    vmFront.listNotification.push(data)
    console.log('[socketNodejs] Se recibe notificación de subida correcta')
})

socketNodejs.on('clearTimeOut', function (data) {
    switch (data.nameTimeOut) {
        case 'listCampaign':
            clearTimeOutListCampaign()
            break
        case 'listCampaignValidacion':
            clearTimeOutListCampaignValidacion()
            break
        case 'listCampaignBase':
            clearTimeOutListCampaignBase()
            break
    }
    console.log('[socketNodejs] Se congela el proceso de TimeOut')
})

socketNodejs.on('validateSession', function () {
    sessionIdle()
})

socketNodejs.on('reloadMenuCliente', function () {
    if (typeof vmFront !== 'undefined') vmFront.getClienteMenu();
})

socketNodejs.on('reloadMenuUsuario', function () {
    if (typeof vmFront !== 'undefined') vmFront.getUserMenu();
})