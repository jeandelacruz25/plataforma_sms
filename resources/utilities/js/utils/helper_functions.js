const axiosChart = async (requestType, url, parametersChart = {}, parametersDeliveryReports = {}) => {
    $('.chartHTML').html('<div class="cssload-loader"></div>')
    $('.chartTable').html('<div class="cssload-loader"></div>')
    try {
        const response = await axios[requestType](`${url}`, parametersChart)
        $('.chartHTML').html(response.data.chartHTML)
        $('.chartScript').html(response.data.chartScript)
        $('.chartTable').html(response.data.chartTable)
    } catch (error) { return error.status }
}

const axiosChartSMS = async (requestType, url, parametersChart = {}, parametersDeliveryReports = {}) => {
    $('.chartHTMLSMS').html('<div class="cssload-loader"></div>')
    $('.chartTableSMS').html('<div class="cssload-loader"></div>')
    try {
        const response = await axios[requestType](`${url}`, parametersChart)
        $('.chartHTMLSMS').html(response.data.chartHTML)
        $('.chartScriptSMS').html(response.data.chartScript)
        $('.chartTableSMS').html(response.data.chartTable)
    } catch (error) { return error.status }
}

const axiosChartReceived = async (requestType, url, parametersChart = {}) => {
    $('.chartHTMLReceived').html('<div class="cssload-loader"></div>')
    try {
        const response = await axios[requestType](`${url}`, parametersChart)
        $('.chartHTMLReceived').html(response.data.chartHTML)
        $('.chartScriptReceived').html(response.data.chartScript)
    } catch (error) { return error.status }
}

const axiosChartDashboardSMSReceived = async (requestType, url, parametersChart = {}) => {
    $('.chartHTMLDashboardSMS').html('<div class="cssload-loader"></div>')
    try {
        const response = await axios[requestType](`${url}`, parametersChart)
        $('.chartHTMLDashboardSMS').html(response.data.chartHTML)
        $('.chartScriptDashboardSMS').html(response.data.chartScript)
    } catch (error) { return error.status }
}

const getNotificationSMS = async () => {
    try {
        await axios['get']('/api/getNotificationSMS')
    } catch (error) { return error.status }
}

const responseModal = async (nameRoute, routeLoad, valID = {}, typeRequest = 'post') => {
    let imageLoading = `<div class="cssload-loader"></div>`
    $(nameRoute).html(imageLoading)
    try {
        const response = await axios[typeRequest](`${routeLoad}`, { valueID: valID })
        $(nameRoute).html(response.data)
    } catch (error) { return error.status }
}

const ajaxDownload = async (urlDownload, parameters = {}) => {
    alertaSimple('', '<i class="fa fa-spinner fa-spin"></i> Descargando...', null, null, false, false, false)
    try {
        const response = await axios['post'](`${urlDownload}`, parameters)
        let pathJSON = response.data
        download(pathJSON.path)
        swal.close()
    } catch (error) { alertaSimple('', 'Hubo un error al realizar la descarga, intenta nuevamente', 'error', 4000, true, true, true) }
}

var typeTimeOutListCampaign = ''

const timeOutListCampaign = () => {
    if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.nodeFetchSMSCampanas()
    typeTimeOutListCampaign = setTimeout(function() {
        timeOutListCampaign()
    }, 4000)
}

var typeTimeOutListCampaignValidacion = ''

const timeOutListCampaignValidacion = () => {
    if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.nodeFetchValidacionCampanas()
    typeTimeOutListCampaignValidacion = setTimeout(function() {
        timeOutListCampaignValidacion()
    }, 4000)
}

var typeTimeOutListCampaignBase = ''

const timeOutListCampaignBase = () => {
    if(typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.nodeFetchBaseCampanas()
    typeTimeOutListCampaignBase = setTimeout(function() {
        timeOutListCampaignBase()
    }, 4000)
}

const clearTimeOutListCampaign = () => {
    setTimeout(function() {
        clearTimeout(typeTimeOutListCampaign)
    }, 4000)
}

const clearTimeOutListCampaignValidacion = () => {
    setTimeout(function() {
        clearTimeout(typeTimeOutListCampaignValidacion)
    }, 4000)
}

const clearTimeOutListCampaignBase = () => {
    setTimeout(function() {
        clearTimeout(typeTimeOutListCampaignBase)
    }, 4000)
}

const getDescriptionStatusSMS = (nameStatus) => {
    switch (nameStatus) {
        case 'NO_ENVIADO':
            return "<p>El mensaje aún no ha sido procesado ni enviado</p>"
            break

        case 'PENDING_WAITING_DELIVERY':
            return "<p>El mensaje ha sido procesado y enviado a la próxima instancia, es decir, operador de telefonía móvil con acuse de recibo de solicitud desde su plataforma. El informe de entrega aún no se ha recibido y se espera, por lo que el estado aún está pendiente.</p>"
            break

        case 'PENDING_ENROUTE':
            return "<p>El mensaje se ha procesado y enviado a la siguiente instancia, es decir, operador de telefonía móvil.</p>"
            break

        case 'PENDING_ACCEPTED':
            return "<p>El mensaje ha sido aceptado y procesado, y está listo para ser enviado a la siguiente instancia, es decir, operador.</p>"
            break

        case 'UNDELIVERABLE_REJECTED_OPERATOR':
            return "<p>El mensaje se ha enviado al operador, mientras que la solicitud se rechazó o se revertió un informe de entrega con el estado <span class='badge px-3 heading-font-family bg-danger'>Rechazado</span>.</p>"
            break

        case 'UNDELIVERABLE_NOT_DELIVERED':
            return "<p>Se ha enviado un mensaje al operador, pero no se ha podido entregar, ya que un informe de entrega con el estado <span class='badge px-3 heading-font-family bg-warning'>No Entregado</span> se revertió del operador.</p>"
            break

        case 'DELIVERED_TO_OPERATOR':
            return "<p>El mensaje ha sido enviado y entregado exitosamente al operador.</p>"
            break

        case 'DELIVERED_TO_HANDSET':
            return "<p>El mensaje ha sido procesado y entregado exitosamente al destinatario.</p>"
            break

        case 'EXPIRED_EXPIRED':
            return "<p>El mensaje fue recibido y enviado al operador. Sin embargo, ha estado pendiente hasta que expire el período de validez, o el operador devolvió el estado <span class='badge px-3 heading-font-family bg-dribbble'>Expirado</span> mientras tanto.</p>"
            break

        case 'EXPIRED_DLR_UNKNOWN':
            return "<p>El mensaje ha sido recibido y enviado al operador para su entrega. Sin embargo, el informe de entrega del operador no se ha formateado correctamente o no se ha reconocido como válido.</p>"
            break

        case 'REJECTED_NETWORK':
            return "<p>Se recibió un mensaje, pero la red está fuera de nuestra cobertura o no está configurada en su cuenta. Su administrador de cuenta puede informarle sobre el estado de la cobertura o configurar la red en cuestión.</p>"
            break

        case 'REJECTED_PREFIX_MISSING':
            return "<p>El mensaje ha sido recibido pero ha sido rechazado ya que el número no es reconocido debido a un número incorrecto de prefijo o número de longitud. Esta información es diferente para cada red y se actualiza regularmente.</p>"
            break

        case 'REJECTED_DND':
            return "<p>El mensaje se ha recibido y rechazado debido a que el usuario está suscrito a los servicios DND (No molestar), lo que deshabilita el tráfico de servicio a su número.</p>"
            break

        case 'REJECTED_SOURCE':
            return "<p>Su cuenta está configurada para aceptar solo ID de remitente registrados, mientras que la ID de remitente definida en la solicitud no se ha registrado en su cuenta.</p>"
            break

        case 'REJECTED_NOT_ENOUGH_CREDITS':
            return "<p>Comunicarse con nuestra área de soporte : support@securitec.pe, por un problema de envio, con el asunto de Corte de Red.</p>"
            break

        case 'REJECTED_SENDER':
            return "<p>La identificación del remitente ha sido incluida en la lista negra en su cuenta comuníquese con el Soporte para obtener más ayuda.</p>"
            break

        case 'REJECTED_DESTINATION':
            return "<p>El número de destino ha sido incluido en la lista negra ya sea en la solicitud del operador o en su cuenta póngase en contacto con el Soporte para obtener más información.</p>"
            break

        case 'REJECTED_PREPAID_PACKAGE_EXPIRED':
            return "<p>Comunicarse con nuestra área de soporte : support@securitec.pe, por un problema de envio, con el asunto de Expirado.</p>"
            break

        case 'REJECTED_DESTINATION_NOT_REGISTERED':
            return "<p>Su cuenta ha sido configurada para enviarse solo a un número único con fines de prueba. Póngase en contacto con su Administrador de cuentas para eliminar la limitación.</p>"
            break

        case 'REJECTED_ROUTE_NOT_AVAILABLE':
            return "<p>Se ha recibido un mensaje en el sistema, sin embargo, su cuenta no se ha configurado para enviar mensajes, es decir, no hay rutas en su cuenta disponibles para su envío posterior. Su administrador de cuenta podrá configurar su cuenta según su preferencia.</p>"
            break

        case 'REJECTED_FLOODING_FILTER':
            return "<p>El mensaje ha sido rechazado debido a un mecanismo anti-flooding. De forma predeterminada, un solo número solo puede recibir 20 mensajes variados y 6 mensajes idénticos por hora. Si hay un requisito, la limitación se puede extender por cuenta a pedido a su Administrador de cuenta.</p>"
            break

        case 'REJECTED_SYSTEM_ERROR':
            return "<p>La solicitud ha sido rechazada debido a un error esperado del sistema. Póngase en contacto con nuestro equipo de soporte técnico para obtener más detalles.</p>"
            break

        case 'REJECTED_DUPLICATE_MESSAGE_ID':
            return "<p>La solicitud ha sido rechazada debido a un ID de mensaje duplicado especificado en la solicitud de envío, mientras que los ID de mensaje deberían ser un valor único.</p>"
            break

        case 'REJECTED_INVALID_UDH':
            return "<p>Se recibió un mensaje y nuestro sistema detectó que el mensaje se formateó incorrectamente debido a un parámetro de clase de ESM no válido o a una cantidad inexacta de caracteres al usar esmclass: 64.</p>"
            break

        case 'REJECTED_MESSAGE_TOO_LONG':
            return "<p>Se ha recibido un mensaje, pero la longitud total del mensaje es de más de 25 partes o texto de mensaje que supera los 4000 bytes según nuestra limitación del sistema.</p>"
            break

        case 'MISSING_TO':
            return "<p>La solicitud se ha recibido, sin embargo, el parámetro 'to' no se ha establecido o está vacío, es decir, debe haber destinatarios válidos para enviar el mensaje.</p>"
            break

        case 'REJECTED_INVALID_DESTINATION':
            return "<p>Se ha recibido la solicitud, sin embargo, el destino no es válido: el prefijo del número no es correcto, ya que no coincide con un prefijo numérico válido de ningún operador de telefonía móvil. La longitud del número también se toma en consideración para verificar la validez del número.</p>"
            break

        case 'BLACK_LIST':
            return "<p>La solicitud de envio ha sido cancelada para este número, ya que se encuentra en la lista negra.</p>"
            break
    }
}

const getStatusSMS = (idStatus) => {
    switch (idStatus){
        case 0:
            return "<span class='px-3 bg-span btn-cian w-100' style='display: inline-block !important;'>Aún sin Enviar</span>"
            break
        case 1:
            return "<span class='px-3 bg-span btn-yellow2 w-100' style='display: inline-block !important;'>Pendiente</span>"
            break
        case 2:
            return "<span class='px-3 bg-span btn-purple2 w-100' style='display: inline-block !important;'>No Entregado</span>"
            break
        case 3:
            return "<span class='px-3 bg-span btn-green2 w-100' style='display: inline-block !important;'>Entregado</span>"
            break
        case 4:
            return "<span class='px-3 bg-span btn-orange2 w-100' style='display: inline-block !important;'>Expirado</span>"
            break
        case 5:
            return "<span class='px-3 bg-span btn-rose2 w-100' style='display: inline-block !important;'>Rechazado</span>"
            break
        case 6:
            return "<span class='px-3 bg-span btn-gray2 w-100' style='display: inline-block !important;'>Lista Negra</span>"
            break
    }
}

const getConceptStatusSMS = (idStatus) => {
    switch (idStatus){
        case 0:
            return "<p>El mensaje aún no es enviado</p>"
            break
        case 1:
            return "<p>El mensaje ha sido procesado y enviado a la próxima instancia, es decir, operador de telefonía móvil.</p>"
            break
        case 2:
            return "<p>El mensaje no ha sido entregado.</p>"
            break
        case 3:
            return "<p>El mensaje ha sido procesado y entregado exitosamente.</p>"
            break
        case 4:
            return "<p>El mensaje se ha enviado y ha caducado debido a la espera más allá de su período de validez (nuestra plataforma predeterminada es 48 horas), o el informe de entrega del operador ha revertido el vencido como estado final.</p>"
            break
        case 5:
            return "<p>El mensaje ha sido recibido pero ha sido rechazado por el sistema, o el operador ha revertido <span class='badge px-3 heading-font-family bg-danger'>Rechazado</span> como estado final.</p>"
            break
        case 6:
            return "<p>El mensaje no ha sido enviado, ya que este número pertenece a su lista negra.</p>"
            break
    }
}

const getCountryMCCMNC = (numberCountry) => {
    switch (numberCountry){
        case 716:
            return "<span class='flag-icon flag-icon-pe' title='País: Perú'></span>"
            break
        default:
            return "<span class='fa fa-window-close-o' title='País: Desconocido'></span>"
            break
    }
}

const getOperatorMCCMNC = (numberOperator) => {
    switch (numberOperator){
        case 06:
            return "<span class='flag-icon flag-icon-operador-movistar' title='Operador: Movistar Perú'></span>"
            break
        case 07:
            return "<span class='flag-icon flag-icon-operador-nextel' title='Operador: Nextel Perú'></span>"
            break
        case 10:
            return "<span class='flag-icon flag-icon-operador-claro' title='Operador: Claro Perú'></span>"
            break
        case 15:
            return "<span class='flag-icon flag-icon-operador-bitel' title='Operador: Bitel Perú'></span>"
            break
        case 17:
            return "<span class='flag-icon flag-icon-operador-entel' title='Operador: Entel Perú'></span>"
            break
        default:
            return "<span class='fa fa-window-close-o' title='Operador: Desconocido'></span>"
            break
    }
}

const getStatusValidacion = (idStatus) => {
    switch (idStatus){
        case 0:
            return "<span class='px-3 bg-span btn-cian w-100' style='display: inline-block !important;'>Aún sin Validar</span>"
            break
        case 5:
            return "<span class='px-3 bg-span btn-gray2 w-100' style='display: inline-block !important;'><i class='fa fa-spin fa-spinner'></i> Validando</span>"
            break
        case 7:
            return "<span class='px-3 bg-span btn-purple2 w-100' style='display: inline-block !important;'>Inexistente</span>"
            break
        case 8:
            return "<span class='px-3 bg-span btn-rose2 w-100' style='display: inline-block !important;'>Buzon de Voz</span>"
            break
        case 9:
            return "<span class='px-3 bg-span btn-green2 w-100' style='display: inline-block !important;'>Prendido</span>"
            break
        case 998:
            return "<span class='px-3 bg-span btn-gray2 w-100' style='display: inline-block !important;'><i class='fa fa-spin fa-spinner'></i> Validando</span>"
            break
        default:
            return "<span class='px-3 bg-span btn-cian w-100' style='display: inline-block !important;'>Sin Validar</span>"
            break
    }
}

const sessionIdle = () => {
    $.timeoutDialog({
        timeout: 1,
        title: 'Validación de Sesión',
        message: 'Tu usuario está intentando iniciar sesión desde otra ubicación, tienes {0} para elegir una opción.',
        question: '',
        keep_alive_button_text: 'No, me quedaré',
        sign_out_button_text: 'Si, desconéctame',
        countdown: 5,
        keep_alive_url: '/keepSession',
        logout_redirect_url: '/logoutSession',
        restart_on_yes: false
    })
}