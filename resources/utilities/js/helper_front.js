// Agrega por defecto el crsf token en las peticiones ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

$(document).ready(function() {
    $("body").tooltip({
        selector: '[data-tooltip=tooltip]',
        trigger: 'hover',
        html: true
    })
    $("body").popover({
        selector: '[data-popover=popover]',
        html : true,
        content: function() {
            let content = $(this).attr("data-popover-content")
            return $(content).children(".popover-body").html()
        },
        title: function() {
            let title = $(this).attr("data-popover-content")
            return $(title).children(".popover-heading").html()
        }
    })
    $(document).on("click", ".popover .close" , function(){
        $(this).parents(".popover").popover('hide')
    })
    $('body').on('show.bs.popover', function () {
        $('[data-toggle="tooltip"], .tooltip').tooltip("hide")
    })
    currentTime()
})

const alertaSimple = (titleAlerta = '', textoAlerta = '', tipoAalerta = null, tiempoAlerta = 2000, outsideClick = false, escapeKey = false, enterKey = false) => {
    swal({
        title: titleAlerta,
        html: textoAlerta,
        type: tipoAalerta,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: outsideClick,
        allowEscapeKey: escapeKey,
        allowEnterKey: enterKey,
        timer: tiempoAlerta
    }).then(
        function () {},
        function (dismiss) { }
    )
}

// Función para cerrar un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModal = (modalID, bodyModal) => {
    $(bodyModal).html('').promise().done(function() {
        $('#'+modalID).modal('toggle')
    })
}

// Función para escuchar el evento cuando se cierra un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModalClose = (modalID, bodyModal) => {
    $('#'+modalID).on('hidden.bs.modal', function () {
        $(bodyModal).html('')
    })
}

// Función que muestra un boton de carga cuando se ejecuta un formulario
const changeButtonForm = (btnShow, btnHide) => {
    $('.' + btnHide).removeClass('d-none')
    $('.' + btnShow).addClass('d-none')
}

// Función que muestra los errores que emite el controlador en un DIV
const showErrorForm = (data, formDiv) => {
    let errors = ''
    for(datos in data){
        errors += '<li>' + data[datos] + '</li>'
    }
    $(formDiv).fadeIn().html(
        '<i class="material-icons list-icon">not_interested</i> <strong>Error</strong>'+
        '<ul class="mr-t-5">' + errors + '</ul>'
    )
    $(formDiv).removeClass('d-none')
}

// Función para ocultar los errores cuando se ingresa en algun campo que se requeria data
const hideErrorForm = (formDiv) => {
    $("input[type=text],input[type=checkbox],input[type=password],input[type=file],div.emojionearea-editor,div.bootstrap-select,button[data-toggle=dropdown]").click(function() {
        $(formDiv).addClass('d-none').html('')
    })
}

const reloadDatatable = (idTable) => {
    $(`.load${idTable}`).removeClass('d-none').addClass('d-block')
    $(`.draw${idTable}`).removeClass('d-block').addClass('d-none')
    return $(`#${idTable}`).DataTable().ajax.reload( null, false )
}

// Función para poder buscar en una tabla
const searchTable = (idTable, idSearch, filterAnnexed = '') => {
    $(idSearch).keyup(function(){
        _this = this
        $.each($(idTable + " tbody tr " + filterAnnexed), function() {
            let value = -1
            let valueSearch = $(_this).val()
            if($(this).text().toLowerCase().indexOf(valueSearch.toLowerCase()) === value) {
                $(this).hide()
            }else {
                $(this).show()
            }
        })
    })
}

// Función para seleccionar todos los checkbox con uno solo
const mark_all = (checkGeneral) => {
    if($(checkGeneral).is(':checked')){
        $("input.checkNew:checkbox").prop("checked", "checked")
        $('tr.trNew').addClass('table-warning')
        $('select.selectNew').removeAttr("disabled").val('1')
    }else{
        $("input.checkNew:checkbox").prop("checked", false)
        $('tr.trNew').removeClass('table-warning').addClass('info')
        $('select.selectNew').attr("disabled", true).val('-')
    }
}

// Función que deshabilita y coloca estilo a la fila de la tabla
const markCheck = (check) => {
    if($('#checkbox_'+check).is(':checked')){
        $('#tr_'+check).addClass('table-warning')
        $('#select_'+check).removeAttr("disabled").val('1')
    }else{
        $('#tr_'+check).removeClass('table-warning').addClass('info')
        $('#select_'+check).attr("disabled", true).val('-')
    }
}

const mark_all_menu = (checkGeneral, checkChildren) => {
    if($(checkGeneral).is(':checked')){
        $("input."+checkChildren+":checkbox").prop("checked", "checked")
    }else{
        $("input."+checkChildren+":checkbox").prop("checked", false)
    }
}

const initDatePicker = (idInput, options = {}) => {
    $(`${idInput}`).datepicker(options)
}

const initDateRangePicker = (idInput, options = {}) => {
    $(`${idInput}`).daterangepicker(options)
}

const initSelectPicker = (idInput, options = {}) => {
    $(`${idInput}`).selectpicker(options)
}

const markReadOnly = (checkRead, inputRead, inputRecover) => {
    if($(checkRead).is(':checked')){
        $(inputRead).val('')
        $(inputRead).prop("readonly",false)
    }else{
        $(inputRead).val($(inputRecover).val())
        $(inputRead).prop("readonly",true)
    }
}

/* Fix para el Datatable con Tabs */
$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust()
} )

const ucwords = (str) => {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase()
    })
}

const currentTime = () => {
    let today = moment()
    today.locale(vmFront.userInformation['locale'])
    let days = ucwords(today.format('DD'))
    let month = ucwords(today.format('MMMM'))
    let years = ucwords(today.format('YYYY'))
    let hours = today.hours()
    let minutes = today.minutes()
    let seconds = today.seconds()
    minutes = checkTime(minutes)
    seconds = checkTime(seconds)
    $('#dateServer').html(`${days} de ${month} del ${years}`)
    $('#hourServer').html(`${hours}:${minutes}:${seconds}`)
    setTimeout(currentTime, 500)
}

const checkTime = (i) => {
    if (i < 10) i = "0" + i
    return i
}

const showImagePreview = (input, divShowImage, avatarOriginalProveedor = '') => {
    if (input.files && input.files[0]) {
        let filerdr = new FileReader()
        filerdr.onload = function (e) {
            $(`${divShowImage}`).attr('src', e.target.result)
            $(`input[name=${avatarOriginalProveedor}]`).val('')
        }
        filerdr.readAsDataURL(input.files[0])
    }
}

const emojiArea = (idInput, options = {}) => {
    $(`${idInput}`).emojioneArea(options)
}

const bootstrapFile = (idInput, options = {}) => {
    $(`${idInput}`).filestyle(options)
}

const tagEditor = (idInput, options = {}) => {
    $(`${idInput}`).tagEditor(options)
}

const soundIo = () => {
    ion.sound({
        sounds: [
            {name: "notification_sms"}
        ],
        path: "sounds/",
        preload: true,
        volume: 1.0,
        loop: true
    })
}

const multiDimensionalUnique = (arr) => {
    let uniques = []
    let itemsFound = {}
    for(let i = 0, l = arr.length; i < l; i++) {
        let stringified = JSON.stringify(arr[i])
        if(itemsFound[stringified]) { continue }
        uniques.push(arr[i])
        itemsFound[stringified] = true
    }
    return uniques
}

const hex2rgba_convert = (hex,opacity) => {
    hex = hex.replace('#','')
    r = parseInt(hex.substring(0,2), 16)
    g = parseInt(hex.substring(2,4), 16)
    b = parseInt(hex.substring(4,6), 16)

    result = 'rgba('+r+','+g+','+b+','+opacity/100+')'
    return result
}

const filterSelectDatatable = (nameSelect, nameDatatable, numColumn) => {
    $(nameSelect).on('change', function() {
        $(nameDatatable).DataTable().columns(numColumn).search(
            this.value,2,true,true
        ).draw()
    })
}

const DataTableHide = (nameDatatable, numeroColumnas) => {
    let DataTableDiv = $(`#${nameDatatable}`).DataTable()
    DataTableDiv.columns(numeroColumnas).visible(false, false)
    DataTableDiv.columns.adjust().draw(false)
}

const SelectChangeData = (divSelect, dataSelect, divHTML) => {
    $(divSelect).change(function() {
        let changeData = $(this).find(':selected').data(dataSelect)
        $(divHTML).html(changeData)
    })
}

const visibleCheckDiv = (divCheck, divVisibility) => {
    if ($(divCheck).is(':checked')) {
        $(divVisibility).removeClass('d-none')
    }else{
        $(divVisibility).addClass('d-none')
    }
}

const downloadFile = (url) => {
    return download(url)
}

const deleteVariableSMSFunction = () => {
    vmSMSChatCampaign = undefined
    vmSMSChatNumbers = undefined
    vmSMSChatWindow = undefined
    vmSMSChatNotCampaign = undefined
    vmSMSChatNumbersNot = undefined
    vmSMSChatWindowNot = undefined
}

const filterNumber = (e) => {
    let key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57 || key === 8 || key === 9)
}

const BlockCopyPaste = (e) => {
    if (e.ctrlKey === true && (e.which === 118 || e.which === 86)) return false
}

const initFlatPickr = (selector, options = {}) => {
    $(`${selector}`).flatpickr(options)
}