$('#formCampaign').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaignValidacion', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente la campaña', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.paginationFetchValidacionCampanas()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignUpload').submit(async function(e) {
    let formData = new FormData($('#formCampaignUpload')[0])
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    await axios['post']('saveFormCampaignValidacionUpload', formData)
        .then(function (response){
            validateError = false
            setTimeout(function() {
                alertaSimple('', 'Se agrego la base de telefonos correctamente', 'success', 2000)
                clearModal('modalScore', 'div.dialogScoreLarge')
                changeButtonForm('btnLoad','btnForm')
                if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.paginationFetchValidacionCampanas()
            }, 1200)
        })
        .catch(function (error) {
            validateError = true
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(error.response.data, '.formError')
        })

    if(!validateError){
        await axios['post']('executeUploadCampaignValidacion', formData)
    }
})

$('#formStatusCampaign').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaignValidacionStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado de la campaña correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.paginationFetchValidacionCampanas()
            timeOutListCampaignValidacion()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignPlay').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = axios['post']('executeValidacionStart', data)
        setTimeout(function() {
            alertaSimple('', 'Se ha iniciado la validación con exito', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.paginationFetchValidacionCampanas()
            changeButtonForm('btnLoad','btnForm')
            timeOutListCampaignValidacion()
        },1200)
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignPause').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = axios['post']('executeValidacionPause', data)
        setTimeout(function() {
            alertaSimple('', 'Se ha pausado la validación con exito', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableValidacionCampanas !== 'undefined') vmDataTableValidacionCampanas.paginationFetchValidacionCampanas()
            changeButtonForm('btnLoad','btnForm')
            clearTimeOutListCampaignValidacion()
        },1200)
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formPersonValidationPlay').submit(async function(e) {
    let data = $(this).serialize()
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    await axios['post']('executePersonValidation', data)
        .then(function (response){
            if(response.data.message == 'Success'){
                validateError = false
            }else{
                validateError = true
            }
        })
        .catch(function () {
            validateError = true
            alertaSimple('', 'Problemas al iniciar la validación individual', 'error', 2000)
            changeButtonForm('btnLoad','btnForm')
        })

    if(!validateError){
        axios['post']('executePersonValidationStart', data)
        setTimeout(function() {
            alertaSimple('', 'Se inicio la validación de los telefonos', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmPersonDetails !== 'undefined'){
                vmPersonDetails.getDataPersonTelefonos()
                vmPersonDetails.initFormValidacion = false
            }
            changeButtonForm('btnLoad','btnForm')
        },800)
    }else{
        alertaSimple('', 'Se acaba de superar los canales de tu suscripción, intentelo mas tarde', 'warning', 2000)
        changeButtonForm('btnLoad','btnForm')
    }
})