$('#formSearchIndividual').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        $('.rowDataSearchIndividual').html(`<div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div>`)
        const response = await axios['post']('/getDataFormIndividualSearch', data)
        if(response.data){
            if(response.data.searchData){
                vmFront.loadOptionMenu(response.data.routeSearch, {
                    numDNI: response.data.numDNI
                }, 'post')
            }else{
                $('.rowDataSearchIndividual').html(response.data)
            }
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        $('.rowDataSearchIndividual').html('')
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formIndividualError')
    }
})

$('#formSearchMasivo').submit(async function(e) {
    let formData = new FormData($('#formSearchMasivo')[0])
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        $('.rowDataSearchMasivo').html(`<div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div>`)
        const response = await axios['post']('/getDataFormMasivoSearch', formData)
        if(response.data){
            $('.rowDataSearchMasivo').html(response.data)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        $('.rowDataSearchMasivo').html('')
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formMasivoError')
    }
})