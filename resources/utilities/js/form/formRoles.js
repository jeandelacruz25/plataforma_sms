/**
 * Created by Carlos on 15/12/2017.
 */
$('#formRole').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormRoles', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente el perfil', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTablePerfiles !== 'undefined') vmDataTablePerfiles.paginationFetchPerfiles()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formRolesAssing').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormRolesAssing', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se asignaron correctamente los perfiles', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formRoleMenu').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormRolesMenu', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se actualizaron los menú del Perfil correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})