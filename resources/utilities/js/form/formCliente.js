$('#formCliente').submit(async function(e) {
    let formData = new FormData($('#formCliente')[0])
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormClientes', formData)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente al cliente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
            if(typeof vmDataTableClientes !== 'undefined') vmDataTableClientes.paginationFetchClientes()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formClienteMenu').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormClientesMenu', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se actualizaron los menú del Cliente correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formClienteStatus').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormClientesStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado del cliente correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableClientes !== 'undefined') vmDataTableClientes.paginationFetchClientes()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formClienteRules').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormClientesRules', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se actualizaron las reglas del cliente con exito', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableClientes !== 'undefined') vmDataTableClientes.paginationFetchClientes()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})