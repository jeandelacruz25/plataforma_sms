$('#formBlackList').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormBlackList', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se registraron los números de forma exitosa', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            reloadDatatable(response.data.datatable)
        }else{
            alertaSimple('', 'Problemas al realizar el envio', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})


$('#formBlackListMasivo').submit(async function(e) {
    let formData = new FormData($('#formBlackListMasivo')[0])
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormBlackListMasivo', formData)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se registraron los números de forma exitosa', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            reloadDatatable(response.data.datatable)
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formDeleteBlackList').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('deleteNumberBlackList', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se elimino el número con exito', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            reloadDatatable(response.data.datatable)
        }else{
            alertaSimple('', 'Problemas al realizar el envio', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})
