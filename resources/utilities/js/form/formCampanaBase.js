$('#formCampaign').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaignBase', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente la campaña', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.paginationFetchBaseCampanas()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignUpload').submit(async function(e) {
    let formData = new FormData($('#formCampaignUpload')[0])
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    await axios['post']('saveFormCampaignBaseUpload', formData)
        .then(function (response){
            validateError = false
            setTimeout(function() {
                alertaSimple('', 'Se agrego la base de telefonos correctamente', 'success', 2000)
                clearModal('modalScore', 'div.dialogScoreLarge')
                changeButtonForm('btnLoad','btnForm')
                if(typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.paginationFetchBaseCampanas()
            }, 1200)
        })
        .catch(function (error) {
            validateError = true
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(error.response.data, '.formError')
        })

    if(!validateError){
        await axios['post']('executeUploadCampaignBase', formData)
    }
})

$('#formStatusCampaignBase').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaignBaseStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado de la campaña correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.paginationFetchBaseCampanas()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignBaseCruce').submit(function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = axios['post']('startCruceBase', data)
        setTimeout(function() {
            alertaSimple('', 'Se inicio proceso para el cruce de bases', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableBaseCampanas !== 'undefined') vmDataTableBaseCampanas.paginationFetchBaseCampanas()
            changeButtonForm('btnLoad','btnForm')
            timeOutListCampaignBase()
        },1200)
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formBaseValidatePhone').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('asyncBaseValidation', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se creo la campaña con exito, espere un momento..', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            vmFront.loadOptionMenu('validacion_telefonos', {}, 'get')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})