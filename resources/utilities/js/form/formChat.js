$('#formChat').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveChatMessage', data)
        if(response.data.message === 'Success'){
            let el = $('#messageChat').emojioneArea()
            el[0].emojioneArea.setText('')
            $('.formError').fadeOut().html('')
            socketNodejs.emit('sendMessageChat')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})