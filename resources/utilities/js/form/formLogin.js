$('#loginForm').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('loginSession', data)
        if(response.data.Auth === 'success'){
            alertaSimple('', 'Inicio de Sesión Exitosa! Espere un momento...', 'success', 2000, false, false, false)
            setTimeout(function() {
                location.reload()
            }, 2000)
            changeButtonForm('btnLoad','btnForm')
        }else if(response.data.Auth === 'password'){
            alertaSimple('', 'La contraseña ingresada no coincide con la cuenta.', 'error', 2000, false, false, false)
            changeButtonForm('btnLoad','btnForm')
        }else{
            alertaSimple('', 'La cuenta se encuentra conectada, espere unos segundos para validar la sesión.', 'info', 8000, false, false, false)
            await axios['post']('emitValidateSession', data)
            setTimeout(async function() {
                try {
                    const responseValidate = await axios['post']('validateSession', data)
                    if(responseValidate.data.Auth === 'logout'){
                        swal.close()
                        alertaSimple('', 'La cuenta se desconectó con éxito, espere un momento, por favor.', 'success', 3000, false, false, false)
                        setTimeout(function() {
                            location.reload()
                        }, 2000)
                    }else{
                        swal.close()
                        alertaSimple('', 'La cuenta conectada seleccionó mantenerse en el sistema.', 'error', 3000, false, false, false)
                    }
                    changeButtonForm('btnLoad','btnForm')
                } catch (error){
                    alertaSimple('', 'Hubo un problema al verificar la respuesta de la cuenta, intentelo nuevamente.', 'error', 3000, false, false, false)
                    changeButtonForm('btnLoad','btnForm')
                }
            }, 6000)
        }
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})