/**
 * Created by Carlos on 15/12/2017.
 */
$('#formUser').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormUsers', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente al usuario', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableUsuarios !== 'undefined') vmDataTableUsuarios.paginationFetchUsuarios()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formUserMenu').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormUsersMenu', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se actualizaron los menú del Usuario correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formStatusUser').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormUsersStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado del usuario correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableUsuarios !== 'undefined') vmDataTableUsuarios.paginationFetchUsuarios()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formAvatar').submit(async function(e) {
    let formData = new FormData($('#formAvatar')[0])
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormAvatar', formData)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio correctamente el avatar', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            vmFront.userInformation.avatar = ''
            vmFront.getUserInformation(false)
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})