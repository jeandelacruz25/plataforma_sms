$('#formSMSMasivo').submit(function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        axios['post']('sendSMSMasivo', data)
        setTimeout(function() {
            alertaSimple('', 'Se empezó el envio de SMS', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.paginationFetchSMSCampanas()
            changeButtonForm('btnLoad','btnForm')
            timeOutListCampaign()
        }, 1200)
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formSMSIndividual').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('sendSMSIndividual', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se enviaron correctamente los SMS', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
            if(typeof vmDataTableSMSSinCampanas !== 'undefined') vmDataTableSMSSinCampanas.paginationFetchSMSSinCampanas()
        }else{
            alertaSimple('', 'Problemas al realizar el envio', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formSeenDetalleSMS').submit(async function (e) {
    let valueCampaing = $('input[name=notCampaign]').val()
    let data = ''

    if(valueCampaing) {
        data = {
            smsDetalleID: vmSMSChatNumbersNot.selected
        }
    } else {
        data = {
            smsDetalleID: vmSMSChatNumbers.selected
        }
    }

    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormChatNumberSeen', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambiaron a leido los chats exitosamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(response.data.notCampaign){
                vmSMSChatNotCampaign.getListChatNumbers(vmSMSChatNotCampaign.idBulkSMS, '#chatNumbersNot', '#chatWindowNot', 'post')
            }else{
                vmSMSChatCampaign.getListChatNumbers(vmSMSChatCampaign.idBulkSMS, '#chatNumbers', '#chatWindow', 'post')
            }
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formChatSMS').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('sendSMSChat', data)
        if(response.data.message === 'Success'){
            $('.formError').fadeOut().html('')
            setTimeout(function() {
                vmSMSChatWindow.getListChatWindow()
                changeButtonForm('btnLoad','btnForm')
                vmSMSChatWindow.mensajeChatCampaign = ''
            }, 1200)
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            changeButtonForm('btnLoad','btnForm')
        }
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formChatNotSMS').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('sendSMSChat', data)
        if(response.data.message === 'Success'){
            $('.formError').fadeOut().html('')
            setTimeout(function() {
                vmSMSChatWindowNot.getListChatWindow()
                changeButtonForm('btnLoad','btnForm')
                vmSMSChatWindowNot.mensajeChatNotCampaign = ''
            }, 1200)
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            changeButtonForm('btnLoad','btnForm')
        }
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formSMSKeyword').submit(async function (e) {
    let formData = new FormData($('#formSMSKeyword')[0])
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()

    await axios['post']('sendSMSKeyword', formData)
        .then(function (response){
            validateError = false
            setTimeout(function() {
                alertaSimple('', 'Se empezara a enviar los SMS, se le notificara cuando este ha concluido', 'success', 4000)
                clearModal('modalScore', 'div.dialogScoreLarge')
                if(typeof vmDataTableSMSKeywordSend !== 'undefined') vmDataTableSMSKeywordSend.paginationFetchSMSKeywordSend()
            }, 1200)
        })
        .catch(function (error) {
            validateError = true
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(error.response.data, '.formError')
        })

    if(!validateError){
        await axios['post']('executeUploadKeyword', formData)
    }
})