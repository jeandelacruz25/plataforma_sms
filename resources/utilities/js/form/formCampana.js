$('#formCampaign').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaign', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente la campaña', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.paginationFetchSMSCampanas()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formCampaignUpload').submit(async function(e) {
    let formData = new FormData($('#formCampaignUpload')[0])
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    await axios['post']('saveFormCampaignUpload', formData)
        .then(function (response){
            validateError = false
            setTimeout(function() {
                alertaSimple('', 'Se agrego la base de usuarios correctamente', 'success', 2000)
                clearModal('modalScore', 'div.dialogScoreLarge')
                changeButtonForm('btnLoad','btnForm')
                if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.paginationFetchSMSCampanas()
            }, 1200)
        })
        .catch(function (error) {
            validateError = true
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(error.response.data, '.formError')
        })

    if(!validateError){
        await axios['post']('executeUploadCampaign', formData)
    }
})

$('#formStatusCampaign').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormCampaignStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado de la campaña correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableSMSCampanas !== 'undefined') vmDataTableSMSCampanas.paginationFetchSMSCampanas()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})