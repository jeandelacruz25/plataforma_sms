$('#formKeyword').submit(async function (e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormSMSKeyword', data)
        if(response.data.message === 'Success'){
            let action = (response.data.action === 'create' ? 'agrego' : 'edito')
            alertaSimple('', 'Se '+ action +' correctamente el keyword', 'success', 2000)
            clearModal('modalScore', 'div.dialogScoreLarge')
            if(typeof vmDataTableSMSKeyword !== 'undefined') vmDataTableSMSKeyword.paginationFetchSMSKeyword()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formBaseKeywordUpload').submit(async function(e) {
    let formData = new FormData($('#formBaseKeywordUpload')[0])
    let validateError = false
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()

    await axios['post']('saveFormBaseKeywordUpload', formData)
        .then(function (response){
            validateError = false
            setTimeout(function() {
                alertaSimple('', 'Se empezara a enviar los SMS, se le notificara cuando este ha concluido', 'success', 4000)
                clearModal('modalScore', 'div.dialogScoreLarge')
                if(typeof vmDataTableSMSKeyword !== 'undefined') vmDataTableSMSKeyword.paginationFetchSMSKeyword()
            }, 1200)
        })
        .catch(function (error) {
            validateError = true
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(error.response.data, '.formError')
        })

    console.dir(validateError)

    if(!validateError){
        console.dir('Holii')
        await axios['post']('executeUploadKeyword', formData)
    }
})

$('#formStatusKeyword').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormKeywordStatus', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado del keyword correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableSMSKeyword !== 'undefined') vmDataTableSMSKeyword.paginationFetchSMSKeyword()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})

$('#formConfirmationKeyword').submit(async function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    e.preventDefault()
    try {
        const response = await axios['post']('saveFormKeywordConfirmation', data)
        if(response.data.message === 'Success'){
            alertaSimple('', 'Se cambio el estado de servicio del keyword correctamente', 'success', 2000)
            clearModal('modalScore', 'div.dialogScore')
            if(typeof vmDataTableSMSKeyword !== 'undefined') vmDataTableSMSKeyword.paginationFetchSMSKeyword()
        }else{
            alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
        }
        changeButtonForm('btnLoad','btnForm')
    } catch (error) {
        changeButtonForm('btnLoad','btnForm')
        showErrorForm(error.response.data, '.formError')
    }
})