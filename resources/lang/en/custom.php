<?php
return [
    'auth' => [
        'phrase' => [
            'textPhrase' => '¡Because there are different ways to contact your customers!',
            'autorPhrase' => 'Securitec Perú'
        ]
    ],

    'current' => [
        'language' => 'Current Language'
    ],

    'choose' => [
        'language' => 'Choose your Language'
    ],

    'copyright' => 'All rights reserved',
];