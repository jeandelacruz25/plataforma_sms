<?php
return [
    'auth' => [
        'phrase' => [
            'textPhrase' => '¡Porque existen diversas formas de contactar a tus clientes!',
            'autorPhrase' => 'Securitec Perú'
        ]
    ],

    'current' => [
        'language' => 'Idioma Actual'
    ],

    'choose' => [
        'language' => 'Escoge tu Idioma'
    ],

    'copyright' => 'Todos los derechos reservados',
];