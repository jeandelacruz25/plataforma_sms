<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="format-detection" content="email=no">
    <title></title>
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- fonts -->
    <style>
        /*fonts*/
        /*@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900');*/
        /*fonts*/

        /*=========*/
        /*fonts*/
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 300;
            src: local('Raleway Light'), local('Raleway-Light'), url(https://fonts.gstatic.com/s/raleway/v11/-_Ctzj9b56b8RgXW8FAriRsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 400;
            src: local('Raleway'), local('Raleway-Regular'), url(https://fonts.gstatic.com/s/raleway/v11/IczWvq5y_Cwwv_rBjOtT0w.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 500;
            src: local('Raleway Medium'), local('Raleway-Medium'), url(https://fonts.gstatic.com/s/raleway/v11/CcKI4k9un7TZVWzRVT-T8xsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 600;
            src: local('Raleway SemiBold'), local('Raleway-SemiBold'), url(https://fonts.gstatic.com/s/raleway/v11/xkvoNo9fC8O2RDydKj12bxsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 700;
            src: local('Raleway Bold'), local('Raleway-Bold'), url(https://fonts.gstatic.com/s/raleway/v11/JbtMzqLaYbbbCL9X6EvaIxsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 800;
            src: local('Raleway ExtraBold'), local('Raleway-ExtraBold'), url(https://fonts.gstatic.com/s/raleway/v11/1ImRNPx4870-D9a1EBUdPBsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        @font-face {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 900;
            src: local('Raleway Black'), local('Raleway-Black'), url(https://fonts.gstatic.com/s/raleway/v11/PKCRbVvRfd5n7BTjtGiFZBsxEYwM7FgeyaSgU71cLG0.woff) format('woff');
        }
        /*fonts*/
        /*=========*/

        /*=========*/
        /*reset styles*/
        html{width: 100%;}
        body{width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; mso-margin-top-alt: 0px; mso-margin-bottom-alt: 0px; mso-padding-alt: 0px 0px 0px 0px;}
        div, p, a, li, td { -webkit-text-size-adjust:none; }
        #outlook a {padding:0;}
        p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}
        table{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        table td {border-collapse: collapse;}
        table p,
        table h1,
        table h2,
        table h3,
        table h4{ margin-top:0!important;margin-right:0!important;margin-bottom:0!important;margin-left:0!important;padding-top:0!important;padding-right:0!important;padding-bottom:0!important;padding-left:0!important;display: inline-block!important;font-family: inherit!important;}
        span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}
        /* IOS blue links */
        a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }a {outline:none;}
        /* IOS blue links */
        td a {color: inherit; text-decoration: none;}
        a {color: inherit; text-decoration: none;}
        .undoreset a, .undoreset a:hover {text-decoration: none !important;}
        .yshortcuts a {border-bottom: none !important;}
        .ios-footer a {color: #aaaaaa !important;text-decoration: none;}
        a {text-decoration: none;}
        a img {border:none;}
        .image_fix {display:block;}
        .ReadMsgBody { width: 100%;}
        .ExternalClass {width: 100%;}
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;}
        img{-ms-interpolation-mode:bicubic;}
        p {margin:0 !important; padding:0 !important;}
        /*reset styles*/
        /*=========*/

        @media only screen and (max-width: 600px)
        {
            body{min-width:100% !important;}
            .container        {width:450px !important; min-width:450px !important;}
            .container_2      {width:450px !important; min-width:450px !important;}
            .container_3      {height:180px !important;}
            .center           {margin:0 auto 0 auto !important; padding-top: 20px;}
            .center_menu      {margin:0 auto 0 auto !important; text-align:center !important;}
            .container_wrap   {display:inline-block !important; width:450px !important; height:auto !important;}
            .container_wrap_2 {display:block !important; width:450px !important; text-align:center !important; height:auto !important;}
            .container_wrap_margin{display:block !important; width:100% !important; height:10px !important;}
            .text             {width:380px !important; text-align:center !important;}
            .title            {width:450px !important;text-align:center !important;}
            .title_2          {width:450px !important;border:none !important; text-align:center !important; padding-left:none !important;}
            .image_container_main a img   {width:450px !important; height:auto !important }
            .image_container  {width: 100% !important; height: auto !important; }
            .disable          {display: none !important;}
            .enable           {display: inline !important;}
            .bgnd             {background-size:cover !important;}
            .padding_top      {padding-top: 20px;}
            .padding_bottom   {padding-bottom: 20px;}
            .height           {height:400px !important;}
        }
        @media only screen and (max-width: 481px)
        {
            body {min-width:100% !important;}
            .container        {width:300px !important; min-width:300px !important;}
            .container_2      {width:300px !important; min-width:300px !important;}
            .container_3      {height:180px !important;}
            .center           {margin:0 auto 0 auto !important;}
            .center_menu      {margin:0 auto 0 auto !important; }
            .container_wrap   {display:inline-block !important; width:300px !important; height:auto !important; border-radius:0 !important;}
            .container_wrap_2 {display:block !important; width:300px !important; height:auto !important;}
            .container_wrap_margin{display:block !important; width:100% !important; height:10px !important;}
            .text             {width:270px !important; text-align:center !important;}
            .title            {text-align:center !important;width:300px !important; }
            .title_2          {width:450px !important;border:none !important; text-align:center !important; padding-left:none !important;}
            .image_container_main a img   {width:300px !important; height:auto !important;}
            .image_container  {width: 100% !important; height: auto !important; }
            .disable          {display: none !important;}
            .enable           {display: inline !important;}
            .bgnd             {background-size:cover !important;}
            .padding_top      {padding-top: 20px;}
            .padding_bottom   {padding-bottom: 20px;}
            .height           {height:300px !important;}
        }
    </style>


    <!-- ========= -->
    <!-- mailchimp -->

    <!-- ////////////////////////// section-s1 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s1
* .section Background
*/
        table.section-s1 td.s1-bg{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s2 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s2
* .section Background
*/
        table.section-s2 td.s2-bg{
            /*@editable*/background-color:#696484 !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s_bg.jpg) !important;
        }
        /**
* @tab section-s2
* .section Button
*/
        table.section-s2 td.s2-btn table{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s3 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s3
* .section Background
*/
        table.section-s3 td.s3-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s4 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s4
* .section Background
*/
        table.section-s4 td.s4-bg{
            /*@editable*/background-color:#eeeeee !important;
        }
    </style>

    <!-- ////////////////////////// section-s5 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s5
* .section Background
*/
        table.section-s5 td.s5-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s6 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s6
* .section Background
*/
        table.section-s6 td.s6-bg{
            /*@editable*/background-color:#696484 !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s6_bg.jpg) !important;
        }
    </style>

    <!-- ////////////////////////// section-s7 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s7
* .section Background
*/
        table.section-s7 td.s7-bg{
            /*@editable*/background-color:#ffffff !important;
        }
        /**
* @tab section-s7
* .section Button
*/
        table.section-s7 td.s7-btn table{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s8 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s8
* .section Background
*/
        table.section-s8 td.s8-bg{
            /*@editable*/background-color:#eeeeee !important;
        }
    </style>

    <!-- ////////////////////////// section-s9 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s9
* .section Background
*/
        table.section-s9 td.s9-bg{
            /*@editable*/background-color:#ffffff !important;
        }
        /**
* @tab section-s9
* .section Img-1
*/
        table.section-s9 table.s9-img-1{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_1.jpg) !important;
        }
        /**
* @tab section-s9
* .section Img-2
*/
        table.section-s9 table.s9-img-2{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_2.jpg) !important;
        }
        /**
* @tab section-s9
* .section Img-3
*/
        table.section-s9 table.s9-img-3{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_3.jpg) !important;
        }
        /**
* @tab section-s9
* .section Img-4
*/
        table.section-s9 table.s9-img-4{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_4.jpg) !important;
        }
        /**
* @tab section-s9
* .section Img-5
*/
        table.section-s9 table.s9-img-5{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_5.jpg) !important;
        }
        /**
* @tab section-s9
* .section Img-6
*/
        table.section-s9 table.s9-img-6{
            /*@editable*/background-color:#cccccc !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s9_img_6.jpg) !important;
        }
    </style>

    <!-- ////////////////////////// section-s10 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s10
* .section Background
*/
        table.section-s10 td.s10-bg{
            /*@editable*/background-color:#eeeeee !important;
        }
    </style>

    <!-- ////////////////////////// section-s11 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s11
* .section Background
*/
        table.section-s11 td.s11-bg{
            /*@editable*/background-color:#696484 !important;
            /*@editable*/background-image: url(http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s11_bg.jpg) !important;
        }
        /**
* @tab section-s11
* .section Button
*/
        table.section-s11 td.s11-btn table{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s12 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s12
* .section Background
*/
        table.section-s12 td.s12-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s13 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s13
* .section Background
*/
        table.section-s13 td.s13-bg{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s14 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s14
* .section Background
*/
        table.section-s14 td.s14-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s15 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s15
* .section Background
*/
        table.section-s15 td.s15-bg{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s16 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s16
* .section Background
*/
        table.section-s16 td.s16-bg{
            /*@editable*/background-color:#eeeeee !important;
        }
        /**
* @tab section-s16
* .section Background-white
*/
        table.section-s16 table.s16-white-bg{
            /*@editable*/background-color:#ffffff !important;
        }
        /**
* @tab section-s16
* .section Background-blue
*/
        table.section-s16 table.s16-blue-bg{
            /*@editable*/background-color:#696484 !important;
        }
        /**
* @tab section-s16
* .section Button
*/
        table.section-s16 td.s16-btn table{
            /*@editable*/background-color:#696484 !important;
        }
    </style>

    <!-- ////////////////////////// section-s17 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s17
* .section Background
*/
        table.section-s17 td.s17-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- ////////////////////////// section-s18 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s18
* .section Background
*/
        table.section-s18 td.s18-bg{
            /*@editable*/background-color:#eeeeee !important;
        }
    </style>

    <!-- ////////////////////////// section-s19 Styles ////////////////////////// -->
    <style type="text/css">
        /**
* @tab section-s19
* .section Background
*/
        table.section-s19 td.s19-bg{
            /*@editable*/background-color:#ffffff !important;
        }
    </style>

    <!-- mailchimp -->
    <!-- ========= -->

    <!--[if (gte mso 9)|(IE)]><style type="text/css">table, tr, td, div, span, p, a {font-family: Arial,Helvetica,sans-serif !important;}td a {color: inherit !important; text-decoration: none !important;}a {color: inherit !important; text-decoration: none !important;}</style><![endif]-->

</head>
<body>

<table class="section-s2" width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin:0 auto; max-width:800px;">
    <!--[if (gte mso 9)|(IE)]>
    <table width="800" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; ">
        <tr>
            <td>
    <![endif]-->
    <tr>
        <td align="center" class="s2-bg" style="background: url(&quot;http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s2_bg.jpg&quot;) center center / cover no-repeat rgb(204, 204, 204);" bgcolor="#cccccc" background="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s2_bg.jpg">
            <!--[if (gte mso 9)|(IE)]>
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:800px; height:380px;">
                <v:fill type="tile" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s2_bg.jpg" color="#cccccc" ></v:fill>
                <v:textbox inset="0,0,0,0">
            <![endif]-->
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" class="container" style="table-layout:fixed;margin:0 auto; border-collapse:collapse; border: 0px;">
                <tr>
                    <td align="center" class="container">
                        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" class="container" style="border-collapse:collapse; border: 0px;">
                            <!-- margin -->
                            <tr>
                                <td height="70" width="2" style="font-size: 2px; line-height: 70px; height:70px;">&nbsp;</td>
                            </tr>
                            <!-- margin -->
                            <tr>
                                <td>
                                    <table class="container" align="center" width="380" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; ">
                                        <tr>
                                            <td height="15" width="2" style="font-size: 2px; line-height: 15px; height:15px;">&nbsp;</td>
                                        </tr>
                                        <!-- margin -->
                                        <tr>
                                            <th align="left" width="280" class="container_wrap">
                                                <table align="left" width="280" class="container" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;">
                                                    <tr>
                                                        <td align="center">
                                                            <table align="center" class="container" width="280" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;">
                                                                <!-- margin -->
                                                                <tr>
                                                                    <td height="20" width="20" style="font-size: 1px; line-height: 20px; height:15px;">&nbsp;</td>
                                                                </tr>
                                                                <!-- margin -->
                                                                <!-- text-->
                                                                <tr>
                                                                    <td class="text" style="text-decoration: none; font-family: 'Raleway', Open Sans, sans-serif; font-size: 30px; font-weight:400; line-height:44px; color:#ffffff; text-align: center; font-weight:600;" data-size="slogan_size" data-min="12" data-max="36" data-color="slogan_color">
                                                                        <multiline>Nuevo Keyword</multiline>
                                                                        <br>
                                                                        <span style="font-weight:600; color:#ffe229;">
			                                                                <multiline>{{ $nameKeyword }}</multiline>
			                                                            </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text" style="text-decoration: none; font-family: 'Raleway', Open Sans, sans-serif; font-size: 15px; line-height:22px; color:#ffffff; text-align: center; font-weight:400;" data-size="slogan_2_size" data-min="12" data-max="36" data-color="slogan_2_color">
                                                                        <multiline>
                                                                            Para el cliente {{ $nameCliente }}
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                                <!-- text-->
                                                                <!-- margin -->
                                                                <tr>
                                                                    <td height="40" width="1" style="font-size: 1px; line-height: 40px; height:40px;">&nbsp;</td>
                                                                </tr>
                                                                <!-- margin -->
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th width="280" align="right" class="container_wrap">
                                                <table align="right" width="280" class="container" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;">
                                                    <tr>
                                                        <td class="image_container_main">
                                                            <img src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/02/17/mTtY5kHVQDONW3pbXxBLrf6w/Stampready/img/s4_img.png" height="156" width="280" alt="img"></td>
                                                    </tr>
                                                    <!-- margin -->
                                                    <tr>
                                                        <td align="center" height="40" width="1" style="font-size: 1px; line-height: 40px; height:40px;">&nbsp;</td>
                                                    </tr>
                                                    <!-- margin -->
                                                </table>
                                            </th>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- margin -->
                            <tr>
                                <td align="center" height="80" width="1" style="font-size: 1px; line-height: 80px; height:80px;">&nbsp;</td>
                            </tr>
                            <!-- margin -->
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if gte mso 9]>
            </v:textbox>
            </v:rect>
            <![endif]-->
        </td>
    </tr>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
</table>
</body>
</html>