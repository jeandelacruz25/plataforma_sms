<div class="row no-gutters">
    <div class="col-md-12 widget-holder widget-full-content border-all mb-3">
        <div class="widget-bg">
            <div class="widget-body clearfix">
                <div class="chatbox row no-gutters" style="height: 70vh">
                    <div class="chatbox-chat-area col-sm-12">
                        <div class="chatbox-header d-flex align-items-center px-4 border-bottom">
                            <div class="media align-items-center mr-tb-30 flex-1">
                                <img class="img-responsive" src="{!! asset('img/logo-front.png?version='.date('YmdHis')) !!}" alt="">
                            </div>
                        </div>
                        <!-- /.chatbox-header -->
                        <div id="chatVue" class="chatbox-body">
                            <div class="vuebar-element" v-bar>
                                <div id="chatContainer" class="chatbox-messages">
                                    <div class="alert alert-info" v-if="listChat.length === 0">
                                        <i class="fa fa-info list-icon mr-r-10"></i> Aún no se encuentra ningun mensaje en este chat
                                    </div>
                                    <div v-else>
                                        <template v-for="(item, index) in listChat">
                                            <div v-bind:class="isUser[index]">
                                                <figure v-bind:class="isUserConnected[index]">
                                                    <a href="javascript:void(0)">
                                                        <img :src="item.users_chat.avatar + '?version=' + new Date().getTime()" class="rounded-circle">
                                                    </a>
                                                </figure>
                                                <div class="media-body">
                                                    <p v-html="item.mensaje"></p>
                                                </div>
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="formChat" class="chatbox-form p-3 border-top d-flex align-items-center pos-relative">
                            <div class="col-md-10">
                                <div class="form-group flex-1 mb-0 ml-4">
                                    <input id="messageChat" type="text" name="messageChat" class="form-control form-control-rounded pd-r-90">
                                </div>
                            </div>
                            <div class="col-md-2 mb-0 mr-4">
                                <button type="submit" class="btn btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnForm"><i class="fa fa-send list-icon mr-r-10" aria-hidden='true'></i> Enviar</button>
                                <button type="button" class="btn btn-info btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnLoad d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="alert alert-icon alert-danger border-danger formError d-none"></div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formChat.js?version='.date('YmdHis'))!!}"></script>
<script src="{!! asset('js/vue/chatVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
    hideErrorForm('.formError')
    emojiArea('#messageChat', {
        search: false,
        filtersPosition: "bottom",
        hidePickerOnBlur: true,
        inline: true,
        autocomplete: true,
        events: {
            keyup: function(editor, event) {
                if(event.which == 13){
                    event.preventDefault()
                }
            }
        }
    })
    deleteVariableSMSFunction()
</script>