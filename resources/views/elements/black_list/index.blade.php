<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="col-md-3 mr-2">
        <div class="input-group input-group-sm search">
            <input type="text" name="fechaFiltroBlackList" class="form-control dateRangeBlackList" placeholder="Escoga un rango de fechas">
            <div class="input-group-addon bg-primary" style="cursor:pointer;" onclick="serachDatatableBlackList()">
                <i class="fa fa-search"></i>
            </div>
        </div>
    </div>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div class="mr-2">
                @if(Auth::user()->authorizeRoles(['Administrador']))
                    <select class="form-control selectClientes show-tick" data-live-search="true">
                        <option value="" selected>Todos los Clientes</option>
                        @foreach($selectOptions['clientes'] as $key => $value)
                            <option value="{{ ucwords(\Illuminate\Support\Str::lower($value['razon_social'])) }}">{{ ucwords($value['razon_social']) }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
            <div class="mr-2">
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formBlackListIndividual',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Números
                </a>
            </div>
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formBlackListMasivo',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-twitter-dark px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Números Masivos
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div class="d-block loadDatatableBlackList loadlistBlackList">
        <div class="cssload-loader"></div>
    </div>
    <div class="d-none drawDatatableBlackList drawlistBlackList">
        <table id="listBlackList" class="table table-bordered centerTableImg dt-responsive nowrap" cellspacing="0" width="100%">
            <thead class="bg-primary">
                <tr>
                    <th>#</th>
                    <th>Telefono</th>
                    <th>Cliente</th>
                    <th>Fecha Registro</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadBlackListDatatable()
    })

    function loadBlackListDatatable(){
        $('.loadDatatableBlackList').removeClass('d-none').addClass('d-block')
        $('.drawDatatableBlackList').removeClass('d-block').addClass('d-none')
        dataTables('listBlackList', '/listBlackList', {}, 'GET')
    }

    function serachDatatableBlackList() {
        $('.loadDatatableBlackList').removeClass('d-none').addClass('d-block')
        $('.drawDatatableBlackList').removeClass('d-block').addClass('d-none')
        dataTables('listBlackList', '/listBlackList', {
            dateFiltro: $('input[name=fechaFiltroBlackList]').val()
        }, 'GET')
    }

    $(document).on('draw.dt', function () {
        $('.loadDatatableBlackList').removeClass('d-block').addClass('d-none')
        $('.drawDatatableBlackList').removeClass('d-none').addClass('d-block')
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().responsive.recalc()
    })

    initDateRangePicker('.dateRangeBlackList', {
        locale: {
            format: 'YYYY-MM-DD',
            customRangeLabel: 'Escoger Rango',
            separator: ' / ',
            applyLabel: 'Aplicar',
            cancelLabel: 'Cerrar',
        },
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Hace 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'center'
    })

    initSelectPicker('.selectClientes', {
        style: "badge bg-facebook px-3 heading-font-family fs-11 text-white"
    })

    filterSelectDatatable('.selectClientes','#listBlackList',2)

    vmFront.nameRoute = ucwords('{{ $titleModule }}')
    deleteVariableSMSFunction()
</script>