<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Agregar Números [Black List]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'black_list') != 0 && searchMenuUsuario(auth()->id(), 'black_list') != 0)
            <form id="formBlackList">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Números [Black List]</label>
                            <div class="fixLineHeight">
                                <input id="numeroBlackList" type="text" name="numeroBlackList" class="form-control" placeholder="Ingresa los numeros separados por comas" value="">
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->authorizeRoles(['Administrador']))
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cliente</label>
                                <select name="clienteBlackList" class="form-control selectpicker show-tick" data-live-search="true">
                                    @foreach($options['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ ucwords($value['razon_social']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <label>Opciones Adicionales</label>
                        <div class="form-group">
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input name="checkDelete" type="checkbox"> <span class="label-text">¿Deseas eliminar los números ingresados?</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#deleteNumber" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="deleteNumber">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Al activar esta opción se borrara los números ingresados, en vez de ser agregados a su lista.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info fixLineHeight">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                            <p>
                                <span>- Los números ingresados deben tener el número del pais primero (ejem : 51999999999), sin colorcar el signo <b>+</b> delante.</span><br>
                            </p>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formBlackList.js?version='.date('YmdHis')) !!}"></script>
<script>
    tagEditor('#numeroBlackList', {
        delimiter: ',',
        placeholder: 'Ingresa números separados por comas o espacios',
        animateDelete: 0
    })

    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })

    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>