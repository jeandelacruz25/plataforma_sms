<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Agregar Números Masivo [Black List]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'black_list') != 0 && searchMenuUsuario(auth()->id(), 'black_list') != 0)
            <form id="formBlackListMasivo">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Base de clientes (.csv, .xlsx, .xls)</label>
                            <input type="file" name="baseNumeros" class="baseNumeros">
                        </div>
                    </div>
                    @if(Auth::user()->authorizeRoles(['Administrador']))
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cliente</label>
                                <select name="clienteBlackList" class="form-control selectpicker show-tick" data-live-search="true">
                                    @foreach($options['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ ucwords($value['razon_social']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <label>Opciones Adicionales</label>
                        <div class="form-group">
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input name="checkDelete" type="checkbox"> <span class="label-text">¿Deseas eliminar los números subidos?</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#deleteNumber" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="deleteNumber">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Al activar esta opción se borrara los números que estan en su archivo a subir, en vez de ser agregarlos a su lista.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info fixLineHeight">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                            <div class="pull-right">
                                <a href="javascript:void(0)" onclick="downloadFile('{{ asset('examples/ejemplo_black_list_masivo.xlsx') }}')" class="btn-orange2 px-3">
                                    Descargar Ejemplo
                                </a>
                            </div>
                            <p>
                                <span>- La base debe contener el titulo : <b>telefono</b>.</span><br>
                                <span>- Los números a subir deben tener el número del pais primero (ejem : 51999999999), sin colorcar el signo <b>+</b> delante.</span><br>
                                <span>- No debes subir números duplicados (De igual manera validaremos el archivo a subir).</span><br>
                            </p>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formBlackList.js?version='.date('YmdHis')) !!}"></script>
<script>
    bootstrapFile('.baseNumeros',{
        dragdrop: true,
        text: 'Subir Archivo',
        btnClass: 'btn-facebook fs-15',
        size: 'sm',
        placehold: 'Solo archivos .csv, .xls, .xlsx',
        htmlIcon: '<span class="feather feather-upload list-icon mr-r-10"></span>&nbsp;'
    })
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>