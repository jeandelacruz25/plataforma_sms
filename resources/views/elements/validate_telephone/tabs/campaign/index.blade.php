<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formCampaignValidacion',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Campaña
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableValidacionCampanasVue">
        <div v-if="initializeValidacionCampanas">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeValidacionCampanas && dataValidacionCampanas.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeValidacionCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchValidacionCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchValidacionCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchValidacionCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table id="listCampaign" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre Campana</th>
                                <th>Estado Envio</th>
                                <th>N° Tel</th>
                                <th>Porcentaje de Envio</th>
                                <th>Inicio Validación</th>
                                <th>Fin Validación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="8">
                                    No hay ninguna campaña de validación creada actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeValidacionCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchValidacionCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchValidacionCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchValidacionCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table id="listCampaign" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                        <tr>
                            <th>#</th>
                            <th>Nombre Campana</th>
                            <th>Estado Envio</th>
                            <th>N° Tel</th>
                            <th>Porcentaje de Envio</th>
                            <th>Inicio Validación</th>
                            <th>Fin Validación</th>
                            <th data-priority="1">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <template v-for="(dataValidacionCampana, index) in dataValidacionCampanas">
                            <tr>
                                <td v-html="pagination.from + index"></td>
                                <td v-html="nameCampana[index]"></td>
                                <td v-html="statusSend[index]"></td>
                                <td v-html="dataValidacionCampana.total_count"></td>
                                <td v-html="percentageValidation[index]"></td>
                                <td v-html="inicioValidacion[index]"></td>
                                <td v-html="finValidacion[index]"></td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <template v-if="parseInt(dataValidacionCampana.estado_envio) === 0 && parseInt(dataValidacionCampana.id_estado) === 1">
                                            <button type="button" @click="clickEvent('div.dialogScore','formCampaignValidacion', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Editar Campaña" class="btn btn-orange2 btn-sm"><i class="fa fa-edit"></i></button>
                                        </template>
                                        <template v-if="parseInt(dataValidacionCampana.estado_envio) === 0 && parseInt(dataValidacionCampana.id_estado) === 1">
                                            <button type="button" @click="clickEvent('div.dialogScore','formCampaignValidacionUpload', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Subir Base de Números" class="btn btn-yellow2 btn-sm"><i class="fa fa-upload"></i></button>
                                        </template>
                                        <button type="button" @click="clickEvent('div.dialogScoreLarge','formCampaignValidacionUsers', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Ver Base de Números" class="btn btn-cian btn-sm" ><i class="fa fa-group"></i></button>
                                        <template v-if="parseInt(dataValidacionCampana.total_telefonos) != 0">
                                            <button type="button" @click="clickEvent('div.dialogScore','miniDashboardValidacionTelefono', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Mini Dashboard" class="btn btn-green-aqua btn-sm"><i class="fa fa-pie-chart"></i></button>
                                        </template>
                                        <template v-if="parseInt(dataValidacionCampana.total_telefonos) != 0">
                                            <button type="button" @click="clickDownload('/downloadReportCampaignValidacion', { idCampana: dataValidacionCampana.id })" title="Descargar Reporte" class="btn btn-green2 btn-sm" ><i class="fa fa-file-excel-o"></i></button>
                                        </template>
                                        <template v-if="parseInt(dataValidacionCampana.estado_envio) === 0 && parseInt(dataValidacionCampana.id_estado) === 1 && parseInt(dataValidacionCampana.total_telefonos) != 0">
                                            <button type="button" @click="clickEvent('div.dialogScore', 'formValidacionStart', { idCampana: dataValidacionCampana.id, reStart: false })" data-toggle="modal" data-target="#modalScore" title="Iniciar Validación" class="btn btn-purple-dark btn-sm"><i class="fa fa-play"></i></button>
                                        </template>
                                        <template v-if="parseInt(dataValidacionCampana.estado_envio) === 4 && parseInt(dataValidacionCampana.id_estado) === 1 && parseInt(dataValidacionCampana.total_telefonos) != 0">
                                            <button type="button" @click="clickEvent('div.dialogScore', 'formValidacionStart', { idCampana: dataValidacionCampana.id, reStart: true })" data-toggle="modal" data-target="#modalScore" title="Renaudar Validación" class="btn btn-purple-dark btn-sm"><i class="fa fa-play"></i></button>
                                        </template>
                                        <template v-if="parseInt(dataValidacionCampana.estado_envio) === 3 && parseInt(dataValidacionCampana.id_estado) === 1 && parseInt(dataValidacionCampana.total_telefonos) != 0">
                                            <button type="button" @click="clickEvent('div.dialogScore','formValidacionPause', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Pausar Validación" class="btn btn-rose2 btn-sm"><i class="fa fa-pause"></i></button>
                                        </template>
                                        <button type="button" @click="clickEvent('div.dialogScore','formCampaignValidacionStatus', dataValidacionCampana.id)" data-toggle="modal" data-target="#modalScore" title="Cambiar Estado" class="btn btn-gray2 btn-sm"><i class="fa fa-refresh"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeValidacionCampanas && dataValidacionCampanas.length > 0 || !initializeValidacionCampanas ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchValidacionCampanas()"></pagination>
            </div>
        </div>
    </div>
</div>

<script>
    function loadCampanaValidationDatatable(){
        vmDataTableValidacionCampanas.fetchValidacionCampanas()
        vmFront.nameRoute = ucwords('{{ $titleCampaignModule }}')
    }
</script>