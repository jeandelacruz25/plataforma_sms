<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Mini Dashboard [ {{ $dataCampaign[0]['nombre_campana'] }} ]</h5>
    </div>
    <div class="modal-body">
        <div class="row">
            @if($dataCliente[0]['score_async'] == 1)
                <div class="col-md-12">
                    <div class="chartHTML"></div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="chartTable fs-10"></div>
                </div>
            @else
                <div class="col-md-6">
                    <div class="chartHTML"></div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="chartTable fs-11"></div>
                </div>
            @endif
        </div>
        @if($dataCliente[0]['score_async'] == 1)
            @include('elements.validate_telephone.tabs.campaign.chart.gestionar')
        @else
            <div class="text-center mt-3">
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        @endif
    </div>
</div>
<div class="chartScript"></div>

@if($dataCliente[0]['score_async'] == 1)
    <script src="{!! asset('js/vue/utilsVue/miniDashboardTelefono.js?version='.date('YmdHis')) !!}"></script>
    <script>
        vmMiniDashboardTelefono.idCampaign = '{{ $dataCampaign[0]['id'] }}'
        vmMiniDashboardTelefono.idCliente = '{{ $dataCliente[0]['id'] }}'
    </script>
@endif

<script>
    clearModalClose('modalScore', 'div.dialogScore')
    $(document).ready(function() {
        axiosChart('post', '/getDataMiniDashboardTelefono', {
            valueID: '{{ $dataCampaign[0]['id'] }}'
        })
    })
</script>