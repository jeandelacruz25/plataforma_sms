<div id="miniDashboardTelefonoVue">
    <template v-if="initFormGenerar">
        <div class="row border_fix border-primary">
            <div class="container">
                <template v-if="loadForm">
                    <div class="col-md mt-2">
                        <div class="cssload-loader"></div>
                    </div>
                </template>
                <template v-else>
                    <div class="col-md mt-2">
                        <template v-if="initFormGenerarMarcadores">
                            <template v-if="selectProveedores.length > 0">
                                <div class="mt-2">
                                    <div class="form-group">
                                        <label>Proveedores</label>
                                        <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedProveedor" @change="changeCarteraMarcadores()">
                                            <option disabled selected>Seleccione un proveedor</option>
                                            <template v-for="(item, index) in selectProveedores">
                                                <option v-html="item.proveedor"></option>
                                            </template>
                                        </select>
                                    </div>
                                    <template v-if="initFormCarteraMarcadores">
                                        <div class="form-group">
                                            <label>Cartera</label>
                                            <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedCartera">
                                                <option disabled selected>Seleccione una cartera</option>
                                                <template v-for="(item, index) in selectCartera">
                                                    <option v-bind:data-content="item.cartera" v-html="item.id_cartera"></option>
                                                </template>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nombre Marcador</label>
                                            <input type="text" name="nameCampaign" class="form-control" placeholder="Ingrese un nombre de un marcador" v-model="nameCartera">
                                        </div>
                                        <div class="form-group">
                                            <label>Tipo Marcador</label>
                                            <select class="form-control selectGenerar show-tick" v-model="selectedMarcador" @change="selectPicker()">
                                                <option disabled selected>Seleccione el tipo de marcador</option>
                                                <option value="2">Predictivo</option>
                                                <option value="3">Progresivo</option>
                                            </select>
                                        </div>
                                        <template v-if="selectedMarcador === '3'">
                                            <div class="form-group">
                                                <label>Tipo Bolsa</label>
                                                <select class="form-control selectGenerar show-tick" v-model="selectedBolsa">
                                                    <option value="1" selected>Bolsa Única</option>
                                                    <option value="2">Bolsa por Usuario</option>
                                                </select>
                                            </div>
                                        </template>
                                        <template v-else-if="selectedMarcador === '2'">
                                            <div class="form-group">
                                                <label>Tipo Bolsa</label>
                                                <select class="form-control selectGenerar show-tick" v-model="selectedBolsa">
                                                    <option value="1" selected>Bolsa Única</option>
                                                </select>
                                            </div>
                                        </template>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-primary btnForm ripple" @click="generarMarcador()"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Generar</button>
                                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                            <button type="button" class="btn btn-default ripple" @click="clearFormGenerarMarcadores()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                        </div>
                                        <div class="clearfix mt-2"></div>
                                    </template>
                                </div>
                            </template>
                            <template v-else>
                                <div class="mt-2">
                                    <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>El cliente asociado no cuenta con data del score.</strong>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default ripple" @click="clearFormGenerarMarcadores()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                    </div>
                                    <div class="clearfix mt-2"></div>
                                </div>
                            </template>
                        </template>
                        <template v-if="initFormGenerarGestiones">
                            <template v-if="selectProveedores.length > 0">
                                <div class="mt-2">
                                    <div class="form-group">
                                        <label>Proovedores</label>
                                        <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedProveedor" @change="changeCartera()">
                                            <option disabled selected>Seleccione un proveedor</option>
                                            <template v-for="(item, index) in selectProveedores">
                                                <option v-html="item.proveedor"></option>
                                            </template>
                                        </select>
                                    </div>
                                    <template v-if="initFormCartera">
                                        <div class="form-group">
                                            <label>Cartera</label>
                                            <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedCartera">
                                                <option disabled selected>Seleccione una cartera</option>
                                                <template v-for="(item, index) in selectCartera">
                                                    <option v-bind:data-content="item.cartera" v-html="item.id_cartera"></option>
                                                </template>
                                            </select>
                                        </div>
                                    </template>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-primary btnForm ripple" @click="generarGestion()"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Generar</button>
                                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                        <button type="button" class="btn btn-default ripple" @click="clearFormGenerarGestiones()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                    </div>
                                    <div class="clearfix mt-2"></div>
                                </div>
                            </template>
                            <template v-else>
                                <div class="mt-2">
                                    <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>El cliente asociado no cuenta con data del score.</strong>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default ripple" @click="clearFormGenerarGestiones()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                    </div>
                                    <div class="clearfix mt-2"></div>
                                </div>
                            </template>
                        </template>
                        <template v-if="initFormGenerarInactivos">
                            <div class="mt-2">
                                <div class="text-center">
                                    <div class="form-group">
                                        <label class="text-center">¿ Deseas inactivar estos telefonos ?</label>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-success btnForm ripple" @click="generarInactivos()"><i class='feather feather-check list-icon mr-r-10' aria-hidden='true'></i> Si</button>
                                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                    <button type="button" class="btn btn-default ripple" @click="clearFormGenerarGestiones()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No</button>
                                </div>
                                <div class="clearfix mt-2"></div>
                            </div>
                        </template>
                    </div>
                </template>
            </div>
        </div>
    </template>
    <template v-else>
        <div class="text-center mt-3">
            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
        </div>
    </template>
</div>