<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Iniciar Validación [{{ $dataCampaign[0]['nombre_campana'] }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formCampaignPlay">
            @if(\Carbon\Carbon::now()->timestamp >= \Carbon\Carbon::parse($inicioMasivo)->timestamp && \Carbon\Carbon::now()->timestamp < \Carbon\Carbon::parse($finMasivo)->timestamp)
                @if($countCampaign == 0)
                    @if($countCanales > 0)
                        <div class="form-group text-center">
                            <label class="pb-2">¿Estás seguro de iniciar la validación?</label>
                        </div>
                    @else
                        <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                            <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>No cuentas con canales disponibles en tu suscripción.</strong>
                        </div>
                    @endif
                @else
                    <div class="alert alert-icon alert-danger fs-12 fixLineHeight">
                        <i class="feather feather-alert-circle list-icon mr-r-10"></i>
                        <strong>Hay una validación en curso, favor de esperar que termine para volver a iniciar otra validación.</strong>
                        <p class="font-weight-bold">* Campaña iniciada por el usuario : {{ $dataCampaign[0]['user']['nombres'].' '.$dataCampaign[0]['user']['apellidos'] }}</p>
                    </div>
                @endif
                <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
                <input type="hidden" name="numCanales" value="{{ $countCanales }}">
                <input type="hidden" name="reStart" value="{{ $reStart }}">
                <div class="text-center">
                    @if($countCampaign == 0 && $countCanales > 0)
                        <button type="submit" class="btn btn-success btnForm ripple"><i class='feather feather-play list-icon mr-r-10' aria-hidden='true'></i> Iniciar</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    @endif
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            @else
                <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                    <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>Estas fuera del horario de validación, recuerda que esta acción la puedes realizar desde las {{ \Carbon\Carbon::parse($inicioMasivo)->format('h:i a') }} hasta las {{ \Carbon\Carbon::parse($finMasivo)->format('h:i a') }} (GMT -5).</strong>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            @endif
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampanaValidacion.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker')
</script>