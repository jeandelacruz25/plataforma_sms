<!-- Modal content-->
<div id="campaignValidacionVue">
    <div class="modal-content">
        <div class="modal-header text-inverse bg-primary">
            <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
            <h5 class="modal-title">Base de la Campaña [{{ ucwords(\Illuminate\Support\Str::lower($dataCampaign[0]['nombre_campana'])) }}]</h5>
        </div>
        <div class="modal-body">
            <div v-if="initializeCampaignValidacionBase">
                <div class="col-md-12">
                    <div class="cssload-loader"></div>
                </div>
            </div>
            <template v-else-if="!initializeCampaignValidacionBase && dataCampaignValidacionBase.length === 0 && !initDataPagination">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-warning fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No hay ninguna base subida para esta campaña</strong>
                    </div>
                </div>
            </template>
            <template v-else-if="initDataPagination">
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseCampaignValidacion()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-warning fixLineHeight">
                            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No se encontro resultados en tu busqueda, ingrese un nuevo número</strong>
                        </div>
                    </div>
                </div>
            </template>
            <template v-else>
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseCampaignValidacion()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                        <table id="table_base_sms" class="table table-bordered centerTableImg nowrap" cellspacing="0" width="100%">
                            <template>
                                <thead>
                                    <tr class="thead-inverse bg-primary">
                                        <th>Número Validado</th>
                                        <th>Número Documento</th>
                                        <th>Fecha Validación</th>
                                        <th>Estado Validación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-for="(baseCampaign, index) in dataCampaignValidacionBase">
                                        <tr class="text-dark">
                                            <td v-html="baseCampaign.telefono"></td>
                                            <td class="text-center" v-html="baseCampaign.num_identidad"></td>
                                            <td v-html="formatDateTime[index]"></td>
                                            <td v-html="statusValidacion[index]"></td>
                                        </tr>
                                    </template>
                                </tbody>
                            </template>
                        </table>
                    </div>
                </div>
            </template>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeCampaignValidacionBase && dataCampaignValidacionBase.length > 0 || !initializeCampaignValidacionBase ? '' : 'disabled'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchBaseCampaignValidacion()"></pagination>
                </div>
            </div>
            <div class="mt-4 chatWindow"></div>
            <div class="text-center mr-b-30 mt-3">
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/utilsVue/campaignValidacionVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmCampaignValidacionBase.id_base = '{{ $dataCampaign[0]['id'] }}'
    vmCampaignValidacionBase.fetchBaseCampaignValidacion()
    clearModalClose('modalScore', 'div.dialogScoreLarge')
</script>