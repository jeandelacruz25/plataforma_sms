<div class="widget-body plr0 pt0">
    <div class="tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link tabCampañasValidacion" href="#campañasValidacion" data-toggle="tab" aria-expanded="true" onclick="loadCampanaValidationDatatable()">
                    <span class="thumb-xxs">
                        <i class="list-icon feather feather-layers"></i> Campañas Validación
                    </span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="campañasValidacion">
                @include('elements.validate_telephone.tabs.campaign.index', ['titleModule' => $titleCampaignModule, 'iconModule' => $iconCampaignModule])
            </div>
        </div>
    </div>
</div>

<script src="{!! asset('js/vue/dataTables/dataTableCampanasValidacionVue.js?version='.date('YmdHis')) !!}"></script>

<script>
    $(document).ready(function() {
        $('.tabCampañasValidacion').click()
    })

    deleteVariableSMSFunction()
</script>