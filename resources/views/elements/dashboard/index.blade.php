<div class="widget-body plr0 pt0">
    <div class="tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link tabDashboardBases" href="#bases" data-toggle="tab" onclick="basesDashboard()">
                    <span class="thumb-xxs">
                        <i class="list-icon icon icon-users"></i> Bases
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#validacion_telefono" data-toggle="tab" onclick="validatePhoneDashboard()">
                    <span class="thumb-xxs">
                        <i class="list-icon icon icon-phone-wave"></i> Validación de Telefonos
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#sms" data-toggle="tab" onclick="smsDashboard()">
                    <span class="thumb-xxs">
                        <i class="list-icon icon icon-bubble-lines3"></i> Mensajeria SMS
                    </span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="bases">
                @if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
                    @include('elements.dashboard.tabs.dashboard_bases')
                @else
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info border-info fixLineHeight" role="alert">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                        </div>
                    </div>
                    <script>
                        function basesDashboard(){ }
                    </script>
                @endif
            </div>
            <div class="tab-pane" id="validacion_telefono">
                @if(searchMenuCliente(auth()->user()->id_cliente, 'validacion_telefonos') != 0 && searchMenuUsuario(auth()->id(), 'validacion_telefonos') != 0)
                    @include('elements.dashboard.tabs.dashboard_validate_phone')
                @else
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info border-info fixLineHeight" role="alert">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                        </div>
                    </div>
                    <script>
                        function validatePhoneDashboard(){ }
                    </script>
                @endif
            </div>
            <div class="tab-pane" id="sms">
                @if(searchMenuCliente(auth()->user()->id_cliente, 'mensajeria_sms') != 0 && searchMenuUsuario(auth()->id(), 'mensajeria_sms') != 0)
                    @include('elements.dashboard.tabs.dashboard_sms')
                @else
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info border-info fixLineHeight" role="alert">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                        </div>
                    </div>
                    <script>
                        function smsDashboard(){ }
                    </script>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    vmFront.nameRoute = ucwords(`Dashboard - {{ monthActual() }}`)
    deleteVariableSMSFunction()

    $(document).ready(function() {
        $('.tabDashboardBases').click()
    })
</script>