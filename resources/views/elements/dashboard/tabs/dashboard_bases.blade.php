<div id="dashboardBasesVue">
    <div class="widget-list row">
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-cian">
                    <div class="widget-title">
                        <span>Usuarios Habilitados</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.usuarios" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-gray2">
                    <div class="widget-title">
                        <span>Campañas Creadas</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.campanas" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-orange2">
                    <div class="widget-title">
                        <span>Campañas sin Cruzar</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.campanassincruzar" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-green2">
                    <div class="widget-title">
                        <span>Doc. Buscados</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.documentosbuscados" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-yellow2">
                    <div class="widget-title">
                        <span>Doc. Encontrados</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.documentosencontrados" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-purple2">
                    <div class="widget-title">
                        <span>Efectividad</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.efectividadbusqueda" :duration=4000></count-to> %
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
                <span class="widget-title my-0 color-white fs-15 fw-600">Servicio Bases [ {{ monthActual() }} ]</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-bar-chart text-score"></i> Total de Encontrados (Diario)</span>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardTotalEncontradosChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-search text-score"></i> Diferenciando Buscados y Encontrados (Diario)</span>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardDiferenciadoChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
            <span class="widget-title my-0 color-white fs-15 fw-600">Cruces Mensuales</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-body border_fix">
                            <div id="dashboardMonthCrucesChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dashboardTotalEncontradosChartJS"></div>
<div id="dashboardDiferenciadoChartJS"></div>
<div id="dashboardMonthCrucesCharJS"></div>

<script src="{!! asset('js/vue/dashboard/dashboardBasesVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    function basesDashboard(){
        vmDashboardBases.loadDashboardBases()
    }
</script>