<div id="dashboardValidatePhoneVue">
    <div class="widget-list row">
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-cian">
                    <div class="widget-title">
                        <span>Campañas Creadas</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.campanas" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-gray2">
                    <div class="widget-title">
                        <span>Campañas sin Lanzar</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.campanassinlanzar" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-orange2">
                    <div class="widget-title">
                        <span>Números Validados</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.validados" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-green2">
                    <div class="widget-title">
                        <span>Prendido</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.prendido" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-rose2">
                    <div class="widget-title">
                        <span>Buzón de Voz</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.buzon" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-purple2">
                    <div class="widget-title">
                        <span>Inexistentes</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.nocontesta" :duration=4000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
            <span class="widget-title my-0 color-white fs-15 fw-600">Servicio Validación de Telefonos [ {{ monthActual() }} ]</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-phone-square text-score"></i> Resultado de Validaciones (Diario)</span>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardValidateDayChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-phone text-score"></i> Números Validados (Diario)</span>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardNumbersValidateChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
            <span class="widget-title my-0 color-white fs-15 fw-600">Validaciones Mensuales</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-body border_fix">
                            <div id="dashboardMonthValidateChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dashboardValidateDayCharJS"></div>
<div id="dashboardNumbersValidateCharJS"></div>
<div id="dashboardMonthValidateCharJS"></div>

<script src="{!! asset('js/vue/dashboard/dashboardValidatePhoneVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    function validatePhoneDashboard(){
        vmDashboardValidatePhone.loadDashboardValidatePhone()
    }
</script>