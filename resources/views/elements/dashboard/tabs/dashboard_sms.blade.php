<div id="dashboardSMSVue">
    <div class="widget-list row">
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-cian">
                    <div class="widget-title">
                        <!--<i class="fa fa-circle fa-fw" style="color: #868e96 !important"></i>-->
                        <span>Sin Enviar</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.sinEnviar" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-yellow2">
                    <div class="widget-title">
                        <span>Pendiente</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.pendiente" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-purple2">
                    <div class="widget-title">
                        <span>No Entregado</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.noEntregado" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-green2">
                    <div class="widget-title">
                        <span>Entregado</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.entregado" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-orange2">
                    <div class="widget-title">
                        <span>Expirado</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.expirado" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-sm widget-border-radius col-md-2">
            <div class="widget-bg">
                <div class="widget-heading widget-dashboard btn-rose2">
                    <div class="widget-title">
                        <span>Rechazado</span>
                    </div>
                </div>
                <div class="widget-body border-top-0 border_fix paddingFix">
                    <div class="counter-w-info">
                        <div class="counter-title color-color-scheme">
                            <span class="counter">
                                <count-to :start-val="0" :end-val="boxCount.rechazado" :duration=2000></count-to>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
            <span class="widget-title my-0 color-white fs-15 fw-600">Servicio SMS [ {{ monthActual() }} ]</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-envelope text-score"></i> Mensajes Enviados</span>
                            <div class="widget-graph-info">
                                <span>
                                    <span class="ml-1 my-0 fw-500">Total de Mensajes : </span>
                                    <span class="color-color-scheme fw-600">
                                        <count-to :start-val="0" :end-val="monthSend.totalSMS" :duration=2000></count-to>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardSendChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="widget-heading border_fix">
                            <span class="widget-title fs-15 text-gray-700"><i class="fa fa-envelope-open text-score"></i> Mensajes Recibidos</span>
                            <div class="widget-graph-info">
                                <span>
                                    <span class="ml-1 my-0 fw-500">Total de Mensajes : </span>
                                    <span class="color-color-scheme fw-600">
                                        <count-to :start-val="0" :end-val="monthReceived.totalSMS" :duration=2000></count-to>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="widget-body border_fix">
                            <div id="dashboardReceivedChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-holder widget-sm widget-border-radius border_fix mt-3">
        <div class="widget-heading bg-score px-3 py-1">
            <span class="widget-title my-0 color-white fs-15 fw-600">Consumo Mensual</span>
        </div>
        <div class="widget-body bg-white border-top-0">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-body border_fix">
                            <div id="dashboardMonthChartHTML">
                                <div class="cssload-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dashboardSendCharJS"></div>
<div id="dashboardReceivedCharJS"></div>
<div id="dashboardMonthCharJS"></div>

<script src="{!! asset('js/vue/dashboard/dashboardSMSVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    function smsDashboard(){
        vmDashboardSMS.loadDashboardSMS()
    }
</script>