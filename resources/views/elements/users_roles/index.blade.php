<div class="col-md-12">
    <div class="widget-body plr0 pt0">
        <div class="tabs">
            <ul class="nav nav-tabs fixLineHeight">
                <li class="nav-item">
                    <a class="nav-link tabUsuarios" href="#Usuarios" data-toggle="tab" aria-expanded="true" onclick="loadUserDatatable()">
                        <span class="thumb-xxs">
                            <i class="list-icon feather feather-users"></i> Usuarios
                        </span>
                    </a>
                </li>
                @if(auth()->user()->authorizeRoles(['Administrador']))
                    <li class="nav-item">
                        <a class="nav-link tabPerfiles" href="#Perfiles" data-toggle="tab" aria-expanded="true" onclick="loadRoleDatatable()">
                            <span class="thumb-xxs">
                                <i class="list-icon fa fa-address-card-o"></i> Perfiles
                            </span>
                        </a>
                    </li>
                @endif
            </ul>
            <div class="tab-content plr0">
                <div class="tab-pane" id="Usuarios">
                    @include('elements.users.index', ['titleModule' => $titleUsersModule, 'iconModule' => $iconUsersModule])
                </div>
                @if(auth()->user()->authorizeRoles(['Administrador']))
                    <div class="tab-pane" id="Perfiles">
                        @include('elements.roles.index', ['titleModule' => $titleRolesModule, 'iconModule' => $iconRolesModule])
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/dataTables/dataTableUsuariosVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/dataTables/dataTablePerfilesVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    $(document).ready(function() {
        $('.tabUsuarios').click()
    })

    function loadUserDatatable(){
        vmDataTableUsuarios.fetchUsuarios()
        vmFront.nameRoute = ucwords('{{ $titleUsersModule }}')
    }

    function loadRoleDatatable(){
        vmDataTablePerfiles.fetchPerfiles()
        vmFront.nameRoute = ucwords('{{ $titleRolesModule }}')
    }

    deleteVariableSMSFunction()
</script>