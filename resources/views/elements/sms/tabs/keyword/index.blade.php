<div class="widget-body plr0">
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link tabKeywords" href="#list_keyword" data-toggle="tab" aria-expanded="true" onclick="loadKeywordDatatable()">
                    <span class="thumb-xxs">
                        <i class="list-icon fa fa-check-square-o"></i> Keywords
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link tabSendKeyword" href="#send_keyword" data-toggle="tab" aria-expanded="true" onclick="loadEnvioSMSKeywordDatatable()">
                    <span class="thumb-xxs">
                        <i class="list-icon fa fa-paper-plane"></i> Envios Keywords
                    </span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="list_keyword">
                @include('elements.sms.tabs.keyword.tabs.keyword_list', ['titleModule' => 'Lista de Keywords', 'iconModule' => 'fa fa-list-ul'])
            </div>
            <div class="tab-pane" id="send_keyword">
                @include('elements.sms.tabs.keyword.tabs.keyword_send', ['titleModule' => 'Envios Keywords', 'iconModule' => 'fa fa-list-ul'])
            </div>
        </div>
    </div>
</div>
