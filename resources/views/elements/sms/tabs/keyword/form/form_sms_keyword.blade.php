<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Keyword</h5>
    </div>
    <div class="modal-body">
        <form id="formKeyword">
            <div class="row">
                @if(!Auth::user()->authorizeRoles(['Administrador']) && $updateForm === false || (Auth::user()->authorizeRoles(['Administrador'])))
                    <div class="@if(Auth::user()->authorizeRoles(['Administrador'])) @if($updateForm === false) col-md-6 @else col-md-12 @endif @else col-md-12 @endif">
                        <div class="form-group">
                            <label>Keyword</label>
                            <div class="input-group"> @if($updateForm === false)<span class="input-group-addon textPalabraClave">@if(!Auth::user()->authorizeRoles(['Administrador'])) {{ auth()->user()->clientes->palabra_clave }} @else - @endif</span>@endif
                                <input type="text" name="nameKeyword" class="form-control" placeholder="Ingrese el Keyword a Crear" value="{{ $dataKeyword ? $dataKeyword[0]['nombre_keyword'] : '' }}">
                            </div>
                        </div>
                    </div>
                @endif
                @if(Auth::user()->authorizeRoles(['Administrador']) && $updateForm === false)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cliente</label>
                            <select name="clienteKeyword" class="form-control selectpicker show-tick clienteKeyword" data-live-search="true">
                                <option data-id="-" value="">Seleccione el cliente</option>
                                @foreach($options['clientes'] as $key => $value)
                                    <option data-id="{{ $value['palabra_clave'] }}" value="{{ $value['id'] }}">{{ ucwords($value['razon_social']) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <label>Opciones Adicionales</label>
                    <div class="form-group">
                        <div class="checkbox checkbox-primary">
                            <label class="checkbox-checked">
                                <input name="statusError" type="checkbox" @if($dataKeyword) {{ $dataKeyword[0]['estado_error'] == 1 ? 'checked' : '' }} @endif> <span class="label-text">Activar Mensaje de Error</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#mensajeError" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            </label>
                        </div>
                    </div>
                    <div class="d-none" id="mensajeError">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Recuerda que al activar esta función cada vez que se recepcione un filtro incorrecto con el Keyword, se devolvera un SMS indicando el mensaje de error que ingresara, este mismo tendra un costo por nuevo SMS.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Mensaje de Error</label>
                        <input type="text" name="mensajeError" class="form-control" placeholder="Ingresa un mensaje que sera el que retorne cuando se envie incorrectamente el keyword" value="{{ $dataKeyword ? $dataKeyword[0]['mensaje_error'] : '' }}">
                    </div>
                </div>
            </div>
            @if($updateForm === false)
                <div class="alert alert-icon alert-info fixLineHeight">
                    <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong><br>
                    <ul class="pl-0 fs-12">
                        <li>Al crear un keyword se notificara al correo de <b>support@securitec.pe</b>.</li>
                        <li>Se debe esperar a que el estado del servicio sea <span class="badge px-3 securitec bg-success">Confirmado</span>, para subir la base de sms.</li>
                        <li>El tiempo de activación es de 24 a 36 horas.</li>
                    </ul>
                </div>
            @endif
            <input type="hidden" name="keywordID" value="{{ $dataKeyword ? $dataKeyword[0]['id'] : '' }}">
            <input type="hidden" name="clienteID" value="{{ $dataKeyword ? $dataKeyword[0]['id_cliente'] : '' }}">
            <input type="hidden" name="statusID" value="{{ $dataKeyword ? $dataKeyword[0]['id_estado'] : ''}}">
            <input type="hidden" name="statusService" value="{{ $dataKeyword ? $dataKeyword[0]['estado_servicio'] : ''}}">
            <input type="hidden" name="userCreated" value="{{ $dataKeyword ? $dataKeyword[0]['user_created'] : ''}}">
            <input type="hidden" name="userUpdated" value="{{ $dataKeyword ? $dataKeyword[0]['user_updated'] : ''}}">
            @if(!Auth::user()->authorizeRoles(['Administrador'])  && $updateForm == true)
                <input type="hidden" name="nameKeyword" value="{{ $dataKeyword[0]['nombre_keyword'] }}">
            @endif
            <div class="text-center">
                <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formKeyword.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
    SelectChangeData('select.clienteKeyword', 'id', '.textPalabraClave')
</script>