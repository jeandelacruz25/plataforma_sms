<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Enviar SMS Keyword</h5>
    </div>
    <div class="modal-body">
        <form id="formSMSKeyword">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Base de clientes (.csv, .xlsx, .xls)</label>
                        <input type="file" name="baseClientes" class="baseClientes">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Escoge tu Keyword</label>
                        <select name="selectKeyword" class="form-control selectpicker show-tick" data-live-search="true">
                            <option value="">Seleccione un keyword</option>
                            @foreach($dataKeyword as $key => $value)
                                <option value="{{ $value['id'] }}">{{ $value['nombre_keyword'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <label>Opciones Adicionales</label>
                    <div class="form-group ">
                        <div class="checkbox checkbox-primary">
                            <label class="checkbox-checked">
                                <input name="flashSMS" type="checkbox"> <span class="label-text">Activar SMS Flash</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#flashSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            </label>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-top: -10px !important;">
                        <span class="col-md-5 col-form-label label-text">Gestionar Fecha de Envio &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#dateSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a></span>
                        <div class="col-md-7">
                            <input type="text" name="dateEnvio" readonly="readonly" class="form-control form-control-sm txt-calendar-clock dateEnvio" placeholder="{{ \Carbon\Carbon::now() }}" value="">
                        </div>
                    </div>
                </div>
                <div class="d-none" id="flashSMS">
                    <div class="popover-body">
                        <div>
                            <label>Información</label>
                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                        </div>
                        <p>Los SMS flash aparecen directamente en la pantalla principal sin interacción del usuario y no se almacenan automáticamente en la bandeja de SMS (en algunos modelos de celulares si podra dejar guardar dicho SMS). El usuario tiene la opción de guardar estos mensajes, con la ID apropiada del emisor, si decide hacerlo, pero no es automático.</p>
                        <p><img src="{{ asset('img/pop-over/smsFlash.png') }}"></p>
                    </div>
                </div>
                <div class="d-none" id="dateSMS">
                    <div class="popover-body">
                        <div>
                            <label>Información</label>
                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                        </div>
                        <p>Puedes gestionar la fecha y la hora en la que sera recepcionado por el usuario, recuerda ingresar correctamente la fecha y la hora de envio.</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-icon alert-info fixLineHeight">
                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                        <div class="pull-right">
                            <a href="javascript:void(0)" onclick="downloadFile('{{ asset('examples/ejemplo_base_keyword.xlsx') }}')" class="btn-orange2 px-3">
                                Descargar Ejemplo
                            </a>
                        </div>
                        <p class="fs-12">
                            <span>- La base debe contener el titulo : <b>telefono, mensaje, filtro, respuesta</b>.</span><br>
                            <span>- Los números a subir deben tener el número del pais primero (ejem : 51999999999, sin colorcar el signo <b>+</b> delante.</span><br>
                            <span>- No debes subir números duplicados (Recuerda que esto sera un costo de igual manera).</span><br>
                            <span>- Si no colocaras un filtro lo recomendable es dejarle un guión.</span><br>
                            <span>- El envio tiene un promedio de envio 40 SMS x cada 1 segundo.</span><br>
                            <span>- El estado de los SMS enviados, pueden tomar su tiempo ya que esto es lo que demore en procesar la información el operador (Claro, Movistar, etc).</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btnForm ripple"><i class='fa fa-send list-icon mr-r-10' aria-hidden='true'></i> Enviar</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No Enviar</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formSMS.js?version='.date('YmdHis')) !!}"></script>
<script>
    bootstrapFile('.baseClientes',{
        dragdrop: true,
        text: 'Subir Archivo',
        btnClass: 'btn-facebook fs-15',
        size: 'sm',
        placehold: 'Solo archivos .csv',
        htmlIcon: '<span class="feather feather-upload list-icon mr-r-10"></span>&nbsp;'
    })

    initDateRangePicker('.dateEnvio', {
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss',
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar"
        },
        startDate: moment(),
        opens: 'left'
    })

    $('.dateEnvio').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'))
    })

    $('.dateEnvio').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('')
    })

    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })

    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')

</script>