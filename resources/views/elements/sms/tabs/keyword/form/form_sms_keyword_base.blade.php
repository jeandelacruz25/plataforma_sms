<!-- Modal content-->
<div id="keywordUserVue">
    <div class="modal-content">
        <div class="modal-header text-inverse bg-primary">
            <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal">&times;</button>
            <h5 class="modal-title">Detalle de Envio [{{ letterMin($dataBaseKeyword[0]['id_bulk_sms']) }}]</h5>
        </div>
        <div class="modal-body">
            <div v-if="initializeKeywordBase">
                <div class="col-md-12">
                    <div class="cssload-loader"></div>
                </div>
            </div>
            <template v-else-if="!initializeKeywordBase && dataKeywordBase.length === 0 && !initDataPagination">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-warning fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No hay ninguna base subida para esta campaña</strong>
                    </div>
                </div>
            </template>
            <template v-else-if="initDataPagination">
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseKeyword()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-warning fixLineHeight">
                            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No se encontro resultados en tu busqueda, ingrese un nuevo número</strong>
                        </div>
                    </div>
                </div>
            </template>
            <template v-else>
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseKeyword()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                        <table id="table_base_bulk" class="table table-bordered centerTableImg nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr class="thead-inverse bg-primary">
                                <th>Número Enviado</th>
                                <th>Prov.</th>
                                <th>Mensaje Enviado</th>
                                <th>Fecha Recepción</th>
                                <th>Estado SMS</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <template v-for="(baseKeyword, index) in dataKeywordBase">
                                <tr class="text-dark">
                                    <td v-html="countrySMS[index] + ' ' + baseKeyword.telefono_usuario"></td>
                                    <td class="text-center" v-html="mobileNetworkSMS[index]"></td>
                                    <td v-html="messageSMS[index]"></td>
                                    <td v-html="formatDateTime[index]"></td>
                                    <td v-html="statusSMS[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-cian btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseKeyword.telefono_usuario" data-popover="popover" data-trigger="focus" title="Información de Estado">
                                                <i class="list-icon fa fa-question-circle-o"></i>
                                            </button>
                                            <button type="button" class="btn btn-yellow2 btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseKeyword.telefono_usuario + '_'" data-popover="popover" data-trigger="focus" title="Detalle de Estado">
                                                <i class="list-icon fa fa-exclamation-circle"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <div class="d-none" v-bind:id="baseKeyword.telefono_usuario">
                                    <div class="popover-body">
                                        <div>
                                            <label>Información Estado</label>
                                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                        </div>
                                        <span v-html="conceptStatusSMS[index]"></span>
                                    </div>
                                </div>
                                <div class="d-none" v-bind:id="baseKeyword.telefono_usuario + '_'">
                                    <div class="popover-body">
                                        <div>
                                            <label>Detalle Estado</label>
                                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                        </div>
                                        <span v-html="descriptionStatusSMS[index]"></span>
                                    </div>
                                </div>
                            </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </template>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeKeywordBase && dataKeywordBase.length > 0 || !initializeKeywordBase ? '' : 'disabled'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchBaseKeyword()"></pagination>
                </div>
            </div>
            <div class="text-center mr-b-30 mt-3">
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/utilsVue/keywordUsersVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmKeywordBaseUsers.id_base = '{{ $dataBaseKeyword[0]['id_bulk_sms'] }}'
    vmKeywordBaseUsers.fetchBaseKeyword()
    clearModalClose('modalScore', 'div.dialogScoreExtraLarge')
</script>