<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formSendKeyword', {}, 'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Enviar SMS Keyword
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableSMSKeywordSendVue">
        <div v-if="initializeSMSKeywordSend">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeSMSKeywordSend && dataSMSKeywordSend.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroKeyword" class="form-control dateRangeKeyword" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSKeywordSend()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="mr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectPickerKeyword show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSKeywordSend()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div style="margin-top: -1px !important;">
                            <select class="form-control selectPickerKeyword show-tick" data-live-search="true" v-model="selectKeyword" @change="fetchSMSKeywordSend()">
                                <option value="" selected>Todos los Keywords</option>
                                @foreach($selectKeywords as $key => $value)
                                    <option value="{{ $value['id'] }}">{{ $value['nombre_keyword'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Bulk ID</th>
                                <th>Keyword</th>
                                <th>Fecha Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="5">
                                    No hay ningún envio con algún keyword actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroKeyword" class="form-control dateRangeKeyword" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSKeywordSend()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="mr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectPickerKeyword show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSKeywordSend()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div style="margin-top: -1px !important;">
                            <select class="form-control selectPickerKeyword show-tick" data-live-search="true" v-model="selectKeyword" @change="fetchSMSKeywordSend()">
                                <option value="" selected>Todos los Keywords</option>
                                @foreach($selectKeywords as $key => $value)
                                    <option value="{{ $value['id'] }}">{{ $value['nombre_keyword'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table id="listSendKeyword" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Bulk ID</th>
                                <th>Keyword</th>
                                <th>Fecha Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataKeywordSend, index) in dataSMSKeywordSend">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="dataKeywordSend.id_bulk_sms"></td>
                                    <td v-html="dataKeywordSend.keyword.nombre_keyword"></td>
                                    <td v-html="dateSend[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button type="button" @click="clickEvent('div.dialogScoreExtraLarge', 'formKeywordUsers', dataKeywordSend.id_bulk_sms)" data-toggle="modal" data-target="#modalScore" title="Ver Base de Usuarios" class="btn btn-cian btn-sm"><i class="fa fa-group"></i></button>
                                            <button type="button" @click="clickDownload('/downloadReportSMSKeyword', { idBulkSMS: dataKeywordSend.id_bulk_sms })" title="Descargar Reporte" class="btn btn-green2 btn-sm" ><i class="fa fa-file-excel-o"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeSMSKeywordSend && dataSMSKeywordSend.length > 0 || !initializeSMSKeywordSend ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchSMSKeywordSend()"></pagination>
            </div>
        </div>
    </div>
</div>

<script>
    function loadEnvioSMSKeywordDatatable(){
        vmDataTableSMSKeywordSend.fetchSMSKeywordSend()
        vmFront.nameRoute = ucwords('{{ $titleModule }}')
    }

    async function getDeliveryKeyword(idKeyword){
        await vmFront.getDeliveryReports(idKeyword, 0, 'div.dialogScoreExtraLarge', 'formKeywordUsers', '/api/getSendSMS', {
            keywordID: idKeyword
        })
    }

    filterSelectDatatable('.selectClientes','#listSendKeyword',2)
    filterSelectDatatable('.selectKeywords','#listSendKeyword',3)
</script>