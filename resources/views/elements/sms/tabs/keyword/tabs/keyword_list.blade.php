<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formSMSKeyword', {}, 'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Keyword
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableSMSKeywordVue">
        <div v-if="initializeSMSKeyword">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeSMSKeyword && dataSMSKeyword.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSKeyword()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Keyword" type="text" v-model="searchKeyword">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSKeyword()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                <table class="table table-bordered centerTableImg nowrap">
                    <thead class="bg-primary">
                        <tr>
                            <th>#</th>
                            <th>Keyword</th>
                            <th>Cliente</th>
                            <th>Estado Servicio</th>
                            <th>Envio de Error</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td colspan="6">
                                No hay ningún keyword creado actualmente.
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSKeyword()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Keyword" type="text" v-model="searchKeyword">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSKeyword()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Keyword</th>
                                <th>Cliente</th>
                                <th>Estado Servicio</th>
                                <th>Envio de Error</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataKeyword, index) in dataSMSKeyword">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameKeyword[index]"></td>
                                    <td v-html="dataKeyword.clientes.razon_social"></td>
                                    <td v-html="statusService[index]"></td>
                                    <td v-html="statusError[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <template v-if="parseInt(dataKeyword.id_estado) === 1">
                                                <button type="button" @click="clickEvent('div.dialogScore','formSMSKeyword', dataKeyword.id)" data-toggle="modal" data-target="#modalScore" title="Editar Keyword" class="btn btn-orange2 btn-sm"><i class="fa fa-edit"></i></button>
                                            </template>
                                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                                <button type="button" @click="clickEvent('div.dialogScore','formKeywordConfirmation', dataKeyword.id)" data-toggle="modal" data-target="#modalScore" title="Confirmar Keyword" class="btn btn-purple-dark btn-sm"><i class="fa fa-bookmark"></i></button>
                                            @endif
                                            <button type="button" @click="clickEvent('div.dialogScore','formKeywordStatus', dataKeyword.id)" data-toggle="modal" data-target="#modalScore" title="Cambiar Estado" class="btn btn-gray2 btn-sm"><i class="fa fa-refresh"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeSMSKeyword && dataSMSKeyword.length > 0 || !initializeSMSKeyword ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchSMSKeyword()"></pagination>
            </div>
        </div>
    </div>
</div>

<script>
    function loadKeywordDatatable(){
        vmDataTableSMSKeyword.fetchSMSKeyword()
        vmFront.nameRoute = ucwords('{{ $titleModule }}')
    }

    async function getDeliveryKeyword(idBulk, idKeyword){
        await vmFront.getDeliveryReports(idKeyword, 'div.dialogScoreLarge', 'formKeywordUsers', '/api/getSendSMS', {
            keywordID: idBulk
        })
    }
</script>