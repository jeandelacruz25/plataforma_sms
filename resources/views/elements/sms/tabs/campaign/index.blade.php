<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formCampaign',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Campaña
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableSMSCampanasVue">
        <div v-if="initializeSMSCampanas">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeSMSCampanas && dataSMSCampanas.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre Campana</th>
                                <th>Estado Envio</th>
                                <th>N° Tel</th>
                                <th>Porcentaje de Envio</th>
                                <th>Fecha Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="7">
                                    No hay ninguna campaña de sms creada actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre Campana</th>
                                <th>Estado Envio</th>
                                <th>N° Tel</th>
                                <th>Porcentaje de Envio</th>
                                <th>Fecha Envio</th>
                                <th data-priority="1">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataSMSCampana, index) in dataSMSCampanas">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameCampana[index]"></td>
                                    <td v-html="statusSend[index]"></td>
                                    <td v-html="dataSMSCampana.total_telefonos"></td>
                                    <td v-html="percentageSend[index]"></td>
                                    <td v-html="dateSend[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <template v-if="parseInt(dataSMSCampana.envio_sms) === 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formCampaign', dataSMSCampana.id)" data-toggle="modal" data-target="#modalScore" title="Editar Campaña" class="btn btn-orange2 btn-sm"><i class="fa fa-edit"></i></button>
                                            </template>
                                            <template v-if="parseInt(dataSMSCampana.envio_sms) === 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formCampaignUpload', dataSMSCampana.id)" data-toggle="modal" data-target="#modalScore" title="Subir Base de Usuarios" class="btn btn-yellow2 btn-sm"><i class="fa fa-upload"></i></button>
                                            </template>
                                            <button type="button" @click="clickEvent('div.dialogScoreExtraLarge','formCampaignUsers', dataSMSCampana.id)" data-toggle="modal" data-target="#modalScore" title="Ver Base de Usuarios" class="btn btn-cian btn-sm" ><i class="fa fa-group"></i></button>
                                            <template v-if="parseInt(dataSMSCampana.envio_sms) === 1">
                                                <button type="button" @click="clickEvent('div.dialogScore','miniDashboardSMS', { idCampana: dataSMSCampana.id })" data-toggle="modal" data-target="#modalScore" title="Mini Dashboard" class="btn btn-green-aqua btn-sm"><i class="fa fa-pie-chart"></i></button>
                                            </template>
                                            <template v-if="parseInt(dataSMSCampana.total_telefonos) != 0">
                                                <button type="button" @click="clickDownload('/downloadReportCampaign', { idCampana: dataSMSCampana.id })" title="Descargar Reporte" class="btn btn-green2 btn-sm" ><i class="fa fa-file-excel-o"></i></button>
                                            </template>
                                            <template v-if="parseInt(dataSMSCampana.envio_sms) === 0 || parseInt(dataSMSCampana.id_status) === 2">
                                                <button type="button" @click="clickEvent('div.dialogScore','formSMSMasivo', dataSMSCampana.id)" data-toggle="modal" data-target="#modalScore" title="Enviar SMS Masivo" class="btn btn-purple2 btn-sm"><i class="fa fa-send"></i></button>
                                            </template>
                                            <button type="button" @click="clickEvent('div.dialogScore','formCampaignStatus', dataSMSCampana.id)" data-toggle="modal" data-target="#modalScore" title="Cambiar Estado" class="btn btn-gray2 btn-sm"><i class="fa fa-refresh"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeSMSCampanas && dataSMSCampanas.length > 0 || !initializeSMSCampanas ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchSMSCampanas()"></pagination>
            </div>
        </div>
    </div>
</div>

<script>
    function loadCampanaDatatable(){
        vmDataTableSMSCampanas.fetchSMSCampanas()
        vmFront.nameRoute = ucwords('{{ $titleCampaignModule }}')
    }
</script>