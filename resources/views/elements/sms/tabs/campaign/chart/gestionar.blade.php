<div id="miniDashboardSMSVue">
    <template v-if="initFormGenerar">
        <div class="row border_fix border-primary">
            <div class="container">
                <template v-if="loadForm">
                    <div class="col-md mt-2">
                        <div class="cssload-loader"></div>
                    </div>
                </template>
                <template v-else>
                    <div class="col-md mt-2">
                        <template v-if="initFormGenerarGestiones">
                            <template v-if="selectProveedores.length > 0">
                                <div class="mt-2">
                                    <div class="form-group">
                                        <label>Proveedores</label>
                                        <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedProveedor" @change="changeCartera()">
                                            <option disabled selected>Seleccione un proveedor</option>
                                            <template v-for="(item, index) in selectProveedores">
                                                <option v-html="item.proveedor"></option>
                                            </template>
                                        </select>
                                    </div>
                                    <template v-if="initFormCartera">
                                        <div class="form-group">
                                            <label>Cartera</label>
                                            <select class="form-control selectGenerar show-tick" data-live-search="true" v-model="selectedCartera">
                                                <option disabled selected>Seleccione una cartera</option>
                                                <template v-for="(item, index) in selectCartera">
                                                    <option v-bind:data-content="item.cartera" v-html="item.id_cartera"></option>
                                                </template>
                                            </select>
                                        </div>
                                    </template>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-primary btnForm ripple" @click="generarGestion()"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Generar</button>
                                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                        <button type="button" class="btn btn-default ripple" @click="clearFormGenerarMarcadores()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                    </div>
                                    <div class="clearfix mt-2"></div>
                                </div>
                            </template>
                            <template v-else>
                                <div class="mt-2">
                                    <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>El cliente asociado no cuenta con data del score.</strong>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default ripple" @click="clearFormGenerarMarcadores()"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                    </div>
                                    <div class="clearfix mt-2"></div>
                                </div>
                            </template>
                        </template>
                    </div>
                </template>
            </div>
        </div>
    </template>
    <template v-else>
        <div class="text-center mt-3">
            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
        </div>
    </template>
</div>