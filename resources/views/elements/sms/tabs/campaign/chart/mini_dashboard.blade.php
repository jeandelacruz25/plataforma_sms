<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Mini Dashboard [ {{ $scriptCampaign ? $dataCampaign[0]['nombre_campana'] : $dataCampaign[0]['id_bulk_sms'] }} ]</h5>
    </div>
    <div class="modal-body">
        <div class="row">
            @if($dataCliente[0]['score_async'] == 1)
                <div class="col-md-12">
                    <div class="chartHTMLSMS"></div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="chartTableSMS fs-10"></div>
                </div>
            @else
                <div class="col-md-6">
                    <div class="chartHTMLSMS"></div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="chartTableSMS fs-11"></div>
                </div>
            @endif
        </div>
        @if($dataCliente[0]['score_async'] == 1)
            @include('elements.sms.tabs.campaign.chart.gestionar')
        @else
            <div class="text-center mt-3">
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        @endif
    </div>

</div>
<div class="chartScriptSMS"></div>

@if($dataCliente[0]['score_async'] == 1)
    <script src="{!! asset('js/vue/utilsVue/miniDashboardSMS.js?version='.date('YmdHis')) !!}"></script>
    <script>
        @if($scriptCampaign)
            vmMiniDashboardSMS.idCampaign = '{{ $dataCampaign[0]['id'] }}'
            vmMiniDashboardSMS.idCliente = '{{ $dataCliente[0]['id'] }}'
        @else
            vmMiniDashboardSMS.idBulkSMS = '{{ $dataCampaign[0]['id_bulk_sms'] }}'
            vmMiniDashboardSMS.idCliente = '{{ $dataCliente[0]['id'] }}'
        @endif
    </script>
@endif
<script>
    clearModalClose('modalScore', 'div.dialogScore')
    $(document).ready(function() {
        axiosChartSMS('post', '/getDataMiniDashboardSMS', {
            @if($scriptCampaign)
                campaignID: '{{ $dataCampaign[0]['id'] }}',
            @else
                bulkID: '{{ $dataCampaign[0]['id_bulk_sms'] }}',
            @endif
        })
    })
</script>