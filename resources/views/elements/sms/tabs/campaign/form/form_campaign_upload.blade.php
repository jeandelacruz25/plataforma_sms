<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Subir Base de Clientes</h5>
    </div>
    <div class="modal-body">
        <form id="formCampaignUpload">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Base de clientes (.csv, .xlsx, .xls)</label>
                        <input type="file" name="baseClientes" class="baseClientes">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-center">
                        <button type="submit" class="btn btn-facebook btnForm ripple"><i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                        <div class="pull-right">
                            <a href="javascript:void(0)" onclick="downloadFile('{{ asset('examples/ejemplo_base_campana.xlsx') }}')" class="btn-orange2 px-3">
                                Descargar Ejemplo
                            </a>
                        </div>
                        <p>
                            <span>- La base debe contener el titulo : <b>telefono, mensaje</b>.</span><br>
                            <span>- Los números a subir deben tener el número del pais primero (ejem : 51999999999), sin colorcar el signo <b>+</b> delante.</span><br>
                            <span>- No debes subir números duplicados (Recuerda que esto sera un costo de igual manera).</span><br>
                            <span>- El envio tiene un promedio de envio 40 SMS x cada 1 segundo.</span><br>
                            <span>- La base puede seguir aumentando sus número siempre y cuando no hayas enviado los SMS aún.</span><br>
                            <span>- El estado de los SMS enviados, pueden tomar su tiempo ya que esto es lo que demore en procesar la información el operador (Claro, Movistar, etc).</span>
                        </p>
                    </div>
                </div>
            </div>
            <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
            <input type="hidden" name="totalTelefonos" value="{{ $dataCampaign[0]['total_telefonos'] }}">
            <input type="hidden" name="clienteID" value="{{ $dataCampaign[0]['id_cliente'] }}">
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampana.js?version='.date('YmdHis')) !!}"></script>
<script>
    bootstrapFile('.baseClientes',{
        dragdrop: true,
        text: 'Subir Archivo',
        btnClass: 'btn-facebook fs-15',
        size: 'sm',
        placehold: 'Solo archivos .csv, .xls, .xlsx',
        htmlIcon: '<span class="feather feather-upload list-icon mr-r-10"></span>&nbsp;'
    })
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>