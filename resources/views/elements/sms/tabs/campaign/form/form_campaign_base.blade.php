<!-- Modal content-->
<div id="campaignUserVue">
    <div class="modal-content">
        <div class="modal-header text-inverse bg-primary">
            <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal">&times;</button>
            <h5 class="modal-title">Base de la Campaña [{{ ucwords(\Illuminate\Support\Str::lower($dataCampaign[0]['nombre_campana'])) }}]</h5>
        </div>
        <div class="modal-body">
            <div v-if="initializeCampaignBase">
                <div class="col-md-12">
                    <div class="cssload-loader"></div>
                </div>
            </div>
            <template v-else-if="!initializeCampaignBase && dataCampaignBase.length === 0 && !initDataPagination">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-warning fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No hay ninguna base subida para esta campaña</strong>
                    </div>
                </div>
            </template>
            <template v-else-if="initDataPagination">
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseCampaign()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-warning fixLineHeight">
                            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No se encontro resultados en tu busqueda, ingrese un nuevo número</strong>
                        </div>
                    </div>
                </div>
            </template>
            <template v-else>
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Telefono" type="text" v-model="searchTelephone" onkeypress="return filterNumber(event)">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="paginationFetchBaseCampaign()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                        <table id="table_base_sms" class="table table-bordered centerTableImg nowrap" cellspacing="0" width="100%">
                            <template v-if="dataCampaignBase[0].estado_sms == 0">
                                <thead>
                                    <tr class="thead-inverse bg-primary">
                                        <th>Número Enviar</th>
                                        <th>Mensaje Enviar</th>
                                        <th>Estado SMS</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-for="(baseCampaign, index) in dataCampaignBase">
                                        <tr class="text-dark">
                                            <td v-html="countrySMS[index] + ' ' + baseCampaign.telefono_usuario"></td>
                                            <td v-html="messageSMS[index]"></td>
                                            <td v-html="statusSMS[index]"></td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-cian btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseCampaign.telefono_usuario" data-popover="popover" data-trigger="focus" title="Información de Estado">
                                                        <i class="list-icon fa fa-question-circle-o"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-yellow2 btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseCampaign.telefono_usuario + '_'" data-popover="popover" data-trigger="focus" title="Detalle de Estado">
                                                        <i class="list-icon fa fa-exclamation-circle"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="d-none" v-bind:id="baseCampaign.telefono_usuario">
                                            <div class="popover-body">
                                                <div>
                                                    <label>Información Estado</label>
                                                    <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                                </div>
                                                <span v-html="conceptStatusSMS[index]"></span>
                                            </div>
                                        </div>
                                        <div class="d-none" v-bind:id="baseCampaign.telefono_usuario + '_'">
                                            <div class="popover-body">
                                                <div>
                                                    <label>Detalle Estado</label>
                                                    <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                                </div>
                                                <span v-html="descriptionStatusSMS[index]"></span>
                                            </div>
                                        </div>
                                    </template>
                                </tbody>
                            </template>
                            <template v-else>
                                <thead>
                                    <tr class="thead-inverse bg-primary">
                                        <th>Número Enviado</th>
                                        <th>Prov.</th>
                                        <th>Mensaje Enviado</th>
                                        <th>Fecha Recepción</th>
                                        <th>Estado SMS</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-for="(baseCampaign, index) in dataCampaignBase">
                                        <tr class="text-dark">
                                            <td v-html="countrySMS[index] + ' ' + baseCampaign.telefono_usuario"></td>
                                            <td class="text-center" v-html="mobileNetworkSMS[index]"></td>
                                            <td v-html="messageSMS[index]"></td>
                                            <td v-html="formatDateTime[index]"></td>
                                            <td v-html="statusSMS[index]"></td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-cian btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseCampaign.telefono_usuario" data-popover="popover" data-trigger="focus" title="Información de Estado">
                                                        <i class="list-icon fa fa-question-circle-o"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-yellow2 btn-sm" data-placement="bottom" v-bind:data-popover-content="'#' + baseCampaign.telefono_usuario + '_'" data-popover="popover" data-trigger="focus" title="Detalle de Estado">
                                                        <i class="list-icon fa fa-exclamation-circle"></i>
                                                    </button>
                                                    <template v-if="baseCampaign.tipo_envio != 6">
                                                        <template v-if="baseCampaign.estado_sms != 6">
                                                            <button type="button" class="btn btn-green-aqua btn-sm" data-placement="bottom" title="Historial de Chat" @click="viewChats(baseCampaign.telefono_usuario, baseCampaign.id_bulk_sms)">
                                                                <i class="list-icon fa fa-comments-o"></i>
                                                            </button>
                                                        </template>
                                                    </template>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="d-none" v-bind:id="baseCampaign.telefono_usuario">
                                            <div class="popover-body">
                                                <div>
                                                    <label>Información Estado</label>
                                                    <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                                </div>
                                                <span v-html="conceptStatusSMS[index]"></span>
                                            </div>
                                        </div>
                                        <div class="d-none" v-bind:id="baseCampaign.telefono_usuario + '_'">
                                            <div class="popover-body">
                                                <div>
                                                    <label>Detalle Estado</label>
                                                    <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                                </div>
                                                <span v-html="descriptionStatusSMS[index]"></span>
                                            </div>
                                        </div>
                                    </template>
                                </tbody>
                            </template>
                        </table>
                    </div>
                </div>
            </template>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeCampaignBase && dataCampaignBase.length > 0 || !initializeCampaignBase ? '' : 'disabled'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchBaseCampaign()"></pagination>
                </div>
            </div>
            <div class="mt-4 chatWindow"></div>
            <div class="text-center mr-b-30 mt-3">
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/utilsVue/campaignUsersVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmCampaignBaseUsers.id_base = '{{ $dataCampaign[0]['id'] }}'
    vmCampaignBaseUsers.fetchBaseCampaign()
    clearModalClose('modalScore', 'div.dialogScoreExtraLarge')
</script>