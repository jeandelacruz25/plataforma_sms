<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Envio de SMS Masivo [{{ $dataCampaign[0]['nombre_campana'] }}]</h5>
    </div>
    <div class="modal-body">
        @if($dataCampaign[0]['total_telefonos'] > 0)
            <form id="formSMSMasivo">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-info fixLineHeight">
                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                        <p class="fs-12">
                            <span>- Al enviar, la campaña pasa estar inhabilitado por lo tanto se podra consultar el detalle de los envios.</span><br>
                            <span>- El envio tiene un promedio de envio 40 SMS x cada 1 segundo.</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <label>Opciones Adicionales</label>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input name="informativoSMS" type="checkbox"> <span class="label-text">SMS Informativo</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#informativoSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input name="flashSMS" type="checkbox"> <span class="label-text">Activar SMS Flash</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#flashSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-top: -10px !important;">
                        <span class="col-md-5 col-form-label label-text">Gestionar Fecha de Envio &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#dateSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a></span>
                        <div class="col-md-7">
                            <input type="text" name="dateEnvio" readonly="readonly" class="form-control form-control-sm txt-calendar-clock dateEnvio" placeholder="{{ \Carbon\Carbon::now() }}" value="">
                        </div>
                    </div>
                </div>
                <div class="d-none" id="informativoSMS">
                    <div class="popover-body">
                        <div>
                            <label>Información</label>
                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                        </div>
                        <p>Al activar esta opción, todos los sms enviados no aceptaran ningún tipo de respuesta por lo tanto este no sera cobrado por la recepción de respuesta.</p>
                    </div>
                </div>
                <div class="d-none" id="flashSMS">
                    <div class="popover-body">
                        <div>
                            <label>Información</label>
                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                        </div>
                        <p>Los SMS flash aparecen directamente en la pantalla principal sin interacción del usuario y no se almacenan automáticamente en la bandeja de SMS (en algunos modelos de celulares si podra dejar guardar dicho SMS). El usuario tiene la opción de guardar estos mensajes, con la ID apropiada del emisor, si decide hacerlo, pero no es automático.</p>
                        <p><img src="{{ asset('img/pop-over/smsFlash.png') }}"></p>
                    </div>
                </div>
                <div class="d-none" id="dateSMS">
                    <div class="popover-body">
                        <div>
                            <label>Información</label>
                            <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                        </div>
                        <p>Puedes gestionar la fecha y la hora en la que sera recepcionado por el usuario, recuerda ingresar correctamente la fecha y la hora de envio.</p>
                    </div>
                </div>
            </div>
            <div class=" col-md-12 text-center">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='fa fa-send list-icon mr-r-10' aria-hidden='true'></i> Enviar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No Enviar</button>
            </div>

            <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
            <input type="hidden" name="clienteID" value="{{ $dataCampaign[0]['id_cliente'] }}">
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
        @else
            <div class="col-md-12">
                <div class="alert alert-icon alert-danger fixLineHeight">
                    <i class="feather feather-x list-icon mr-r-10"></i> <strong>No puede enviarse ningun SMS, ya que esta campaña no cuenta con una base de usuarios.</strong>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formSMS.js?version='.date('YmdHis')) !!}"></script>
<script>
    initDateRangePicker('.dateEnvio', {
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss',
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar"
        },
        startDate: moment(),
        opens: 'left'
    })

    $('.dateEnvio').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'))
    })

    $('.dateEnvio').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('')
    })

    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>