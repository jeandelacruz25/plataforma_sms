<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Campaña</h5>
    </div>
    <div class="modal-body">
        <form id="formCampaign">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nombre Campaña</label>
                        <input type="text" name="nameCampaign" class="form-control" placeholder="Ingresa el nombre de la campaña" value="{{ $dataCampaign ? $dataCampaign[0]['nombre_campana'] : '' }}">
                    </div>
                </div>
                @if(Auth::user()->authorizeRoles(['Administrador']))
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Cliente</label>
                            <select name="clienteCampaign" class="form-control selectpicker show-tick" data-live-search="true">
                                @foreach($options['clientes'] as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($dataCampaign) {{ $dataCampaign[0]['id_cliente'] === $value['id'] ? 'selected' : '' }} @endif>{{ ucwords($value['razon_social']) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if($updateForm === false)
                        <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                            <p>- Al crear una nueva campaña, debes pasar a subir la base de usuarios que pertenecen a dicha campaña.</p>
                        </div>
                    @endif
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="campaignID" value="{{ $dataCampaign ? $dataCampaign[0]['id'] : '' }}">
            <input type="hidden" name="totalTelefonos" value="{{ $dataCampaign ? $dataCampaign[0]['total_telefonos'] : '' }}">
            <input type="hidden" name="envioSMS" value="{{ $dataCampaign ? $dataCampaign[0]['envio_sms'] : '' }}">
            <input type="hidden" name="tipoEnvio" value="{{ $dataCampaign ? $dataCampaign[0]['tipo_envio'] : '' }}">
            <input type="hidden" name="fechaEnvio" value="{{ $dataCampaign ? $dataCampaign[0]['fecha_envio'] : '' }}">
            <input type="hidden" name="userCreated" value="{{ $dataCampaign ? $dataCampaign[0]['user_created'] : '' }}">
            <input type="hidden" name="statusID" value="{{ $dataCampaign ? $dataCampaign[0]['id_status'] : '' }}">
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampana.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>