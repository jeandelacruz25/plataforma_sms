<div class="border-color-gray-200 border-all rounded">
    <div class="chatbox">
        <div class="chatbox-chat-area">
            <div class="chatbox-header d-flex align-items-center px-4 border-bottom">
                <div class="media align-items-center mr-tb-30 flex-1">
                    <figure class="thumb-xs2 mr-3">
                        <a href="#">
                            <img class="rounded-circle" src="{{ asset("img/modules/user_sms.svg?version=").date('YmdHis') }}">
                        </a>
                    </figure>
                    <div class="media-body">
                        <h6 class="mt-0 mb-1">Historial de Chat</h6>
                        <p class="text-muted heading-font-family mb-0">{{ $numberChat }}</p>
                    </div>
                </div>
                <a href="javascript:void(0)" onclick="cleanHistory()" class="btn btn-circle btn-danger fs-20 mr-2"><i class="feather feather-x"></i></a>
            </div>
            <div class="chatbox-body">
                <div class="chatbox-messages divOverflowY">
                    @foreach($dataChat as $key => $value)
                        <div class="{{ $value['envio_sms'] == 0 || $value['envio_sms'] == 5 ? "message reply media" : "message media" }}">
                            <figure class="avatar thumb-xs2 mr-b-0">
                                <a href="javascript:void(0)">
                                    <img src="{{ $value['envio_sms'] == 0 ? asset("img/modules/user_sms.svg?version=").date('YmdHis') : asset("img/modules/user_sms_reply.svg?version=").date('YmdHis') }}" class="rounded-circle">
                                </a>
                            </figure>
                            <div class="media-body">
                                <p class="mw-100">{{ $value['mensaje_sms'] }}</p>
                                <small class="text-gray-500">{{ convertTime($value['fecha_sms']) }}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function cleanHistory() {
        $('.chatWindow').html('')
    }
</script>