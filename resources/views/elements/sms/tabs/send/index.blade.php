<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formSMSIndividual', {}, 'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Enviar SMS Individual
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableSMSSinCampanasVue">
        <div v-if="initializeSMSSinCampanas">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeSMSSinCampanas && dataSMSSinCampanas.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroIndividual" class="form-control dateRangeIndividual" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSSinCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSSinCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Bulk ID</th>
                                <th>Cliente</th>
                                <th>N° Tel</th>
                                <th>Fecha Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="6">
                                    No hay ningún envio individual actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroIndividual" class="form-control dateRangeIndividual" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchSMSSinCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchSMSSinCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Bulk ID</th>
                                <th>Cliente</th>
                                <th>N° Tel</th>
                                <th>Fecha Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataSMSSinCampana, index) in dataSMSSinCampanas">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="dataSMSSinCampana.id_bulk_sms"></td>
                                    <td v-html="dataSMSSinCampana.cliente.razon_social"></td>
                                    <td v-html="dataSMSSinCampana.countTelephone"></td>
                                    <td v-html="dateSend[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button type="button" @click="clickEvent('div.dialogScoreExtraLarge','formBuilkIDUsers', dataSMSSinCampana.id_bulk_sms)" data-toggle="modal" data-target="#modalScore" title="Ver Base de Usuarios" class="btn btn-cian btn-sm"><i class="fa fa-group"></i></button>
                                            <button type="button" @click="clickEvent('div.dialogScore','miniDashboardSMS', { idBulkSMS: dataSMSSinCampana.id_bulk_sms })" data-toggle="modal" data-target="#modalScore" title="Mini Dashboard" class="btn btn-green-aqua btn-sm"><i class="fa fa-pie-chart"></i></button>
                                            <button type="button" @click="clickDownload('/downloadReportSMSIndividual', { idBulkSMS: dataSMSSinCampana.id_bulk_sms })" title="Descargar Reporte" class="btn btn-green2 btn-sm" ><i class="fa fa-file-excel-o"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeSMSSinCampanas && dataSMSSinCampanas.length > 0 || !initializeSMSSinCampanas ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchSMSSinCampanas()"></pagination>
            </div>
        </div>
    </div>
</div>

<script>
    function loadEnvioSMSDatatable(){
        vmDataTableSMSSinCampanas.fetchSMSSinCampanas()
        vmFront.nameRoute = ucwords('Envio SMS (sin Campaña)')
    }
</script>