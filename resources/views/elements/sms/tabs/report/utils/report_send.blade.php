<div id="smsReportVue">
    <div class="widget-holder widget-sm widget-border-radius col-md-12">
        <div class="widget-bg">
            <div class="widget-heading bg-score">
            <span class="widget-title my-0 color-white fs-15 fw-600">
                Filtros Personalizados
            </span>
            </div>
            <div class="widget-body border_fix">
                <div class="row">
                    <div class="col-md-3 @if(!auth()->user()->authorizeRoles(['Administrador'])) disabled @endif">
                        <div class="form-group">
                            <label>Clientes</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#sendInfoCliente" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpicker show-tick selectClientesReport" name="selectClientes" data-live-search="true" v-model="selectClientes" @change="changeCliente">
                                @if(auth()->user()->authorizeRoles(['Administrador']))
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ ucwords($value['razon_social']) }}</option>
                                    @endforeach
                                @else
                                    <option value="{{ auth()->user()->id_cliente }}" selected>{{ ucwords(auth()->user()->clientes->razon_social) }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoCliente">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Este filtro por defecto, seleccionara el cliente al cual perteneces.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado de SMS</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#sendInfoEstadoSMS" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpicker show-tick estadoSMS" name="estadoSMS" multiple data-actions-box="true" data-selected-text-format="count > 2">
                                <option value="0" data-content="<span class='px-3 bg-span btn-cian'>Aún sin Enviar</span>"></option>
                                <option value="1" data-content="<span class='px-3 bg-span btn-yellow2'>Pendiente</span>"></option>
                                <option value="2" data-content="<span class='px-3 bg-span btn-purple2'>No Entregado</span>"></option>
                                <option value="3" data-content="<span class='px-3 bg-span btn-green2'>Entregado</span>"></option>
                                <option value="4" data-content="<span class='px-3 bg-span btn-orange2'>Expirado</span>"></option>
                                <option value="5" data-content="<span class='px-3 bg-span btn-rose2'>Rechazado</span>"></option>
                                <option value="6" data-content="<span class='px-3 bg-span btn-gray2'>Lista Negra</span>"></option>
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoEstadoSMS">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Este filtro es para poder seleccionar que estados de tus envios, precisamente deseas observar.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tipo de Envio</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#sendInfoTipoEnvio" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpicker show-tick tipoEnvio" name="tipoEnvio" v-model="selectCampana" @change="changeCampana">
                                <option value="" selected>Seleccione</option>
                                <option value="0">Campaña</option>
                                <option value="1">Sin Campaña</option>
                                <option value="2">Keyword</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoTipoEnvio">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Breve explicación de los tipos de envios : .</p>
                            <p><b class="text-score">- Campaña :</b> Se enlistara todas tus campañas creadas, hasta el momento.</p>
                            <p><b class="text-score">- Sin Campaña :</b> Se enlistara todos los bulkID, que enviaste hasta el momento.</p>
                            <p><b class="text-score">- Keyword :</b> Se enlistara todos tus keywords creados, hasta el momento.</p>
                        </div>
                    </div>
                    <div v-bind:class="selectClientes != '' && selectCampana != '' ? 'col-md-3' : 'col-md-3 disabled'">
                        <div class="form-group">
                            <label v-if="selectCampana === '1'">Lista de BulkID</label>
                            <label v-if="selectCampana === '2'">Lista de Keywords</label>
                            <label v-if="selectCampana === '0' || selectCampana === ''">Lista de Campañas</label>
                            <template v-if="selectCampana === ''">
                                <select class="form-control selectpicker show-tick"></select>
                            </template>
                            <template v-else>
                                <template v-if="selectCampana === '0'">
                                    <select class="form-control selectpicker show-tick selectListEnvio" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="options.id" v-html="options.nombre_campana"></option>
                                    </select>
                                </template>
                                <template v-else-if="selectCampana === '1'">
                                    <select class="form-control selectpicker show-tick selectListEnvio" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="options.id_bulk_sms" v-html="options.id_bulk_sms"></option>
                                    </select>
                                </template>
                                <template v-else>
                                    <select class="form-control selectpicker show-tick selectListEnvio" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="options.id" v-html="options.nombre_keyword"></option>
                                    </select>
                                </template>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div v-bind:class="selectClientes != '' && selectCampana != '' ? 'col-md-3' : 'col-md-3 disabled'">
                        <div class="form-group">
                            <label>Filtro Envio</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#sendInfoFiltroEnvio" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpicker show-tick filtroEnvio" name="filtroEnvio">
                                <option value="" selected v-if="selectCampana === '0' || selectCampana === '1'">Envios SMS</option>
                                <option value="" selected v-if="selectCampana === '2'">Envios Keyword</option>
                                <option value="3" v-if="selectCampana === '0' || selectCampana === '1'">Respuesta SMS</option>
                                <option value="5" v-if="selectCampana === '2'">Respuestas Keyword</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoFiltroEnvio" v-if="selectCampana === '0' || selectCampana === '1'">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Breve explicación de los filtros de envio : .</p>
                            <p><b class="text-score">- Envios SMS :</b> Son todos los envios que has realizado desde el módulo Gestion SMS.</p>
                            <p><b class="text-score">- Respuesta SMS :</b> Son todos los envios que has interactuado desde el Chat SMS.</p>
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoFiltroEnvio" v-if="selectCampana === '2'">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Breve explicación de los filtros de envio : .</p>
                            <p><b class="text-score">- Envios Keyword :</b> Son todos los envios que has realizado con el keyword previamente seleccionado.</p>
                            <p><b class="text-score">- Respuesta Keyword :</b> Son todos los envios que ha realizado tu keyword por el filtro o errores según la configuración asignada.</p>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Fecha de Envio</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#sendInfoFechaEnvio" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <input type="text" name="fechaEnvio" class="form-control form-control-fix-select dateRange fechaFiltro" placeholder="Escoga un rango de fechas">
                        </div>
                    </div>
                    <div class="d-none" id="sendInfoFechaEnvio">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Este filtro te permite seleccionar un rango de fechas para poder tener mayor alcance en tus reportes.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-center mr-t-10">
                            <button type="button" class="btn btn-facebook" onclick="reportSMSEnvio()"><i class="fa fa-pie-chart list-icon mr-r-10"></i>Graficar Reporte de Envios</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="chartHTML"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="chartTable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="chartScript"></div>

<script src="{!! asset('js/vue/reportSMS/smsReportVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    initSelectPicker('.selectpicker', {
        style: 'btn-default btn-sm'
    })

    initDateRangePicker('.dateRange', {
        locale: {
            format: 'YYYY-MM-DD',
            customRangeLabel: 'Escoger Rango',
            separator: ' / ',
            applyLabel: 'Aplicar',
            cancelLabel: 'Cerrar',
        },
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'center'
    })

    function reportSMSEnvio(){
        axiosChart('post', '/chartSend', {
            selectClientes: $('select.selectClientesReport').val(),
            estadoSMS: $('select.estadoSMS').val(),
            tipoEnvio: $('select.tipoEnvio').val(),
            filtroEnvio: $('select.filtroEnvio').val(),
            selectListEnvio: $('select.selectListEnvio').val(),
            fechaReporte: $('input[name=fechaEnvio]').val()
        }, {
            tipoEnvio: $('select.tipoEnvio').val()
        })
    }

    @if(!auth()->user()->authorizeRoles(['Administrador']))
        vmSMSReport.selectClientes = '{{ auth()->user()->id_cliente }}'
    @endif

</script>