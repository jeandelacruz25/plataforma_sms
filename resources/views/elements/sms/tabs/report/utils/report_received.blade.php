<div id="smsReceivedReportVue">
    <div class="widget-holder widget-sm widget-border-radius col-md-12">
        <div class="widget-bg">
            <div class="widget-heading bg-score">
            <span class="widget-title my-0 color-white fs-15 fw-600">
                Filtros Personalizados
            </span>
            </div>
            <div class="widget-body border_fix">
                <div class="row">
                    <div class="col-md-4 @if(!auth()->user()->authorizeRoles(['Administrador'])) disabled @endif">
                        <div class="form-group">
                            <label>Clientes</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#receivedInfoCliente" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpickerRecibido show-tick selectClientesReport" name="selectClientes" data-live-search="true" v-model="selectClientes" @change="changeCliente">
                                @if(auth()->user()->authorizeRoles(['Administrador']))
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ ucwords($value['razon_social']) }}</option>
                                    @endforeach
                                @else
                                    <option value="{{ auth()->user()->id_cliente }}" selected>{{ ucwords(auth()->user()->clientes->razon_social) }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="receivedInfoCliente">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Este filtro por defecto, seleccionara el cliente al cual perteneces.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tipo de Envio</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#receivedInfoTipoEnvio" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpickerRecibido show-tick tipoEnvioReceived" name="tipoEnvioReceived" v-model="selectCampana" @change="changeCampana">
                                <option value="" selected>Seleccione</option>
                                <option value="0">Campaña</option>
                                <option value="1">Sin Campaña</option>
                                <option value="2">Keyword</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="receivedInfoTipoEnvio">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Breve explicación de los tipos de envios : .</p>
                            <p><b class="text-score">- Campaña :</b> Se enlistara todas tus campañas creadas, hasta el momento.</p>
                            <p><b class="text-score">- Sin Campaña :</b> Se enlistara todos los bulkID, que enviaste hasta el momento.</p>
                            <p><b class="text-score">- Keyword :</b> Se enlistara todos tus keywords creados, hasta el momento.</p>
                        </div>
                    </div>
                    <div v-bind:class="selectClientes != '' && selectCampana != '' ? 'col-md-4' : 'col-md-4 disabled'">
                        <div class="form-group">
                            <label v-if="selectCampana === '1'">Lista de BulkID</label>
                            <label v-if="selectCampana === '2'">Lista de Keywords</label>
                            <label v-if="selectCampana === '0' || selectCampana === ''">Lista de Campañas</label>
                            <template v-if="selectClientes === ''">
                                <select class="form-control selectpickerRecibido show-tick"></select>
                            </template>
                            <template v-else>
                                <template v-if="selectCampana === '0'">
                                    <select class="form-control selectpickerRecibido show-tick selectListEnvioReceived" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="((options.clientes.palabra_clave).toLowerCase())+'_campana_'+options.id_cliente+'_'+options.id" v-html="options.nombre_campana"></option>
                                    </select>
                                </template>
                                <template v-else-if="selectCampana === '1'">
                                    <select class="form-control selectpickerRecibido show-tick selectListEnvioReceived" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="options.id_bulk_sms" v-html="options.id_bulk_sms"></option>
                                    </select>
                                </template>
                                <template v-else>
                                    <select class="form-control selectpickerRecibido show-tick selectListEnvioReceived" data-live-search="true">
                                        <option v-for="options in optionsListEnvio" v-bind:value="options.nombre_keyword" v-html="options.nombre_keyword"></option>
                                    </select>
                                </template>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div v-bind:class="selectCampana === '2' ? 'col-md-4' : 'd-none'">
                        <div class="form-group">
                            <label>Filtro Recibidos</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#receivedInfoFiltroRecibidos" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <select class="form-control selectpickerRecibido show-tick filtroRecibido" name="filtroRecibido">
                                <option value="" selected>Rec. Activos</option>
                                <option value="3">Rec. Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-none" id="receivedInfoFiltroRecibidos">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Breve explicación de los filtros de envio : .</p>
                            <p><b class="text-score">- Rec. Activos :</b> Son todos las respuestas recibidas mientras que el Keyword se encuentra activo.</p>
                            <p><b class="text-score">- Rec. Inactivo :</b> Son todos las respuestas recibidas mientras que el Keyword se encuentre inactivo <b class="text-danger">(Recuerde notificar cuando se deshabilite un keyword, para que no ocurra este detalle)</b>.</p>
                        </div>
                    </div>
                    <div v-bind:class="selectCampana === '2' ? 'col-md-4' : 'col-md-8'">
                        <div class="form-group">
                            <label>Fecha de Recepción</label> &nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#receivedInfoFechaRecepcion" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                            <input type="text" name="fechaEnvioReceived" class="form-control form-control-fix-select dateRangeRecibido fechaFiltro" placeholder="Escoga un rango de fechas">
                        </div>
                    </div>
                    <div class="d-none" id="receivedInfoFechaRecepcion">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>Este filtro te permite seleccionar un rango de fechas para poder tener mayor alcance en tus reportes.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-center mr-t-10">
                            <button type="button" class="btn btn-facebook" onclick="reportSMSRecibido()"><i class="fa fa-pie-chart list-icon mr-r-10"></i>Graficar Reporte de Recibidos</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="chartHTMLReceived"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="chartScriptReceived"></div>

<script src="{!! asset('js/vue/reportSMS/smsReceivedReportVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    initSelectPicker('.selectpickerRecibido', {
        style: 'btn-default btn-sm'
    })

    initDateRangePicker('.dateRangeRecibido', {
        locale: {
            format: 'YYYY-MM-DD',
            customRangeLabel: 'Escoger Rango',
            separator: ' / ',
            applyLabel: 'Aplicar',
            cancelLabel: 'Cerrar',
        },
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'center'
    })

    function reportSMSRecibido(){
        axiosChartReceived('post', '/chartReceived', {
            selectClientes: $('select.selectClientesReport').val(),
            tipoEnvio: $('select.tipoEnvioReceived').val(),
            filtroRecibido: $('select.filtroRecibido').val(),
            selectListEnvio: $('select.selectListEnvioReceived').val(),
            fechaReporte: $('input[name=fechaEnvioReceived]').val()
        })
    }

    @if(!auth()->user()->authorizeRoles(['Administrador']))
        vmSMSReceivedReport.selectClientes = '{{ auth()->user()->id_cliente }}'
    @endif
</script>