<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions"></div>
</div>
<div class="widget-body plr0">
    <div class="tabs tabs-vertical">
        <ul class="nav nav-tabs flex-column">
            <li class="nav-item" aria-expanded="false">
                <a class="nav-link active" href="#smsEnviados" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-envelope"></i> SMS Enviados
                </a>
            </li>
            <li class="nav-item" aria-expanded="false">
                <a class="nav-link" href="#smsRecibidos" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-envelope-open"></i> SMS Recibidos
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="smsEnviados" aria-expanded="false">
                @include('elements.sms.tabs.report.utils.report_send')
            </div>
            <div class="tab-pane" id="smsRecibidos" aria-expanded="false">
                @include('elements.sms.tabs.report.utils.report_received')
            </div>
        </div>
    </div>
</div>

