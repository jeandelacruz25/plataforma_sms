<div class="widget-body plr0 pt0">
    <div class="tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link tabCampañas" href="#campañas" data-toggle="tab" aria-expanded="true" onclick="loadCampanaDatatable()">
                    <span class="thumb-xxs">
                        <i class="list-icon feather feather-layers"></i> Campañas SMS
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link tabEnviarSMS" href="#enviarSMS" data-toggle="tab" aria-expanded="true" onclick="loadEnvioSMSDatatable()">
                    <span class="thumb-xxs">
                        <i class="list-icon feather feather-mail"></i> Envio SMS (sin Campaña)
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link tabKeywordSMS" href="#keywordSMS" data-toggle="tab" aria-expanded="true" onclick="$('.tabKeywords').click()">
                    <span class="thumb-xxs">
                        <i class="list-icon fa fa-key"></i> Keyword SMS
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link tabReportesSMS" href="#reportesSMS" data-toggle="tab" aria-expanded="true" onclick="">
                    <span class="thumb-xxs">
                        <i class="list-icon fa fa-bar-chart"></i> Reportes SMS
                    </span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="campañas">
                @include('elements.sms.tabs.campaign.index', ['titleModule' => $titleCampaignModule, 'iconModule' => $iconCampaignModule])
            </div>
            <div class="tab-pane" id="enviarSMS">
                @include('elements.sms.tabs.send.index', ['titleModule' => $titleSMSSinCampaignModule, 'iconModule' => $iconSMSSinCampaignModule])
            </div>
            <div class="tab-pane" id="keywordSMS">
                @include('elements.sms.tabs.keyword.index', ['titleModule' => $titleKeywordSMSModule, 'iconModule' => $iconKeywordSMSModule])
            </div>
            <div class="tab-pane" id="reportesSMS">
                @include('elements.sms.tabs.report.index', ['titleModule' => $titleReportSMSModule, 'iconModule' => $iconReportSMSModule])
            </div>
        </div>
    </div>
</div>

<script src="{!! asset('js/vue/dataTables/dataTableCampanasSMSVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/dataTables/dataTableSinCampanasSMSVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/dataTables/dataTableKeywordSMSVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/dataTables/dataTableKeywordSMSSendVue.js?version='.date('YmdHis')) !!}"></script>

<script>
    $(document).ready(function() {
        $('.tabCampañas').click()
    })

    deleteVariableSMSFunction()
</script>