<div class="widget-heading widget-heading-border">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formRoles', {}, 'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Perfil
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTablePerfilesVue">
        <div v-if="initializePerfiles">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializePerfiles && dataPerfiles.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Perfil" type="text" v-model="searchPerfil">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchPerfiles()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="5">
                                    No hay ningún perfil creado actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Perfil" type="text" v-model="searchPerfil">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchPerfiles()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table id="listRoles" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataPerfil, index) in dataPerfiles">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="dataPerfil.name"></td>
                                    <td v-html="dataPerfil.description"></td>
                                    <td v-html="statusPerfil[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="javascript:void(0)" class="btn btn-orange2 btn-sm" @click="clickEvent('div.dialogScore','formRoles', dataPerfil.id)" data-toggle="modal" data-target="#modalScore"><i title="Editar Cliente" class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-rose2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formRolesMenu', dataPerfil.id)" data-toggle="modal" data-target="#modalScore"><i title="Actualizar Menú" class="fa fa-bars"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializePerfiles && dataPerfiles.length > 0 || !initializePerfiles ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchPerfiles()"></pagination>
            </div>
        </div>
    </div>
</div>