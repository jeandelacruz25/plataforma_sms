<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Asignar Usuarios [Perfil {{ $nameRole }}]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'usersRole') != 0 && searchMenuUsuario(auth()->id(), 'usersRole') != 0)
            <div class="col-xs-12">
                <div class="input-group input-group-sm">
                    <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </div>
                    <input type="text" class="form-control" id="search_roles_user" placeholder="Ingrese el nombre o usuario a buscar">
                </div>
            </div>
            <form id="formRolesAssing">
                <div class="col-xs-12 mt-3">
                    <table id="table_roles_user" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="thead-inverse bg-primary">
                            <th class="text-center col-xs-2">
                                <input type="checkbox" class="checkGeneral" onclick="mark_all('.checkGeneral')">
                            </th>
                            <th class="col-xs-7">Nombre de Usuario</th>
                            <th class="col-xs-3">Username</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($UsersRole as $user)
                                @php $ifExistRole = ($user['id_rol'] == $idRole ? true : false) @endphp
                                <tr class="text-dark @if($ifExistRole) table-success @else table-info @endif @if(!$ifExistRole) trNew @endif" id="tr_{{ $user['id'] }}">
                                    <td class="col-xs-2 text-center">
                                        <input type="checkbox" name="checkUser[]" value="{{ $user['id'] }}" id="checkbox_{{ $user['id'] }}" class="@if(!$ifExistRole) checkNew @endif"
                                           @if($ifExistRole)
                                               @if($user['id_rol'] == $idRole) checked @endif
                                           @endif onclick="markCheck('{{ $user['id'] }}')">
                                    </td>
                                    <td class="col-xs-7">
                                        {{ $user['nombres'].' '.$user['apellidos'] }}
                                    </td>
                                    <td class="col-xs-3">
                                        {{ $user['username'] }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <input type="hidden" name="roleID" value="{{ $idRole }}">
                <div class="col-xs-12 mt-1">
                    <div class="row text-center text-dark">
                        <div class="col-md-4">
                            <span><i class="badge table-success py-1 px-2" aria-hidden="true">&nbsp;</i> Asignado</span>
                        </div>
                        <div class="col-md-4">
                            <span><i class="badge table-info py-1 px-2" aria-hidden="true">&nbsp;</i> No Asignado</span>
                        </div>
                        <div class="col-md-4">
                            <span><i class="badge table-warning py-1 px-2" aria-hidden="true">&nbsp;</i> Seleccionado</span>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-primary btnForm ripple"><i class="feather feather-save list-icon mr-r-10"></i> Guardar</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formRoles.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    searchTable('#table_roles_user','#search_roles_user')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
</script>