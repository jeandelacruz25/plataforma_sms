<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Perfil</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'usersRole') != 0 && searchMenuUsuario(auth()->id(), 'usersRole') != 0)
            <form id="formRole">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" name="nameRole" class="form-control" placeholder="Ingresa el nombre del perfil" value="{{ $nameRole }}">
                </div>
                <div class="form-group">
                    <label>Descripción</label>
                    <input type="text" name="descriptionRole" class="form-control" placeholder="Ingresa una breve descripción del perfil" value="{{ $descriptionRole }}">
                </div>
                @if($updateForm === false)
                    <div class="alert alert-icon alert-info fixLineHeight">
                        <i class="feather feather-info list-icon mr-r-10"></i> <strong>Recuerda que al crear un nuevo perfil, debes asignarle a que Menús podra acceder</strong>
                    </div>
                @endif
                <input type="hidden" name="roleID" value="{{ $idRole }}">
                <input type="hidden" name="statusID" value="{{ $statusRole ? $statusRole : '1' }}">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formRoles.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>