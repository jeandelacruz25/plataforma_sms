<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Actualizar Menú Cliente [{{ $dataCliente[0]['razon_social'] }}]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'clientes') != 0 && searchMenuUsuario(auth()->id(), 'clientes') != 0)
            <form id="formClienteMenu">
                <div class="container col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="sitemap-default sitemap">
                                <h5 class="box-title text-primary mb-1">Menú Del Sistema</h5>
                                <ul class="sitemap-list sitemap-list-icons list-unstyled fs-12">
                                    <div class="checkbox checkbox-primary">
                                        <label class="checkbox-checked">
                                            <input type="checkbox" class="checkGeneral" onclick="mark_all_menu('.checkGeneral', 'checkNew')"> <span class="label-text">Seleccionar todos los menús</span>
                                        </label>
                                    </div>
                                    @foreach ($menus as $key => $item)
                                        @if ($item['parent'] != 0)
                                            @break
                                        @endif
                                        @include('partials.menu-assign-item', ['item' => $item, 'subMenuCheck' => true, 'menuCheck' => $checkMenu])
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-icon alert-info border-info fixLineHeight" role="alert">
                                <i class="feather feather-info list-icon mr-r-10"></i> <strong>Es importante tener en cuenta :</strong>
                                <ul class="mr-t-10 pl-0 fs-12">
                                    <li>Al realizar un cambio al cliente, este mismo debe actualizar la plataforma para visualizar los cambios.</li>
                                </ul>
                            </div>
                            <input type="hidden" name="clienteID" value="{{ $dataCliente[0]['id'] }}">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Actualizar</button>
                                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCliente.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
</script>