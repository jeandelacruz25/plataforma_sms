<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm == true ? "Editar" : "Agregar" }} Cliente</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'clientes') != 0 && searchMenuUsuario(auth()->id(), 'clientes') != 0)
            <form id="formCliente" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <figure class="imgOverlay thumb-md">
                            <img class="img-thumbnail" id="imageCliente" src="{{ asset($dataCliente ? $dataCliente[0]['avatar'].'?version='.date('YmdHis') : 'img/clientes/img-default.svg?version='.date('YmdHis')) }}" />
                            <div class="middle">
                                <div class="fileUpload btn btn-primary">
                                    <span><i class="fa fa-upload list-icon"></i></span>
                                    <input type="file" class="upload" onchange="showImagePreview(this, '#imageCliente', 'avatarOriginalCliente')" name="avatarCliente" value="{{ $dataCliente ? $dataCliente[0]['avatar'] : '' }}"/>
                                </div>
                            </div>
                        </figure>
                        <p class="text-danger font-weight-bold">(128 x 128)</p>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <input type="text" name="nombreCliente" class="form-control" placeholder="Ingresa la razón social" value="{{ $dataCliente ? $dataCliente[0]['razon_social'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Número de RUC</label>
                            <input type="text" name="rucCliente" class="form-control" placeholder="Ingresa el número de RUC" value="{{ $dataCliente ? $dataCliente[0]['ruc'] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" name="telefonoCliente" class="form-control" placeholder="Ingrese el número de telefono" value="{{ $dataCliente ? $dataCliente[0]['telefono'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Correo de Contacto</label>
                            <input type="text" name="emailContacto" class="form-control" placeholder="Ingrese el correo de contacto" value="{{ $dataCliente ? $dataCliente[0]['correo_contacto'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Palabra Clave</label>
                            <input type="text" name="palabraClave" class="form-control" placeholder="Ingrese una palabra clave" value="{{ $dataCliente ? $dataCliente[0]['palabra_clave'] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" name="direccionCliente" class="form-control" placeholder="Ingrese la dirección" value="{{ $dataCliente ? $dataCliente[0]['direccion_cliente'] : '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sincronización con Score</label>
                            <div class="checkbox checkbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="checkSincronizacion" @if($updateForm) {{ $dataCliente[0]['score_async'] === '1' ? 'checked' : '' }} @endif>
                                    <span></span>
                                    Activar opciones de sincronización
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <input type="hidden" name="clienteID" value="{{ $dataCliente ? $dataCliente[0]['id'] : '' }}">
                        <input type="hidden" name="userCreate" value="{{ $dataCliente ? $dataCliente[0]['user_cre'] : '' }}">
                        <input type="hidden" name="userUpdate" value="{{ $dataCliente ? $dataCliente[0]['user_upd'] : '' }}">
                        <input type="hidden" name="dateCreate" value="{{ $dataCliente ? $dataCliente[0]['created_at'] : '' }}">
                        <input type="hidden" name="dateUpdate" value="{{ $dataCliente ? $dataCliente[0]['updated_at'] : '' }}">
                        <input type="hidden" name="statusID" value="{{ $dataCliente ? $dataCliente[0]['id_estado'] : '' }}">
                        <input type="hidden" name="avatarOriginalCliente" value="{{ $dataCliente ? $dataCliente[0]['avatar'] : '' }}">
                        <div>
                            <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
                <div class="row mr-t-5">
                    <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
                </div>
            </form>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCliente.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
</script>