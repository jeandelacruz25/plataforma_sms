<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Actualizar Reglas [{{ $dataCliente[0]['razon_social'] }}]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'clientes') != 0 && searchMenuUsuario(auth()->id(), 'clientes') != 0)
            <form id="formClienteRules">
                <div class="tabs">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item">
                            <a class="nav-link tabCampañas active show" href="#bases" data-toggle="tab" aria-expanded="true">
                                <span class="thumb-xxs">
                                    <i class="list-icon icon icon-users"></i> Bases
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link tabReportesSMS" href="#validacion" data-toggle="tab" aria-expanded="true">
                                <span class="thumb-xxs">
                                    <i class="list-icon icon icon-phone-wave"></i> Validación Telefonos
                                </span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="bases">
                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label for="numberValidate">Registros validos por campaña</label>
                                        <div class="form-input-icon"><i class="material-icons list-icon">done</i>
                                            <input class="form-control" name="documentValidate" placeholder="Ingrese la cantidad de documentos" type="text" value="{{ isset($dataRules['cantidad_documento_validaciones']) ? $dataRules['cantidad_documento_validaciones'] : 0 }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="validacion">
                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label for="numberValidate">Número de Canales</label>
                                        <div class="form-input-icon"><i class="material-icons list-icon">done</i>
                                            <input class="form-control" name="numberCanal" placeholder="Ingrese la cantidad de canales" type="text" value="{{ isset($dataRules['cantidad_numero_canales']) ? $dataRules['cantidad_numero_canales'] : 0 }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label for="numberValidate">Números a validar diariamente</label>
                                        <div class="form-input-icon"><i class="material-icons list-icon">done</i>
                                            <input class="form-control" name="numberValidate" placeholder="Ingrese la cantidad de números" type="text" value="{{ isset($dataRules['cantidad_numero_validaciones']) ? $dataRules['cantidad_numero_validaciones'] : 0 }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="sample3UserName" class="text-sm-right col-sm-4 col-form-label">Mas. Hora Inicio</label>
                                        <div class="col-sm-8">
                                            <div class="form-input-icon"><i class="material-icons list-icon">alarm</i>
                                                <input class="form-control timePicker" name="inicioMasivo" placeholder="Hora de inicio" type="text" value="{{ isset($dataRules['inicio_validacion_telefono']) ? $dataRules['inicio_validacion_telefono'] : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="sample3UserName" class="text-sm-right col-sm-4 col-form-label">Mas. Hora Fin</label>
                                        <div class="col-sm-8">
                                            <div class="form-input-icon"><i class="material-icons list-icon">alarm</i>
                                                <input class="form-control timePicker" name="finMasivo" placeholder="Hora de fin" type="text" value="{{ isset($dataRules['fin_validacion_telefono']) ? $dataRules['fin_validacion_telefono'] : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="clienteID" value="{{ $dataCliente[0]['id'] }}">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Actualizar</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCliente.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
    initFlatPickr('.timePicker', {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i"
    })
</script>