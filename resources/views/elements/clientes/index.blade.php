<div class="col-md-12">
    <div class="widget-heading widget-heading-border mlr0">
        <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
        <div class="widget-actions">
            <a href="javascript:void(0)" onclick="responseModal('div.dialogScoreLarge','formClientes',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family"><i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Cliente</a>
        </div>
    </div>
    <div class="widget-body plr0">
        <div id="dataTableClientesVue">
            <div v-if="initializeClientes">
                <div class="col-md-12">
                    <div class="cssload-loader"></div>
                </div>
            </div>
            <template v-else-if="!initializeClientes && dataClientes.length === 0 && !initDataPagination">
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Cliente" type="text" v-model="searchCliente">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="fetchClientes()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered centerTableImg nowrap">
                            <thead class="bg-primary">
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>N° RUC</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td colspan="5">No hay ningún cliente actualmente.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </template>
            <template v-else>
                <div class="widget-heading mlr0">
                    <div class="widget-title">&nbsp;</div>
                    <div class="widget-actions">
                        <div class="input-group">
                            <input class="form-control" placeholder="Buscar Cliente" type="text" v-model="searchCliente">
                            <div class="input-group-addon cursor-pointer bg-primary" @click="fetchClientes()">
                                <i class="feather feather-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body plr0">
                    <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                        <table class="table table-bordered centerTableImg dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead class="bg-primary">
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>N° RUC</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-for="(dataCliente, index) in dataClientes">
                                    <tr>
                                        <td v-html="pagination.from + index"></td>
                                        <td v-html="nameCliente[index]"></td>
                                        <td v-html="dataCliente.ruc"></td>
                                        <td v-html="statusCliente[index]"></td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="javascript:void(0)" class="btn btn-orange2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formClientes', dataCliente.id)" data-toggle="modal" data-target="#modalScore"><i title="Editar Cliente" class="fa fa-edit"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-rose2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formClientesMenu', dataCliente.id)" data-toggle="modal" data-target="#modalScore"><i title="Actualizar Menú" class="fa fa-bars"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-green2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formClientesRules', dataCliente.id)" data-toggle="modal" data-target="#modalScore"><i title="Reglas del Cliente" class="fa fa-shield"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-gray2 btn-sm" @click="clickEvent('div.dialogScore','formClientesStatus', dataCliente.id)" data-toggle="modal" data-target="#modalScore"><i title="Cambiar Estado" class="fa fa-refresh"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </template>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeClientes && dataClientes.length > 0 || !initializeClientes ? '' : 'disabled'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchClientes()"></pagination>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/dataTables/dataTableClientesVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmDataTableClientes.fetchClientes()
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
    deleteVariableSMSFunction()
</script>