<div class="widget-body plr0 pt0">
    <div class="tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link tabCampañas" href="#campañas" data-toggle="tab" aria-expanded="true">
                    <span class="thumb-xxs">
                        <i class="list-icon feather feather-layers"></i> Campañas Email
                    </span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="campañas">
                @include('elements.mailing.tabs.campaign.index', ['titleModule' => $titleCampaignModule, 'iconModule' => $iconCampaignModule])
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.tabCampañas').click()
    })

    deleteVariableSMSFunction()
</script>