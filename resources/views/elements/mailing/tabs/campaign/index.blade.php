<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="col-md-3 mr-2">
        <div class="input-group input-group-sm search">
            <input type="text" name="fechaFiltroCampania" class="form-control dateRangeCampania" placeholder="Escoga un rango de fechas">
            <div class="input-group-addon bg-primary" style="cursor:pointer;" onclick="serachDatatableCampania()">
                <i class="fa fa-search"></i>
            </div>
        </div>
    </div>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div class="mr-2">
                @if(auth()->user()->authorizeRoles(['Administrador']))
                    <select class="form-control selectClientes show-tick" data-live-search="true">
                        <option value="" selected>Todos los Clientes</option>
                        @foreach($selectOptions['clientes'] as $key => $value)
                            <option value="{{ ucwords(\Illuminate\Support\Str::lower($value['razon_social'])) }}">{{ ucwords($value['razon_social']) }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formCampaign',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Campaña
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div class="d-block loadDatatableCampaign loadlistCampaign">
        <div class="cssload-loader"></div>
    </div>
    <div class="d-none drawDatatableCampaign drawlistCampaign">
        <table id="listCampaign" class="table table-bordered centerTableImg dt-responsive nowrap" cellspacing="0" width="100%">
            <thead class="bg-primary">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Nombre Campana</th>
                <th>Cliente</th>
                <th>Estado Envio</th>
                <th>N° Correos</th>
                <th>Porcentaje de Envio</th>
                <th>Fecha Envio</th>
                <th data-priority="1">Acciones</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    initSelectPicker('.selectClientes', {
        style: "badge bg-facebook px-3 heading-font-family fs-11 text-white"
    })

    initDateRangePicker('.dateRangeCampania', {
        locale: {
            format: 'YYYY-MM-DD',
            customRangeLabel: 'Escoger Rango',
            separator: ' / ',
            applyLabel: 'Aplicar',
            cancelLabel: 'Cerrar',
        },
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Hace 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'center'
    })

    filterSelectDatatable('.selectClientes','#listCampaign',2)
</script>