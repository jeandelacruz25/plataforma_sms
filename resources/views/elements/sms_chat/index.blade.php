<div class="widget-holder col-md-12">
    <div class="widget-bg-transparent">
        <div class="widget-heading">
            <div class="widget-title">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#smsCampana" class="nav-link tabSMSCampana" data-toggle="tab" aria-expanded="true" onclick="vmFront.nameRoute = ucwords('Chat SMS')">
                        <span class="thumb-xxs">
                            <i class="list-icon fa fa-envelope"></i> SMS C/ Campaña
                        </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#smsSinCampana" class="nav-link tabSMSSinCampana" data-toggle="tab" aria-expanded="false" onclick="vmFront.nameRoute = ucwords('Chat SMS Sin Campaña')">
                        <span class="thumb-xxs">
                            <i class="list-icon fa fa-comment"></i> SMS S/ Campaña
                        </span>
                        </a>
                    </li>
                </ul>
            </div>
            @if(auth()->user()->authorizeRoles(['Administrador']))
                <div class="widget-actions mr-10">
                    <select class="form-control selectClientes show-tick" data-live-search="true" onchange="vmSMSChatCampaign.getlistChatCampaign(this.value, '#chatNumbers', '#chatWindow'); vmSMSChatNotCampaign.getlistChatNotCampaign(this.value, '#chatNumbersNot', '#chatWindowNot')">
                        @foreach($selectOptions['clientes'] as $key => $value)
                            <option value="{{ $value['id'] }}" {{ auth()->user()->id_cliente == $value['id'] ? 'selected' : '' }}>{{ ucwords($value['razon_social']) }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="smsCampana" aria-expanded="true">
        @include('elements.sms_chat.tabs.sms_campana')
    </div>
    <div role="tabpanel" class="tab-pane" id="smsSinCampana" aria-expanded="false">
        @include('elements.sms_chat.tabs.sms_sin_campana')
    </div>
</div>
<script>
    $('.tabSMSCampana').click()
    vmSMSChatCampaign.getlistChatCampaign()
    vmSMSChatNotCampaign.getlistChatNotCampaign()
    initSelectPicker('.selectClientes', {
        style: "badge bg-facebook px-3 heading-font-family fs-14 text-white"
    })
</script>