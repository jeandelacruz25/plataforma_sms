<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Cambiar Chats a Visto</h5>
    </div>
    <div class="modal-body">
        <form id="formSeenDetalleSMS">
            <div class="form-group text-center">
                <label>¿Deseas cambiar los chats seleccionados a leido?</label>
            </div>
            <input type="hidden" name="notCampaign" value="{{ $notCampaign }}">
            <div class="alert alert-icon alert-danger border-danger formError d-none"></div>
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Si</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formSMS.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>