<div id="smsChatNotCampaignVue" class="row">
    <div class="col-md-12 widget-holder widget-full-content border-all px-0">
        <div class="widget-bg">
            <div class="widget-body clearfix">
                <div class="row no-gutters">
                    <div class="col-md-3 mail-sidebar rounded-top rounded-bottom">
                        <h5 class="pl-3 mt-3 mb-0 fs-16 text-primary">
                            <i class="list-icon fa fa-list-ul"></i> Lista BulkID
                        </h5>
                        <div class="mail-inbox-header">
                            <div class="form-group mb-0">
                                <input id="search_list_not_campaign" type="search" class="form-control form-control-rounded pr-xl-5 fs-13 heading-font-family inputSearch" placeholder="Busca tu bulkID"> <i class="feather feather-search pos-absolute pos-right vertical-center mr-3"></i>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="vuebar-element-sms" v-bar>
                                <table id="table_list_not_campaign" class="mail-list contact-list table-responsive listChatCampaign">
                                    <tbody>
                                        <template v-if="listChatNotCampaign.length === 0">
                                            <div class="row mt-3">
                                                <div class="alert alert-info fixLineHeight">
                                                    <i class="fa fa-info list-icon mr-r-10"></i> No hay bulkID disponibles o la data no se cargo correctamente
                                                    <span class="badge bg-color-scheme text-uppercase fs-10 py-2 mx-2 mr-l-70 align-middle" style="cursor: pointer" onclick="vmSMSChatNotCampaign.getlistChatNotCampaign()">Actualizar</span>
                                                </div>
                                            </div>
                                        </template>
                                        <template v-else v-for="(item, index) in listChatNotCampaign">
                                            <tr class="cursor-pointer" @click="getListChatNumbers(item.id_bulk_sms, '#chatNumbersNot', '#chatWindowNot', 'post')">
                                                <td class="contact-list-user">
                                                    <label class="mail-select-checkbox">
                                                        <i data-tooltip="tooltip" data-placement="bottom" :title="item.id_bulk_sms" class="icon icon-bubble7"></i>
                                                    </label>
                                                </td>
                                                <td class="contact-list-message">
                                                    <a href="javascript:void(0)"><span v-html="nameNotCampaign[index]"></span></a><br>
                                                    <small v-html="item.cliente.razon_social"></small>
                                                    <small class="d-none" v-html="item.id_bulk_sms"></small>
                                                </td>
                                                <td class="contact-list-badge">
                                                    <span class="badge text-uppercase fs-10 py-2 mx-2 mr-l-60 align-middle" v-bind:class="item.total_received == 0 ? 'bg-primary' : 'bg-danger'">
                                                        <i class="icon icon-bubble6 list-icon mr-r-10 text-white"></i><span v-html="item.total_received"></span>
                                                    </span>
                                                </td>
                                            </tr>
                                        </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="mail-sidebar rounded-top rounded-bottom" v-bind:class="idBulkSMS != 0 ? 'col-md-4' : 'col-md-9'">
                        <h5 class="pl-3 mt-4 mb-1 text-facebook" v-if="idBulkSMS != 0">
                            <span class="thumb-xxs">
                                <i class="list-icon fa fa-list-ul"></i> Lista Chats
                            </span>
                        </h5>
                        <div id="chatNumbersNot">
                            <div class="px-4 mr-t-100">
                                <div class="text-center mt-4 mb-4">
                                    <img alt="" src="{{ asset('img/select.png') }}">
                                </div>
                                <label class="text-center width-100">Seleccione un bulkID</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 mail-sidebar rounded-top rounded-bottom" v-if="idBulkSMS != 0">
                        <div id="chatWindowNot">
                            <div class="px-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/smsChat/notCampaign/smsChatNotCampaignVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmFront.chatSMSUbicationNotCampaign = 'smsSinCampana'
    searchTable('#table_list_not_campaign','#search_list_not_campaign')
</script>