<div id="smsChatNumbersNotVue">
    <div :class="[listChatNumbersNot.length ? 'mail-inbox-header' : 'mail-inbox-header disabled']">
        <div class="mail-inbox-tools d-flex align-items-center">
            <span class="checkbox checkbox-primary bw-3 heading-font-family fs-14 fw-semibold headings-color mail-inbox-select-all mr-r-20">
                <label>
                    <input type="checkbox" v-model="selectAll"> <span class="label-text">Seleccionar todos</span>
                </label>
            </span>
            <div class="btn-group">
                <a href="javascript:void(0)" class="btn btn-sm btn-link mr-2 text-muted" onclick="vmSMSChatNotCampaign.getListChatNumbers(vmSMSChatNotCampaign.idBulkSMS, '#chatNumbersNot', '#chatWindowNot', 'post')">
                    <i class="list-icon fs-18 feather feather-refresh-cw"></i>
                </a>
                <template v-if="selected.length > 0">
                    <a href="javascript:void(0)" class="btn btn-sm btn-link mr-2 text-muted" onclick="responseModal('div.dialogScore','formChatNumberNotSeen', {}, 'get')" data-toggle="modal" data-target="#modalScore">
                        <i class="list-icon fs-18 feather feather-eye"></i>
                    </a>
                </template>
            </div>
        </div>
    </div>
    <div class="px-4">
        <div class="vuebar-element-sms" v-bar>
            <table class="mail-list contact-list table-responsive listChatNumers">
                <tbody>
                    <template v-for="(item, index) in listChatNumbersNot">
                        <tr>
                            <td class="contact-list-user">
                                <label class="mail-select-checkbox mr-r-10">
                                    <template v-if="item.leido_sms == '1'">
                                        <input type="checkbox" v-model="selected" :value="item.id">
                                    </template>
                                    <figure class="thumb-xs2">
                                        <i data-tooltip="tooltip" data-placement="bottom" :title="item.mensaje_usuario" class="icon icon-bubble-lines4"></i>
                                    </figure>
                                </label>
                            </td>
                            <td class="contact-list-message mr-r-10 w-100 cursor-pointer" @click="getListChatWindow(item.id_bulk_sms, item.telefono_usuario, '#chatWindowNot', 'post')">
                                <a href="javascript:void(0)"><span class="contact-list-phone d-block" v-html="'+' + item.telefono_usuario"></span></a>
                                <small class="text-gray-800" v-html="item.leido_sms === 0 ? mensajeUsuario[index] : item.mensaje_usuario.substring(0,30)+'..'"></small>
                            </td>
                            <td class="contact-list-badge" v-if="item.leido_sms == '1'">
                                <span class="badge bg-danger-contrast text-uppercase fs-10 py-2 mx-2 mr-l-70 align-middle">
                                   NUEVO
                                </span>
                            </td>
                            <td v-else>
                                <span class="badge bg-transparent text-uppercase fs-10 py-2 mx-2 mr-l-70 align-middle">&nbsp;</span>
                            </td>
                        </tr>
                    </template>
                    <template v-if="listChatNumbersNot.length === 0">
                        <div class="row mt-3">
                            <div class="alert alert-info fixLineHeight">
                                <i class="fa fa-info list-icon mr-r-10"></i> No hay ningun SMS enviado en esta campaña
                            </div>
                        </div>
                    </template>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/smsChat/notCampaign/smsChatNumbersNotVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmFront.chatSMSUbicationNotCampaign = 'smsChatListNot'
    vmSMSChatNumbersNot.idBulkSMS = '{{ $idCampana }}'
    vmSMSChatNumbersNot.getlistChatNumbers()
</script>