<div class="col-md-12 widget-holder widget-full-contentborder-all px-0">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title"><i class="icon icon-cog"></i> Visualizar Chat C/ Campaña</h5>
        </div>
        <div class="widget-body plr0 clearfix border-all mr-t-20">
            <div class="row no-gutters">
                <div class="mail-sidebar rounded-top rounded-bottom col-md-5">
                    <h5 class="pl-3 mt-3 mb-0 fs-16 text-primary">
                        <span class="thumb-xxs">
                            <i class="list-icon fa fa-list-ul"></i> Lista Chats
                        </span>
                    </h5>
                    <div id="chatNumbers">
                        <div id="smsChatNumbersVue">
                                <div class="mail-inbox-header">
                                    <div class="mail-inbox-tools d-flex align-items-center">
                                        <span class="checkbox checkbox-primary bw-3 heading-font-family fs-14 fw-semibold headings-color mail-inbox-select-all mr-r-20">
                                            <label>
                                                <input type="checkbox" v-model="selectAll"> <span class="label-text">Seleccionar todos</span>
                                            </label>
                                        </span>
                                        <div class="btn-group">
                                            <template v-if="selected.length > 0">
                                                <a href="javascript:void(0)" class="btn btn-sm btn-link mr-2 text-muted" onclick="responseModal('div.dialogScore','formChatNumberSeen', {}, 'get')" data-toggle="modal" data-target="#modalScore">
                                                    <i class="list-icon fs-18 feather feather-eye"></i>
                                                </a>
                                            </template>
                                        </div>
                                    </div>
                                </div>
                                <div class="px-4">
                                    <div class="vuebar-element-sms" v-bar>
                                        <table class="mail-list contact-list table-responsive listChatNumers">
                                            <tbody>
                                                <template v-for="(item, index) in listChatNumbers">
                                                    <tr>
                                                        <td class="contact-list-user">
                                                            <label class="mail-select-checkbox mr-r-10">
                                                                <template v-if="item.leido_sms == '1'">
                                                                    <input type="checkbox" v-model="selected" :value="item.id">
                                                                </template>
                                                                <figure class="thumb-xs2">
                                                                    <i data-tooltip="tooltip" data-placement="bottom" :title="item.mensaje_usuario" class="icon icon-bubble-lines4"></i>
                                                                </figure>
                                                            </label>
                                                        </td>
                                                        <td class="contact-list-message mr-r-10 w-100 cursor-pointer" @click="getChatView(item.id_campana, item.id_bulk_sms, item.telefono_usuario, '#chatWindow', 'post')">
                                                            <a href="javascript:void(0)"><span class="contact-list-phone d-block" v-html="'+' + item.telefono_usuario"></span></a>
                                                            <small class="text-gray-800" v-html="item.leido_sms === 0 ? mensajeUsuario[index] : item.mensaje_usuario.substring(0,30)+'..'"></small>
                                                        </td>
                                                        <td class="contact-list-badge" v-if="item.leido_sms == '1'">
                                                            <span class="badge bg-danger-contrast text-uppercase fs-10 py-2 mx-2 mr-l-70 align-middle">
                                                               NUEVO
                                                            </span>
                                                        </td>
                                                        <td v-else>
                                                            <span class="badge bg-transparent text-uppercase fs-10 py-2 mx-2 mr-l-70 align-middle">&nbsp;</span>
                                                        </td>
                                                    </tr>
                                                </template>
                                                <template v-if="listChatNumbers.length === 0">
                                                    <div class="row mt-3">
                                                        <div class="alert alert-info" >
                                                            <i class="fa fa-info list-icon mr-r-10"></i> No hay ningun SMS enviado en esta campaña
                                                        </div>
                                                    </div>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-7 mail-sidebar rounded-top rounded-bottom">
                    <div id="chatWindow">
                        <div class="px-4 mr-t-100">
                            <div class="text-center mt-4 mb-4">
                                <img alt="" src="{{ asset('img/select.png') }}">
                            </div>
                            <label class="text-center width-100">Seleccione un Chat</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/smsChat/campaign/smsChatNumbersVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmFront.nameRoute = 'Visualizar Chat'
    vmFront.chatSMSUbicationCampaign = 'smsChatList'
    vmSMSChatNumbers.idBulkSMS = '{{ $idCampana }}'
    vmSMSChatNumbers.getlistChatNumbers()
    var chatPreviewIDCampaign = '{{ $idCampana }}'
    Object.keys(vmFront.listNotificationSMS).filter(key => vmFront.listNotificationSMS[key].id_campana === chatPreviewIDCampaign).some((item, index) => { vmFront.listNotificationSMS.splice(item, 1) })
    vmFront.setViewSMSDetalle(0, chatPreviewIDCampaign)
</script>