<div id="smsChatWindowVue" class="chatbox row no-gutters" style="height: 60vh">
    <div class="chatbox-chat-area w-100">
        <div class="chatbox-body">
            <div class="vuebar-element-sms" v-bar>
                <div id="chatContainer" class="chatbox-messages">
                    <template v-for="(item, index) in listChatWindow">
                        <div v-bind:class="isSystem[index]">
                            <figure class="avatar thumb-xs mr-b-0">
                                <a href="javascript:void(0)">
                                    <img :src="isAvatarSystem[index]" class="rounded-circle">
                                </a>
                            </figure>
                            <div class="media-body">
                                <p class="mw-100 fixLineHeightChat" v-html="item.mensaje_sms"></p>
                                <small class="text-gray-500" v-html="isTimeChat[index]"></small>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
        <form id="formChatSMS" class="chatbox-form pb-0 pt-3 border-top d-flex align-items-center pos-relative">
            <div v-bind:class="mensajeChatCampaign.length === 0 ? 'col-md-11' : 'col-md-10'">
                <div class="form-group flex-1 mb-0 ml-4">
                    <input id="smsChat" type="text" name="smsChat" class="form-control form-control-rounded pd-r-90 border border-primary" placeholder="Ingresa tu mensaje aqui !" v-model="mensajeChatCampaign">
                    <input type="hidden" name="smsBulkSMS" v-model="idBulkSMS">
                    <input type="hidden" name="smsIdFrom[]" v-model="idFrom">
                </div>
            </div>
            <div class="col-md-2 mb-0 mr-4" v-bind:class="mensajeChatCampaign.length === 0 ? 'd-none disabled' : ''">
                <button type="submit" class="btn btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnForm"><i class="fa fa-send list-icon mr-r-10" aria-hidden='true'></i> Enviar</button>
                <button type="button" class="btn btn-info btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnLoad d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
            </div>
        </form>
    </div>
</div>
<div class="mr-t-10" style="position: absolute;bottom: 100px; width: 100%;">
    <div class="col-md-12">
        <div class="alert alert-icon alert-danger py-1 mb-0 border-danger formError d-none"></div>
    </div>
</div>
<script src="{!! asset('js/vue/smsChat/campaign/smsChatWindowVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/form/formSMS.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmFront.chatSMSUbicationCampaign = 'smsChatWindow'
    vmSMSChatWindow.idBulkSMS = '{{ $remitenteSMS }}'
    vmSMSChatWindow.idFrom = '{{ $telefonoSMS }}'
    vmSMSChatWindow.getListChatWindow()
</script>