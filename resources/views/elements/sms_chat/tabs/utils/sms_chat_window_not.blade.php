<div id="smsChatWindowNotVue" class="chatbox row no-gutters" style="height: 60vh">
    <div class="chatbox-chat-area w-100">
        <div class="chatbox-body">
            <div class="vuebar-element-sms" v-bar>
                <div id="chatContainerNot" class="chatbox-messages">
                    <template v-for="(item, index) in listChatWindowNot">
                        <div v-bind:class="isSystem[index]">
                            <figure class="avatar thumb-xs mr-b-0">
                                <a href="javascript:void(0)">
                                    <img :src="isAvatarSystem[index]" class="rounded-circle">
                                </a>
                            </figure>
                            <div class="media-body">
                                <p class="mw-100 fixLineHeightChat" v-html="item.mensaje_sms"></p>
                                <small class="text-gray-500" v-html="isTimeChat[index]"></small>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
        <form id="formChatNotSMS" class="chatbox-form pt-3 pb-0 border-top d-flex align-items-center pos-relative">
            <div v-bind:class="mensajeChatNotCampaign.length === 0 ? 'col-md-11' : 'col-md-10'">
                <div class="form-group flex-1 mb-0 ml-4">
                    <input id="smsChatNot" type="text" name="smsChat" class="form-control form-control-rounded pd-r-90 border border-primary" placeholder="Ingresa tu mensaje aqui !" v-model="mensajeChatNotCampaign">
                    <input type="hidden" name="smsBulkSMS" v-model="idBulkSMS">
                    <input type="hidden" name="smsIdFrom[]" v-model="idFrom">
                </div>
            </div>
            <div class="col-md-2 mb-0 mr-4" v-bind:class="mensajeChatNotCampaign.length === 0 ? 'd-none disabled' : ''">
                <button type="submit" class="btn btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnForm"><i class="fa fa-send list-icon mr-r-10" aria-hidden='true'></i> Enviar</button>
                <button type="button" class="btn btn-info btn-rounded btn-color-scheme pos-absolute pos-right vertical-center border-hidden mr-3 btnLoad d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
            </div>
        </form>
    </div>
</div>
<div class="mr-t-10">
    <div class="col-md-12">
        <div class="alert alert-icon alert-danger border-danger formError d-none"></div>
    </div>
</div>
<script src="{!! asset('js/vue/smsChat/notCampaign/smsChatWindowNotVue.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/form/formSMS.js?version='.date('YmdHis')) !!}"></script>
<script>
    vmFront.chatSMSUbicationNotCampaign = 'smsChatWindowNot'
    vmSMSChatWindowNot.idBulkSMS = '{{ $remitenteSMS }}'
    vmSMSChatWindowNot.idFrom = '{{ $telefonoSMS }}'
    vmSMSChatWindowNot.getListChatWindow()
</script>