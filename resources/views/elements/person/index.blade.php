<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
</div>
<div class="tabs mt-3">
    <ul class="nav nav-tabs nav-justified">
        <li class="nav-item">
            <a class="nav-link tabSearchIndividual" href="#sIndividual" data-toggle="tab" onclick="loadSearchIndividual()">
                <span class="thumb-xxs">
                    <i class="list-icon icon icon-user3"></i> Búsqueda Individual
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabSearchMasiva" href="#sMasivo" data-toggle="tab" onclick="loadSearchMasivo()">
                <span class="thumb-xxs">
                    <i class="list-icon icon icon-users4"></i> Búsqueda Masiva
                </span>
            </a>
        </li>
    </ul>
    <div class="tab-content plr0">
        <div class="tab-pane" id="sIndividual">
            @include('elements.person.tabs.individual.index')
        </div>
        <div class="tab-pane" id="sMasivo">
            @include('elements.person.tabs.masiva.index')
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.tabSearchIndividual').click()
    })
    deleteVariableSMSFunction()
</script>