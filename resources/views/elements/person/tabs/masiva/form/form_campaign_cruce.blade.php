<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Iniciar Cruce [{{ $dataCampaign[0]['nombre_campana'] }}]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
            @if($countCampaign == 0)
                @if($dataCampaign[0]['total_documentos'] > 0)
                    <form id="formCampaignBaseCruce">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-icon alert-info fixLineHeight">
                                    <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                                    <p class="fs-12">
                                        <span>- Este proceso puede tomar un tiempo promedio de mas de 10 minutos.</span><br>
                                        <span>- Se le notificara cuando se haya acabado el proceso.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btnForm ripple"><i class='fa fa-play list-icon mr-r-10' aria-hidden='true'></i> Iniciar</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No Iniciar</button>
                        </div>

                        <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
                        <input type="hidden" name="clienteID" value="{{ $dataCampaign[0]['id_cliente'] }}">
                    </form>
                    <div class="row mr-t-5">
                        <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-danger fixLineHeight">
                            <i class="feather feather-x list-icon mr-r-10"></i> <strong>No puedes iniciar el cruce, ya que esta campaña no cuenta con ningun documento.</strong>
                        </div>
                    </div>
                @endif
            @else
                <div class="alert alert-icon alert-danger fs-12 fixLineHeight">
                    <i class="feather feather-alert-circle list-icon mr-r-10"></i>
                    <strong>Hay un cruce en curso, favor de esperar que termine para volver a iniciar otro cruce.</strong>
                    <p class="font-weight-bold">* Campaña iniciada por el usuario : {{ $dataCampaign[0]['user']['nombres'].' '.$dataCampaign[0]['user']['apellidos'] }}</p>
                </div>
            @endif
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCampanaBase.js?version='.date('YmdHis')) !!}"></script>
<script>
    initDateRangePicker('.dateEnvio', {
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss',
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar"
        },
        startDate: moment(),
        opens: 'left'
    })

    $('.dateEnvio').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'))
    })

    $('.dateEnvio').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('')
    })

    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>