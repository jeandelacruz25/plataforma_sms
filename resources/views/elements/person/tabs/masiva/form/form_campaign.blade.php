<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Campaña</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
            <form id="formCampaign">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nombre Campaña</label>
                            <input type="text" name="nameCampaign" class="form-control" placeholder="Ingresa el nombre de la campaña" value="{{ $dataCampaign ? $dataCampaign[0]['nombre_campana'] : '' }}">
                        </div>
                    </div>
                    @if(Auth::user()->authorizeRoles(['Administrador']))
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cliente</label>
                                <select name="clienteCampaign" class="form-control selectpicker show-tick" data-live-search="true">
                                    @foreach($options['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}" @if($dataCampaign) {{ $dataCampaign[0]['id_cliente'] === $value['id'] ? 'selected' : '' }} @endif>{{ ucwords($value['razon_social']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Criterio Principal</label>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="2" @if($updateForm === false) checked @elseif($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 2) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Telefonos
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Otros Criterios</label>
                            <div class="row">
                                <div class="col-sm">
                                    <!-- <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="todosCriterios" class="checkGeneral" onclick="mark_all_menu('.checkGeneral', 'checkChild')">
                                            <span></span>
                                            Todos los criterios
                                        </label>
                                    </div> -->
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="1" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 1) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Datos Personales
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="3" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 3) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Direcciones
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="4" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 4) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Familiares
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="5" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 5) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Sueldos
                                        </label>
                                    </div>
                                    <!-- <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="6" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 6) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Sunat
                                        </label>
                                    </div> -->
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="7" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 7) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Financiero
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="8" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 8) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Autos
                                        </label>
                                    </div>
                                </div>
                                <!-- <div class="col-sm">
                                    <div class="checkbox checkbox-switch switch-primary">
                                        <label>
                                            <input type="checkbox" name="checkCriterio[]" class="checkChild" value="8" @if($updateForm) {{ search($dataCampaign[0]['criterios'], 'id_criterio', 8) ? 'checked disabled' : 'disabled' }} @endif>
                                            <span></span>
                                            Autos
                                        </label>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if($updateForm === false)
                            <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                                <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                                <p>- Al crear una nueva campaña, debes pasar a subir la base de documentos que pertenceran a dicha campaña.</p>
                                <p>- Recomendamos elegir el criterio telefonos, ya que es mucho mas rapido de procesar.</p>
                                <p>- Los criterios son elegidos una sola vez, luego de crearlo no se podra cambiar.</p>
                            </div>
                        @endif
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="campaignID" value="{{ $dataCampaign ? $dataCampaign[0]['id'] : '' }}">
                <input type="hidden" name="totalDocumentos" value="{{ $dataCampaign ? $dataCampaign[0]['total_documentos'] : '' }}">
                <input type="hidden" name="estadoBase" value="{{ $dataCampaign ? $dataCampaign[0]['estado_base'] : '' }}">
                <input type="hidden" name="fechaEnvio" value="{{ $dataCampaign ? $dataCampaign[0]['fecha_envio'] : '' }}">
                <input type="hidden" name="userCreated" value="{{ $dataCampaign ? $dataCampaign[0]['user_created'] : '' }}">
                <input type="hidden" name="statusID" value="{{ $dataCampaign ? $dataCampaign[0]['id_status'] : '' }}">
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCampanaBase.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>