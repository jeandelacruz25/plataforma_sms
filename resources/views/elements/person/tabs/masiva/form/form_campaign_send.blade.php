<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Validación Telefonos [{{ $dataCampaign[0]['nombre_campana'] }}]</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
            <form id="formBaseValidatePhone">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nombre Campaña</label>
                            <input type="text" name="nameCampaign" class="form-control" placeholder="Ingresa el nombre de la campaña" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                            <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                            <p>- Al realizar una nueva campaña, seras redireccionado automaticamente a la vista de validación de telefonos par poder iniciar la validación.</p>
                        </div>
                    </div>
                </div>
                <div class=" col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btnForm ripple"><i class='fa fa-plus list-icon mr-r-10' aria-hidden='true'></i> Crear</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> No Enviar</button>
                </div>
                <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
                <input type="hidden" name="clienteID" value="{{ $dataCampaign[0]['id_cliente'] }}">
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formCampanaBase.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
</script>