<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Cambiar Estado [{{ $dataCampaign[0]['nombre_campana'] }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formStatusCampaignBase">
            <div class="form-group">
                <label class="pb-2">Recuerda que cambiaras el estado <span class="badge px-3 securitec {{ $dataCampaign[0]['status']['color'] }}">{{ $dataCampaign[0]['status']['nombre'] }}</span> a :</label>
                <select name="statusCampaign" class="form-control selectpicker show-tick">
                    <option value="">Seleccione un estado</option>
                    @foreach($options as $key => $value)
                        <option data-content="<span class='badge px-3 securitec {{ $value['color'] }}'>{{ mb_strtoupper($value['nombre']) }}</span>" value="{{ $value['id'] }}">{{ $value['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
            <div class="text-center">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Actualizar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampanaBase.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker')
</script>