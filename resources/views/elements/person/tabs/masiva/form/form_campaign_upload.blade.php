<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Subir Base de Documentos</h5>
    </div>
    <div class="modal-body">
        <form id="formCampaignUpload">
            @if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
                @if($validationRules)
                    @if($documentValidation)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="pull-left">
                                        <label>Base de Documentos (.csv, .xlsx, .xls)</label>
                                    </div>
                                    <div class="pull-right">
                                        <label class="@if($countDocumentValidation <= 100) text-danger @endif">Documentos restantes : {{ $countDocumentValidation }}</label>
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <input type="file" name="baseClientes" class="baseClientes">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-facebook btnForm ripple"><i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar</button>
                                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="alert alert-icon alert-info fs-12 fixLineHeight">
                                    <i class="feather feather-info list-icon mr-r-10"></i> <strong>Debes tener en cuenta lo siguiente : </strong>
                                    <div class="pull-right">
                                        <a href="javascript:void(0)" onclick="downloadFile('{{ asset('examples/ejemplo_base_masivo.xlsx') }}')" class="btn-orange2 px-3">
                                            Descargar Ejemplo
                                        </a>
                                    </div>
                                    <p>
                                        <span>- La base debe contener la cabecera : <b>dni</b>.</span><br>
                                        <span>- Documentos maximos a subir dependerá de su supscripción.</span><br>
                                        <span>- No debes subir documentos duplicados (Recuerda que tambien sera un costo).</span><br>
                                        <span>- La base puede seguir aumentando, siempre y cuando no hayas aún iniciado el cruce.</span><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="campaignID" value="{{ $dataCampaign[0]['id'] }}">
                        <input type="hidden" name="totalDocumentos" value="{{ $dataCampaign[0]['total_documentos'] }}">
                        <input type="hidden" name="clienteID" value="{{ $dataCampaign[0]['id_cliente'] }}">
                        <input type="hidden" name="documentCliente" value="{{ $countDocumentValidation }}">
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-icon alert-danger fs-12 fixLineHeight">
                                    <i class="feather feather-info list-icon mr-r-10"></i> <strong>Llegaste al límite de tu licencia para poder subir documentos por campaña.</strong>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                                <i class="feather feather-info list-icon mr-r-10"></i> <strong>No cuentas con una regla definida, favor de ponerse en contacto con soporte.</strong>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-danger fixLineHeight">
                            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            @endif
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampanaBase.js?version='.date('YmdHis')) !!}"></script>
<script>
    bootstrapFile('.baseClientes',{
        dragdrop: true,
        text: 'Subir Archivo',
        btnClass: 'btn-facebook fs-15',
        size: 'sm',
        placehold: 'Solo archivos .csv, .xls, .xlsx',
        htmlIcon: '<span class="feather feather-upload list-icon mr-r-10"></span>&nbsp;'
    })
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>