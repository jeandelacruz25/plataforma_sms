<div class="widget-heading widget-heading-border mlr0">
    <h5 class="widget-title">&nbsp;</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','formCampaignBase',{},'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Campaña
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableBaseCampanasVue">
        <div v-if="initializeBaseCampanas">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeBaseCampanas && dataBaseCampanas.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchBaseCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="paginationFetchBaseCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchBaseCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table id="listCampaignBase" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre Campana</th>
                                <th>N° Doc</th>
                                <th>Fecha Envio</th>
                                <th>Estado de Cruce</th>
                                <th>Estado Envio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td colspan="6">
                                    No hay ninguna campaña creada actualmente.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2">
                            <div class="input-group">
                                <input type="text" name="fechaFiltroCampania" class="form-control dateRangeCampania" placeholder="Escoga un rango de fechas" v-model="searchDateRange">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchBaseCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="paginationFetchBaseCampanas()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Campaña" type="text" v-model="searchCampana">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchBaseCampanas()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table id="listCampaignBase" class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre Campana</th>
                                <th>N° Doc</th>
                                <th>Estado Envio</th>
                                <th>Estado de Cruce</th>
                                <th>Fecha Envio</th>
                                <th>Fecha Fin</th>
                                <th data-priority="1">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataBaseCampana, index) in dataBaseCampanas">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameCampana[index]"></td>
                                    <td v-html="dataBaseCampana.total_documentos"></td>
                                    <td v-html="statusBase[index]"></td>
                                    <td v-html="statusCruce[index]"></td>
                                    <td v-html="dateSend[index]"></td>
                                    <td v-html="dateFinish[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <template v-if="parseInt(dataBaseCampana.estado_base) === 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formCampaignBase', dataBaseCampana.id)" data-toggle="modal" data-target="#modalScore" title="Editar Campaña" class="btn btn-orange2 btn-sm"><i class="fa fa-edit"></i></button>
                                            </template>
                                            <template v-if="parseInt(dataBaseCampana.estado_base) === 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formCampaignBaseUpload', dataBaseCampana.id)" data-toggle="modal" data-target="#modalScore" title="Subir Base de Documentos" class="btn btn-yellow2 btn-sm"><i class="fa fa-upload"></i></button>
                                            </template>
                                            <template>
                                                <div class="btn-group dropdown m-r-10">
                                                    <button aria-expanded="false" data-toggle="dropdown" class="btn btn-green2 btn-sm dropdown-toggle" type="button">
                                                        <i class="fa fa-file-excel-o"></i>
                                                    </button>
                                                    <div role="menu" class="dropdown-menu fs-13">
                                                        <a class="dropdown-item" href="javascript:void(0)"><strong>Reportes <i class="fa fa-download"></i></strong></a>
                                                        <template v-if="dataBaseCampana.criterios_count_count === dataBaseCampana.criterios_count"><a class="dropdown-item" href="javascript:void(0)" @click="clickDownload('/downloadReportBase', { idCampana: dataBaseCampana.id, typeDownload: 0 })">Todos</a></template>
                                                        <template v-if="(dataBaseCampana.criterios_finish).length > 0">
                                                            <template v-for="(criterios, index) in dataBaseCampana.criterios_finish">
                                                                <a class="dropdown-item" href="javascript:void(0)" @click="clickDownload('/downloadReportBase', { idCampana: dataBaseCampana.id, typeDownload: parseInt(criterios.id_criterio) })" v-html="nameCriterio(criterios.id_criterio)"></a>
                                                            </template>
                                                        </template>
                                                        <template v-else>
                                                            <a class="dropdown-item" href="javascript:void(0)"> Sin criterios culminados </a>
                                                        </template>
                                                    </div>
                                                </div>
                                            </template>
                                            <template v-if="(parseInt(dataBaseCampana.estado_base) === 0 || parseInt(dataBaseCampana.id_status) === 2) && parseInt(dataBaseCampana.total_documentos) != 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formBaseCruce', dataBaseCampana.id)" data-toggle="modal" data-target="#modalScore" title="Iniciar Cruce" class="btn btn-purple-dark btn-sm"><i class="fa fa-play"></i></button>
                                            </template>
                                            <!-- <template v-if="parseInt(dataBaseCampana.criterio_phone_count) > 0">
                                                <button type="button" @click="clickEvent('div.dialogScore','formBaseSMS', dataBaseCampana.id)" data-toggle="modal" data-target="#modalScore" title="Validar Telefonos" class="btn btn-rose2 btn-sm"><i class="icon icon-phone-wave"></i></button>
                                            </template> -->
                                            <button type="button" @click="clickEvent('div.dialogScore','formCampaignBaseStatus', dataBaseCampana.id)" data-toggle="modal" data-target="#modalScore" title="Cambiar Estado" class="btn btn-gray2 btn-sm"><i class="fa fa-refresh"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeBaseCampanas && dataBaseCampanas.length > 0 || !initializeBaseCampanas ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchBaseCampanas()"></pagination>
            </div>
        </div>
    </div>
</div>

<script src="{!! asset('js/vue/dataTables/dataTableCampanasBaseVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    function loadSearchMasivo(){
        vmFront.nameRoute = ucwords('Busqueda De Personas - Masiva')
        vmDataTableBaseCampanas.fetchBaseCampanas()
    }
    hideErrorForm('.formMasivoError')
</script>