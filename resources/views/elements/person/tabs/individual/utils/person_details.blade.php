@if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0)
    <div class="mt-3">
        <div class="accordion" id="accordion-4" role="tablist" aria-multiselectable="true">
            <div class="card card-outline-primary">
                <div class="card-header" role="tab" id="headPersona">
                    <h6 class="card-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion-4" href="#busquedaPersonas" aria-expanded="false" aria-controls="collapse41" class="collapsed collapse-header-card">
                            <i class="align-middle material-icons md-18 mr-1 mr-0-rtl ml-1-rtl">search</i> Busqueda de Personas
                        </a>
                    </h6>
                </div>
                <div id="busquedaPersonas" class="card-collapse collapse" role="tabpanel" aria-labelledby="headPersona">
                    <div class="card-body collapse-card-body">
                        @include('elements.person.tabs.individual.index')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="personDetailsVue">
        <div class="mt-3">
            <div class="widget-list">
                <div class="row">
                    <div class="col-12 col-md-3 widget-holder widget-full-content">
                        <div class="widget-bg">
                            <div class="widget-body border_fix border-color-gray-200 clearfix">
                                <div class="widget-user-profile mt-2">
                                    <div class="profile-body">
                                        <figure class="thumb-md">
                                            <div v-if="personPhoto.length === 0">
                                                <span class="text-center text-primary">
                                                    <i class="fa fa-spin fa-spinner fa-5x"></i>
                                                </span>
                                            </div>
                                            <template v-else>
                                                <img class="img-thumbnail" :src="personPhoto" @error="imageLoadError" />
                                            </template>
                                        </figure>
                                        <div class="mt-2 w-100">
                                            <div class="d-flex justify-content-between custom-title btn-cian fs-14">
                                                <div>
                                                    <h5>
                                                        <i class="fa fa-address-card"></i> Datos Generales
                                                    </h5>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered fs-11">
                                                    <tr>
                                                        <th class="text-primary align-middle">Nombre:</th>
                                                        <td class="align-middle" v-if="dataPerson.length === 0"><i class="fa fa-spin fa-spinner"></i> Cargando</td>
                                                        <td class="align-middle" v-else v-html="namePersona"></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-primary align-middle">DNI:</th>
                                                        <td class="align-middle" v-if="dataPerson.length === 0"><i class="fa fa-spin fa-spinner"></i> Cargando</td>
                                                        <td class="align-middle" v-else v-html="dataPerson.NRODOC"></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-primary align-middle">Nac - Edad:</th>
                                                        <td class="align-middle" v-if="dataPerson.length === 0"><i class="fa fa-spin fa-spinner"></i> Cargando</td>
                                                        <td class="align-middle" v-else v-html="dateBirthday + ' - ' + yearsPersona"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="d-flex justify-content-between custom-title btn-cian fs-14">
                                                <div>
                                                    <h5>
                                                        <i class="icon icon-phone"></i> Telefonos
                                                    </h5>
                                                </div>
                                                <div v-if="initializeDataTelefonos && dataTelefonos.length > 0 && initFormValidacion">
                                                    <h5>
                                                        <div class="btn-group" role="group">
                                                            <button type="button" @click="clickEvent('div.dialogScore', 'formPersonValidation', { numDNI: dataPerson.NRODOC })" data-toggle="modal" data-target="#modalScore" title="Validar Números" class="btn btn-green2 btn-sm mr-3"><i class="fa fa-play fs-10"></i></button>
                                                        </div>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-custom fs-11 centerTableImg">
                                                    <thead>
                                                    <tr>
                                                        <th>Número</th>
                                                        <th>Es.Valid.</th>
                                                        <th>Fec.Valid.</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <template v-if="!initializeDataTelefonos">
                                                        <tr>
                                                            <td colspan="3" class="text-center">
                                                                <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                                                            </td>
                                                        </tr>
                                                    </template>
                                                    <template v-if="initializeDataTelefonos && dataTelefonos.length === 0">
                                                        <tr>
                                                            <td colspan="3" class="text-center">
                                                                No se encontro telefonos para esta persona
                                                            </td>
                                                        </tr>
                                                    </template>
                                                    <template v-if="initializeDataTelefonos && dataTelefonos.length > 0">
                                                        <template v-for="(telefono, index) in dataTelefonos">
                                                            <tr>
                                                                <td v-html="telefono.TELEFONO"></td>
                                                                <td v-html="statusValidacion[index]"></td>
                                                                <td v-html="formatDateTimeValidacion[index]"></td>
                                                            </tr>
                                                        </template>
                                                    </template>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-9 mr-b-30">
                        <div class="tabs">
                            <ul class="nav nav-tabs border-0">
                                <li class="nav-item">
                                    <a href="#info-sunat" class="nav-link active" data-toggle="tab">
                                        <i class="list-icon fa fa-address-card" aria-hidden="true"></i> Información y Sunat en línea
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#laboral-financiero" class="nav-link" data-toggle="tab">
                                        <i class="list-icon icon icon icon-stats-dots"></i> Laboral y Financiero
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content border_fix border-color-gray-200">
                                <div role="tabpanel" class="tab-pane active" id="info-sunat">
                                    <div class="row">
                                        @include('elements.person.tabs.individual.utils.tabs.info_sunat')
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="laboral-financiero">
                                    <div class="row">
                                        @include('elements.person.tabs.individual.utils.tabs.laboral_finanzas')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{!! asset('js/vue/searchPerson/personDetailsVue.js?version='.date('YmdHis'))!!}"></script>
    <script>
        $(document).ready(function() {
            $('.tabFicha').click()
        })
        vmPersonDetails.numDNI = '{{ $numDNI }}'
        vmPersonDetails.getDataAll()
        deleteVariableSMSFunction()
    </script>
@else
    <div class="mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-icon alert-danger fixLineHeight">
                    <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                </div>
            </div>
        </div>
    </div>
    <script>
        deleteVariableSMSFunction()
    </script>
@endif