<div class="col-md-6">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-map4"></i> Sunat en linea
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <template v-if="!initializeDataSunat">
            <table class="table table-bordered table-vertical fs-11 centerTableImg">
                <tr>
                    <th>RUC:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Resumen Social:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Tipo Contribuyente:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Nombre Comercial:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Fecha de Inscripción:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Estado del Contribuyente:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Condición:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Teléfono:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Dirección:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Profesión u Oficio:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Actividad de comercio exterior:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Sistema de Emisión de Comprobante:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Sistema de Contabilidad:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Emisor electrónico desde:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
                <tr>
                    <th>Afiliado al PLE desde:</th>
                    <td><i class="fa fa-spin fa-spinner text-primary"></i></td>
                </tr>
            </table>
        </template>
        <template v-if="initializeDataSunat && dataSunat.length === 0">
            <table class="table table-bordered table-vertical fs-11 centerTableImg">
                <tr>
                    <th>RUC:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Resumen Social:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Tipo Contribuyente:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Nombre Comercial:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Fecha de Inscripción:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Estado del Contribuyente:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Condición:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Teléfono:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Dirección:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Profesión u Oficio:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Actividad de comercio exterior:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Sistema de Emisión de Comprobante:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Sistema de Contabilidad:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Emisor electrónico desde:</th>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Afiliado al PLE desde:</th>
                    <td>-</td>
                </tr>
            </table>
        </template>
        <template v-if="initializeDataSunat && dataSunat.RUC">
            <div class="table-responsive">
                <table class="table table-bordered table-vertical fs-11 centerTableImg">
                    <tr>
                        <th>RUC:</th>
                        <td v-html="dataSunat.RUC"></td>
                    </tr>
                    <tr>
                        <th>Resumen Social:</th>
                        <td v-html="dataSunat.RazonSocial"></td>
                    </tr>
                    <tr>
                        <th>Tipo Contribuyente:</th>
                        <td v-html="dataSunat.Tipo"></td>
                    </tr>
                    <tr>
                        <th>Nombre Comercial:</th>
                        <td v-html="dataSunat.NombreComercial"></td>
                    </tr>
                    <tr>
                        <th>Fecha de Inscripción:</th>
                        <td v-html="dataSunat.Inscripcion"></td>
                    </tr>
                    <tr>
                        <th>Estado del Contribuyente:</th>
                        <td v-html="dataSunat.Estado"></td>
                    </tr>
                    <tr>
                        <th>Condición:</th>
                        <td v-text="dataSunat.Condicion"></td>
                    </tr>
                    <tr>
                        <th>Teléfono:</th>
                        <td v-html="dataSunat.Telefono"></td>
                    </tr>
                    <tr>
                        <th>Dirección:</th>
                        <td v-html="dataSunat.Direccion"></td>
                    </tr>
                    <tr>
                        <th>Profesión u Oficio:</th>
                        <td v-html="dataSunat.Oficio"></td>
                    </tr>
                    <tr>
                        <th>Actividad de comercio exterior:</th>
                        <td v-html="dataSunat.ActividadExterior"></td>
                    </tr>
                    <tr>
                        <th>Sistema de Emisión de Comprobante:</th>
                        <td v-html="dataSunat.SistemaEmision"></td>
                    </tr>
                    <tr>
                        <th>Sistema de Contabilidad:</th>
                        <td v-html="dataSunat.SistemaContabilidad"></td>
                    </tr>
                    <tr>
                        <th>Emisor electrónico desde:</th>
                        <td v-html="dataSunat.Inscripcion"></td>
                    </tr>
                    <tr>
                        <th>Afiliado al PLE desde:</th>
                        <td v-html="dataSunat.PLE"></td>
                    </tr>
                </table>
            </div>
        </template>
    </div>
</div>
<div class="col-md-6">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-location4"></i> Direcciones
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-custom fs-11 centerTableImg">
            <thead>
                <tr>
                    <th>Direccion</th>
                    <th>Departamento</th>
                    <th>Provincia</th>
                    <th>Distrito</th>
                </tr>
            </thead>
            <tbody>
                <template v-if="!initializeDataAddress">
                    <tr>
                        <td colspan="4" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataAddress && dataAddress.length === 0">
                    <tr>
                        <td colspan="4" class="text-center">
                            No se encontro direcciones para esta persona
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataAddress && dataAddress.length > 0">
                    <template v-for="(address, index) in dataAddress">
                        <tr>
                            <td class="cursor-pointer text-primary" v-html="address['DIRECCION']" @click="getDataGoogleMaps(address['DIRECCION'] + ', ' + address['DISTRITO'] + ' / ' + address['DISTRITO'])"></td>
                            <td v-html="address['DEPARTAMENTO'] ? address['DEPARTAMENTO'] : '-'"></td>
                            <td v-html="address['PROVINCIA'] ? address['PROVINCIA'] : '-'"></td>
                            <td v-html="distritoAddress[index]"></td>
                        </tr>
                    </template>
                </template>
            </tbody>
        </table>
    </div>
    <template v-if="!initializeGoogleMaps">
        <div class="d-flex justify-content-center mt-4">
            <span class="text-center text-primary">
                <i class="fa fa-spin fa-spinner fa-5x"></i>
            </span>
        </div>
    </template>
    <template v-else>
        <gmap-map
                :center="googleMaps.center"
                :zoom="googleMaps.zoom"
                map-type-id="terrain"
                style="position: relative;padding-bottom: 60%;height: 0;overflow: hidden;"
        >
            <gmap-marker :position="googleMaps.marker">
            </gmap-marker>
        </gmap-map>
    </template>
</div>
<div class="mt-1">&nbsp;</div>
<div class="col-md-12">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-users3"></i> Datos familiares
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-custom fs-11 centerTableImg">
            <thead>
                <tr>
                    <th>Doc</th>
                    <th>Paterno</th>
                    <th>Materno</th>
                    <th>Nombres</th>
                    <th>Fec Nac.</th>
                    <th>Edad</th>
                    <th>Parent.</th>
                </tr>
            </thead>
            <tbody>
                <template v-if="!initializeDataFamiliares">
                    <tr>
                        <td colspan="8" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataFamiliares && dataFamiliares.length === 0">
                    <tr>
                        <td colspan="8" class="text-center">
                            No se encontro datos en familiares para esta persona
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataFamiliares && dataFamiliares.length > 0">
                    <template v-for="(familiar, index) in dataFamiliares">
                        <tr>
                            <td v-html="familiar['NRODOC_F'] ? familiar['NRODOC_F'] : '-'"></td>
                            <td v-html="familiar['PATERNO_F']  ? familiar['PATERNO_F'] : '-'"></td>
                            <td v-html="familiar['MATERNO_F']  ? familiar['MATERNO_F'] : '-'"></td>
                            <td v-html="familiar['NOMBRES_F']  ? familiar['NOMBRES_F'] : '-'"></td>
                            <td v-html="dateBirthdayFamily[index]"></td>
                            <td v-html="yearsFamily[index]"></td>
                            <td v-html="typeFamily[index]"></td>
                        </tr>
                    </template>
                </template>
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-12">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-car2"></i> Autos
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-custom fs-11 centerTableImg">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Placa</th>
                    <th>Fecha</th>
                    <th>Tipo / Auto</th>
                    <th>Color</th>
                    <th>Combustible</th>
                </tr>
            </thead>
            <tbody>
                <template v-if="!initializeDataAutos">
                    <tr>
                        <td colspan="8" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataAutos && dataAutos.length === 0">
                    <tr>
                        <td colspan="8" class="text-center">
                            No se encontraron autos para esta persona
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataAutos && dataAutos.length > 0">
                    <template v-for="(auto, index) in dataAutos">
                        <tr>
                            <td v-html="(index + 1)"></td>
                            <td v-html="auto['MARCA'] ? auto['MARCA'] : '-'"></td>
                            <td v-html="auto['MODELO'] ? auto['MODELO'] : '-'"></td>
                            <td v-html="auto['PLACA'] ? auto['PLACA'] : '-'"></td>
                            <td v-html="auto['COMPRA'] ? auto['COMPRA'] : '-'"></td>
                            <td v-html="auto['CLASE'] ? auto['CLASE'] : '-'"></td>
                            <td v-html="auto['COLOR'] ? auto['COLOR'] : '-'"></td>
                            <td v-html="auto['COMBUSTIBLE'] ? auto['COMBUSTIBLE'] : '-'"></td>
                        </tr>
                    </template>
                </template>
            </tbody>
        </table>
    </div>
</div>