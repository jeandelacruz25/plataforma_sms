<div class="col-md-12">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-drawer"></i> Resumen Laboral
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-custom fs-11 centerTableImg">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Fecha Registro</th>
                    <th>Nombre empresa</th>
                    <th>RUC</th>
                    <th>Condicion</th>
                    <th>Sueldo</th>
                </tr>
            </thead>
            <tbody>
                <template v-if="!initializeDataJob">
                    <tr>
                        <td colspan="6" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataJob && dataJobs.length === 0">
                    <tr>
                        <td colspan="6" class="text-center">
                            No se encontro datos en el resumen laboral para esta persona
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataJob && dataJobs.length > 0">
                    <template v-for="(job, index) in dataJobs">
                        <tr>
                            <td v-html="index + 1"></td>
                            <td v-html="dateJob[index]"></td>
                            <td v-html="job['E_RAZONSOCIAL']"></td>
                            <td v-html="job['E_NRODOC']"></td>
                            <td v-html="situationJob[index]"></td>
                            <td v-html="job['INGRESOS'] ? job['INGRESOS'] : '0.00'"></td>
                        </tr>
                    </template>
                </template>
            </tbody>
        </table>
    </div>
</div>

<div class="col-md-12">
    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-piggy-bank"></i> Reporte Crediticio
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-custom fs-11 centerTableImg" v-bind:class="[!detalleSBS.loadDetalleSBS ? 'disabled' : '']">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Fecha Reporte</th>
                    <th>Ent. Financieras</th>
                    <th>Calf. Normal</th>
                    <th>Calf. CPP</th>
                    <th>Calf. Deficiente</th>
                    <th>Calf. Dudoso</th>
                    <th>Calf. Pérdida</th>
                </tr>
            </thead>
            <tbody>
                <template v-if="!initializeDataReporteSBS">
                    <tr>
                        <td colspan="8" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataReporteSBS && dataReporteSBS.length === 0">
                    <tr>
                        <td colspan="8" class="text-center">
                            No se encontro datos en los reportes de la SBS para esta persona
                        </td>
                    </tr>
                </template>
                <template v-if="initializeDataReporteSBS && dataReporteSBS.length > 0">
                    <template v-for="(reporteSBS, index) in dataReporteSBS">
                        <tr>
                            <td v-html="index + 1"></td>
                            <template v-if="reporteSBS.detalle_count > 0">
                                <td class="cursor-pointer text-primary" @click="getDataPersonDetalleSBS(reporteSBS.CODIGOSBS)" v-html="dateFormatReportSBS[index]"></td>
                            </template>
                            <template v-else>
                                <td v-html="dateFormatReportSBS[index]"></td>
                            </template>
                            <td v-html="numberEntidadesReportSBS[index]"></td>
                            <td v-html="(reporteSBS.CALIFICACION_0)"></td>
                            <td v-html="(reporteSBS.CALIFICACION_1)"></td>
                            <td v-html="(reporteSBS.CALIFICACION_2)"></td>
                            <td v-html="(reporteSBS.CALIFICACION_3)"></td>
                            <td v-html="(reporteSBS.CALIFICACION_4)"></td>
                        </tr>
                    </template>
                </template>
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-between custom-title btn-cian fs-14">
        <div>
            <h5>
                <i class="icon icon-list"></i> Detalle Crediticio
            </h5>
        </div>
    </div>

    <div class="text-center pb-2 border d-none d-sm-block fs-12">
        <span class="inline-block mt-2"><button class="btn bg-nor">NOR</button><label class="pl-1 pr-3">Normal</label></span>
        <span class="inline-block mt-2"><button class="btn bg-cpp">CPP</button><label class="pl-1 pr-3">Con Problemas Potenciales</label></span>
        <span class="inline-block mt-2"><button class="btn bg-def">DEF</button><label class="pl-1 pr-3">Deficiente</label></span>
        <span class="inline-block mt-2"><button class="btn bg-dud">DUD</button><label class="pl-1 pr-3">Dudoso</label></span>
        <span class="inline-block mt-2"><button class="btn bg-per">PER</button><label class="pl-1 pr-3">Pérdida</label></span>
        <span class="inline-block mt-2"><button class="btn bg-scal">SCAL</button><label class="pl-1 pr-3">Sin Calificación</label></span>
    </div>

    <div class="text-left pt-2 pb-2 border d-block d-sm-none">
        <div class="table-responsive">
            <table class="table table-borderless fs-12">
                <tr>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-nor">NOR</button><label class="pl-1 pr-3">Normal2</label></td>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-cpp">CPP</button><label class="pl-1 pr-3">Con Problemas Potenciales</label></td>
                </tr>
                <tr>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-def">DEF</button><label class="pl-1 pr-3">Deficiente</label></td>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-dud">DUD</button><label class="pl-1 pr-3">Dudoso</label></td>
                </tr>
                <tr>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-per">PER</button><label class="pl-1 pr-3">Pérdida</label></td>
                    <td><button class="btn pl-2 pr-2 pt-0 pb-0 bg-scal">SCAL</button><label class="pl-1 pr-3">Sin Calificación</label></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-custom table-bordered fs-11 centerTableImg">
            <thead>
            <tr>
                <th>#</th>
                <th>Entidad Financiera</th>
                <th>Tipo Producto</th>
                <th>Monto</th>
                <th>Calificación</th>
            </tr>
            </thead>
            <tbody>
            <template v-if="!initializeDataDetalleSBS">
                <tr>
                    <td colspan="6" class="text-center">
                        Debes seleccionar una fecha del reporte para poder visualizar su detalle.
                    </td>
                </tr>
            </template>
            <template v-if="initializeDataDetalleSBS">
                <template v-if="!detalleSBS.loadDetalleSBS">
                    <tr>
                        <td colspan="6" class="text-center">
                            <i class="fa fa-spin fa-spinner text-primary fa-2x"></i>
                        </td>
                    </tr>
                </template>
                <template v-else>
                    <template v-for="(detalleSBS, index) in detalleSBS.dataDetalleSBS">
                        <tr>
                            <td v-html="index + 1"></td>
                            <td v-html="nameEmpresaSBSDetalle[index]"></td>
                            <td v-html="tipoProductoSBS[index]"></td>
                            <td v-html="detalleSBS.SALDO"></td>
                            <td v-bind:class="calificationClassDetalleSBS[index]" v-html="calificationDetalleSBS[index]"></td>
                        </tr>
                    </template>
                </template>
            </template>
            </tbody>
        </table>
    </div>
</div>