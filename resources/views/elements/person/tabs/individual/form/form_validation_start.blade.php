<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Iniciar Validación [{{ $numDNI }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formPersonValidationPlay">
            @if(searchMenuCliente(auth()->user()->id_cliente, 'validacion_telefonos') != 0 && searchMenuUsuario(auth()->id(), 'validacion_telefonos') != 0)
                @if(\Carbon\Carbon::now()->timestamp >= \Carbon\Carbon::parse($inicioMasivo)->timestamp && \Carbon\Carbon::now()->timestamp < \Carbon\Carbon::parse($finMasivo)->timestamp)
                    @if(\Carbon\Carbon::parse(isset($baseIndividual['fecha_validacion']) ? $baseIndividual['fecha_validacion'] : \Carbon\Carbon::now()->addDays(1))->format('d') != \Carbon\Carbon::now()->format('d'))
                        @if($countCampaign == 0)
                            @if($countCanales > 0)
                                @if($countCanales - $countCanalesUsed > 0)
                                    <div class="form-group text-center">
                                        <label class="pb-2">¿Estás seguro de iniciar la validación?</label>
                                    </div>
                                @else
                                    <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                                        <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>Se acaba de superar los canales de tu suscripción, intentelo mas tarde.</strong>
                                    </div>
                                @endif
                            @else
                                <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                                    <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>No cuentas con canales disponibles en tu suscripción.</strong>
                                </div>
                            @endif
                        @else
                            <div class="alert alert-icon alert-danger fs-12 fixLineHeight">
                                <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>Hay una validación en curso, favor de esperar que termine para volver a iniciar otra validación.</strong>
                            </div>
                        @endif
                        <input type="hidden" name="numDNI" value="{{ $numDNI }}">
                        <input type="hidden" name="numCanales" value="{{ $countCanales }}">
                        <div class="text-center">
                            @if($countCampaign == 0 && $countCanales > 0 && $countCanales - $countCanalesUsed != 0)
                                <button type="submit" class="btn btn-success btnForm ripple"><i class='feather feather-play list-icon mr-r-10' aria-hidden='true'></i> Iniciar</button>
                                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                            @endif
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    @else
                        <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                            <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>Este dni ya se ha validado el día de hoy, vuelva intentarlo el {{ \Jenssegers\Date\Date::parse($baseIndividual['fecha_validacion'])->addDays(1)->format('d/m') }}.</strong>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    @endif
                @else
                    <div class="alert alert-icon alert-warning fs-12 fixLineHeight">
                        <i class="feather feather-alert-circle list-icon mr-r-10"></i> <strong>Estas fuera del horario de validación, recuerda que esta acción la puedes realizar desde las {{ \Carbon\Carbon::parse($inicioMasivo)->format('h:i a') }} hasta las {{ \Carbon\Carbon::parse($finMasivo)->format('h:i a') }} (GMT -5).</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                @endif
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-icon alert-danger fixLineHeight">
                            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            @endif
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formCampanaValidacion.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker')
</script>