<div id="searchIndividualVue">
    <div class="mt-3">
        <div class="card">
            <div class="card-header card-header-search">
                Ingrese datos de la persona a buscar
                <span class="widget-heading-icon" v-bind:class="[formData.placaAuto.length > 0 || formData.apePaterno.length > 0 || formData.apeMaterno.length > 0 || formData.nomPersona.length > 0 || formData.numDNI.length > 0 ? 'text-danger' : 'text-gray-500']">
                    <i class="icon icon-eraser cursor-pointer" data-tooltip="tooltip" data-placement="bottom" title="Limpiar formulario" @click="eraseFormData()"></i>
                </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2" v-bind:class="[formData.placaAuto.length > 0 || formData.apePaterno.length > 0 || formData.apeMaterno.length > 0 || formData.nomPersona.length > 0 ? 'disabled' : '']">
                        <label>Documento</label>&nbsp;<a href="javascript:void(0)" data-placement="right" data-popover-content="#infoDocument" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                        <input type="text" name="dniPersona" class="form-control" placeholder="Ingrese N° documento" v-model="formData.numDNI" @keyup.enter="searchPerson()" :disabled="formData.placaAuto.length > 0 || formData.apePaterno.length > 0 || formData.apeMaterno.length > 0 || formData.nomPersona.length > 0 ? true : false">
                    </div>
                    <div class="col-md-2" v-bind:class="[formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? 'disabled' : '']">
                        <label>Apellido Paterno</label>
                        <input type="text" name="apePatPersona" class="form-control" placeholder="Ingrese apellido paterno" v-model="formData.apePaterno" @keyup.enter="searchPerson()" :disabled="formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? true : false">
                    </div>
                    <div class="col-md-2" v-bind:class="[formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? 'disabled' : '']">
                        <label>Apellido Materno</label>
                        <input type="text" name="apeMatPersona" class="form-control" placeholder="Ingrese apellido materno" v-model="formData.apeMaterno" @keyup.enter="searchPerson()" :disabled="formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? true : false">
                    </div>
                    <div class="col-md-3" v-bind:class="[formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? 'disabled' : '']">
                        <label>Nombres</label>
                        <input type="text" name="nombrePersona" class="form-control" placeholder="Ingrese nombres" v-model="formData.nomPersona" @keyup.enter="searchPerson()" :disabled="formData.numDNI.length > 0 || formData.placaAuto.length > 0 ? true : false">
                    </div>
                    <div class="col-md-2" v-bind:class="[formData.numDNI.length > 0 || formData.apePaterno.length > 0 || formData.apeMaterno.length > 0 || formData.nomPersona.length > 0 ? 'disabled' : '']">
                        <label>Placa auto</label>
                        <input type="text" name="numPlacaAuto" class="form-control" placeholder="Ingrese placa" v-model="formData.placaAuto" @keyup.enter="searchPerson()" :disabled="formData.numDNI.length > 0 || formData.apePaterno.length > 0 || formData.apeMaterno.length > 0 || formData.nomPersona.length > 0 ? true : false">
                    </div>
                    <div class="col-md-1 text-center">
                        <label>&nbsp;</label>
                        <button type="button" class="btn btn-primary pt-2 pb-2 btnForm btnCustom" @click="searchPerson()" @keyup.enter="searchPerson()"><i class='icon icon-search'></i></button>
                        <button type="button" class="btn btn-info pt-2 pb-2 btnLoad btnCustom ripple d-none"><i class="fa fa-spin fa-spinner"></i></button>
                    </div>
                    <div class="d-none" id="infoDocument">
                        <div class="popover-body">
                            <div>
                                <label>Información</label>
                                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                            </div>
                            <p>En este campo puede ingresar DNI, carnet de extranjería o RUC.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mr-t-5">
        <div v-if="initializeSugerencias || initializeSugerenciasAuto || initializeSearchDNI || initializeSearchRUC">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="initDataPagination">
            <div class="widget-body plr0">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-warning fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No se encontraron registros, con los campos ingresados.</strong>
                    </div>
                </div>
            </div>
        </template>
        <template v-else-if="dataPerson.length != 0">
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                        <tr>
                            <th class="align-middle">#</th>
                            <th class="align-middle">Tipo Documento</th>
                            <th class="align-middle">N° Documento</th>
                            <th class="align-middle">Nombres Completos</th>
                            <th class="align-middle">Fecha de Nacimiento</th>
                            <th class="align-middle">Edad</th>
                        </tr>
                        </thead>
                        <tbody>
                        <template v-for="(data, index) in dataPerson">
                            <tr>
                                <td v-html="pagination.from + index"></td>
                                <td v-html="tipoDoc[index]"></td>
                                <td><b><a class="cursor-pointer text-primary" @click="clickLoadOption(data.NRODOC)" v-html="data.NRODOC"></a></b></td>
                                <td v-html="namePerson[index]"></td>
                                <td v-html="dateBirthday[index]"></td>
                                <td v-html="yearsPersona[index]"></td>
                            </tr>
                        </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeSugerencias && dataPerson.length > 0 || !initializeSugerencias ? '' : 'disabled'">
                    <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="loadTableSugerencias()"></pagination>
                </div>
            </div>
        </template>
        <template v-else-if="dataAutos.length != 0">
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                        <tr>
                            <th>#</th>
                            <th>N° Documento</th>
                            <th>Titular</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <template v-for="(data, index) in dataAutos">
                            <tr>
                                <td v-html="paginationAutos.from + index"></td>
                                <td><b><a class="cursor-pointer text-primary" @click="clickLoadOption(data.NRODOC)" v-html="data.NRODOC"></a></b></td>
                                <td v-html="data.TITULAR"></td>
                                <td v-html="data.MARCA"></td>
                                <td v-html="data.MODELO"></td>
                            </tr>
                        </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                <div v-bind:class="!initializeSugerenciasAuto && dataAutos.length > 0 || !initializeSugerenciasAuto ? '' : 'disabled'">
                    <pagination v-if="paginationAutos.last_page > 1" :pagination="paginationAutos" :offset="5" @paginate="loadTableAutoSugerencias()"></pagination>
                </div>
            </div>
        </template>
        <template v-else-if="dataSunat.RUC">
            <div class="row">
                <div class="col-md-12 mt-3 pt-1">
                    <div class="custom-title mt-0 text-white">
                        <div>
                            <h5>Datos Principales</h5>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical nowrap">
                            <tr>
                                <th>RUC</th>
                                <th>Resumen Social</th>
                                <th>Tipo Contribuyente</th>
                                <th>Nombre Comercial</th>
                                <th>Fecha de Inscripción</th>
                            </tr>
                            <tr>
                                <td v-html="dataSunat.RUC"></td>
                                <td class="nowrap-normal" v-html="dataSunat.RazonSocial"></td>
                                <td class="nowrap-normal" v-html="dataSunat.Tipo"></td>
                                <td class="nowrap-normal" v-html="dataSunat.NombreComercial"></td>
                                <td v-html="dataSunat.Inscripcion"></td>
                                
                            </tr>
                            <tr>
                                <th>Estado del Contribuyente</th>
                                <th>Condición</th>
                                <th>Teléfono</th>
                                <th colspan="2">Profesión u Oficio</th>
                            </tr>
                            <tr>
                                <td v-html="dataSunat.Estado"></td>
                                <td class="nowrap-normal" v-text="dataSunat.Condicion"></td>
                                <td v-html="dataSunat.Telefono"></td>
                                <td colspan="2" class="nowrap-normal" v-html="dataSunat.Oficio ? dataSunat.Oficio : '-'"></td>
                            </tr>
                            <tr>
                                <th colspan="5">Dirección</th>
                            </tr>
                            <tr>
                                <td colspan="5" class="nowrap-normal" v-html="dataSunat.Direccion"></td>
                            </tr>
                            <tr>
                                <th>Actividad de comercio exterior</th>
                                <th>Sistema de Emisión de Comprobante</th>
                                <th>Sistema de Contabilidad</th>
                                <th>Emisor electrónico desde</th>
                                <th>Afiliado al PLE desde</th>
                            </tr>
                            <tr>
                                <td class="nowrap-normal" v-html="dataSunat.ActividadExterior"></td>
                                <td class="nowrap-normal" v-html="dataSunat.SistemaEmision"></td>
                                <td class="nowrap-normal" v-html="dataSunat.SistemaContabilidad"></td>
                                <td class="nowrap-normal" v-html="dataSunat.Inscripcion"></td>
                                <td class="nowrap-normal" v-html="dataSunat.PLE"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 mt-3 pt-1">
                    <div class="custom-title mt-0 text-white">
                        <div>
                            <h5>Representantes Legales</h5>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-custom centerTableImg nowrap">
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>Nombre</th>
                                    <th>Tipo Doc</th>
                                    <th>N° Doc</th>
                                    <th>Cargo</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                            <template v-if="dataSunat.representantes_legales.length === 0">
                                <tr>
                                    <td colspan="6" class="text-center">
                                        No se encontro representantes legales para este RUC
                                    </td>
                                </tr>
                            </template>
                            <template v-if="dataSunat.representantes_legales.length > 0">
                                <template v-for="(representante, index) in dataSunat.representantes_legales">
                                    <tr>
                                        <td class="text-center">
                                            <a href="javascript:void(0)">
                                                <img width="40" class="" :src="'images/faces/' + representante.numdoc + '.jpg'" onerror="this.src='img/avatars/default.png'" />
                                            </a>
                                        </td>
                                        <td  class="single-amount align-middle" v-html="representante.nombre"></td>
                                        <td class="single-amount align-middle" v-html="representante.tipodoc"></td>
                                        <td class="single-amount align-middle" v-html="representante.numdoc"></td>
                                        <td class="single-amount align-middle" v-html="representante.cargo"></td>
                                        <td class="single-amount align-middle" v-html="representante.desde"></td>
                                    </tr>
                                </template>
                            </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </template>
    </div>
    <div class="mr-t-5">
        <div class="alert alert-icon alert-danger border-danger formIndividualError d-none fixLineHeight col-md-12"></div>
    </div>
</div>

<div id="chartHTMLSunat"></div>
<div id="chartScriptSunat"></div>

<script src="{!! asset('js/vue/searchPerson/searchIndividualVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    function loadSearchIndividual(){
        vmFront.nameRoute = ucwords('Busqueda De Personas - Individual')
    }
    hideErrorForm('.formIndividualError')
</script>