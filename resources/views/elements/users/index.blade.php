<div class="widget-heading widget-heading-border">
    <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
    <div class="widget-actions">
        <div class="d-flex justify-content-end">
            <div>
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScoreLarge','formUsers', {}, 'get')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="fa fa-plus list-icon mr-r-10" aria-hidden="true"></i> Agregar Usuario
                </a>
            </div>
        </div>
    </div>
</div>
<div class="widget-body plr0">
    <div id="dataTableUsuariosVue">
        <div v-if="initializeUsuarios">
            <div class="col-md-12">
                <div class="cssload-loader"></div>
            </div>
        </div>
        <template v-else-if="!initializeUsuarios && dataUsuarios.length === 0 && !initDataPagination">
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchUsuarios()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Usuario" type="text" v-model="searchUsuario">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchUsuarios()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td colspan="5">
                                No hay ningún usuario creado actualmente.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <template v-else>
            <div class="widget-heading mlr0">
                <div class="widget-title">&nbsp;</div>
                <div class="widget-actions">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pr-2" style="margin-top: -1px !important;">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <select class="form-control selectClientes show-tick" data-live-search="true" v-model="selectCliente" @change="fetchUsuarios()">
                                    <option value="" selected>Todos los Clientes</option>
                                    @foreach($selectOptions['clientes'] as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['razon_social'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <input class="form-control" placeholder="Buscar Usuario" type="text" v-model="searchUsuario">
                                <div class="input-group-addon cursor-pointer bg-primary" @click="fetchUsuarios()">
                                    <i class="feather feather-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-body plr0">
                <div class="col-md-12 table-responsive" v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
                    <table class="table table-bordered centerTableImg centerTableImg nowrap">
                        <thead class="bg-primary">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(dataUsuario, index) in dataUsuarios">
                                <tr>
                                    <td v-html="pagination.from + index"></td>
                                    <td v-html="nameUsuario[index]"></td>
                                    <td v-html="dataUsuario.username"></td>
                                    <td v-html="statusUsuario[index]"></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="javascript:void(0)" class="btn btn-orange2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formUsers', dataUsuario.id)" data-toggle="modal" data-target="#modalScore"><i title="Editar Cliente" class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-rose2 btn-sm" @click="clickEvent('div.dialogScoreLarge','formUsersMenu' ,{ idUser: dataUsuario.id, idCliente: dataUsuario.id_cliente})" data-toggle="modal" data-target="#modalScore"><i title="Actualizar Menú" class="fa fa-bars"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-gray2 btn-sm" @click="clickEvent('div.dialogScore','formUsersStatus', dataUsuario.id)" data-toggle="modal" data-target="#modalScore"><i title="Cambiar Estado" class="fa fa-refresh"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </template>
        <div v-bind:class="!initializeLoadPaginate ? 'disabled' : ''">
            <div v-bind:class="!initializeUsuarios && dataUsuarios.length > 0 || !initializeUsuarios ? '' : 'disabled'">
                <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="paginationFetchUsuarios()"></pagination>
            </div>
        </div>
    </div>
</div>