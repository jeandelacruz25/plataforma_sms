<div class="modal-content">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" type="button">×</button>
        <h4 id="myLargeModalLabel3" class="modal-title">Cambiar Avatar</h4>
    </div>
    <div class="modal-body">
        <form id="formAvatar" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 text-center">
                    <figure class="imgOverlay thumb-lg">
                        <img class="img-thumbnail" id="imageAvatar" src="{{ asset($dataUser ? $dataUser[0]['avatar'].'?version='.date('YmdHis') : 'img/proveedores/img-default.svg?version='.date('YmdHis')) }}" />
                        <div class="middle">
                            <div class="fileUpload btn btn-primary">
                                <span><i class="fa fa-upload list-icon"></i></span>
                                <input type="file" class="upload" onchange="showImagePreview(this, '#imageAvatar', 'avatarOriginal')" name="avatarUser" value="{{ $dataUser ? $dataUser[0]['avatar'] : '' }}"/>
                            </div>
                        </div>
                    </figure>
                    <input type="hidden" name="avatarOriginal" value="{{ $dataUser ? $dataUser[0]['avatar'] : '' }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btnForm ripple"><i class='feather feather-refresh-cw list-icon mr-r-10' aria-hidden='true'></i> Actualizar</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                </div>
            </div>
        </form>
        <div class="row mr-t-5">
            <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
        </div>
    </div>
</div>
<script src="{!! asset('js/form/formUsers.js?version='.date('YmdHis')) !!}"></script>
<script>
    clearModalClose('modalScore', 'div.dialogScore')
</script>