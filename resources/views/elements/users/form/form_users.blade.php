<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header text-white bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Usuario</h5>
    </div>
    <div class="modal-body">
        @if(searchMenuCliente(auth()->user()->id_cliente, 'usersRole') != 0 && searchMenuUsuario(auth()->id(), 'usersRole') != 0)
            <form id="formUser">
                <div class="container col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nombres</label>
                                <input type="text" name="userNombres" class="form-control form-control-fix-select" placeholder="Ingrese sus nombres" value="{{ $dataUser ? $dataUser[0]['nombres'] : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Apellidos</label>
                                <input type="text" name="userApellidos" class="form-control form-control-fix-select" placeholder="Ingrese sus apellidos" value="{{ $dataUser ? $dataUser[0]['apellidos'] : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Fecha de Nacimiento</label>
                                <input type="text" name="birthDay" readonly="readonly" class="form-control txt-calendar datepicker" value="{{ $dataUser ? $dataUser[0]['fecha_nacimiento'] : \Carbon\Carbon::now()->format('Y-m-d') }}">
                            </div>
                            <div class="form-group">
                                <label>Tipo de Documento</label>
                                <select name="typeIdentity" class="form-control selectpicker show-tick">
                                    <option value="">Seleccione un tipo de documento</option>
                                    @foreach($options['tipoIdentidad'] as $key => $value)
                                        <option value="{{ $value['id'] }}" @if($dataUser) {{ $dataUser[0]['identity']['id'] === $value['id'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['tipo_identidad'])) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Número de Documento</label>
                                <input type="text" name="numIdentity" class="form-control form-control-fix-select" placeholder="Ingrese su número de documento" value="{{ $dataUser ? $dataUser[0]['num_identidad'] : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Correo Electronico</label>
                                <input type="text" name="email" class="form-control form-control-fix-select" placeholder="Ingrese su correo electronico" value="{{ $dataUser ? $dataUser[0]['email'] : '' }}">
                            </div>
                            <div class="form-group">
                                <label class="text-danger">Usuario</label>
                                <input type="text" name="username" class="form-control form-control-fix-select" placeholder="Ingrese su usuario" value="{{ $dataUser ? $dataUser[0]['username'] : '' }}">
                            </div>
                            <div class="form-group">
                                <label class="text-danger">Contraseña</label>
                                <input id="inputRead" type="password" name="password" class="form-control form-control-fix-select" placeholder="Ingrese su contraseña" value="{{ $dataUser ? $dataUser[0]['password'] : '' }}" {{ $updateForm === true ? "readonly" : "" }}>
                                @if($updateForm === true)
                                    <input id="inputRecover" type="text" name="passwordValidate" value="{{ $dataUser[0]['password'] }}" hidden>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            @if(Auth::user()->authorizeRoles(['Administrador']))
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select name="clienteUser" class="form-control selectpicker show-tick">
                                        <option value="">Seleccione un cliente</option>
                                        @foreach($options['clientes'] as $key => $value)
                                            <option value="{{ $value['id'] }}" @if($dataUser) {{ $dataUser[0]['id_cliente'] == $value['id'] ? 'selected' : '' }} @endif>{{ ucwords($value['razon_social']) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Perfil del Usuario</label>
                                <select name="roleUser" class="form-control selectpicker show-tick">
                                    <option value="">Seleccione un perfil</option>
                                    @foreach($options['roles'] as $key => $value)
                                        <option value="{{ $value['id'] }}" @if($dataUser) {{ $dataUser[0]['id_rol'] == $value['id'] ? 'selected' : '' }} @endif>{{ ucwords($value['name']) }}</option>
                                    @endforeach
                                </select>
                                <div class="checkbox checkbox-primary">
                                    <label class="checkbox-checked">
                                        <input name="checkRolMenu" type="checkbox" {{ $updateForm === true ? '' : 'checked' }}> <span class="label-text">Heredar permisos del perfil</span> &nbsp;&nbsp;<a href="javascript:void(0)" data-placement="bottom" data-popover-content="#deleteNumber" data-popover="popover" data-trigger="focus"><i class="list-icon fa fa-question-circle"></i></a>
                                    </label>
                                </div>
                                @if($updateForm === true)
                                    <div class="checkbox checkbox-primary">
                                        <label class="checkbox-checked">
                                            <input id="checkRead" name="changePassword" type="checkbox" onclick="markReadOnly('#checkRead','#inputRead','#inputRecover')"> <span class="label-text fs-12">¿Deseas cambiar la contraseña?</span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <div class="d-none" id="deleteNumber">
                                <div class="popover-body">
                                    <div>
                                        <label>Información</label>
                                        <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="feather feather-x list-icon mr-r-10"></i></button>
                                    </div>
                                    <p>Si este check se encuentra activo, se agregaran los menú por defecto que cuenta el perfil anteriormente seleccionado.</p>
                                </div>
                            </div>
                            <input type="hidden" name="userID" value="{{ $dataUser ? $dataUser[0]['id'] : '' }}">
                            <input type="hidden" name="statusID" value="{{ $dataUser ? $dataUser[0]['id_status'] : '1' }}">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='feather feather-edit-2 list-icon mr-r-10' aria-hidden='true'></i> Editar" : "<i class='feather feather-plus list-icon mr-r-10' aria-hidden='true'></i> Agregar" !!}</button>
                                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="fa fa-spin fa-spinner list-icon mr-r-10" aria-hidden="true"></i> Cargando</button>
                                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if($updateForm === false)
                            <div class="col-md-12">
                                <div class="alert alert-icon alert-info border-info fixLineHeight" role="alert">
                                    <i class="feather feather-info list-icon mr-r-10"></i> <strong>Es importante tener en cuenta :</strong>
                                    <li>Que al elegir si se desea pasar el menu por defecto, este actualizara al usuario con el menu del perfil, si no se elige se tendra que seleccionar el menu de manera manual.</li>
                                    <li>Para actualizar la contraseña se hara de forma independiente en otra opción por seguridad.</li>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </form>
            <div class="row mr-t-5">
                <div class="alert alert-icon alert-danger border-danger formError d-none fixLineHeight col-md-12"></div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger fixLineHeight">
                        <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No cuentas con este servicio habilitado.</strong>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal"><i class="feather feather-x list-icon mr-r-10" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{!! asset('js/form/formUsers.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
    initDatePicker('.datepicker', {
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: '{{ config('app.locale') }}',
        showOnFocus: true
    })
    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })
</script>