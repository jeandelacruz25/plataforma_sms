@if ($item['submenu'] == [])
    <li class="checkbox checkbox-primary">
        <label class="checkbox-checked">
            <input type="checkbox" name="checkMenu[]" value="{{ $item['vue_name'] }}" class="checkNew" {{ (collect($menuCheck)->pluck('vue_name')->contains($item['vue_name']) ? 'checked' : '') }}>
                <span class="label-text">
                    <a href="javascript:void(0);">
                        <span class="thumb-xxs">
                            <i class="list-icon {{ $item['icon'] }} text-white"></i> {{ $item['name'] }}
                        </span>
                    </a>
                </span>
        </label>
    </li>
@else
    <li class="menu-item-has-children checkbox checkbox-primary">
        <label class="checkbox-checked">
            <input type="checkbox" name="checkMenu[]" value="{{ $item['vue_name'] }}"
                   {{ (collect($menuCheck)->pluck('vue_name')->contains($item['vue_name']) ? 'checked' : '') }}
                   class="checkNew @if($subMenuCheck) checkGeneralSub_{{ $item['id'] }} @else checkGeneralSubMenu_{{ $item['id'] }} {{ isset($parent) ? 'checkMenuSub_'.$parent : 'checkMenuSub' }} @endif"
                   onclick="@if($subMenuCheck) mark_all_menu('.checkGeneralSub_{{ $item['id'] }}', 'checkMenuSub_{{ $item['id'] }}') @else mark_all_menu('.checkGeneralSubMenu_{{ $item['id'] }}', 'checkMenuSub_{{ $item['id'] }}') @endif">

            <span class="label-text">
                <a href="javascript:void(0);">
                    <span class="thumb-xxs">
                        <i class="list-icon {{ $item['icon'] }} text-white"></i> {{ $item['name'] }}
                    </span>
                </a>
            </span>
        </label>
        <ul class="sub-menu list-unstyled">
            @foreach ($item['submenu'] as $key => $submenu)
                @if ($submenu['submenu'] == [])
                    <li class="checkbox checkbox-primary">
                        <label class="checkbox-checked">
                            <input type="checkbox" name="checkMenu[]" value="{{ $submenu['vue_name'] }}"
                                   {{ (collect($menuCheck)->pluck('vue_name')->contains($submenu['vue_name']) ? 'checked' : '') }}
                                   class="checkNew checkMenuSub_{{ $submenu['parent'] }} @if(!$subMenuCheck) {{ isset($parent) ? 'checkMenuSub_'.$parent : 'checkMenuSub_'.$submenu['parent'] }} checkMenuSub_{{ $submenu['parent'] }} @endif">

                            <span class="label-text">
                                <a href="javascript:void(0);">
                                    <span class="thumb-xxs">
                                        <i class="list-icon {{ $submenu['icon'] }} text-white"></i> {{ $submenu['name'] }}
                                    </span>
                                </a>
                            </span>
                        </label>
                    </li>
                @else
                    @include('partials.menu-assign-item', [ 'item' => $submenu, 'subMenuCheck' => false, 'menuCheck' => $checkMenu, 'parent' => $submenu['parent'] ])
                @endif
            @endforeach
        </ul>
    </li>
@endif