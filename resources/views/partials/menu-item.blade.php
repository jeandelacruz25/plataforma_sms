<template v-if="userInformationCliente.id_estado == '1'">
    <template v-if="userInformation.id_status == '1'">
        @if ($item['submenu'] == [])
            <li v-if="menuCliente.indexOf('{{ $item["vue_name"] }}') >= 0 && menuUsuario.indexOf('{{ $item["vue_name"] }}') >= 0">
                <a class="thumb-xxs mr-b-0" href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $item['vue_name'] }}', {}, 'get')">
                    <i class="list-icon {{ $item['icon'] }} text-white"></i> <span class="hide-menu text-white">{{ $item['name'] }} </span>
                </a>
            </li>
        @else
            <li class="menu-item-has-children" v-if="menuCliente.indexOf('{{ $item["vue_name"] }}') >= 0 && menuUsuario.indexOf('{{ $item["vue_name"] }}') >= 0">
                <a class="thumb-xxs mr-b-0" href="javascript:void(0);">
                    <i class="list-icon {{ $item['icon'] }} text-white"></i> <span class="@if($hide) hide-menu @endif text-white">{{ $item['name'] }}</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    @foreach ($item['submenu'] as $submenu)
                        @if ($submenu['submenu'] == [])
                            <li v-if="menuCliente.indexOf('{{ $item["vue_name"] }}') >= 0 && menuUsuario.indexOf('{{ $submenu["vue_name"] }}') >= 0">
                                <a class="thumb-xxs mr-b-0" href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $submenu['vue_name'] }}', {}, 'get')">
                                    <i class="list-icon {{ $submenu['icon'] }} text-white"></i> <span class="text-white">{{ $submenu['name'] }} </span>
                                </a>
                            </li>
                        @else
                            @include('partials.menu-item', [ 'item' => $submenu, 'hide' => false ])
                        @endif
                    @endforeach
                </ul>
            </li>
        @endif
    </template>
</template>