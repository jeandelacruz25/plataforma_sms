<div class="card">
    <header class="card-header d-flex align-items-center mb-0">
        <span class="heading-font-family flex-1 text-center fw-400 fs-13">SMS Notificaciones</span>
    </header>
    <template v-if="listNotificationSMS.length === 0">
        <ul class="card-body list-unstyled dropdown-list-group">
            <li class="bg-white">
                <a href="javascript:void(0)" class="media fs-12 fixLineHeight">
                    <span class="d-flex thumb-xs">
                        <i class="fa fa-info list-icon"></i>
                    </span>
                    <span class="media-body">
                        <span class="media-content">No hay ninguna notificación nueva actualmente!</span>
                    </span>
                </a>
            </li>
        </ul>
    </template>
    <template v-else-if="listNotificationSMS.length >= 4">
        <div class="vuebar-element" v-bar>
            <ul class="card-body list-unstyled dropdown-list-group">
                <template v-for="(item, index) in listNotificationSMS">
                    <li class="bg-gray-300-contrast">
                        <a href="javascript:void(0)" class="media fs-12 fixLineHeight" v-on:click="chatViewNumberList(item.id_campana, item.id_bulk_sms)">
                            <span class="d-flex thumb-xs">
                                <img :src="userInformationCliente.avatar" class="rounded-circle" alt="">
                            </span>
                            <span class="media-body">
                                <span class="media-content">Has recibido respuestas en tu <label class="media-content" v-html="item.campana === null ? 'bulkID' : 'campaña'"></label> : <span class="heading-font-family media-heading text-score" v-html="item.campana === null ? item.id_bulk_sms : item.campana.nombre_campana"></span></span>
                            </span>
                        </a>
                    </li>
                </template>
            </ul>
        </div>
    </template>
    <template v-else>
        <ul class="card-body list-unstyled dropdown-list-group">
            <template v-for="(item, index) in listNotificationSMS">
                <li class="bg-gray-300-contrast">
                    <a href="javascript:void(0)" class="media fs-12 fixLineHeight" v-on:click="chatViewNumberList(item.id_campana, item.id_bulk_sms)">
                        <span class="d-flex thumb-xs">
                            <img :src="userInformationCliente.avatar" class="rounded-circle" alt="">
                        </span>
                        <span class="media-body">
                            <span class="media-content">Has recibido respuestas en tu <label class="media-content" v-html="item.campana === null ? 'bulkID' : 'campaña'"></label> : <span class="heading-font-family media-heading text-score" v-html="item.campana === null ? item.id_bulk_sms : item.campana.nombre_campana"></span></span>
                        </span>
                    </a>
                </li>
            </template>
        </ul>
    </template>
    <footer class="card-footer text-center" v-if="listNotificationSMS.length != 0">
        <template>
            <a href="javascript:void(0);" v-on:click="loadOptionMenu('chat', {}, 'get')" class="content-font-family text-uppercase fs-13">Ver todas las notificaciones</a>
        </template>
    </footer>
</div>