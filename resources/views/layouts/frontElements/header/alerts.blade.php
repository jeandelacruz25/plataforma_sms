<div class="card">
    <header class="card-header d-flex justify-content-between mb-0">
        <a href="javascript:void(0);">
            <i class="feather feather-bell color-color-scheme" aria-hidden="true"></i>
        </a>
        <span class="heading-font-family flex-1 text-center fw-400">Alerts</span>
        <a href="javascript:void(0);">
            <i class="feather feather-settings color-content"></i>
        </a>
    </header>
    <ul class="card-body list-unstyled dropdown-list-group">
        <li>
            <a href="#" class="media">
                <span class="d-flex">
                    <i class="material-icons list-icon">check</i>
                </span>
                <span class="media-body">
                    <span class="heading-font-family media-heading">Invitation accepted</span>
                    <span class="media-content">Your have been Invited ...</span>
                </span>
            </a>
        </li>
        <li>
            <a href="#" class="media">
                <span class="d-flex thumb-xs">
                    <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                </span>
                <span class="media-body">
                    <span class="heading-font-family media-heading">Steve Smith</span>
                    <span class="media-content">I slowly updated projects</span>
                    <span class="user--online float-right"></span>
                </span>
            </a>
        </li>
        <li>
            <a href="#" class="media">
                <span class="d-flex">
                    <i class="material-icons list-icon">event_available</i>
                </span>
                <span class="media-body">
                    <span class="-heading-font-family media-heading">To Do</span>
                    <span class="media-content">Meeting with Nathan on Friday 8 AM ...</span>
                </span>
            </a>
        </li>
    </ul>
    <footer class="card-footer text-center">
        <a href="javascript:void(0);" class="text-uppercase fs-13">See all activity</a>
    </footer>
</div>