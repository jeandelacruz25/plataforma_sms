<span style="cursor: pointer" data-toggle="dropdown">
    <figure class="avatar thumb-xs2 user--online mr-r-15 mt-3">
        <img :src="userInformation.avatar ? avatarUser : 'img/loading_avatar.gif'" class="rounded-circle avatar-navbar" alt="">
        <i class="feather feather-chevron-down list-icon"></i>
    </figure>
</span>
<div class="dropdown-menu dropdown-left dropdown-card animated flipInY paddingFix">
    <div class="card">
        <header class="card-header d-flex justify-content-between mb-0">
            <span class="heading-font-family flex-1 text-center fw-400 fs-13">Opciones de Cuenta</span>
        </header>
        <ul class="card-body dropdown-list-group dropdown-flags">
            <li class="bg-white mr-t-10">
                <a href="javascript:void(0)" onclick="responseModal('div.dialogScore','/formAvatar',{}, 'get')" data-toggle="modal" data-target="#modalScore" class="media fs-12 fixLineHeight paddingFix2">
                    <span class="d-flex thumb-xs">
                        <span class="media-body">
                            <img src="{{ asset('img/utils/change_avatar.svg?version='.date('YmdHis')) }}" style="width: 40px; height: 25px;" onerror="this.src='img/clientes/default.png'" />&nbsp;
                            <span class="media-content">Cambiar Avatar</span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="bg-white mr-t-10 d-block d-sm-none">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="media fs-12 fixLineHeight paddingFix2">
                    <span class="d-flex thumb-xs">
                        <span class="media-body">
                            <img src="{{ asset('img/utils/logout.svg?version='.date('YmdHis')) }}" style="width: 40px; height: 25px;" onerror="this.src='img/clientes/default.png'" />&nbsp;
                            <span class="media-content">Desconectarse</span>
                        </span>
                    </span>
                </a>
            </li>
        </ul>
        <footer class="card-footer text-center" >
            <span class="content-font-family text-uppercase fs-13 text-gray-500">Bienvenido, <span class="text-score">{{ ucwords(Auth::user()->username) }} / </span></span>
            <span class="content-font-family text-uppercase fs-13 text-gray-500"><span class="text-brown">{{ Auth::user()->roles ? ucwords(Auth::user()->roles->name) : 'Sin Rol' }}</span></span>
        </footer>
    </div>
</div>