<div class="card">
    <header class="card-header d-flex align-items-center mb-0">
        <span class="heading-font-family flex-1 text-center fw-400 fs-13">Notificaciones</span>
    </header>
    <template v-if="listNotification.length === 0">
        <ul class="card-body list-unstyled dropdown-list-group">
            <li class="bg-white">
                <a href="javascript:void(0)" class="media fs-12 fixLineHeight">
                    <span class="d-flex thumb-xs">
                        <i class="fa fa-info list-icon"></i>
                    </span>
                    <span class="media-body">
                        <span class="media-content">No hay ninguna notificación nueva actualmente</span>
                    </span>
                </a>
            </li>
        </ul>
    </template>
    <template v-else-if="listNotification.length >= 4">
        <div class="vuebar-element" v-bar>
            <ul class="card-body list-unstyled dropdown-list-group">
                <template v-for="(item, index) in listFixNotification">
                    <li class="bg-gray-300-contrast">
                        <a href="javascript:void(0)" class="media fs-12 fixLineHeight" v-on:click="redirectNotification(item); deleteIndexNotification(index);">
                            <span class="d-flex">
                                <template v-if="parseInt(item.tipoSubida) === 0">
                                    <i class="material-icons list-icon">file_upload</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 1">
                                    <i class="material-icons list-icon">insert_comment</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 2">
                                    <i class="material-icons list-icon">send</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) == 3">
                                    <i class="material-icons list-icon">file_upload</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) == 4">
                                    <i class="material-icons list-icon">done_all</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) == 5">
                                    <i class="material-icons list-icon">file_upload</i>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) == 6">
                                    <i class="material-icons list-icon">done_all</i>
                                </template>
                            </span>
                            <span class="media-body">
                                <template v-if="parseInt(item.tipoSubida) === 0">
                                    <span class="media-content">Se subieron los números en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 1">
                                    <span class="media-content">Se enviaron los SMS del keyword : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 2">
                                    <span class="media-content">Se enviaron los SMS de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 3 && !item.licenseExceeded">
                                    <span class="media-content">Se subieron los números en la campaña de validación : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 3 && item.licenseExceeded">
                                    <span class="media-content">Se subieron los números en la campaña de validación : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>, acabas de exceder el limite de números validados al día.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 4">
                                    <span class="media-content">Se validaron los números de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 5 && !item.licenseExceeded">
                                    <span class="media-content">Se subieron los documentos en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 5 && item.licenseExceeded">
                                    <span class="media-content">Se subieron los documentos en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>, esta campaña excedió el total de registros a validar.</span>
                                </template>
                                <template v-if="parseInt(item.tipoSubida) === 6">
                                    <span class="media-content">El cruce de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span> se ha culminado.</span>
                                </template>
                            </span>
                        </a>
                    </li>
                </template>
            </ul>
        </div>
    </template>
    <template v-else>
        <ul class="card-body list-unstyled dropdown-list-group">
            <template v-for="(item, index) in listFixNotification">
                <li class="bg-gray-300-contrast">
                    <a href="javascript:void(0)" class="media fs-12 fixLineHeight" v-on:click="redirectNotification(item); deleteIndexNotification(index);">
                        <span class="d-flex">
                            <template v-if="parseInt(item.tipoSubida) === 0">
                                <i class="material-icons list-icon">file_upload</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 1">
                                <i class="material-icons list-icon">insert_comment</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 2">
                                <i class="material-icons list-icon">send</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) == 3">
                                <i class="material-icons list-icon">file_upload</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) == 4">
                                <i class="material-icons list-icon">done_all</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) == 5">
                                <i class="material-icons list-icon">file_upload</i>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) == 6">
                                <i class="material-icons list-icon">done_all</i>
                            </template>
                        </span>
                        <span class="media-body">
                            <template v-if="parseInt(item.tipoSubida) === 0">
                                <span class="media-content">Se subieron los números en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 1">
                                <span class="media-content">Se enviaron los SMS del keyword : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 2">
                                <span class="media-content">Se enviaron los SMS de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 3 && !item.licenseExceeded">
                                <span class="media-content">Se subieron los números en la campaña de validación : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 3 && item.licenseExceeded">
                                <span class="media-content">Se subieron los números en la campaña de validación : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>, acabas de exceder el limite de números validados al día.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 4">
                                <span class="media-content">Se validaron los números de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 5 && !item.licenseExceeded">
                                <span class="media-content">Se subieron los documentos en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 5 && item.licenseExceeded">
                                <span class="media-content">Se subieron los documentos en la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span>, acabas de exceder el limite de documentos validados al día.</span>
                            </template>
                            <template v-if="parseInt(item.tipoSubida) === 6">
                                <span class="media-content">El cruce de la campaña : <span class="heading-font-family media-heading text-score" v-html="item.nameCampaign"></span> se ha culminado.</span>
                            </template>
                        </span>
                    </a>
                </li>
            </template>
        </ul>
    </template>
</div>