<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        @foreach ($menus as $key => $item)
            @if ($item['parent'] != 0)
                @break
            @endif
            @include('partials.menu-item', ['item' => $item, 'hide' => true])
        @endforeach
    </ul>
</nav>