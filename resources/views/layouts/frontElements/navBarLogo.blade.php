<div class="navbar-header">
    <a href="javascript:void(0)" class="navbar-brand bg-score">
        <img class="logo-expand" alt="" src="{!! asset('img/logo-login.png?version='.date('YmdHis')) !!}">
        <img class="logo-collapse" alt="" src="{!! asset('img/logo-collapse.png?version='.date('YmdHis')) !!}">
        <!-- <p>BonVue</p> -->
    </a>
</div>

<!-- Left Menu & Sidebar Toggle -->
<ul class="nav navbar-nav">
    <li class="sidebar-toggle">
        <a href="javascript:void(0)" class="ripple">
            <i class="feather feather-menu list-icon fs-20 text-gray-600"></i>
        </a>
    </li>
</ul>
<!-- /.navbar-left -->