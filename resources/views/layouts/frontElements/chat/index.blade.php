<aside class="right-sidebar scrollbar-enabled suppress-x">
    <div class="sidebar-chat" data-plugin="chat-sidebar">
        <div class="sidebar-chat-info">
            <h6 class="fs-16">Chat List</h6>
            <form class="mr-t-10">
                <div class="form-group">
                    <input type="search" class="form-control form-control-rounded fs-13 heading-font-family pd-r-30" placeholder="Search for friends ..."> <i class="feather feather-search post-absolute pos-right vertical-center mr-3 text-muted"></i>
                </div>
            </form>
        </div>
        <div class="chat-list">
            <div class="list-group row">
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julein Renvoye">
                    <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Gene Newman</span>  <span class="username">@gene_newman</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebanovkiy">
                    <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Billy Black</span>  <span class="username">@billyblack</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Cameron Moll">
                    <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Herbert Diaz</span>  <span class="username">@herbert</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Bill S Kenny">
                    <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Sylvia Harvey</span>  <span class="username">@sylvia</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Trent Walton">
                    <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Marsha Hoffman</span>  <span class="username">@m_hoffman</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julien Renvoye">
                    <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Mason Grant</span>  <span class="username">@masongrant</span> </span>
                </a>
                <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebaovskiy">
                    <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                        <img src="img/avatars/user_default.png" class="rounded-circle" alt="">
                    </figure><span><span class="name">Shelly Sullivan</span>  <span class="username">@shelly</span></span>
                </a>
            </div>
            <!-- /.list-group -->
        </div>
        <!-- /.chat-list -->
    </div>
    <!-- /.sidebar-chat -->
</aside>

@include('layouts.frontElements.chat.chat')