<ul class="nav navbar-nav d-lg-flex ml-2">
    <li class="dropdown">
        @include('layouts.frontElements.header.user')
    </li>
</ul>

<ul class="nav navbar-nav d-none d-lg-flex ml-2">
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropdown-toggle ripple" data-toggle="dropdown">
            <span class="feather feather-bell list-icon text-gray-600"></span>
            <span class="badge mr-10" v-bind:class="listNotification.length >= 1 ? 'bg-danger' : 'bg-score'" v-html="listNotification.length">0</span>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY paddingFix">
            @include('layouts.frontElements.header.portal_notification')
        </div>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropdown-toggle ripple" data-toggle="dropdown">
            <span class="feather feather-message-square list-icon text-gray-600"></span>
            <span class="badge mr-10" v-bind:class="listNotificationSMS.length >= 1 ? 'bg-danger' : 'bg-score'" v-html="listNotificationSMS.length">0</span>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY paddingFix">
            @include('layouts.frontElements.header.sms_notification')
        </div>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
            <i class="feather feather-globe list-icon text-gray-600"></i>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY paddingFix">
            @include('vendor.language.frontFlags')
        </div>
    </li>
    <li class="dropdown">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-toggle ripple">
            <i class="feather feather-log-out list-icon text-gray-600"></i>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>