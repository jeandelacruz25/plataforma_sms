<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="icon" type="image/png" href="{!! asset('favicon.png?version='.date('YmdHis'))!!}" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no ,shrink-to-fit=no" name="viewport">
        <link href="{!! asset('css/securitec_front.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/front.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        {!! Charts::styles(['chartjs']) !!}
        @yield('styles')
    </head>
    <body class="header-dark sidebar-light sidebar-collapse">
        <div id="wrapper" class="wrapper">
            <div id="frontVue">
                <nav class="navbar">
                    @include('layouts.frontElements.navBarLogo')
                    <div class="spacer"></div>
                    @include('layouts.frontElements.navBarHeader')
                </nav>
                <div class="content-wrapper">
                    @include('layouts.frontElements.sideBar')
                    <main class="main-wrapper clearfix">
                        <div id="utilVue" class="row page-title clearfix">
                            <div class="page-title-left">
                                <!--<p class="page-title-heading mr-0 mr-r-5">Bienvenido <span class="text-primary">{{ Auth::user()->full_name }}</span></p>-->
                                <span v-if="nameRoute.length > 0" class="page-title-description mr-0 d-none d-md-inline-block">módulo: <span class="text-score" v-html="nameRoute"></span></span>
                            </div>
                            <div class="page-title-right d-none d-xl-inline-block">
                                <i class="list-icon fa fa-calendar text-score"></i> <a class="text-gray-500" id="dateServer"></a> /
                                <i class="list-icon fa fa-clock-o text-score"></i> <a class="text-gray-500" id="hourServer"></a>
                            </div>
                            <div class="page-title-right d-xl-none" v-if="nameRoute.length > 0">
                                <i class="list-icon fa fa-dashboard text-score"></i> <a class="text-gray-500" v-html="nameRoute"></a>
                            </div>
                        </div>
                        <div class="widget-list mr-t-10">
                            @yield('content')
                        </div>
                    </main>
                    @include('layouts.frontElements.chat.index')
                    @include('layouts.recursos.modals')
                </div>
            </div>
        </div>
        <script src="{!! asset('js/app_score.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/node_front.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('assets/utilities/metismenu/metisMenu.min.js?version='.date('YmdHis'))!!}"></script>
        {!! Charts::scripts(['chartjs']) !!}
        <script src="{!! asset('assets/utilities/bonvue/theme.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/helper_front.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/vue/frontVue.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/score_datatables.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/scoreNode.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/helper_functions.js?version='.date('YmdHis'))!!}"></script>
        <script>
            var locale = '@lang("auth.formAuth.loading")'
        </script>
        @yield('scripts')
    </body>
</html>