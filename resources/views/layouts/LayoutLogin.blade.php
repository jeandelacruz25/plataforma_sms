<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="icon" type="image/png" href="{!! asset('favicon.png?version='.date('YmdHis'))!!}" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no ,shrink-to-fit=no" name="viewport">
        <link href="{!! asset('css/securitec_login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/pace.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
    </head>
    <body class="body-bg-full profile-page">
        <div id="wrapper" class="wrapper">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-md-8">
                        <img class="logo-expand" alt="" src="{!! asset('img/logo-securitec-login.png?version='.date('YmdHis')) !!}">
                    </div>
                    <div class="col-md-4 mt-3">
                        @include('vendor.language.loginFlags')
                    </div>
                </div>
                <div class="row mt-5 pt-5 ">
                    <div class="col text-white phrase d-none d-xl-block mt-0 mr-4">
                        <h1 class="mt-4 mb-0">@lang('custom.auth.phrase.textPhrase')</h1>
                        <h5>@lang('custom.auth.phrase.autorPhrase')</h5>
                    </div>
                    <div class="col-md-4">
                        <div class="login-center mt-0">
                            @yield('content')
                        </div>
                        <footer class="col-sm-12 clearfix text-white text-center">
                            <b>
                                @lang('custom.copyright') © <a href="http://www.securitec.pe/" class="text-white m-l-5"><b>Securitec Perú SAC</b></a> {{ \Carbon\Carbon::now()->format('Y') }}
                            </b>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
        <script src="{!! asset('assets/utilities/popper/popper.min.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/app_score.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/node_login.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/helper_login.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/form/formLogin.js?version='.date('YmdHis'))!!}"></script>
        <script>
            var textLoading = '@lang("auth.formAuth.loading")'
            hideErrorForm('.formError')
        </script>
    </body>
</html>