@if(!$model->customId)
    @include('charts::_partials.container.canvas2')
@endif

<script type="text/javascript">
    var ctx = document.getElementById("{{ $model->id }}")
    var {{ $model->id }} = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                @foreach($model->labels as $label)
                    "{!! $label !!}",
                @endforeach
            ],
            datasets: [
                @for ($i = 0; $i < count($model->datasets); $i++)
                    {
                        fill: true,
                        label: "{!! $model->datasets[$i]['label'] !!}",
                        lineTension: 0.3,
                        @if($model->colors and count($model->colors) > $i and !isset($model->datasets[$i]['color']))
                            borderColor: "{{ $model->colors[$i] }}",
                            backgroundColor: "{{ $model->colors[$i] }}",
                        @else
                            borderColor: "{{ $model->datasets[$i]['color'] }}",
                            backgroundColor: "{{ $model->datasets[$i]['color'] }}",
                        @endif
                        data: [
                            @foreach($model->datasets[$i]['values'] as $dta)
                                {{ $dta === 'NE' ? 0 : $dta }},
                            @endforeach
                        ],
                    },
                @endfor
            ]
        },
        options: {
            legend: {
                display: {{ $model->legend ? 'true' : 'false' }}
            },
            @if($model->stacked)
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
            @endif
            responsive: {{ $model->responsive || !$model->width ? 'true' : 'false' }},
            maintainAspectRatio: false,
                @if($model->title)
                    title: {
                display: true,
                    text: "{!! $model->title !!}",
                    fontSize: 20,
            },
            @endif
            scales: {
                xAxes: [{
                    @if($model->stacked)
                        stacked: true,
                    @endif
                }],
                yAxes: [{
                    @if($model->stacked)
                        stacked: true,
                    @endif
                    display: true,
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
</script>
