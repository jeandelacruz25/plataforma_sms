@if($model->values->isEmpty() != 1)
    @if(!$model->customId)
        @include('charts::_partials.container.canvas2')
    @endif

    <script type="text/javascript">
        var ctx = document.getElementById("{{ $model->id }}")

        var {{ $model->id }} = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    @foreach($model->labels as $label)
                        "{!! $label !!}",
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($model->values as $dta)
                        {{ $dta }},
                        @endforeach
                    ],
                    backgroundColor: [
                        @if($model->colors)
                                @foreach($model->colors as $color)
                            "{{ $color }}",
                        @endforeach
                                @else
                                @foreach($model->values as $dta)
                            "{{ sprintf('#%06X', mt_rand(0, 0xFFFFFF)) }}",
                        @endforeach
                        @endif
                    ],
                }]
            },
            options: {
                legend: {
                    display: {{ $model->legend ? 'true' : 'false' }},
                    labels: {
                        usePointStyle: true
                    },
                    position: 'right',
                },
                responsive: {{ $model->responsive || !$model->width ? 'true' : 'false' }},
                maintainAspectRatio: false,
                @if($model->title)
                title: {
                    display: true,
                    text: "{!! $model->title !!}",
                    fontSize: 20,
                }
                @endif
            }
        });
    </script>
@else
    <div class="col-md-12">
        <div class="alert alert-icon alert-warning">
            <i class="feather feather-alert-triangle list-icon mr-r-10"></i> <strong>No se encontró información con los filtros seleccionados</strong>
        </div>
    </div>

    <script></script>
@endif