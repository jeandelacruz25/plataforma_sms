<div class="card">
    <header class="card-header d-flex justify-content-between mb-0">
        <span class="heading-font-family flex-1 text-center fw-400 fs-13">Idioma <strong class="text-danger">(beta)</strong></span>
    </header>
    <ul class="card-body dropdown-list-group dropdown-flags">
        <li class="bg-white mr-t-10">
            @foreach (language()->allowed() as $code => $name)
                <a href="{{ language()->back($code) }}" class="media fs-12 fixLineHeight paddingFix2">
                    <span class="d-flex thumb-xs">
                        <img src="{{ asset('img/flags/'.strtolower($code).'.png') }}" alt="{{ $name }}" style="width: 35px; height: 20px;" /> &nbsp;
                        <span class="media-body">
                            <span class="media-content">{{ $name }}</span>
                        </span>
                    </span>
                </a>
            @endforeach
        </li>
    </ul>
    <footer class="card-footer text-center">
        <span class="text-uppercase fs-13"><strong class="text-score">@lang('custom.current.language')</strong> : <img src="{{ asset('img/flags/'.strtolower(App::getLocale()).'.png') }}" alt="{{ language()->getName(strtolower(App::getLocale())) }}" width="{{ config('language.flags.width') }}" /> &nbsp; {{ language()->getName(strtolower(App::getLocale())) }}</span>
    </footer>
</div>