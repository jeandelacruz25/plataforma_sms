<div class="btn-group dropdown m-r-10 float-right">
    <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle ripple" type="button">
        <i class="fa fa-globe list-icon"></i> <span class="caret"></span>
    </button>
    <div role="menu" class="dropdown-menu">
        <a class="dropdown-item" href="javascript:void(0)"><strong>@lang('custom.current.language') <label class="text-danger">(beta)</label></strong></a>
        <a class="dropdown-item" href="javascript:void(0)">
            <img src="{{ asset('img/flags/'.strtolower(App::getLocale()).'.png') }}" alt="{{ language()->getName(strtolower(App::getLocale())) }}" width="{{ config('language.flags.width') }}" /> &nbsp; {{ language()->getName(strtolower(App::getLocale())) }}
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="javascript:void(0)"><strong>@lang('custom.choose.language')</strong></a>
        @foreach (language()->allowed() as $code => $name)
            <a class="dropdown-item" href="{{ language()->back($code) }}">
                <img src="{{ asset('img/flags/'.strtolower($code).'.png') }}" alt="{{ $name }}" width="{{ config('language.flags.width') }}" /> &nbsp; {{ $name }}
            </a>
        @endforeach
    </div>
</div>