<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class smsSendInformativo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smsinformativo:getsend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob que actualizara todos los SMS Informativos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->getSendSMSInformativo();

        $this->info('Se actualizaron los SMS Informativo');
    }
}
