<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class smsDosVias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smsdosvias:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob que emitira las notificaciones si hay una nueva respuesta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->getNotificationSMS();

        $this->info('Se emitio socket ya que existe nuevas respuesta');
    }
}
