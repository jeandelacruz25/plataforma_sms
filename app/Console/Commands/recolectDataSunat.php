<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class recolectDataSunat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recolect:sunat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando captura la información de la SUNAT para nuestra BD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->recolectDataSunat();

        $this->info('Se recolecta data de la SUNAT');
    }
}
