<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class recolectDataSearchPeru extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recolect:searchperu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este cron es para poder obtener toda la información de searchperu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->searchPeruTelefonos();
        //$controller->searchPeruDirecciones();
        //$controller->searchPeruResumenLaboral();

        $this->info('Se recolecta data de SearchPeru');
    }
}
