<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class smsKeyword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smskeyword:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob que respondera todos los keywords recibidos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->getKeywordSMS();

        $this->info('Envio de respuesta para los keywords');
    }
}
