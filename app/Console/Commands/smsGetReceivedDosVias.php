<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class smsGetReceivedDosVias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smsdosvias:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob que traera todas las respuesta de los SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new \App\Http\Controllers\WebServiceController();
        $controller->getSMSReceivedLogs();

        $this->info('Se trajeron nuevas respuestas de SMS');
    }
}
