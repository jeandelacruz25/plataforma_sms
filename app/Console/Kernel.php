<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\smsKeyword::class,
        Commands\smsDosVias::class,
        Commands\smsGetReceivedDosVias::class,
        Commands\smsSendDosVias::class,
        Commands\smsSendInformativo::class,
        Commands\smsSendKeyword::class,
        Commands\recolectDataSunat::class,
        Commands\recolectDataSearchPeru::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->exec("php artisan smskeyword:update");
        $schedule->exec("php artisan smsdosvias:update");
        $schedule->exec("php artisan smsdosvias:notification");
        $schedule->exec("php artisan smsdosvias:getsend");
        $schedule->exec("php artisan smsinformativo:getsend");
        $schedule->exec("php artisan smskeyword:getsend");
        $schedule->exec("php artisan recolect:sunat");
        $schedule->exec("php artisan recolect:searchperu");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
