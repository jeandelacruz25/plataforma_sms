<?php

namespace App\Http\Controllers;

use App\Http\Requests\formAvatarRequest;
use App\Http\Requests\formUserRequest;
use App\Http\Requests\formUserStatusRequest;
use App\Models\Menu;
use App\Models\MenuRoles;
use App\Models\MenuUsers;
use App\Models\Estados;
use App\Models\User;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function paginationUsuarios(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            $whereCustom2 = ['id_rol', '!=', 1];
            array_push($whereArray, $whereCustom);
            array_push($whereArray, $whereCustom2);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        $usuarios = User::Select('id', 'id_cliente', 'avatar', 'nombres', 'apellidos', 'username', 'id_cliente', 'id_status', 'id_rol', 'session_id')
            ->with(['status', 'clientes'])
            ->where($whereArray)
            ->whereRaw('concat(nombres," ",apellidos) like ?', "%{$request->searchUsuario}%")
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $usuarios->total(),
                'per_page' => $usuarios->perPage(),
                'current_page' => $usuarios->currentPage(),
                'last_page' => $usuarios->lastPage(),
                'from' => $usuarios->firstItem(),
                'to' => $usuarios->lastItem()
            ],
            'data' => $usuarios
        ];

        return response()->json($response);
    }

    public function formAvatar(Request $request){
        if ($request->isMethod('get')) {
            $getUser = $this->getUser(Auth::user()->id);
            return view('elements/users/form/form_users_avatar')->with(array(
                'dataUser'  => $getUser
            ));
        }
        return view('errors/permission');
    }

    public function formUsers(Request $request){
        if($request->valueID == null){
            return view('elements/users/form/form_users')->with(array(
                'dataUser'          => '',
                'updateForm'        => false,
                'options'           => $this->selectionOption()
            ));
        }else{
            $getUser = $this->getUser($request->valueID);
            return view('elements/users/form/form_users')->with(array(
                'dataUser'          => $getUser,
                'updateForm'        => true,
                'options'           => $this->selectionOption()
            ));
        }
    }

    public function formUsersMenu(Request $request){
        $dataUser  = $this->getUser($request->valueID['idUser']);
        $getMenuUsers = $this->getMenuUser($request->valueID['idUser']);
        $getMenuCliente = $this->getMenuCliente($request->valueID['idCliente']);
        $arrayWhere = collect($getMenuCliente)->map(function ($value) {
            return $value['vue_name'];
        });
        $menuArray = Menu::menusClientes($arrayWhere);

        return view('elements/users/form/form_users_menu')->with(array(
            'dataUser'      => $dataUser,
            'menus'         => $menuArray,
            'checkMenu'     => $getMenuUsers
        ));
    }

    public function formUsersStatus(Request $request){
        $dataUser  = $this->getUser($request->valueID);
        $statusOptions = $this->getNotUserStatus($dataUser[0]['status']['id']);
        return view('elements/users/form/form_users_status')->with(array(
            'dataUser'      => $dataUser,
            'options'       => $statusOptions
        ));
    }

    public function saveFormAvatar(formAvatarRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $nameDirectory = public_path('img/avatars/');

            if (!File::exists($nameDirectory)) {
                $this->makeDirectory($nameDirectory);
            }

            if($request->avatarOriginal){
                $srcAvatar = $request->avatarOriginal;
                $userQuery = User::where('id', Auth::user()->id)
                    ->update([
                        'avatar'    => $srcAvatar
                    ]);
                if($userQuery){
                    return ['message' => 'Success'];
                }
            }else{
                $extensionFile = $request->file('avatarUser')->getClientOriginalExtension();
                $filename = Auth::user()->username.'.'.$extensionFile;

                $srcAvatar = 'img/avatars/'.$filename;
                $userQuery = User::where('id', Auth::user()->id)
                    ->update([
                        'avatar'    => $srcAvatar
                    ]);

                if ($request->file('avatarUser')->getClientOriginalExtension() == 'gif') {
                    $saveImage = copy($request->file('avatarUser')->getRealPath(), $nameDirectory.$filename);
                    $this->resetCookie('cookie_avatar', Auth::user()->avatar);
                }
                else {
                    $saveImage = Image::make($request->file('avatarUser')->getRealPath())->resize(512, 512)->save($nameDirectory . $filename);
                    $this->resetCookie('cookie_avatar', Auth::user()->avatar);
                }

                if($userQuery){
                    if($saveImage){
                        return ['message' => 'Success'];
                    }
                }
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsers(formUserRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $password = $request->password === $request->passwordValidate ? $request->password : Hash::make($request->password);
            $clienteUser = $request->clienteUser ? $request->clienteUser : Auth::user()->id_cliente;

            $userQuery = User::updateOrCreate([
                'id'    => $request->userID
            ], [
                'nombres'               => $request->userNombres,
                'apellidos'             => $request->userApellidos,
                'primer_apellido'       => $request->lastName,
                'segundo_apellido'      => $request->lastSecondName,
                'id_tipo_identidad'     => $request->typeIdentity,
                'num_identidad'         => $request->numIdentity,
                'fecha_nacimiento'      => $request->birthDay,
                'email'                 => $request->email,
                'username'              => $request->username,
                'password'              => $password,
                'id_status'             => $request->statusID,
                'id_rol'                => $request->roleUser,
                'id_cliente'            => $clienteUser
            ]);
            $userLastID = User::latest()->first();

            if($request->checkRolMenu){
                MenuUsers::where('user_id', $request->userID ? $request->userID : $userLastID['id'])->delete();
                $menuRole = MenuRoles::Select()
                    ->where('role_id', $request->roleUser)
                    ->get()
                    ->toArray();

                foreach ($menuRole as $keyUserRole => $valUserRole) {
                    MenuUsers::Insert([
                        'user_id'   => $request->userID ? $request->userID : $userLastID['id'],
                        'vue_name'  => $valUserRole['vue_name']
                    ]);
                }
            }
            $action = $request->userID ? 'update' : 'create';
            if ($userQuery) {
                return ['message' => 'Success', 'action' => $action, 'datatable' => 'listUsers'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsersMenu(Request $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            MenuUsers::where('user_id', $request->userID)->delete();
            if ($request->checkMenu) {
                foreach ($request->checkMenu as $keyRoleMenu => $valRoleMenu) {
                    $userMenuQuery = MenuUsers::updateOrCreate([
                        'user_id'   => $request->userID,
                        'vue_name'  => $valRoleMenu
                    ], [
                        'user_id'   => $request->userID,
                        'vue_name'  => $valRoleMenu
                    ]);
                }
                if ($userMenuQuery) {
                    $dataUser = $this->getUser($request->userID);
                    $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                    $clientNode->initialize();
                    $clientNode->emit('reloadMenuUsuario', [
                        'nameRoom'  => Str::lower($dataUser[0]['clientes']['palabra_clave']),
                        'userID'    => $request->userID
                    ]);
                    $clientNode->close();
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsersStatus(formUserStatusRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = User::where('id', $request->userID)
                ->update(['id_status' => $request->statusUser]);
            if($usersQuery){
                return ['message' => 'Success', 'datatable' => 'listUsers'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function keepSession(){
        Auth::user()->session_validate = 0;
        Auth::user()->save();

        return 'OK';
    }

    public function getUser($idUser){
        $user = User::Select()
            ->with(['identity', 'status', 'roles', 'clientes'])
            ->where('id', $idUser)
            ->get()
            ->toArray();
        return $user;
    }

    public function getNotUserStatus($idStatus){
        $statusUser = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusUser;
    }

    public function getMenuUser($idUser) {
        $user_menus = MenuUsers::Select()
            ->where('user_id', $idUser)
            ->get()
            ->toArray();

        return $user_menus;
    }
}
