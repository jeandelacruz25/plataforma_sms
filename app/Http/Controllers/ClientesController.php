<?php

namespace App\Http\Controllers;

use App\Http\Requests\formClienteRequest;
use App\Http\Requests\formClienteStatusRequest;
use App\Models\Clientes;
use App\Models\ClientesReglas;
use App\Models\Estados;
use App\Models\Menu;
use App\Models\MenuClientes;
use App\Models\ValidatePhoneModels\TelefonosCanales;
use Carbon\Carbon;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ClientesController extends SecuritecController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        if(searchMenuCliente(auth()->user()->id_cliente, 'clientes') != 0 && searchMenuUsuario(auth()->id(), 'clientes') != 0){
            return view('elements/clientes/index')->with(array(
                'titleModule'   => 'Administrar Clientes',
                'iconModule'    => 'fa fa-cogs'
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Administrar Clientes',
                'menuNombre'            => 'Clientes'
            ));
        }
    }

    protected function cliente_list_query()
    {
        $cliente_list_query = Clientes::Select()
            ->with('status')
            ->get()
            ->toArray();
        return $cliente_list_query;
    }

    public function paginationClientes(Request $request){
        $clientes = Clientes::Select()
            ->with('status')
            ->where([
                ['razon_social', 'LIKE', '%'.$request->searchCliente.'%']
            ])
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $clientes->total(),
                'per_page' => $clientes->perPage(),
                'current_page' => $clientes->currentPage(),
                'last_page' => $clientes->lastPage(),
                'from' => $clientes->firstItem(),
                'to' => $clientes->lastItem()
            ],
            'data' => $clientes
        ];

        return response()->json($response);
    }

    public function formClientes(Request $request){
        if($request->valueID == null){
            return view('elements/clientes/form/form_cliente')->with(array(
                'dataCliente'           => '',
                'updateForm'            => false
            ));
        }else{
            $getCliente = $this->getCliente($request->valueID);
            return view('elements/clientes/form/form_cliente')->with(array(
                'dataCliente'           => $getCliente,
                'updateForm'            => true
            ));
        }
    }

    public function formClientesStatus(Request $request){
        $dataCliente  = $this->getCliente($request->valueID);
        $statusOptions = $this->getNotClienteStatus($dataCliente[0]['status']['id']);
        return view('elements/clientes/form/form_cliente_status')->with(array(
            'dataCliente'       => $dataCliente,
            'options'           => $statusOptions
        ));
    }

    public function formClientesMenu(Request $request){
        $dataCliente  = $this->getCliente($request->valueID);
        $getMenuCliente = $this->getMenuCliente($request->valueID);

        return view('elements/clientes/form/form_cliente_menu')->with(array(
            'dataCliente'       => $dataCliente,
            'menus'             => Menu::menus(),
            'checkMenu'         => $getMenuCliente
        ));
    }

    public function formClientesRules(Request $request){
        $dataCliente  = $this->getCliente($request->valueID);
        return view('elements/clientes/form/form_cliente_reglas')->with(array(
            'dataCliente'       => $dataCliente,
            'dataRules'         => $dataCliente[0]['rules']['reglas']
        ));
    }

    public function saveFormClientes(formClienteRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $nameDirectory = public_path('img/clientes/');
            if(!File::exists($nameDirectory)){
                $this->makeDirectory($nameDirectory);
            }

            if($request->avatarOriginalCliente){
                $srcAvatar = $request->avatarOriginalCliente;
            }else{
                $srcAvatar = 'img/clientes/'.$request->rucCliente.'.png';
                Image::make($request->file('avatarCliente')->getRealPath())->resize(128, 128)->save($nameDirectory.$request->rucCliente.'.png');
            }

            $userCreate = $request->userCreate ? $request->userCreate : Auth::user()->id;
            $dateCreate = $request->dateCreate ? $request->dateCreate : Carbon::now();
            $statusID = $request->statusID ? $request->statusID : '1';

            $clientesQuery = Clientes::updateOrCreate([
                'id'   => $request->clienteID
            ], [
                'avatar'            => $srcAvatar,
                'ruc'               => $request->rucCliente,
                'razon_social'      => $request->nombreCliente,
                'correo_contacto'   => $request->emailContacto,
                'palabra_clave'     => Str::upper($request->palabraClave),
                'telefono'          => $request->telefonoCliente,
                'direccion_cliente' => $request->direccionCliente,
                'score_async'       => $request->checkSincronizacion ? 1 : 0,
                'id_estado'         => $statusID,
                'user_cre'          => $userCreate,
                'user_upd'          => Auth::user()->id,
                'created_at'        => $dateCreate,
                'updated_at'        => Carbon::now()
            ]);

            if(!$request->clienteID){
                $idLast = Clientes::Select('id')->orderBy('id', 'desc')->first();
                TelefonosCanales::insert([
                    'id_cliente'            => $idLast['id'],
                    'num_canales'           => 0,
                    'fecha_actualizacion'   => null
                ]);
            }

            if($clientesQuery){
                return ['message' => 'Success', 'datatable' => 'listClientes'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormClientesStatus(formClienteStatusRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $clientesQuery = Clientes::where('id', $request->clienteID)
                ->update(['id_estado' => $request->statusCliente]);
            if($clientesQuery){
                $action = $request->clienteID ? 'update' : 'create';
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormClientesMenu(Request $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            MenuClientes::where('cliente_id', $request->clienteID)->delete();
            if ($request->checkMenu) {
                foreach ($request->checkMenu as $keyRoleMenu => $valRoleMenu) {
                    $clienteMenuQuery = MenuClientes::updateOrCreate([
                        'cliente_id'    => $request->clienteID,
                        'vue_name'      => $valRoleMenu
                    ], [
                        'cliente_id'    => $request->clienteID,
                        'vue_name'      => $valRoleMenu
                    ]);
                }
                if ($clienteMenuQuery) {
                    $dataCliente = $this->getCliente($request->clienteID);
                    $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                    $clientNode->initialize();
                    $clientNode->emit('reloadMenuCliente', [
                        'nameRoom' => Str::lower($dataCliente[0]['palabra_clave'])
                    ]);
                    $clientNode->close();
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormClientesRules(Request $request){
        if ($request->isMethod('post')) {
            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $arrayRules = array();

            $arrayNumeroCanales = ['cantidad_numero_canales' => $request->numberCanal ? $request->numberCanal : 0];
            $arrayRules = array_merge($arrayRules, $arrayNumeroCanales);

            $arrayNumeroValidaciones = ['cantidad_numero_validaciones' => $request->numberValidate ? $request->numberValidate : 0];
            $arrayRules = array_merge($arrayRules, $arrayNumeroValidaciones);

            $arrayInicioValidacion = ['inicio_validacion_telefono'  => $request->inicioMasivo ? $request->inicioMasivo : '07:00'];
            $arrayRules = array_merge($arrayRules, $arrayInicioValidacion);

            $arrayFinValidacion = ['fin_validacion_telefono'  => $request->finMasivo ? $request->finMasivo : '20:00'];
            $arrayRules = array_merge($arrayRules, $arrayFinValidacion);

            $arrayDocumentoValidacion = ['cantidad_documento_validaciones' => $request->documentValidate ? $request->documentValidate : 0];
            $arrayRules = array_merge($arrayRules, $arrayDocumentoValidacion);

            $clientesReglas = ClientesReglas::updateOrCreate([
                'id_cliente'    => $request->clienteID,
            ], [
                'id_cliente'    => $request->clienteID,
                'reglas'        => $arrayRules
            ]);

            if ($clientesReglas) {
                return ['message' => 'Success'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function getNotClienteStatus($idStatus){
        $statusCliente = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusCliente;
    }
}
