<?php

namespace App\Http\Controllers\PersonControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formCampaignStatusBaseRequest;
use App\Http\Requests\formSearchMasiveRequest;
use App\Http\Requests\formSearchMasiveUploadRequest;
use App\Models\Estados;
use App\Models\SearchModels\BaseCampanas;
use App\Models\SearchModels\BaseCriterio;
use App\Models\SearchModels\BaseDetalle;
use App\Models\SearchModels\CruceAutos;
use App\Models\SearchModels\CruceDirecciones;
use App\Models\SearchModels\CruceFamiliares;
use App\Models\SearchModels\CruceFinanciero;
use App\Models\SearchModels\CrucePersonas;
use App\Models\SearchModels\CruceSueldos;
use App\Models\SearchModels\CruceSunat;
use App\Models\SearchModels\CruceTelefonos;
use Carbon\Carbon;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class SearchMasivoController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function paginationBaseCampanas(Request $request){
        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->searchCampana){
            $whereCustom = ['nombre_campana', 'LIKE', '%'.$request->searchCampana.'%'];
            array_push($whereArray, $whereCustom);
        }

        $campanas = BaseCampanas::Select()
            ->with(['status', 'clientes', 'criterios', 'criterios_finish'])
            ->withCount(['criterios', 'criterios_count', 'criterio_phone'])
            ->where($whereArray)
            ->where(function($query) use ($request) {
                if($request->searchDateRange){
                    $dateFilter = explode(' / ', $request->searchDateRange);
                    $query->whereBetween('created_at', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
                }else{
                    $query->whereMonth('created_at', Carbon::now()->format('m'));
                }
            })
            ->orderby('id_status', 'asc')
            ->orderby('created_at', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $campanas->total(),
                'per_page' => $campanas->perPage(),
                'current_page' => $campanas->currentPage(),
                'last_page' => $campanas->lastPage(),
                'from' => $campanas->firstItem(),
                'to' => $campanas->lastItem()
            ],
            'data' => $campanas
        ];

        return response()->json($response);
    }

    public function formCampaignBase(Request $request){
        if($request->valueID == null){
            return view('elements/person/tabs/masiva/form/form_campaign')->with(array(
                'dataCampaign'          => '',
                'updateForm'            => false,
                'options'               => $this->selectionOption()
            ));
        }else{
            $getCampaign = $this->getCampaignBase($request->valueID);
            return view('elements/person/tabs/masiva/form/form_campaign')->with(array(
                'dataCampaign'          => $getCampaign,
                'updateForm'            => true,
                'options'               => $this->selectionOption()
            ));
        }
    }

    public function formCampaignBaseUpload(Request $request){
        $getCampaign = $this->getCampaignBase($request->valueID);
        $getCliente = $this->getCliente($getCampaign[0]['id_cliente']);
        $documentValidation = isset($getCliente[0]['rules']['reglas']['cantidad_documento_validaciones'])  ? $getCliente[0]['rules']['reglas']['cantidad_documento_validaciones'] : 0;

        $documentCliente = BaseDetalle::whereHas('campana', function ($query) use($getCampaign) {
            $query->where('id_cliente', '=', $getCampaign[0]['id_cliente']);
        })->whereIn('id_campania', [$getCampaign[0]['id']])->select(DB::raw('count(*) as cantidad_documentos'))->get()->toArray();

        $countDocumentValidation = ($documentValidation - $documentCliente[0]['cantidad_documentos']);

        return view('elements/person/tabs/masiva/form/form_campaign_upload')->with(array(
            'dataCampaign'              => $getCampaign,
            'validationRules'           => $documentValidation <= 0 ? false : true,
            'documentValidation'        => $countDocumentValidation <= 0 ? false : true,
            'countDocumentValidation'   => $countDocumentValidation
        ));
    }

    public function formCampaignBaseStatus(Request $request){
        $dataCampaign  = $this->getCampaignBase($request->valueID);
        $statusOptions = $this->getNotCampaignStatus($dataCampaign[0]['status']['id']);
        return view('elements/person/tabs/masiva/form/form_campaign_status')->with(array(
            'dataCampaign'      => $dataCampaign,
            'options'           => $statusOptions
        ));
    }

    public function saveFormCampaignBase(formSearchMasiveRequest $request){
        if ($request->isMethod('post')) {
            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $campaignQuery = BaseCampanas::updateOrCreate([
                'id'                    => $request->campaignID
            ], [
                'nombre_campana'        => $request->nameCampaign,
                'estado_base'           => $request->estadoBase ? $request->estadoBase : 0,
                'total_documentos'      => $request->totalDocumentos ? $request->totalDocumentos : 0,
                'id_cliente'            => $request->clienteCampaign ? $request->clienteCampaign : Auth::user()->id_cliente,
                'user_created'          => $request->userCreated ? $request->userCreated : Auth::user()->id,
                'user_updated'          => Auth::user()->id
            ]);

            if($campaignQuery){
                $action = $request->campaignID ? 'update' : 'create';

                if(!$request->campaignID){
                    BaseCriterio::where('id_campana', $request->campaignID)->delete();

                    $lastCampaign = BaseCampanas::latest()->first();

                    foreach($request->checkCriterio as $key => $value){
                        BaseCriterio::insert([
                            'id_campana'    => $lastCampaign['id'],
                            'id_criterio'   => $value,
                            'fecha_reg'     => Carbon::now(),
                            'id_estado'     => 0
                        ]);
                    }

                    return ['message' => 'Success', 'action' => $action];
                }

                return ['message' => 'Success', 'action' => $action];
            }
        }
        return ['message' => 'Error'];
    }

    public function saveFormCampaignBaseUpload(formSearchMasiveUploadRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            return [
                'message' => 'Success'
            ];
        }
        return ['message' => 'Error'];
    }

    public function executeUploadCampaignBase(Request $request){
        $idCampana = $request->campaignID;

        $dataCampaign = $this->getCampaignBase($idCampana);

        BaseCampanas::where('id', $idCampana)->update([
            'estado_base'   => 2
        ]);

        try {

            app()->instance('insertCampaignBaseUpload', collect());
            Excel::filter('chunk')->load($request->file('baseClientes')->getRealPath())->chunk(4000, function ($rows) {
                app()->instance('insertCampaignBaseUpload', app('insertCampaignBaseUpload')->merge($rows->toArray()));
            }, $shouldQueue = false);

            $dataSheet = app('insertCampaignBaseUpload')->toArray();

            $dbInsert = [];
            $count = 0;
            foreach (collect($dataSheet)->chunk(4000)->toArray() as $chunk) {
                foreach($chunk as $key => $value){
                    if(strlen($value['dni']) == 8 || strlen($value['dni']) == 11){
                        $count++;
                        if($count <= $request->documentCliente){
                            $arrayInsert = [
                                'id_campania'    => $idCampana,
                                'nrodoc'         => $value['dni'],
                                'fecha_subida'   => Carbon::now()
                            ];

                            array_push($dbInsert, $arrayInsert);
                        }
                    }
                }
            }

            DB::beginTransaction();

            try {
                $insertDetalle = BaseDetalle::insert($dbInsert);

                if($insertDetalle){
                    $documentosDetalleCount = BaseDetalle::where('id_campania', $idCampana)->count();

                    $documentCampanaUpdate = BaseCampanas::where('id', $idCampana)->update([
                        'total_documentos'   => $documentosDetalleCount,
                        'estado_base'        => 0
                    ]);

                    if($documentCampanaUpdate){
                        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                        $clientNode->initialize();
                        $clientNode->emit('reloadDatatable', [
                            'nameDatatable' => 'listCampaignBase',
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave'])
                        ]);
                        $clientNode->emit('notificationCampaign', [
                            'nameCampaign' => $dataCampaign[0]['nombre_campana'],
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                            'tipoSubida' => 5,
                            'licenseExceeded' => ($request->documentCliente - $count) < 0 ? true : false
                        ]);
                        $clientNode->close();

                        DB::commit();

                        return ['message' => 'Success'];
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                BaseCampanas::where('id', $idCampana)->update([
                    'estado_base'   => 0
                ]);
                return ['message' => 'Error'];
            }

        }catch (\Exception $e){
            BaseCampanas::where('id', $idCampana)->update([
                'estado_base'   => 0
            ]);
            return ['message' => 'Error'];
        }
    }

    public function saveFormCampaignBaseStatus(formCampaignStatusBaseRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = BaseCampanas::where('id', $request->campaignID)
                ->update(['id_status' => $request->statusCampaign]);
            if($usersQuery){
                return ['message' => 'Success'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function formBaseSMS(Request $request){
        $getCampaign = $this->getCampaignBase($request->valueID);
        return view('elements/person/tabs/masiva/form/form_campaign_send')->with(array(
            'dataCampaign'          => $getCampaign
        ));
    }

    public function formBaseCruce(Request $request){
        $getCampaign = $this->getCampaignBase($request->valueID);
        $countCampaign = $this->getCountCampaignBase($getCampaign[0]['id_cliente']);
        return view('elements/person/tabs/masiva/form/form_campaign_cruce')->with(array(
            'dataCampaign'          => $getCampaign,
            'countCampaign'         => $countCampaign
        ));
    }

    public function startCruceBase(Request $request){
        if ($request->isMethod('post')) {
            $campaignBase = BaseCampanas::where('id', $request->campaignID)
                ->update([
                    'usuario_envio' => Auth::id(),
                    'estado_base'   => 3,
                    'fecha_envio'   => Carbon::now()
                ]);

            if($campaignBase){
                try{
                    $return = DB::statement("CALL SP_GENERA_CRUCEBASES(" . $request->campaignID . ", " . Auth::id() . ")");
                    return 'Success';
                }catch (\Exception $e){
                    BaseCampanas::where('id', $request->campaignID)
                        ->update([
                            'estado_base'   => 0,
                            'fecha_envio'   => null
                        ]);
                }
            }
        }
    }

    public function finishCruceBase(Request $request){
        $dataCampaign = $this->getCampaignBase($request->campaignID);

        $validateSocket = BaseCampanas::where([
            ['id_cliente', '=', $dataCampaign[0]['id_cliente']],
            ['estado_base', '=', 3]
        ])->count();

        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
        $clientNode->initialize();
        $clientNode->emit('reloadDatatable', [
            'nameDatatable' => 'listCampaignBase',
            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave'])
        ]);
        $clientNode->emit('notificationCampaign', [
            'nameCampaign' => $dataCampaign[0]['nombre_campana'],
            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
            'tipoSubida' => 6,
            'licenseExceeded' => false
        ]);

        if($validateSocket == 1){
        	BaseCampanas::where([
	            ['id', '=', $request->campaignID]
	        ])->update([
	        	'estado_base' => 1,
                'fecha_fin' => Carbon::now()
	        ]);

            $clientNode->emit('clearTimeOut', [
                'nameTimeOut' => 'listCampaignBase',
                'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                'userID' => auth()->user()->id
            ]);
        }

        $clientNode->close();

        return 'Success';
    }

    public function asyncBaseValidation(Request $request){
        $idCampaign = $request->campaignID;
        $nameCampaign = $request->nameCampaign;

        $callProcedure = DB::statement('CALL SP_GENERA_VALIDACION('.$idCampaign.', "'.$nameCampaign.'", '.Auth::id().')');

        if($callProcedure){
            return ['message' => 'Success'];
        }

        return ['message' => 'Error'];
    }

    protected function builderExportReportCruce($cruce_query, $type_cruce)
    {
        $posicion = 0;
        foreach ($cruce_query as $query) {
            if($type_cruce == 1){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Apellido Paterno']         = ucwords(Str::lower($query['apellidopat']));
                $builderview[$posicion]['Apellido Materno']         = ucwords(Str::lower($query['apellidomat']));
                $builderview[$posicion]['Nombres']                  = ucwords(Str::lower($query['nombres']));
            }else if($type_cruce == 2){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Nro Telefono']             = $query['telefono'];
                $builderview[$posicion]['Tipo']                     = $query['tipo'];
                $builderview[$posicion]['Estado']                   = getStatusValidacion($query['id_estado']);
                $builderview[$posicion]['Fecha Validación']         = $query['fecha_val'] ? Carbon::parse($query['fecha_val'])->format('d/m/Y H:i:s a') : '-';
            }else if($type_cruce == 3){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Departamento']             = ucwords(Str::lower($query['dpto']));
                $builderview[$posicion]['Provincia']                = ucwords(Str::lower($query['prov']));
                $builderview[$posicion]['Distrito']                 = ucwords(Str::lower($query['dist']));
                $builderview[$posicion]['Dirección']                = ucwords(Str::lower($query['direccion']));
                $builderview[$posicion]['Referencia']               = $query['referencia'] ? $query['referencia'] : '-';
            }else if($type_cruce == 4){
                $builderview[$posicion]['Nro Documento']                = $query['nrodoc'];
                $builderview[$posicion]['Parentezco']                   = $query['tipo_vinculo'];
                $builderview[$posicion]['Nro Documento Familiar']       = $query['nrodoc_f'];
                $builderview[$posicion]['Apellido Paterno Familiar']    = ucwords(Str::lower($query['apellidopat_f']));
                $builderview[$posicion]['Apellido Materno Familiar']    = ucwords(Str::lower($query['apellidomat_f']));
                $builderview[$posicion]['Nombres Familiar']             = ucwords(Str::lower($query['nombres_f']));
            }else if($type_cruce == 5){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Empresa']                  = ucwords(Str::lower($query['empresa']));
                $builderview[$posicion]['Periodo']                  = $query['periodo'];
                $builderview[$posicion]['Sueldo']                   = $query['sueldo'];
            }else if($type_cruce == 6){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['RUC']                      = $query['ruc'];
                $builderview[$posicion]['Razón Social']             = $query['razonsocial'];
                $builderview[$posicion]['Telefono']                 = $query['telefono'];
                $builderview[$posicion]['Condición']                = $query['condicion'];
                $builderview[$posicion]['Nombre Comercial']         = $query['nombrecomercial'];
                $builderview[$posicion]['Tipo']                     = $query['tipo'];
                $builderview[$posicion]['Inscripción']              = $query['inscripcion'];
                $builderview[$posicion]['Estado']                   = $query['estado'];
                $builderview[$posicion]['Dirección']                = $query['direccion'];
                $builderview[$posicion]['Sistema Emisión']          = $query['sistemaemision'];
                $builderview[$posicion]['Actividad Exterior']       = $query['actividadexterior'];
                $builderview[$posicion]['Sistema de Contabilidad']  = $query['sistemacontabilidad'];
                $builderview[$posicion]['Oficio']                   = $query['oficio'];
                $builderview[$posicion]['Emisión Electronica']      = $query['emisionelectronica'];
                $builderview[$posicion]['PLE']                      = $query['ple'];
                $builderview[$posicion]['Representantes Legales']   = $query['representantes_legales'];
                $builderview[$posicion]['Cantidad Trabajadores']    = $query['cantidad_trabajadores'];
            }else if($type_cruce == 7){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Fecha Reporte']            = $query['fechareporte'];
                $builderview[$posicion]['Número de Entidades']      = $query['nroempresas'];
                $builderview[$posicion]['Calificación Normal']      = $query['calif_0'];
                $builderview[$posicion]['Calificación CPP']         = $query['calif_1'];
                $builderview[$posicion]['Calificación Deficiente']  = $query['calif_2'];
                $builderview[$posicion]['Calificación Dudoso']      = $query['calif_3'];
                $builderview[$posicion]['Calificación Perdida']     = $query['calif_4'];
            }else if($type_cruce == 8){
                $builderview[$posicion]['Nro Documento']            = $query['nrodoc'];
                $builderview[$posicion]['Placa']                    = $query['placa'];
                $builderview[$posicion]['Marca']                    = $query['marca'];
                $builderview[$posicion]['Modelo']                   = $query['modelo'];
                $builderview[$posicion]['Fecha']                    = $query['fabricacion'];
                $builderview[$posicion]['Tipo / Auto']              = $query['tipo'];
                $builderview[$posicion]['Color']                    = $query['color'];
                $builderview[$posicion]['Combustible']              = $query['combustible'];
            }
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    public function getCruceCampaign($idCampaign, $typeCruce){
        $dataCruce = [];

        switch ($typeCruce){
            case 1:
                $dataCruce = CrucePersonas::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 2:
                $dataCruce = CruceTelefonos::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 3:
                $dataCruce = CruceDirecciones::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 4:
                $dataCruce = CruceFamiliares::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 5:
                $dataCruce = CruceSueldos::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 6:
                $dataCruce = CruceSunat::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 7:
                $dataCruce = CruceFinanciero::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
            case 8:
                $dataCruce = CruceAutos::Select()->where('id_campania', $idCampaign)->get()->toArray();
                break;
        }

        return $dataCruce;
    }

    public function downloadReportBase(Request $request){
        $detalleBase = BaseDetalle::where('id_campania', $request->idCampana)->get()->toArray();

        if($detalleBase){
            $campana = BaseCampanas::where('id', $request->idCampana)->first();
            $typeDownload = $request->typeDownload;
            $nameCampana = substr(cleanString(str_replace(' ', '_', $campana['nombre_campana'])).'_base_'.time(), 0, 31);
            $baseCriterios = BaseCriterio::where('id_campana', $request->idCampana)->get()->toArray();

            Excel::create($nameCampana, function($excel) use($campana, $typeDownload, $baseCriterios) {
                if($typeDownload != 0){
                    $excel->sheet(nameCruce($typeDownload), function($sheet) use($campana, $typeDownload) {
                        $sheet->fromArray($this->builderExportReportCruce($this->getCruceCampaign($campana['id'], $typeDownload), $typeDownload));
                    });
                }else{
                    foreach($baseCriterios as $key => $value){
                        $excel->sheet(nameCruce($value['id_criterio']), function($sheet) use($value) {
                            $sheet->fromArray($this->builderExportReportCruce($this->getCruceCampaign($value['id_campana'], $value['id_criterio']), $value['id_criterio']));
                        });
                    }
                }
            })->store('xlsx', 'exports');

            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameCampana.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    public function getCampaignBase($idCampaign){
        $campaign = BaseCampanas::Select()
            ->with(['status', 'clientes', 'criterios', 'user'])
            ->where('id', $idCampaign)
            ->get()
            ->toArray();

        return $campaign;
    }

    public function getNotCampaignStatus($idStatus){
        $statusCampaign = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusCampaign;
    }

    public function getCountCampaignBase($idCliente){
        $campaign = BaseCampanas::Select()
            ->where('id_cliente', $idCliente)
            ->whereIn('estado_base', [3])
            ->count();

        return $campaign;
    }
}
