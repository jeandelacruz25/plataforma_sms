<?php

namespace App\Http\Controllers\PersonControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formSearchPersonRequest;
use App\Models\SearchModels\Autos;
use App\Models\SearchModels\BaseIndividual;
use App\Models\SearchModels\BasesEstadisticas;
use App\Models\SearchModels\Financiero;
use App\Models\SearchModels\FinancieroDetalle;
use App\Models\SearchModels\Personas;
use App\Models\SearchModels\SunatDetalle;
use App\Models\SearchModels\Telefonos;
use App\Models\ValidatePhoneModels\TelefonosCanales;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use DataFalabella\DataFalabella;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Sunat\Sunat;

class SearchController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(searchMenuCliente(auth()->user()->id_cliente, 'search_person') != 0 && searchMenuUsuario(auth()->id(), 'search_person') != 0){
            return view('elements/person/index')->with(array(
                'titleModule'           => 'Busqueda de personas',
                'iconModule'            => 'fa fa-search',
                'selectOptions'         => $this->selectionOption(),
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Busqueda de personas',
                'menuNombre'            => 'Bases'
            ));
        }
    }

    public function getDataFormIndividualSearch(formSearchPersonRequest $request){
        if ($request->isMethod('post')) {
            if($request->dniPersona && !$request->numPlacaAuto && !$request->apePatPersona){
                $viewRuc = strlen($request->dniPersona) > 9 ? true : false;
                return ['message' => 'Success', 'numDNI' => $request->dniPersona, 'viewRuc' => $viewRuc];
            }else if($request->numPlacaAuto){
                return ['message' => 'SuccessAuto'];
            }else{
                return ['message' => 'Success'];
            }
        }
        return ['message' => 'Error'];
    }

    public function getPersonDNI(Request $request){
        $dataPersona = Personas::Select('NRODOC', 'FECHA_EVAL')
            ->where('NRODOC', $request->dniPersona)
            ->first();

        return $dataPersona;
    }

    public function paginationDataPersona(Request $request){

        $arrayWhere = [];
        if($request->dniPersona){
            $arrayCustom = ['NRODOC', '=', $request->dniPersona];
            array_push($arrayWhere, $arrayCustom);
        }

        if($request->nomPersona){
            $arrayCustom = ['NOMBRES', 'LIKE', '%'.$request->nomPersona.'%'];
            array_push($arrayWhere, $arrayCustom);
        }

        if($request->apePaterno){
            $arrayCustom = ['APELLIDO_PAT', '=', $request->apePaterno];
            array_push($arrayWhere, $arrayCustom);
        }

        if($request->apeMaterno){
            $arrayCustom = ['APELLIDO_MAT', '=', $request->apeMaterno];
            array_push($arrayWhere, $arrayCustom);
        }

        $dataPersona = Personas::Select('NRODOC', 'TIPODOC', 'APELLIDO_PAT', 'APELLIDO_MAT', 'NOMBRES', 'DIRECCION', 'FECHA_NAC')
            ->where($arrayWhere)->paginate(15);

        /*if($request->departamento || $request->provincia || $request->distrito){
            $dataPersona->whereHas('direccion', function ($query) use($request) {
                if($request->departamento) $query->where('DEPARTAMENTO', '=', $request->departamento);
                if($request->provincia) $query->where('PROVINCIA', '=', $request->provincia);
                if($request->distrito) $query->where('DISTRITO', '=', $request->distrito);
            });
        }*/

        /*$dataPagination = $dataPersona->with('direccion')
            ->paginate(10);*/

        /*$arrayDepartamentos = [];
        $arrayProvincias = [];
        $arrayDistritos = [];

        foreach ($dataPagination as $key => $value) {
            $direccion = $value['direccion'];
            if($direccion){
                array_push($arrayDepartamentos, $direccion['DEPARTAMENTO']);
                array_push($arrayProvincias, $direccion['PROVINCIA']);
                array_push($arrayDistritos, $direccion['DISTRITO']);
            }
        }*/

        $response = [
            'pagination' => [
                'total' => $dataPersona->total(),
                'per_page' => $dataPersona->perPage(),
                'current_page' => $dataPersona->currentPage(),
                'last_page' => $dataPersona->lastPage(),
                'from' => $dataPersona->firstItem(),
                'to' => $dataPersona->lastItem()
            ],
            'data' => $dataPersona/*,
            'dataDepartamentos' => collect($arrayDepartamentos)->unique()->sort()->values()->toArray(),
            'dataProvincias' => collect($arrayProvincias)->unique()->sort()->values()->toArray(),
            'dataDistritos' => collect($arrayDistritos)->unique()->sort()->values()->toArray(),*/
        ];

        return response()->json($response);
    }

    public function paginationDataAuto(Request $request){

        $arrayWhere = [];
        if($request->numPlaca){
            $arrayCustom = ['PLACA', '=', $request->numPlaca];
            array_push($arrayWhere, $arrayCustom);
        }

        $dataAuto = Autos::Select()
            ->where($arrayWhere)
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $dataAuto->total(),
                'per_page' => $dataAuto->perPage(),
                'current_page' => $dataAuto->currentPage(),
                'last_page' => $dataAuto->lastPage(),
                'from' => $dataAuto->firstItem(),
                'to' => $dataAuto->lastItem()
            ],
            'data' => $dataAuto
        ];

        return response()->json($response);
    }

    public function personDetails(Request $request){
        return view('elements/person/tabs/individual/utils/person_details')->with(array(
            'numDNI'    => $request->numDNI
        ));
    }

    public function personDetailsFirst(Request $request){
        return view('elements/person/tabs/individual/utils/person_details_first')->with(array(
            'numDNI'    => $request->numDNI
        ));
    }

    public function formPersonValidation(Request $request){
        $countCampaign = $this->getCountCampaign(Auth::user()->id_cliente);
        $countCanalesUsed = TelefonosCanales::where('id_cliente', Auth::user()->id_cliente)->first();
        $getCliente = $this->getCliente(Auth::user()->id_cliente);
        $getBaseIndividual = $this->getBaseIndividual($request->valueID['numDNI']);
        $countCanales = isset($getCliente[0]['rules']['reglas']['cantidad_numero_canales']) ? $getCliente[0]['rules']['reglas']['cantidad_numero_canales'] : 0;
        $inicioMasivo = isset($getCliente[0]['rules']['reglas']['inicio_validacion_telefono']) ? $getCliente[0]['rules']['reglas']['inicio_validacion_telefono'] : '07:00';
        $finMasivo = isset($getCliente[0]['rules']['reglas']['fin_validacion_telefono']) ? $getCliente[0]['rules']['reglas']['fin_validacion_telefono'] : '20:00';
        return view('elements/person/tabs/individual/form/form_validation_start')->with(array(
            'numDNI'            => $request->valueID['numDNI'],
            'countCanalesUsed'  => $countCanalesUsed['num_canales'],
            'countCampaign'     => $countCampaign,
            'countCanales'      => $countCanales,
            'inicioMasivo'      => $inicioMasivo,
            'finMasivo'         => $finMasivo,
            'baseIndividual'    => $getBaseIndividual
        ));
    }

    public function executePersonValidation(Request $request){
        $numCanales = $request->numCanales;

        $countCanalesUsed = TelefonosCanales::where('id_cliente', Auth::user()->id_cliente)->first();

        if($numCanales - $countCanalesUsed['num_canales'] > 0){
            return ['message' => 'Success'];
        }else{
            return ['message' => 'Error'];
        }
    }

    public function executePersonValidationStart(Request $request){
        $numDNI = $request->numDNI;
        $numCanales = $request->numCanales;

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST','http://192.168.80.3/individual.php?numDNI='.$numDNI.'&nroCanales='.$numCanales.'&idCliente='.Auth::user()->id_cliente);
        $statusResponse = $response->getBody()->getContents();

        $dataCliente = $this->getCliente(auth()->user()->id_cliente);

        if($statusResponse != 'stop'){
            $searchData = BaseIndividual::where('nrodoc', $numDNI)->count();

            if($searchData > 0){
                BaseIndividual::where('nrodoc', $numDNI)->update(['fecha_validacion' => Carbon::now()]);
            }else{
                BaseIndividual::insert(['fecha_validacion' => Carbon::now(), 'nrodoc' => $numDNI,]);
            }

            $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
            $clientNode->initialize();
            $clientNode->emit('reloadDatatable', [
                'nameDatatable' => 'dataPersonDetailsTelephone',
                'nameRoom' => Str::lower($dataCliente[0]['palabra_clave'])
            ]);
            $clientNode->close();
        }

        return ['message' => 'Success'];
    }

    public function baseEstadistica(Request $request){
        $founded = $request->found;
        if($founded){
            BasesEstadisticas::insert([
                'id_cliente'    => Auth::user()->id_cliente,
                'nrodoc'        => $request->numDNI,
                'fecha'         => Carbon::now(),
                'personas'      => 1,
                'telefonos'     => 1,
                'direcciones'   => 1,
                'familiares'    => 1,
                'sueldos'       => 1,
                'sunat'         => 1,
                'financiero'    => 1,
                'autos'         => 1,
                'user_id'       => Auth::id(),
                'id_campania'   => 0
            ]);
        }else{
            BasesEstadisticas::insert([
                'id_cliente'    => Auth::user()->id_cliente,
                'nrodoc'        => $request->numDNI,
                'fecha'         => Carbon::now(),
                'personas'      => 0,
                'telefonos'     => 0,
                'direcciones'   => 0,
                'familiares'    => 0,
                'sueldos'       => 0,
                'sunat'         => 0,
                'financiero'    => 0,
                'autos'         => 0,
                'user_id'       => Auth::id(),
                'id_campania'   => 0
            ]);
        }

        return 'Success';
    }

    public function getDataPersona(Request $request){

        $dataPersona = Personas::Select()
            ->with(['telefonos', 'direcciones', 'familiares', 'autos', 'laboral'])
            ->where('NRODOC', $request->dniPersona)
            ->get()->toArray();

        $resultPerson = $dataPersona;

        return response()->json([
            'datosPersonales' => [
                'APELLIDO_MAT'      => Str::upper($resultPerson[0]['APELLIDO_MAT']),
                'APELLIDO_PAT'      => Str::upper($resultPerson[0]['APELLIDO_PAT']),
                'FECHA_NAC'         => $resultPerson[0]['FECHA_NAC'],
                'NOMBRES'           => Str::upper($resultPerson[0]['NOMBRES']),
                'NRODOC'            => $resultPerson[0]['NRODOC'],
                'PROFESION'         => $resultPerson[0]['PROFESION'],
                'SEXO'              => $resultPerson[0]['SEXO'],
                'TIPODOC'           => $resultPerson[0]['TIPODOC']
            ],
            'datosTelefonos' => $resultPerson[0]['telefonos'],
            'datosDirecciones' => $resultPerson[0]['direcciones'],
            'datosFamiliares' => $resultPerson[0]['familiares'],
            'datosAutos' => $resultPerson[0]['autos'],
            'datosLaborales' => $resultPerson[0]['laboral'],
        ]);
    }

    public function getDataTelefonos(Request $request)
    {
        $dataTelefonos = Telefonos::Select()
            ->where('NRODOC', $request->dniPersona)
            ->get()->toArray();

        return $dataTelefonos;
    }

    public function getDataPersonSunat(Request $request){
        $sunatAPI = new Sunat();
        if($request->dniPersona){
            $dataSunat = $sunatAPI->search($request->dniPersona, false);

            if($dataSunat['success']){
                $resultSunat = $this->latin_to_utf8($dataSunat['result']);

                $dataFecha = Carbon::now();

                SunatDetalle::updateOrCreate([
                    'NRODOC'                    => $request->dniPersona,
                    'RUC'                       => $resultSunat['RUC'],
                ], [
                    'NRODOC'                    => $request->dniPersona,
                    'FECHA_REG'                 => $dataFecha,
                    'RUC'                       => $resultSunat['RUC'],
                    'RazonSocial'               => $resultSunat['RazonSocial'],
                    'Telefono'                  => $resultSunat['Telefono'],
                    'Condicion'                 => $resultSunat['Condicion'],
                    'NombreComercial'           => $resultSunat['NombreComercial'],
                    'Tipo'                      => $resultSunat['Tipo'],
                    'Inscripcion'               => $resultSunat['Inscripcion'],
                    'Estado'                    => $resultSunat['Estado'],
                    'Direccion'                 => $resultSunat['Direccion'],
                    'SistemaEmision'            => $resultSunat['SistemaEmision'],
                    'ActividadExterior'         => $resultSunat['ActividadExterior'],
                    'SistemaContabilidad'       => $resultSunat['SistemaContabilidad'],
                    'Oficio'                    => isset($resultSunat['Oficio']) ? $resultSunat['Oficio'] : '-',
                    'EmisionElectronica'        => $resultSunat['EmisionElectronica'],
                    'PLE'                       => $resultSunat['PLE'],
                    'representantes_legales'    => $resultSunat['representantes_legales'],
                    'cantidad_trabajadores'     => $resultSunat['cantidad_trabajadores'],
                ]);

            }

            return $dataSunat;
        }

        return false;
    }

    public function getDataPersonSunatGraph(Request $request){
        $sunatAPI = new Sunat();
        if($request->dniPersona){
            $dataSunat = $sunatAPI->search($request->dniPersona, false);

            if($dataSunat['success']){
                $cantidadTrabajadores = $this->latin_to_utf8($dataSunat['result']['cantidad_trabajadores']);

                if($cantidadTrabajadores){
                    $dataLabel = collect($cantidadTrabajadores)->map(function ($item, $key) {
                        return $item['periodo'];
                    })->values()->toArray();

                    $dataSetTrabajadores = collect($cantidadTrabajadores)->map(function ($item, $key) {
                        return $item['total_trabajadores'];
                    })->values()->toArray();

                    $dataSetPensionistas = collect($cantidadTrabajadores)->map(function ($item, $key) {
                        return $item['pensionista'];
                    })->values()->toArray();

                    $dataSetPrestador = collect($cantidadTrabajadores)->map(function ($item, $key) {
                        return $item['prestador_servicio'];
                    })->values()->toArray();

                    $chart = Charts::multi('bar', 'chartjs')
                        ->title('')
                        ->dataset('Total Trabajadores', $dataSetTrabajadores, '#1abc9c')
                        ->dataset('Prestador de Servicios', $dataSetPrestador, '#41D5B7')
                        ->dataset('Pensionistas', $dataSetPensionistas, '#62E6CB')
                        ->labels($dataLabel)
                        ->dimensions(0, 400)
                        ->legend(true)
                        ->stacked(true)
                        ->credits(true);

                    $resultSunat = $this->latin_to_utf8($dataSunat['result']);

                    $dataFecha = Carbon::now();

                    SunatDetalle::updateOrCreate([
                        'NRODOC'                    => $request->dniPersona,
                        'RUC'                       => $resultSunat['RUC'],
                    ], [
                        'NRODOC'                    => $request->dniPersona,
                        'FECHA_REG'                 => $dataFecha,
                        'RUC'                       => $resultSunat['RUC'],
                        'RazonSocial'               => $resultSunat['RazonSocial'],
                        'Telefono'                  => $resultSunat['Telefono'],
                        'Condicion'                 => $resultSunat['Condicion'],
                        'NombreComercial'           => $resultSunat['NombreComercial'],
                        'Tipo'                      => $resultSunat['Tipo'],
                        'Inscripcion'               => $resultSunat['Inscripcion'],
                        'Estado'                    => $resultSunat['Estado'],
                        'Direccion'                 => $resultSunat['Direccion'],
                        'SistemaEmision'            => $resultSunat['SistemaEmision'],
                        'ActividadExterior'         => $resultSunat['ActividadExterior'],
                        'SistemaContabilidad'       => $resultSunat['SistemaContabilidad'],
                        'Oficio'                    => isset($resultSunat['Oficio']) ? $resultSunat['Oficio'] : '-',
                        'EmisionElectronica'        => $resultSunat['EmisionElectronica'],
                        'PLE'                       => $resultSunat['PLE'],
                        'representantes_legales'    => $resultSunat['representantes_legales'],
                        'cantidad_trabajadores'     => $resultSunat['cantidad_trabajadores'],
                    ]);

                    return response()->json([
                        'chartHTML' => $chart->html(),
                        'chartScript' => $chart->script(),
                        'dataSunat' => $this->latin_to_utf8($dataSunat)
                    ]);
                }else{
                    return response()->json([
                        'chartHTML' => '',
                        'chartScript' => '',
                        'dataSunat' => $this->latin_to_utf8($dataSunat)
                    ]);
                }
            }

            return $dataSunat;
        }

        return false;
    }

    public function getDataPersonReporteSBS(Request $request){
        $arrayWhere = [];
        if($request->dniPersona){
            $arrayCustom = ['NRODOC', '=', $request->dniPersona];
            array_push($arrayWhere, $arrayCustom);
        }

        $dataPersona = Financiero::Select()
            ->where($arrayWhere)
            ->withCount('detalle')
            ->get()
            ->toArray();

        return $dataPersona;
    }

    public function getDataPersonDetalleSBS(Request $request){
        $arrayWhere = [];
        if($request->codigoSBS){
            $arrayCustom = ['CODIGOSBS', '=', $request->codigoSBS];
            array_push($arrayWhere, $arrayCustom);
        }

        $dataPersona = FinancieroDetalle::Select()
            ->where($arrayWhere)
            ->get()
            ->toArray();

        return $dataPersona;
    }

    public function getSearchFalabella(Request $request){
        $searchPeruApi = new DataFalabella();
        $dataPerson = $searchPeruApi->getDataPersonSearchFalabella($request->dniPersona);

        return $dataPerson;
    }

    public function getSearchFalabellaFirst(Request $request){
        $searchPeruApi = new DataFalabella();
        $dataPerson = $searchPeruApi->getDataPersonSearchFalabellaFirst($request->dniPersona);

        return $dataPerson;
    }

    public function getBaseIndividual($nroDoc){
        $baseIndividual = BaseIndividual::where('nrodoc', $nroDoc)->first();
        return $baseIndividual;
    }
}
