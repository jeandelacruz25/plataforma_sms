<?php

namespace App\Http\Controllers;

use App\Http\Requests\formRoleRequest;
use App\Http\Requests\RolesAssignRequest;
use App\Models\Menu;
use App\Models\MenuRoles;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class RoleController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function paginationPerfiles(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id', '!=', 1];
            array_push($whereArray, $whereCustom);
        }

        $perfiles = Role::Select()
            ->with('status')
            ->where($whereArray)
            ->whereRaw('name like ?', "%{$request->searchPerfil}%")
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $perfiles->total(),
                'per_page' => $perfiles->perPage(),
                'current_page' => $perfiles->currentPage(),
                'last_page' => $perfiles->lastPage(),
                'from' => $perfiles->firstItem(),
                'to' => $perfiles->lastItem()
            ],
            'data' => $perfiles
        ];

        return response()->json($response);
    }

    public function formRoles(Request $request)
    {
        if($request->valueID == null){
            return view('elements/roles/form/form_roles')->with(array(
                'idRole'            => '',
                'nameRole'          => '',
                'descriptionRole'   => '',
                'statusRole'        => '',
                'updateForm'        => false
            ));
        }else{
            $getRole = $this->getRole($request->valueID);
            return view('elements/roles/form/form_roles')->with(array(
                'idRole'            => $request->valueID,
                'nameRole'          => $getRole[0]['name'],
                'descriptionRole'   => $getRole[0]['description'],
                'statusRole'        => $getRole[0]['id_status'],
                'updateForm'        => true
            ));
        }
    }

    public function formRolesAssing(Request $request) {
        $getRole  = $this->getRole($request->valueID);
        $getUsersRole = $this->UsersRoles($request->valueID);
        return view('elements/roles/form/form_roles_assing')->with(array(
            'idRole'        => $getRole[0]['id'],
            'nameRole'      => ucwords(Str::lower($getRole[0]['name'])),
            'UsersRole'     => $getUsersRole
        ));
    }

    public function formRolesMenu(Request $request){
        $getRole  = $this->getRole($request->valueID);
        $getMenuRoles = $this->getMenuRole($request->valueID);
        return view('elements/roles/form/form_roles_menu')->with(array(
            'idRole'        => $getRole[0]['id'],
            'nameRole'      => ucwords(Str::lower($getRole[0]['name'])),
            'menus'         => Menu::menus(),
            'checkMenu'     => $getMenuRoles
        ));
    }

    public function saveFormRoles(formRoleRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $roleQuery = Role::updateOrCreate([
                'id'            => $request->roleID
            ], [
                'name'          => $request->nameRole,
                'description'   => $request->descriptionRole,
                'id_status'     => $request->statusID
            ]);
            $action = ($request->roleID ? 'update' : 'create');
            if ($roleQuery) {
                return ['message' => 'Success', 'action' => $action, 'datatable' => 'listRoles'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormRolesAssing(RolesAssignRequest $request)
    {
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            if ($request->checkUser) {
                foreach ($request->checkUser as $keyUserRole => $valUserRole) {
                    $roleUserQuery = User::where([
                        'id' => $valUserRole,
                    ])->update(['id_rol' => $request->roleID]);
                }
                if ($roleUserQuery) {
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormRolesMenu(Request $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            MenuRoles::where('role_id', $request->roleID)->delete();
            if ($request->checkMenu) {
                foreach ($request->checkMenu as $keyRoleMenu => $valRoleMenu) {
                    $roleMenuQuery = MenuRoles::updateOrCreate([
                        'role_id'   => $request->roleID,
                        'vue_name'  => $valRoleMenu
                    ], [
                        'role_id'   => $request->roleID,
                        'vue_name'  => $valRoleMenu
                    ]);
                }
                if ($roleMenuQuery) {
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }

    public function getRole($idRole){
        $role = Role::Select()
            ->where('id', $idRole)
            ->get()
            ->toArray();

        return $role;
    }

    public function getData(){
        $role = Role::Select()
            ->get()
            ->toArray();

        $users = User::Select()
            ->get()
            ->toArray();

        $data['role'] = $role;
        $data['users'] = $users;

        return $data;
    }

    public function getMenuRole($idRol) {
        $role_menus = MenuRoles::Select()
            ->where('role_id', $idRol)
            ->get()
            ->toArray();

        return $role_menus;
    }

    protected function UsersRoles($idRol)
    {
        $users = User::Select()
            ->where([
                ['id_cliente', '=', Auth::user()->clientes->id]
            ])
            ->whereIn('id_rol', [0, $idRol]);

        if(!Auth::user()->authorizeRoles(['Administrador'])) {
            $users->where([
                ['id_rol', '!=', '1']
            ]);
        }

        return $users->get()->toArray();
    }
}
