<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\formLoginRequest;
use App\Models\User;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL) ? $this->username() : 'username';
        if(!Cookie::get('cookie_locale')) Cookie::queue('cookie_locale', config('app.locale'), 2147483647);
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        Auth::user()->session_id = Session::getId();
        Auth::user()->save();
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    public function authenticate(formLoginRequest $request)
    {
        $userdata = array(
            'username'  => $request->username,
            'password'  => $request->password
        );

        if (Auth::attempt($userdata)) {
            $request->session()->regenerate();

            $previous_session = Auth::User()->session_id;
            if ($previous_session) {
                Auth::user()->session_validate = 1;
                Auth::logout();
                return response()->json([
                    'Auth' => 'session'
                ]);
            }

            Auth::user()->session_id = Session::getId();
            Auth::user()->session_validate = 0;
            Auth::user()->save();
            $this->clearLoginAttempts($request);

            return [
                'Auth'  => 'success'
            ];
        }else{
            return response()->json([
                'Auth' => 'password'
            ]);
        }
    }

    protected function emitValidateSession(Request $request){
        $dataUser = User::where('username', $request->username)->with('clientes')->first();

        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
        $clientNode->initialize();
        $clientNode->emit('validateSession', [
            'nameDatatable' => 'listCampaignBase',
            'nameRoom' => Str::lower($dataUser['clientes']['palabra_clave']),
            'userID' => $dataUser['id']
        ]);
        $clientNode->close();
    }

    protected  function validateSession(Request $request){
        $dataUser = User::where('username', $request->username)->first();

        if($dataUser['session_validate'] != 1){
            return response()->json([
                'Auth' => 'keep'
            ]);
        }else{
            $userdata = array(
                'username'  => $request->username,
                'password'  => $request->password
            );

            Auth::attempt($userdata);
            Auth::user()->session_id = Session::getId();
            Auth::user()->session_validate = 0;
            Auth::user()->save();
            $this->clearLoginAttempts($request);

            return response()->json([
                'Auth' => 'logout'
            ]);
        }
    }

    protected function logout(Request $request){
        if(Auth::check()){
            $dataCliente = User::where('id', auth()->user()->id)->with('clientes')->first();
            $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
            $clientNode->initialize();
            $clientNode->emit('reloadManageUsers', [
                'nameRoom' => Str::lower($dataCliente['clientes']['palabra_clave'])
            ]);
            $clientNode->close();
            Auth::user()->session_id = '';
            Auth::user()->save();
        }
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
