<?php

namespace App\Http\Controllers\ValidatePhoneControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formValidacionCampaignRequest;
use App\Http\Requests\formValidacionCampaignStatusRequest;
use App\Http\Requests\formValidacionCampaignUploadRequest;
use App\Models\Estados;
use App\Models\ValidatePhoneModels\TelefonosCampanas;
use App\Models\ValidatePhoneModels\TelefonosDetalle;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ValidatePhoneController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(searchMenuCliente(auth()->user()->id_cliente, 'validacion_telefonos') != 0 && searchMenuUsuario(auth()->id(), 'validacion_telefonos') != 0){
            return view('elements/validate_telephone/index')->with(array(
                'titleCampaignModule'           => 'Administrar Campañas [Validación]',
                'iconCampaignModule'            => 'fa fa-cogs',
                'titleReportSMSModule'          => 'Reportes SMS',
                'iconReportSMSModule'           => 'feather feather-bar-chart',
                'selectOptions'                 => $this->selectionOption()
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Administrar Campañas [Validación]',
                'menuNombre'            => 'Validación Telefonos'
            ));
        }
    }

    public function paginationValidacionCampanas(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->searchCampana){
            $whereCustom = ['nombre_campana', 'LIKE', '%'.$request->searchCampana.'%'];
            array_push($whereArray, $whereCustom);
        }

        $validacion = TelefonosCampanas::Select()
            ->with(['status', 'clientes'])
            ->withCount('detalle_count')
            ->withCount('total')
            ->where($whereArray)
            ->where(function($query) use ($request) {
                if($request->searchDateRange){
                    $dateFilter = explode(' / ', $request->searchDateRange);
                    $query->whereBetween('created_at', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
                }else{
                    $query->whereMonth('created_at', Carbon::now()->format('m'));
                }
            })
            ->orderby('id_estado', 'asc')
            ->orderby('created_at', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $validacion->total(),
                'per_page' => $validacion->perPage(),
                'current_page' => $validacion->currentPage(),
                'last_page' => $validacion->lastPage(),
                'from' => $validacion->firstItem(),
                'to' => $validacion->lastItem()
            ],
            'data' => $validacion
        ];

        return response()->json($response);
    }

    public function formCampaignValidacion(Request $request){
        if($request->valueID == null){
            return view('elements/validate_telephone/tabs/campaign/form/form_campaign')->with(array(
                'dataCampaign'          => '',
                'updateForm'            => false,
                'options'               => $this->selectionOption()
            ));
        }else{
            $getCampaign = $this->getCampaignValidacion($request->valueID);
            return view('elements/validate_telephone/tabs/campaign/form/form_campaign')->with(array(
                'dataCampaign'          => $getCampaign,
                'updateForm'            => true,
                'options'               => $this->selectionOption()
            ));
        }
    }

    public function formCampaignValidacionUpload(Request $request){
        $getCampaign = $this->getCampaignValidacion($request->valueID);
        $getCliente = $this->getCliente($getCampaign[0]['id_cliente']);
        $numbersValidation = isset($getCliente[0]['rules']['reglas']['cantidad_numero_validaciones'])  ? $getCliente[0]['rules']['reglas']['cantidad_numero_validaciones'] : 0;

        $numbersCliente = TelefonosDetalle::whereHas('campana', function ($query) use($getCampaign) {
            $query->where('id_cliente', '=', $getCampaign[0]['id_cliente']);
        })->whereDay('fecha_subida', '=', Carbon::now()->format('d'))->select(DB::raw('count(*) as cantidad_telefonos'))->get()->toArray();

        $countNumbersValidation = ($numbersValidation - $numbersCliente[0]['cantidad_telefonos']);

        return view('elements/validate_telephone/tabs/campaign/form/form_campaign_upload')->with(array(
            'dataCampaign'              => $getCampaign,
            'validationRules'           => $numbersValidation <= 0 ? false : true,
            'numbersValidation'         => $countNumbersValidation <= 0 ? false : true,
            'countNumbersValidation'    => $countNumbersValidation
        ));
    }

    public function formCampaignValidacionStatus(Request $request){
        $dataCampaign  = $this->getCampaignValidacion($request->valueID);
        $statusOptions = $this->getNotCampaignStatus($dataCampaign[0]['status']['id']);
        return view('elements/validate_telephone/tabs/campaign/form/form_campaign_status')->with(array(
            'dataCampaign'      => $dataCampaign,
            'options'           => $statusOptions
        ));
    }

    public function formCampaignValidacionUsers(Request $request){
        $getCampaign = $this->getCampaignValidacion($request->valueID);
        return view('elements/validate_telephone/tabs/campaign/form/form_campaign_base')->with(array(
            'dataCampaign'      => $getCampaign
        ));
    }

    public function formValidacionStart(Request $request){
        $getCampaign = $this->getCampaignValidacion($request->valueID['idCampana']);
        $idCliente = $getCampaign[0]['id_cliente'];
        $countCampaign = $this->getCountCampaign($idCliente);
        $getCliente = $this->getCliente($idCliente);
        $countCanales = isset($getCliente[0]['rules']['reglas']['cantidad_numero_canales']) ? $getCliente[0]['rules']['reglas']['cantidad_numero_canales'] : 0;
        $inicioMasivo = isset($getCliente[0]['rules']['reglas']['inicio_validacion_telefono']) ? $getCliente[0]['rules']['reglas']['inicio_validacion_telefono'] : '07:00';
        $finMasivo = isset($getCliente[0]['rules']['reglas']['fin_validacion_telefono']) ? $getCliente[0]['rules']['reglas']['fin_validacion_telefono'] : '20:00';
        return view('elements/validate_telephone/tabs/campaign/form/form_campaign_start')->with(array(
            'dataCampaign'      => $getCampaign,
            'countCampaign'     => $countCampaign,
            'countCanales'      => $countCanales,
            'inicioMasivo'      => $inicioMasivo,
            'finMasivo'         => $finMasivo,
            'reStart'           => $request->reStart
        ));
    }

    public function formValidacionPause(Request $request){
        $getCampaign = $this->getCampaignValidacion($request->valueID);
        return view('elements/validate_telephone/tabs/campaign/form/form_campaign_pause')->with(array(
            'dataCampaign'      => $getCampaign
        ));
    }

    public function saveFormCampaignValidacion(formValidacionCampaignRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $campaignQuery = TelefonosCampanas::updateOrCreate([
                'id'                => $request->campaignID
            ], [
                'nombre_campana'        => $request->nameCampaign,
                'id_cliente'            => $request->clienteCampaign ? $request->clienteCampaign : Auth::user()->id_cliente,
                'estado_envio'          => $request->estadoEnvio ? $request->estadoEnvio : 0,
                'id_estado'             => $request->statusID ? $request->statusID : 1,
                'total_telefonos'       => $request->totalTelefonos ? $request->totalTelefonos : '0',
                'inicio_validacion'     => $request->inicioValidacion ? $request->inicioValidacion : null,
                'fin_validacion'        => $request->finValidacion ? $request->finValidacion : null,
                'user_created'          => $request->userCreated ? $request->userCreated : Auth::user()->id,
                'user_updated'          => Auth::user()->id
            ]);
            $action = $request->campaignID ? 'update' : 'create';
            if($campaignQuery){
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormCampaignValidacionUpload(formValidacionCampaignUploadRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            return [
                'message' => 'Success'
            ];
        }
        return ['message' => 'Error'];
    }

    public function executeUploadCampaignValidacion(Request $request){
        $idCampana = $request->campaignID;

        $dataCampaign = $this->getCampaignValidacion($idCampana);

        TelefonosCampanas::where('id', $idCampana)->update([
            'estado_envio'   => 2
        ]);

        try {

            app()->instance('insertCampaignValidacionUpload', collect());
            Excel::filter('chunk')->load($request->file('baseClientes')->getRealPath())->chunk(1000, function ($rows) {
                app()->instance('insertCampaignValidacionUpload', app('insertCampaignValidacionUpload')->merge($rows->toArray()));
            }, $shouldQueue = false);

            $dataSheet = app('insertCampaignValidacionUpload')->toArray();

            $arrayBlackList = $this->getBlackList();
            $collectArrayList = collect($arrayBlackList)->values()->toArray();

            $dbInsert = [];
            $count = 0;
            foreach (collect($dataSheet)->chunk(1000)->toArray() as $chunk) {
                $keys = array_keys($chunk);
                $lastKey = array_pop($keys);
                foreach($chunk as $key => $value){
                    $telefono = $value['telefono'];
                    if($value['telefono'] || $value['dni']){
                        if(strlen($value['telefono']) == 9){
                            $count++;
                            if($count <= $request->numbersCliente){
                                $arrayInsert = [
                                    'telefono'              => $telefono,
                                    'num_identidad'         => $value['dni'],
                                    'fecha_validacion'      => null,
                                    'estado_validacion'     => array_search($value['telefono'], $collectArrayList) !== false ? 6 : 0,
                                    'fecha_subida'          => Carbon::now(),
                                    //'ultimo_telefono'       => $key == $lastKey ? 1 : 0,
                                    'id_campana'            => $idCampana
                                ];

                                array_push($dbInsert, $arrayInsert);
                            }
                        }
                    }
                }
            }

            DB::beginTransaction();

            try {
                $insertDetalle = TelefonosDetalle::insert($dbInsert);

                if($insertDetalle){
                    $telefonoDetalleCount = TelefonosDetalle::where('id_campana', $idCampana)->count();

                    $smsCampanaUpdate = TelefonosCampanas::where('id', $idCampana)->update([
                        'total_telefonos'   => $telefonoDetalleCount,
                        'estado_envio'         => 0
                    ]);

                    if($smsCampanaUpdate){
                        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                        $clientNode->initialize();
                        $clientNode->emit('reloadDatatable', [
                            'nameDatatable' => 'listCampaignValidacion',
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave'])
                        ]);
                        $clientNode->emit('notificationCampaign', [
                            'nameCampaign' => $dataCampaign[0]['nombre_campana'],
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                            'tipoSubida' => 3,
                            'licenseExceeded' => ($request->numbersCliente - $count) < 0 ? true : false
                        ]);
                        $clientNode->close();

                        DB::commit();

                        return ['message' => 'Success'];
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                return ['message' => 'Error'];
            }

        }catch (\Exception $e){
            return ['message' => 'Error'];
        }
    }

    public function saveFormCampaignValidacionStatus(formValidacionCampaignStatusRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = TelefonosCampanas::where('id', $request->campaignID)
                ->update(['id_estado' => $request->statusCampaign]);
            if($usersQuery){
                return ['message' => 'Success'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function executeValidacionStart(Request $request){
        $idCampana = $request->campaignID;
        $numCanales = $request->numCanales;

        if($request->reStart){
            TelefonosCampanas::where('id', $idCampana)
                ->update([
                    'inicio_validacion' => Carbon::now(),
                    'estado_envio'      => 3
                ]);
        }else{
            TelefonosCampanas::where('id', $idCampana)
                ->update([
                    'user_validacion' => Auth::id(),
                    'inicio_validacion' => Carbon::now(),
                    'estado_envio'      => 3
                ]);
        }

        $client = new \GuzzleHttp\Client();

        $dataCampaign = $this->getCampaignValidacion($idCampana);
        $client->request('POST','http://192.168.80.3/validacion.php?idCampana='.$idCampana.'&idEstado='.$dataCampaign[0]['id_estado'].'&numCanales='.$numCanales.'&idCliente='.$dataCampaign[0]['id_cliente']);

        return 'Success';
    }

    public function executeValidacionPause(Request $request){
        $idCampana = $request->campaignID;

        $client = new \GuzzleHttp\Client();

        $dataCampaign = $this->getCampaignValidacion($idCampana);
        $nameCliente = Str::lower($dataCampaign[0]['clientes']['palabra_clave']);

        $response = $client->request('POST','http://192.168.80.3/validacion.php?idCampana='.$idCampana.'&idEstado=4&nameCliente='.$nameCliente);

        $statusResponse = $response->getBody()->getContents();

        if($statusResponse == 'stop'){
            TelefonosCampanas::where('id', $idCampana)
                ->update([
                    'estado_envio'      => 4
                ]);

            $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
            $clientNode->initialize();
            $clientNode->emit('clearTimeOut', [
                'nameTimeOut' => 'listCampaignValidacion',
                'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                'userID' => auth()->user()->id
            ]);
        }

        return 'Success';
    }

    public function finishValidationCampaign(Request $request){
        $idCampana = $request->campaignID;

        $dataCampaign = $this->getCampaignValidacion($idCampana);
        $nameCliente = Str::lower($dataCampaign[0]['clientes']['palabra_clave']);

        $datatableName = 'listCampaignValidacion';

        TelefonosCampanas::where('id', $idCampana)
            ->update([
                'fin_validacion' => Carbon::now(),
                'estado_envio'      => 1
            ]);

        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
        $clientNode->initialize();

        $validateSocket = TelefonosCampanas::where([
            ['id_cliente', '=', $dataCampaign[0]['id_cliente']],
            ['estado_envio', '=', 3]
        ])->count();

        if($validateSocket == 1){
            $clientNode->emit('clearTimeOut', [
                'nameTimeOut' => $datatableName,
                'nameRoom' => $nameCliente,
                'userID' => auth()->user()->id
            ]);
        }

        $clientNode->emit('notificationCampaign', [
            'nameCampaign' => $dataCampaign[0]['nombre_campana'],
            'nameRoom' => $nameCliente,
            'tipoSubida' => 4,
            'licenseExceeded' => ''
        ]);
        $clientNode->close();
    }

    public function paginationFormCampaignValidationTelefonos(Request $request){
        $baseCampaign = TelefonosDetalle::Select()
            ->with('campana')
            ->where([
                ['id_campana', '=', $request->valueID],
                ['telefono', 'LIKE', '%'.$request->searchTelephone.'%']
            ])
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $baseCampaign->total(),
                'per_page' => $baseCampaign->perPage(),
                'current_page' => $baseCampaign->currentPage(),
                'last_page' => $baseCampaign->lastPage(),
                'from' => $baseCampaign->firstItem(),
                'to' => $baseCampaign->lastItem()
            ],
            'data' => $baseCampaign
        ];
        return response()->json($response);
    }

    public function miniDashboardValidacionTelefono(Request $request){
        $dataCampaign = $this->getCampaignValidacion($request->valueID);
        $dataCliente = $this->getCliente($dataCampaign[0]['id_cliente']);

        return view('elements/validate_telephone/tabs/campaign/chart/mini_dashboard')->with(array(
            'dataCampaign'  => $dataCampaign,
            'dataCliente'   => $dataCliente
        ));
    }

    public function getDataMiniDashboardTelefono(Request $request)
    {
        $validacionDetalle = TelefonosDetalle::with('campana')
            ->where([
                ['id_campana', '=', $request->valueID]
            ])
            ->orderBy('estado_validacion', 'desc')
            ->get()
            ->toArray();

        $campaignDetalle = TelefonosCampanas::where('id', $request->valueID)->select('id_cliente')->first();

        $validacionDetalleAsync = TelefonosDetalle::Select(DB::raw('COUNT(CASE WHEN async_status = 1 THEN 1 ELSE NULL END) as sincronizado, COUNT(CASE WHEN async_status = 0 THEN 0 ELSE NULL END) as no_sincronizado'))
            ->where([
                ['id_campana', '=', $request->valueID]
            ])
            ->orderBy('estado_validacion', 'desc')
            ->groupBy('estado_validacion')
            ->get()
            ->toArray();

        $dataLabel = collect($validacionDetalle)->map(function ($item, $key) {
            return getStatusValidacion($item['estado_validacion']);
        })->unique()->toArray();

        $dataLabelTable = collect($validacionDetalle)->map(function ($item, $key) {
            return $item['estado_validacion'];
        })->unique()->toArray();

        $backgrounLabel = collect($validacionDetalle)->map(function ($item, $key) {
            return getColorValidacionStatus($item['estado_validacion']);
        })->unique()->toArray();

        $valuesArray = collect($validacionDetalle)->mapToGroups(function ($item) {
            return [$item['estado_validacion'] => $item['estado_validacion']];
        })->toArray();

        $out = array();
        foreach ($valuesArray as $key => $value){
            foreach ($value as $keyValue => $valueValue){
                $index = $valueValue;
                if (array_key_exists($index, $out)){
                    $out[$index]++;
                } else {
                    $out[$index] = 1;
                }
            }
        }
        $dataValues = collect($out)->values();

        $chart = Charts::create('pie', 'chartjs')
            ->title('')
            ->colors($backgrounLabel)
            ->labels($dataLabel)
            ->values($dataValues)
            ->responsive(false)
            ->dimensions(0, 280)
            ->legend(false)
            ->credits(false);

        $getCliente = $this->getCliente($campaignDetalle['id_cliente']);
        $asyncScore = $getCliente[0]['score_async'] === '1' ? true : false;

        $dataTable = $this->chartBarTableValidacionTelephone($dataLabelTable, $dataValues, $asyncScore, $validacionDetalleAsync);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);

    }

    protected function builderExportReportCampaignValidacion($campana_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($campana_list_query as $query) {

            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['número_celular']           = $query['telefono'];
            $builderview[$posicion]['número_documento']         = $query['num_identidad'];
            $builderview[$posicion]['campaña']                  = ucwords(Str::lower($query['campana']['nombre_campana']));
            $builderview[$posicion]['fecha_validacion']         = $query['fecha_validacion'] ? Carbon::parse($query['fecha_validacion'])->format('d/m/Y H:i:s a') : '-';
            $builderview[$posicion]['estado_validacion']        = getStatusValidacion($query['estado_validacion']);
            $builderview[$posicion]['sincronizado']             = $query['async_status'] == 0 ? 'no' : 'si';
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    public function downloadReportCampaignValidacion(Request $request){
        $detalleSMS = TelefonosDetalle::Select()->where('id_campana', $request->idCampana)->get()->toArray();
        if($detalleSMS){
            $campana = TelefonosCampanas::Select()->where('id', $detalleSMS[0]['id_campana'])->get()->toArray();
            $nameCampana = substr(cleanString(str_replace(' ', '_', $campana[0]['nombre_campana'])).'_validacion_'.time(), 0, 31);

            Excel::create($nameCampana, function($excel) use($campana, $nameCampana) {
                $excel->sheet('validacion_telefonos', function($sheet) use($campana) {
                    $sheet->fromArray($this->builderExportReportCampaignValidacion($this->getTelefonoDetalle($campana[0]['id'])));
                });
            })->store('xlsx', 'exports');


            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameCampana.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    public function getCampaignValidacion($idCampaign){
        $campaign = TelefonosCampanas::Select()
            ->with(['status', 'clientes', 'user'])
            ->where('id', $idCampaign)
            ->get()
            ->toArray();

        return $campaign;
    }

    public function getTelefonoDetalle($idCampaing){
        $baseCampaign = TelefonosDetalle::Select()
            ->with('campana')
            ->where([
                ['id_campana', '=', $idCampaing]
            ])
            ->get()
            ->toArray();

        return $baseCampaign;
    }

    public function getNotCampaignStatus($idStatus){
        $statusCampaign = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusCampaign;
    }
}
