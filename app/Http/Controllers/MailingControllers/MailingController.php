<?php

namespace App\Http\Controllers\MailingControllers;

use App\Http\Controllers\SecuritecController;
use Illuminate\Http\Request;

class MailingController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        return view('elements/mailing/index')->with(array(
            'titleCampaignModule'           => 'Administrar Campañas',
            'iconCampaignModule'            => 'fa fa-cogs',
            'selectOptions'                 => $this->selectionOption()
        ));
    }
}
