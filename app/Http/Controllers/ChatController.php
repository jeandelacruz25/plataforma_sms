<?php

namespace App\Http\Controllers;

use App\Http\Requests\formChatRequest;
use App\Models\Chat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('elements/chat/index')->with(array(
            'titleModule'   =>  'Chat'
        ));
    }

    public function getChat(){
        $chat = Chat::Select()
                ->with('users_chat')
                ->get()
                ->toArray();

        return $chat;
    }

    public function saveChatMessage(formChatRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $chatQuery = Chat::insert([
                'mensaje' => $request->messageChat,
                'user_id' => Auth::id(),
                'fecha_reg' => Carbon::now(),
                'fecha_act' => Carbon::now()
            ]);
            if($chatQuery){
                return ['message' => 'Success'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }
}
