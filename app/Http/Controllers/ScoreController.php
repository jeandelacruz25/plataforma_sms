<?php

namespace App\Http\Controllers;

use App\Models\ScoreModels\ClientesCarteras;
use App\Models\ScoreModels\ClientesEquivalencias;
use App\Models\SMSModels\SMSDetalle;
use App\Models\ValidatePhoneModels\TelefonosDetalle;
use Illuminate\Http\Request;

class ScoreController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProveedores(Request $request){
        $carteras = ClientesCarteras::where('id_cliente', $request->idCliente)->select('proveedor')
            ->distinct()->get()->toArray();

        return $carteras;
    }

    public function getCarteras(Request $request){
        $carteras = ClientesCarteras::where('proveedor', $request->nameProveedor)
            ->get()->toArray();

        return $carteras;
    }

    public function generarMarcador(Request $request){
        $dataTelefonos = TelefonosDetalle::Select('telefono', 'num_identidad')->where([
            ['estado_validacion', '=', 9],
            ['id_campana', '=', $request->idCampaign],
            ['async_status', '=', 0]
        ])->get()->toArray();

        $arrayInfo = [];
        foreach ($dataTelefonos as $key => $value){
            $arrayInfo[$key]['id'] = $key + 1;
            $arrayInfo[$key]['id_cliente'] = $value['num_identidad'];
            $arrayInfo[$key]['telefono'] = $value['telefono'];
        }

        $client = new \GuzzleHttp\Client();

        $requestCurl = $client->post('http://190.102.145.19:8084/api-platcon/marcadores',array(
            'form_params' => [
                'tipo' => 'validacion',
                'nombre' => $request->nameCartera,
                'id_cartera' => $request->idCartera,
                'tipo_marcador' => $request->tipoMarcador,
                'tipo_bolsa' => $request->tipoBolsa,
                'id_campania' => $request->idCampaign,
                'info' => $arrayInfo
            ]
        ));

        $resultRequest = $requestCurl->getBody()->getContents();
        $jsonResult = json_decode($resultRequest);

        if(isset($jsonResult->errors)){
            foreach($jsonResult->errors->telefono as $key => $value){
                TelefonosDetalle::where([
                    ['id_campana', '=', $request->idCampaign],
                    ['estado_validacion', '=', 9],
                    ['telefono', '=', $value]
                ])->update([
                    'async_status' => 0
                ]);
            }
        }

        if(isset($jsonResult->success)){
            foreach($jsonResult->success->telefono as $key => $value){
                TelefonosDetalle::where([
                    ['id_campana', '=', $request->idCampaign],
                    ['estado_validacion', '=', 9],
                    ['telefono', '=', $value]
                ])->update([
                    'async_status' => 1
                ]);
            }
        }

        return 'Success';
    }

    public function generarGestion(Request $request){

        if($request->tipoRequest == 'sms'){
            $dataTelefonos = SMSDetalle::Select('telefono_usuario')
                ->where([
                    ['estado_sms', '=', 3],
                    ['id_campana', '=', $request->idCampaign],
                    ['tipo_envio', '=', 2],
                    ['async_status', '=', 0]
                ])->get()->toArray();
        }else{
            $dataTelefonos = TelefonosDetalle::Select('telefono', 'num_identidad')->where([
                ['estado_validacion', '=', $request->idStatus],
                ['id_campana', '=', $request->idCampaign],
                ['async_status', '=', 0]
            ])->get()->toArray();
        }

        $arrayInfo = [];
        foreach ($dataTelefonos as $key => $value){
            $arrayInfo[$key]['id'] = $key + 1;
            $arrayInfo[$key]['id_cliente'] = $request->tipoRequest == 'sms' ? '' : $value['num_identidad'];
            $arrayInfo[$key]['telefono'] = $request->tipoRequest == 'sms' ? $value['telefono_usuario'] : $value['telefono'];
        }

        if($request->tipoRequest == 'validacion') {

            $campanaEquivalencia = ClientesEquivalencias::where([
                ['id_cliente', '=', 6],
                ['id_cartera', '=', $request->idCartera],
                ['id_estado_atk', '=', $request->idStatus]
            ])->select('id_resultado')->first();

            if(empty($campanaEquivalencia['id_resultado'])) {
                return 'equivalencia';
            }
        }

        $client = new \GuzzleHttp\Client();

        $requestCurl = $client->post('http://190.102.145.19:8084/api-platcon/gestiones',array(
            'form_params' => [
                'id_cartera' => $request->idCartera,
                'tipo' => $request->tipoRequest,
                'id_resultado' => $request->tipoRequest == 'sms' ? 0 : $campanaEquivalencia['id_resultado'],
                'id_campania' => $request->idCampaign,
                'info' => $arrayInfo
            ]
        ));

        $resultRequest = $requestCurl->getBody()->getContents();
        $jsonResult = json_decode($resultRequest);

        if($request->tipoRequest == 'sms') {
            if (isset($jsonResult->success)) {
                foreach ($jsonResult->success->telefono as $key => $value) {
                    SMSDetalle::where([
                        ['estado_sms', '=', 3],
                        ['id_campana', '=', $request->idCampaign],
                        ['tipo_envio', '=', 2],
                        ['telefono_usuario', '=', '51'.$value]
                    ])->update([
                        'async_status' => 1
                    ]);
                }
            }

            if (isset($jsonResult->errors)) {
                foreach ($jsonResult->errors->telefono as $key => $value) {
                    SMSDetalle::where([
                        ['estado_sms', '=', 3],
                        ['id_campana', '=', $request->idCampaign],
                        ['tipo_envio', '=', 2],
                        ['telefono_usuario', '=', '51'.$value]
                    ])->update([
                        'async_status' => 0
                    ]);
                }
            }
        }else{
            if (isset($jsonResult->errors)) {
                foreach ($jsonResult->errors->telefono as $key => $value) {
                    TelefonosDetalle::where([
                        ['id_campana', '=', $request->idCampaign],
                        ['estado_validacion', '=', $request->idStatus],
                        ['telefono', '=', $value]
                    ])->update([
                        'async_status' => 0
                    ]);
                }
            }

            if (isset($jsonResult->success)) {
                foreach ($jsonResult->success->telefono as $key => $value) {
                    TelefonosDetalle::where([
                        ['id_campana', '=', $request->idCampaign],
                        ['estado_validacion', '=', $request->idStatus],
                        ['telefono', '=', $value]
                    ])->update([
                        'async_status' => 1
                    ]);
                }
            }
        }

        return 'Success';
    }

    public function generarInactivos(Request $request){
        $dataTelefonos = TelefonosDetalle::Select('telefono', 'num_identidad')->where([
            ['estado_validacion', '=', 7],
            ['id_campana', '=', $request->idCampaign],
            ['async_status', '=', 0]
        ])->get()->toArray();

        $arrayInfo = [];
        foreach ($dataTelefonos as $key => $value){
            $arrayInfo[$key]['id'] = $key + 1;
            $arrayInfo[$key]['telefono'] =  $value['telefono'];
        }

        $client = new \GuzzleHttp\Client();

        $requestCurl = $client->post('http://190.102.145.19:8084/api-platcon/inactivar-telefonos',array(
            'form_params' => [
            	'id_campania'	=> $request->idCampaign,
                'info' 			=> $arrayInfo
            ]
        ));

        $resultRequest = $requestCurl->getBody()->getContents();
        $jsonResult = json_decode($resultRequest);

        if (isset($jsonResult->errors)) {
            foreach ($jsonResult->errors->telefono as $key => $value) {
                TelefonosDetalle::where([
                    ['id_campana', '=', $request->idCampaign],
                    ['estado_validacion', '=', 7],
                    ['telefono', '=', $value]
                ])->update([
                    'async_status' => 0
                ]);
            }
        }

        if (isset($jsonResult->success)) {
            foreach ($jsonResult->success->telefono as $key => $value) {
                TelefonosDetalle::where([
                    ['id_campana', '=', $request->idCampaign],
                    ['estado_validacion', '=', 7],
                    ['telefono', '=', $value]
                ])->update([
                    'async_status' => 1
                ]);
            }
        }
    }
}
