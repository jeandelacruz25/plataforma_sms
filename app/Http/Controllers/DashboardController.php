<?php

namespace App\Http\Controllers;

use App\Models\MenuUsers;
use App\Models\SearchModels\BaseCampanas;
use App\Models\SearchModels\BasesEstadisticas;
use App\Models\SMSModels\SMSChat;
use App\Models\SMSModels\SMSDetalle;
use App\Models\ValidatePhoneModels\TelefonosCampanas;
use App\Models\ValidatePhoneModels\TelefonosDetalle;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

class DashboardController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        return view('elements/dashboard/index')->with(array(
            'titleModule'           => 'Dashboard',
            'iconModule'            => 'feather feather-bar-chart-2'
        ));
    }

    public function listEstadosSMS(Request $request){
        $whereChart = array();

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $whereCustom = ['id_cliente', $idCliente];
        array_push($whereChart, $whereCustom);

        $smsDetalle = SMSDetalle::where($whereChart);
        $smsDetalleSinEnviar = SMSDetalle::where($whereChart);

        $smsDetalle->whereMonth('fecha_envio', '=', Carbon::now()->format('m'));
        $smsDetalleSinEnviar->whereNull('fecha_envio');

        $dataEstados = $smsDetalle->orderBy('fecha_envio', 'asc')->get()->toArray();
        $dataEstadosSinEnviar = $smsDetalleSinEnviar->get()->toArray();

        $grouped = collect($dataEstados)->groupBy(function ($item, $key) {
            return getStatusResponse($item['estado_sms']);
        });

        $groupedSinEnviar = collect($dataEstadosSinEnviar)->groupBy(function ($item, $key) {
            return getStatusResponse($item['estado_sms']);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        $groupCountSinEnviar = $groupedSinEnviar->map(function ($item, $key) {
            return collect($item)->count();
        });

        return response()->json([
            'smsEnviados'     => $groupCount,
            'smsSinEnviar'    => $groupCountSinEnviar
        ]);
    }

    public function dashboardSMSSend(Request $request){
        $whereChart = array();

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $whereCustom = ['id_cliente', $idCliente];
        array_push($whereChart, $whereCustom);

        $smsChat = SMSDetalle::where($whereChart);

        $smsChat->whereIn('tipo_envio', [1, 2, 3, 4, 5, 6]);
        $smsChat->whereNotIn('estado_sms', [5, 6]);

        $smsChat->whereMonth('fecha_envio', '=', Carbon::now()->format('m'));

        $dataChart = $smsChat->orderBy('fecha_envio', 'asc')->get()->toArray();

        $dataLabel = collect($dataChart)->map(function ($item, $key) {
            return Date::parse($item['fecha_envio'])->format('D d');
        })->unique()->values()->toArray();

        $grouped = collect($dataChart)->groupBy(function ($item, $key) {
            return Date::parse($item['fecha_envio'])->format('D d');
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        })->values();

        $chart = Charts::create('area', 'chartjs')
            ->title('')
            ->elementLabel('Mensajes Enviados')
            ->colors([
                '#95a5a6'
            ])
            ->labels($dataLabel)
            ->values($groupCount)
            ->dimensions(0, 220)
            ->legend(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'countSMS'  => collect($groupCount)->values()->sum()
        ]);
    }

    public function dashboardSMSReceived(Request $request){
        $whereChart = array();

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $whereCustom = ['id_cliente', $idCliente];
        array_push($whereChart, $whereCustom);

        $smsChat = SMSChat::where($whereChart);

        $smsChat->whereIn('envio_sms', [1, 3]);

        $smsChat->whereMonth('fecha_sms', '=', Carbon::now()->format('m'));

        $dataChart = $smsChat->orderBy('fecha_sms', 'asc')->get()->toArray();

        $dataLabel = collect($dataChart)->map(function ($item, $key) {
            return Date::parse($item['fecha_sms'])->format('D d');
        })->unique()->values()->toArray();

        $grouped = collect($dataChart)->groupBy(function ($item, $key) {
            return Date::parse($item['fecha_sms'])->format('D d');
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        })->values();

        $chart = Charts::create('area', 'chartjs')
            ->title('')
            ->elementLabel('Mensajes Recibidos')
            ->colors([
                '#2ecc71'
            ])
            ->labels($dataLabel)
            ->values($groupCount)
            ->dimensions(0, 220)
            ->legend(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'countSMS'  => collect($groupCount)->values()->sum()
        ]);
    }

    public function listEstadosValidatePhone(Request $request){

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $smsDetalle = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        });
        $campanasCreadas = TelefonosCampanas::where('id_cliente', $idCliente);
        $campanasSinLanzar = TelefonosCampanas::where([
            ['id_cliente', '=', $idCliente],
            ['estado_envio', '=', 0]
        ]);

        $smsDetalle->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'));
        $campanasCreadas->whereMonth('created_at', '=', Carbon::now()->format('m'));
        $campanasSinLanzar->whereMonth('created_at', '=', Carbon::now()->format('m'));

        $dataEstados = $smsDetalle->orderBy('fecha_validacion', 'asc')->get()->toArray();

        $countCampanas = $campanasCreadas->count();
        $countCampanasSinLanzar = $campanasSinLanzar->count();
        $countTelefonos = $smsDetalle->count();

        $grouped = collect($dataEstados)->groupBy(function ($item, $key) {
            return getStatusValidacionText($item['estado_validacion']);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        return response()->json([
            'estadosValidados'     => $groupCount,
            'campanasCreadas'      => $countCampanas,
            'campanasSinLanzar'    => $countCampanasSinLanzar,
            'numerosValidados'     => $countTelefonos
        ]);
    }

    public function dashboardValidateTelephoneDay(Request $request){
        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $telefonoDetalle = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc');

        $dataChartLabel = $telefonoDetalle->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        $dataLabel = collect($dataChartLabel)->map(function ($item, $key) {
            return Date::parse($item['fecha_validacion'])->format('D d');
        })->values()->toArray();

        /* Data del estado prendido */
        $labelPrendido = collect($dataChartLabel)->map(function ($item, $key) {
            return Carbon::parse($item['fecha_validacion'])->format('Y-m-d');
        })->values()->toArray();

        $dataPrendido = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc')->whereIn('estado_validacion', [9])->select(DB::raw('count(*) as total_prendido'), DB::raw('DATE_FORMAT(fecha_validacion, "%Y-%m-%d") as fecha_validacion'))->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        foreach($dataPrendido as $key => $value){
            $keySearch = array_search($value['fecha_validacion'], $labelPrendido);
            $labelPrendido[$keySearch] = $value['total_prendido'];
        }

        $dataChartPrendido = collect($labelPrendido)->map(function ($item, $key) {
            return (strpos($item, '-') !== false) ? '0' : $item;
        })->values()->toArray();

        /* Data del estado buzon de voz */
        $labelBuzondeVoz = collect($dataChartLabel)->map(function ($item, $key) {
            return Carbon::parse($item['fecha_validacion'])->format('Y-m-d');
        })->values()->toArray();

        $dataBuzonVoz = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc')->whereIn('estado_validacion', [8])->select(DB::raw('count(*) as total_buzon'), DB::raw('DATE_FORMAT(fecha_validacion, "%Y-%m-%d") as fecha_validacion'))->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        foreach($dataBuzonVoz as $key => $value){
            $keySearch = array_search($value['fecha_validacion'], $labelBuzondeVoz);
            $labelBuzondeVoz[$keySearch] = $value['total_buzon'];
        }

        $dataChartBuzonVoz = collect($labelBuzondeVoz)->map(function ($item, $key) {
            return (strpos($item, '-') !== false) ? '0' : $item;
        })->values()->toArray();

        /* Data del estado inexistente */
        $labelInexistente = collect($dataChartLabel)->map(function ($item, $key) {
            return Carbon::parse($item['fecha_validacion'])->format('Y-m-d');
        })->values()->toArray();

        $dataInexistente = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc')->whereIn('estado_validacion', [7])->select(DB::raw('count(*) as total_nocontesta'), DB::raw('DATE_FORMAT(fecha_validacion, "%Y-%m-%d") as fecha_validacion'))->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        foreach($dataInexistente as $key => $value){
            $keySearch = array_search($value['fecha_validacion'], $labelInexistente);
            $labelInexistente[$keySearch] = $value['total_nocontesta'];
        }

        $dataChartInexistente = collect($labelInexistente)->map(function ($item, $key) {
            return (strpos($item, '-') !== false) ? '0' : $item;
        })->values()->toArray();

        $chart = Charts::multi('area', 'chartjs')
            ->title('')
            ->dataset('Prendido', $dataChartPrendido, '#84F962')
            ->dataset('Buzón de Voz', $dataChartBuzonVoz, '#fb9c94')
            ->dataset('Inexistente', $dataChartInexistente, '#ea92e1')
            ->colors(['#84F962', '#fb9c94', '#ea92e1'])
            ->labels($dataLabel)
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->credits(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);
    }

    public function dashboardNumbersValidate(Request $request){
        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $telefonoDetalle = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc');

        $dataChartLabel = $telefonoDetalle->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        $dataLabel = collect($dataChartLabel)->map(function ($item, $key) {
            return Date::parse($item['fecha_validacion'])->format('D d');
        })->values()->toArray();

        $dataNumbers = TelefonosDetalle::whereHas('campana', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->whereMonth('fecha_validacion', '=', Carbon::now()->format('m'))->orderBy('fecha_validacion', 'asc')->select(DB::raw('count(*) as total_numeros'))->groupBy(DB::raw('Date(fecha_validacion)'))->get()->toArray();

        $chartDataNumbers = collect($dataNumbers)->map(function ($item, $key) {
            return $item['total_numeros'];
        })->values();

        $chart = Charts::create('bar2', 'chartjs')
            ->title('')
            ->elementLabel('Números Validados')
            ->dimensions(0, 250)
            ->legend(false)
            ->colors(['#fee7c3'])
            ->stacked(true)
            ->labels($dataLabel)
            ->values($chartDataNumbers);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);
    }

    public function dashboardSMSMonth(){
        $dataDashboardMonth =
            DB::select('
                    SELECT DATE_FORMAT(t.fecha, "%m-%Y") mes, SUM(t.cnt_sms) nro_gestiones
                      FROM 
                          (  
                            SELECT DATE(fecha_envio) fecha,
                                   COUNT(1) cnt_sms
                            FROM sms_detalle 
                            WHERE id_cliente = '.Auth::user()->id_cliente.' AND
                                  tipo_envio IN (1, 2, 3, 4, 5) AND 
                                  estado_sms NOT IN (5, 6) 
                              GROUP BY DATE(fecha_envio)
                            
                            UNION ALL
                            
                            SELECT DATE(fecha_sms) fecha,
                                   COUNT(1) cnt_sms 
                            FROM sms_chat 
                            WHERE id_cliente = '.Auth::user()->id_cliente.' AND
                                  envio_sms IN (1, 3) 
                              GROUP BY DATE(fecha_sms)
                        ) t
                    WHERE DATE(t.fecha) >= DATE_ADD(SUBDATE(CURDATE(), DAYOFMONTH(CURDATE()) - 1), INTERVAL -1 YEAR) AND
                          DATE(t.fecha) <= LAST_DAY(CURDATE())
                    GROUP BY DATE_FORMAT(t.fecha, "%m-%Y")
               ');

        $dataLabel = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->mes;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->nro_gestiones;
        })->values();

        $chart = Charts::create('bar2', 'chartjs')
            ->title('')
            ->elementLabel('Total Validado')
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->colors(['#c2e9fb'])
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);

    }

    public function dashboardValidatePhoneMonth(){
        $dataDashboardMonth =
            DB::select('
                    SELECT DATE_FORMAT(td.fecha_validacion, "%m-%Y") mes,
                        COUNT(1) as nro_gestiones
                    FROM telefonos_campanas tc, telefonos_detalle td
                    WHERE tc.id = td.id_campana AND
                       tc.id_cliente = '.Auth::user()->id_cliente.' AND
                       DATE(td.fecha_validacion) >= DATE_ADD(SUBDATE(CURDATE(), DAYOFMONTH(CURDATE()) - 1), INTERVAL -1 YEAR) AND
                          DATE(td.fecha_validacion) <= LAST_DAY(CURDATE())
                    GROUP BY DATE_FORMAT(td.fecha_validacion, "%m-%Y")
               ');

        $dataLabel = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->mes;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->nro_gestiones;
        })->values();

        $chart = Charts::create('bar2', 'chartjs')
            ->title('')
            ->elementLabel('Total Gestionado')
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->colors(['#c2e9fb'])
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);

    }

    public function listBoxBases(Request $request){
        $whereChart = array();

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $whereCustom = ['id_cliente', $idCliente];
        array_push($whereChart, $whereCustom);

        $usuariosBases = MenuUsers::whereHas('user', function ($query) use($idCliente) {
            $query->where('id_cliente', '=', $idCliente);
        })->where('vue_name', 'search_person')->count();
        $campanasCreadas = BaseCampanas::where('id_cliente', $idCliente)->count();
        $campanasSinCruzar = BaseCampanas::where([
            ['id_cliente', '=', $idCliente],
            ['estado_base', '=', 0]
        ])->count();
        $queryBasesRaw = DB::select('
            SELECT COUNT(1) total,
                SUM(IF((be.autos + be.direcciones + be.familiares + be.financiero + be.personas + be.sueldos + be.sunat + be.telefonos)>0,1,0)) encontrados,
                (SUM(IF((be.autos + be.direcciones + be.familiares + be.financiero + be.personas + be.sueldos + be.sunat + be.telefonos)>0,1,0))/COUNT(1))*100 efectivos
            FROM bases_estadisticas be WHERE be.id_cliente = '.$idCliente.'
        ');

        return response()->json([
            'usuariosBases'         => $usuariosBases,
            'campanasCreadas'       => $campanasCreadas,
            'campanasSinCruzar'     => $campanasSinCruzar,
            'queryBasesRaw'         => $queryBasesRaw
        ]);
    }

    public function dashboardTotalEncontrados(Request $request){
        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $documentosEncontrados = BasesEstadisticas::Select(DB::raw('SUM(IF((autos + direcciones + familiares + financiero + personas + sueldos + sunat + telefonos)>0,1,0)) encontrados, fecha'))->where('id_cliente', $idCliente)->whereMonth('fecha', '=', Carbon::now()->format('m'))->orderBy('fecha', 'asc');

        $dataChartLabel = $documentosEncontrados->groupBy(DB::raw('Date(fecha)'))->get()->toArray();

        $dataLabel = collect($dataChartLabel)->map(function ($item, $key) {
            return Date::parse($item['fecha'])->format('D d');
        })->values()->toArray();

        $dataSetEncontrados = collect($dataChartLabel)->map(function ($item, $key) {
            return $item['encontrados'];
        })->values()->toArray();

        $chart = Charts::multi('area', 'chartjs')
            ->title('')
            ->dataset('Encontrados', $dataSetEncontrados, '#fee7c3')
            ->colors(['#fee7c3'])
            ->labels($dataLabel)
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->credits(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);
    }

    public function dashboardBuscadosEncontrados(Request $request){
        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        $documentosEncontrados = BasesEstadisticas::Select(DB::raw('SUM(IF((autos + direcciones + familiares + financiero + personas + sueldos + sunat + telefonos)>0,1,0)) encontrados, COUNT(1) total, fecha'))->where('id_cliente', $idCliente)->whereMonth('fecha', '=', Carbon::now()->format('m'))->orderBy('fecha', 'asc');

        $dataChartLabel = $documentosEncontrados->groupBy(DB::raw('Date(fecha)'))->get()->toArray();

        $dataLabel = collect($dataChartLabel)->map(function ($item, $key) {
            return Date::parse($item['fecha'])->format('D d');
        })->values()->toArray();

        $dataSetBuscados = collect($dataChartLabel)->map(function ($item, $key) {
            return $item['total'];
        })->values()->toArray();

        $dataSetEncontrados = collect($dataChartLabel)->map(function ($item, $key) {
            return $item['encontrados'];
        })->values()->toArray();

        $chart = Charts::multi('bar', 'chartjs')
            ->title('')
            ->dataset('Buscados', $dataSetBuscados, '#c7f9b7')
            ->dataset('Encontrados', $dataSetEncontrados, '#fee7c3')
            ->colors(['#c7f9b7', '#fee7c3'])
            ->labels($dataLabel)
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->credits(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);
    }

    public function dashboardBasesMonth(){
        $dataDashboardMonth =
            DB::select('
                    SELECT DATE_FORMAT(ce.fecha, "%m-%Y") mes,
                           COUNT(1) as nro_gestiones
                    FROM bases_estadisticas ce
                    WHERE ce.id_cliente = '.Auth::user()->id_cliente.' AND
                          DATE(ce.fecha) >= DATE_ADD(SUBDATE(CURDATE(), DAYOFMONTH(CURDATE()) - 1), INTERVAL -1 YEAR) AND
                          DATE(ce.fecha) <= LAST_DAY(CURDATE())
                    GROUP BY DATE_FORMAT(ce.fecha, "%m-%Y")
               ');

        $dataLabel = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->mes;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataDashboardMonth)->map(function ($item, $key) {
            return $item->nro_gestiones;
        })->values();

        $chart = Charts::create('bar2', 'chartjs')
            ->title('')
            ->elementLabel('Total Cruzado')
            ->dimensions(0, 250)
            ->legend(false)
            ->stacked(true)
            ->colors(['#c2e9fb'])
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);

    }
}
