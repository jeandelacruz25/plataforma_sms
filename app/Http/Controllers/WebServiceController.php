<?php

namespace App\Http\Controllers;

use App\Models\Clientes;
use App\Models\ScoreModels\ClientesCarteras;
use App\Models\SearchModels\Autos;
use App\Models\SearchModels\Direcciones;
use App\Models\SearchModels\EsSalud;
use App\Models\SearchModels\Familiares;
use App\Models\SearchModels\Financiero;
use App\Models\SearchModels\FinancieroDetalle;
use App\Models\SearchModels\Personas;
use App\Models\SearchModels\SunatDetalle;
use App\Models\SearchModels\SunatDNI;
use App\Models\SMSModels\SMSChat;
use App\Models\SMSModels\SMSDetalle;
use App\Models\SMSModels\SMSKeyword;
use Carbon\Carbon;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use infobip\api\client\GetReceivedMessages;
use infobip\api\client\GetReceivedSmsLogs;
use infobip\api\client\GetSentSmsDeliveryReports;
use infobip\api\client\GetSentSmsLogs;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mo\logs\GetReceivedSmsLogsExecuteContext;
use infobip\api\model\sms\mt\logs\GetSentSmsLogsExecuteContext;
use infobip\api\model\sms\mo\reports\GetReceivedMessagesExecuteContext;
use infobip\api\model\sms\mt\reports\GetSentSmsDeliveryReportsExecuteContext;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Jcf\Geocode\Geocode;
use DataFalabella\DataFalabella;
use Sunat\Sunat;
use Webpatser\Uuid\Uuid;

class WebServiceController extends SecuritecController
{
    public function connectInfoBip(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_DOSVIAS_USERNAME','Securitec002'), env('INFOBIP_DOSVIAS_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function connectInfoBipInformativo(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_INFO_USERNAME','Securitec3'), env('INFOBIP_INFO_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function connectInfoBipKeyword(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_KEYWORD_USERNAME','Securitec001'), env('INFOBIP_KEYWORD_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function getSMSLogs(){
        $client = new GetSentSmsLogs($this->connectInfoBip());

        $context = new GetSentSmsLogsExecuteContext();

        $response = $client->execute($context);

        dd($response->getResults());
    }

    public function getSMSReceivedLogs(){
        $client = new GetReceivedSmsLogs($this->connectInfoBip());

        $context = new GetReceivedSmsLogsExecuteContext();

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach (array_reverse($sentMessageInfo) as $key => $value){

            $smsBukIDCount = SMSDetalle::where('id_bulk_sms', $value->getTo())->count();

            if($smsBukIDCount > 0){
                $smsMessageIDCount = SMSChat::where('id_sms', $value->getMessageId())->count();
                if($smsMessageIDCount == 0){

                    $arrayIDBulk = explode('_', $value->getTo());
                    $arraySearch = array_search('campana', $arrayIDBulk);
                    $tipoEnvio = !$arraySearch ? 1 : 2;

                    $smsDetalle = SMSDetalle::where([
                        ['id_bulk_sms', '=', $value->getTo()],
                        ['tipo_envio', '=', $tipoEnvio],
                        ['telefono_usuario', '=', $value->getFrom()]
                    ])->update([
                        'leido_sms'             => 1,
                        'sms_respondido'        => 1,
                        'fecha_respuesta_sms'   => Carbon::parse($value->getReceivedAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s'),
                        'visto_sms'             => 1
                    ]);

                    $smsDetalleSearch = SMSDetalle::Select()
                        ->where([
                            ['id_bulk_sms', '=', $value->getTo()],
                            ['tipo_envio', '=', $tipoEnvio],
                            ['telefono_usuario', '=', $value->getFrom()]
                        ])
                        ->get()
                        ->toArray();

                    if($smsDetalle){
                        SMSChat::updateOrCreate([
                            'id_sms'        => $value->getMessageId(),
                            'telefono_sms'  => $value->getFrom(),
                            'remitente_sms' => $value->getTo()
                        ],[
                            'id_sms'        => $value->getMessageId(),
                            'mensaje_sms'   => $value->getCleanText(),
                            'telefono_sms'  => $value->getFrom(),
                            'envio_sms'     => 1,
                            'remitente_sms' => $value->getTo(),
                            'fecha_sms'     => Carbon::parse($value->getReceivedAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s'),
                            'id_cliente'    => $smsDetalleSearch[0]['id_cliente']
                        ]);
                    }
                }
            }
        }
        return 'receivedSMS';
    }

    public function getSendSMS(Request $request){

        if($request->bulkID){
            if($request->envioTipo == '1'){
                $client = new GetSentSmsDeliveryReports($this->connectInfoBipInformativo());
            }else{
                $client = new GetSentSmsDeliveryReports($this->connectInfoBip());
            }
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }else if($request->keywordID) {
            $client = new GetSentSmsDeliveryReports($this->connectInfoBipKeyword());
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }else{
            $client = new GetSentSmsDeliveryReports($this->connectInfoBip());
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach ($sentMessageInfo as $key => $value) {
            SMSDetalle::where([
                ['id_sms', '=', $value->getMessageId()]
            ])->update([
                'estado_sms'            => $value->getStatus()->getGroupId(),
                'estado_descripcion'    => $value->getStatus()->getName(),
                'numero_mccmnc'         => $value->getMccMnc(),
                'fecha_recepcion'       => Carbon::parse($value->getDoneAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s')
            ]);
        }
        return 'sendSMS';
    }

    public function getSendSMSDosVias(){
        $client = new GetSentSmsDeliveryReports($this->connectInfoBip());
        $context = new GetSentSmsDeliveryReportsExecuteContext();

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach ($sentMessageInfo as $key => $value) {

            SMSDetalle::where([
                ['id_sms', '=', $value->getMessageId()]
            ])->update([
                'estado_sms'            => $value->getStatus()->getGroupId(),
                'estado_descripcion'    => $value->getStatus()->getName(),
                'numero_mccmnc'         => $value->getMccMnc(),
                'fecha_recepcion'       => Carbon::parse($value->getDoneAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s')
            ]);
        }
        return 'sendSMSDosVias';
    }

    public function getSendSMSInformativo(){
        $client = new GetSentSmsDeliveryReports($this->connectInfoBipInformativo());
        $context = new GetSentSmsDeliveryReportsExecuteContext();

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach ($sentMessageInfo as $key => $value) {
            SMSDetalle::where([
                ['id_sms', '=', $value->getMessageId()]
            ])->update([
                'estado_sms'            => $value->getStatus()->getGroupId(),
                'estado_descripcion'    => $value->getStatus()->getName(),
                'numero_mccmnc'         => $value->getMccMnc(),
                'fecha_recepcion'       => Carbon::parse($value->getDoneAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s')
            ]);
        }
        return 'sendSMSDosVias';
    }

    public function getSendSMSKeyword(){
        $client = new GetSentSmsDeliveryReports($this->connectInfoBipKeyword());
        $context = new GetSentSmsDeliveryReportsExecuteContext();

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach ($sentMessageInfo as $key => $value) {
            SMSDetalle::where([
                ['id_sms', '=', $value->getMessageId()]
            ])->update([
                'estado_sms'            => $value->getStatus()->getGroupId(),
                'estado_descripcion'    => $value->getStatus()->getName(),
                'numero_mccmnc'         => $value->getMccMnc(),
                'fecha_recepcion'       => Carbon::parse($value->getDoneAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s')
            ]);
        }
        return 'sendSMSDosVias';
    }

    public function getSendSMSReport(Request $request){

        if($request->tipoEnvio == 0){
            $client = new GetSentSmsDeliveryReports($this->connectInfoBip());
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }else if($request->tipoEnvio == 2) {
            $client = new GetSentSmsDeliveryReports($this->connectInfoBipKeyword());
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }else{
            $client = new GetSentSmsDeliveryReports($this->connectInfoBip());
            $context = new GetSentSmsDeliveryReportsExecuteContext();
        }

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        foreach ($sentMessageInfo as $key => $value) {
            SMSDetalle::where([
                ['id_sms', '=', $value->getMessageId()]
            ])->update([
                'estado_sms'            => $value->getStatus()->getGroupId(),
                'estado_descripcion'    => $value->getStatus()->getName(),
                'numero_mccmnc'         => $value->getMccMnc(),
                'fecha_recepcion'       => Carbon::parse($value->getDoneAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s')
            ]);
        }
        return 'sendSMS';
    }

    public function getSMSReceived(){
        $client = new GetReceivedMessages($this->connectInfoBip());

        $context = new GetReceivedMessagesExecuteContext();

        $response = $client->execute($context);

        dd($response->getResults());
    }

    public function getNotificationSMS(){
        $client = new GetReceivedSmsLogs($this->connectInfoBip());

        $context = new GetReceivedSmsLogsExecuteContext();

        $response = $client->execute($context);

        $sentMessageInfo = $response->getResults();

        $arrayCliente = [];

        foreach ($sentMessageInfo as $key => $value){
            $smsBukIDCount = SMSDetalle::where('id_bulk_sms', $value->getTo())->count();

            if($smsBukIDCount > 0){
                $smsMessageIDCount = SMSChat::where('id_sms', $value->getMessageId())->count();
                if($smsMessageIDCount != 0){
                    $smsMessageNew = SMSDetalle::with('cliente')->with('campana')->where([['id_bulk_sms', '=', $value->getTo()], ['leido_sms', '=', 1], ['visto_sms', '=', 1], ['sms_respondido', '=', 1]])->orderBy('fecha_respuesta_sms', 'desc')->get()->toArray();
                    $nameCliente = collect($smsMessageNew)->unique(function ($item) {
                        return $item['id_bulk_sms'].$item['cliente']['razon_social'];
                    })->toArray();

                    foreach($nameCliente as $key => $value){
                        array_push($arrayCliente, $value);
                    }
                }
            }
        }

        if(!empty($arrayCliente)){
            $arrayUnique = array_unique($arrayCliente, SORT_REGULAR);
            if(!empty($arrayUnique)){
                $client = new Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                $client->initialize();
                $client->emit('notificationSMS', ['arrayCliente' => $arrayUnique]);
                $client->close();
            }
        }
    }

    public function getKeywordSMS(){
        $client = new GetReceivedMessages($this->connectInfoBipKeyword());

        $context = new GetReceivedMessagesExecuteContext();

        $response = $client->execute($context);

        $result = $response->getResults();

        foreach ($result as $key => $value) {

            $nameKeyword = SMSKeyword::Select()
                ->where([
                    ['nombre_keyword', '=', $value->getKeyword()],
                    ['id_estado', '=', 1]
                ])
                ->get()
                ->toArray();

            if($nameKeyword){
                $queryChat = SMSChat::create([
                    'id_sms'            => $value->getMessageId(),
                    'mensaje_sms'       => $value->getText(),
                    'telefono_sms'      => $value->getFrom(),
                    'envio_sms'         => 1,
                    'remitente_sms'     => $value->getKeyword(),
                    'fecha_sms'         => Carbon::parse($value->getReceivedAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s'),
                    'id_cliente'        => $nameKeyword[0]['id_cliente'],
                ]);

                if($queryChat){
                    $queryResponseKeyword = SMSDetalle::Select()
                        ->with('keyword')
                        ->where([
                            ['filtro_mensaje', '=', $value->getCleanText()],
                            ['id_keyword', '=', $nameKeyword[0]['id']]
                        ])
                        ->orderBy('fecha_envio', 'desc')
                        ->get()->toArray();

                    if(!$queryResponseKeyword && $nameKeyword[0]['estado_error'] == 0){
                        return 'getKeyword';
                    }

                    $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBipKeyword());

                    $messages = array();
                    $numeroEnviar = collect($value->getFrom())->toArray();
                    foreach ($numeroEnviar as $valueNumero) {
                        $smsText = $queryResponseKeyword ? $queryResponseKeyword[0]['mensaje_respuesta'] : $nameKeyword[0]['mensaje_error'];
                        $destination = new Destination();
                        $message = new Message();
                        $destination->setTo($valueNumero);
                        $message->setFrom(76333);
                        $message->setDestinations([$destination]);
                        $message->setText($smsText);
                        $message->setSendAt(Carbon::now());
                        $messages[] = $message;
                    }

                    $requestBody = new SMSAdvancedTextualRequest();
                    $requestBody->setMessages($messages);

                    $response = $client->execute($requestBody);
                    $sentMessageInfo = $response->getMessages();
                    foreach ($sentMessageInfo as $valueMessageInfo) {
                        SMSChat::create([
                            'id_sms'            =>  $valueMessageInfo->getMessageId(),
                            'mensaje_sms'       =>  $queryResponseKeyword ? $queryResponseKeyword[0]['mensaje_respuesta'] : $nameKeyword[0]['mensaje_error'],
                            'telefono_sms'      =>  $valueMessageInfo->getTo(),
                            'envio_sms'         =>  0,
                            'remitente_sms'     =>  $value->getKeyword(),
                            'fecha_sms'         =>  Carbon::now(),
                            'id_cliente'        =>  $nameKeyword[0]['id_cliente']
                        ]);

                        SMSDetalle::create([
                            'telefono_usuario'      => $valueMessageInfo->getTo(),
                            'mensaje_usuario'       => $queryResponseKeyword ? $queryResponseKeyword[0]['mensaje_respuesta'] : $nameKeyword[0]['mensaje_error'],
                            'filtro_mensaje'        => '-',
                            'mensaje_respuesta'     => '-',
                            'tipo_envio'            => 5,
                            'flash_sms'             => 0,
                            'id_sms'                => $valueMessageInfo->getMessageId(),
                            'id_bulk_sms'           => $queryResponseKeyword[0]['id_bulk_sms'],
                            'estado_sms'            => $valueMessageInfo->getStatus()->getGroupId(),
                            'estado_descripcion'    => $valueMessageInfo->getStatus()->getName(),
                            'leido_sms'             => 0,
                            'sms_respondido'        => 0,
                            'fecha_envio'           => Carbon::now(),
                            'fecha_recepcion'       => null,
                            'fecha_respuesta_sms'   => null,
                            'visto_sms'             => 0,
                            'numero_mccmnc'         => 0,
                            'id_campana'            => 0,
                            'id_keyword'            => $nameKeyword[0]['id'],
                            'id_cliente'            => $nameKeyword[0]['id_cliente'],
                            'async_status'          => 0
                        ]);
                    }
                }
            }else{
                SMSChat::create([
                    'id_sms'            => $value->getMessageId(),
                    'mensaje_sms'       => $value->getText(),
                    'telefono_sms'      => $value->getFrom(),
                    'envio_sms'         => 3,
                    'remitente_sms'     => $value->getKeyword(),
                    'fecha_sms'         => Carbon::parse($value->getReceivedAt())->setTimezone('America/Lima')->format('Y-m-d H:i:s'),
                    'id_cliente'        => $nameKeyword[0]['id_cliente']
                ]);
            }
        }
        return 'getKeyword';
    }

    public function getCliente($idCliente){
        $cliente = Clientes::Select()
            ->with('status')
            ->where('id', $idCliente)
            ->get()
            ->toArray();

        return $cliente;
    }

    public function getUUID($ver, $node){
        $uuid = Uuid::generate($ver,$node)->string;
        return $uuid;
    }

    public function recolectDataSunat(){
        $dataDNI = Personas::Select()
            ->where('FECHA_EVAL', null)
            ->limit(5000)
            ->get()
            ->toArray();

        $sunatAPI = new Sunat();

        if($dataDNI){
            foreach($dataDNI as $key => $value){
                $dataSunat = $sunatAPI->search($value['NRODOC'], false);
                if($dataSunat['success']){
                    $resultSunat = $this->latin_to_utf8($dataSunat['result']);

                    $dataFecha = Carbon::now();

                    $detalleSunatInsert = SunatDetalle::updateOrCreate([
                        'NRODOC'                    => $value['NRODOC']
                    ], [
                        'NRODOC'                    => $value['NRODOC'],
                        'FECHA_REG'                 => $dataFecha,
                        'RUC'                       => $resultSunat['RUC'],
                        'RazonSocial'               => $resultSunat['RazonSocial'],
                        'Telefono'                  => $resultSunat['Telefono'],
                        'Condicion'                 => $resultSunat['Condicion'],
                        'NombreComercial'           => $resultSunat['NombreComercial'],
                        'Tipo'                      => $resultSunat['Tipo'],
                        'Inscripcion'               => $resultSunat['Inscripcion'],
                        'Estado'                    => $resultSunat['Estado'],
                        'Direccion'                 => $resultSunat['Direccion'],
                        'SistemaEmision'            => $resultSunat['SistemaEmision'],
                        'ActividadExterior'         => $resultSunat['ActividadExterior'],
                        'SistemaContabilidad'       => $resultSunat['SistemaContabilidad'],
                        'Oficio'                    => isset($resultSunat['Oficio']) ? $resultSunat['Oficio'] : '-',
                        'EmisionElectronica'        => $resultSunat['EmisionElectronica'],
                        'PLE'                       => $resultSunat['PLE'],
                        'representantes_legales'    => $resultSunat['representantes_legales'],
                        'cantidad_trabajadores'     => $resultSunat['cantidad_trabajadores'],
                    ]);

                    if($detalleSunatInsert){
                        Personas::where('NRODOC', $value['NRODOC'])
                            ->update([
                                'FECHA_EVAL'    => $dataFecha,
                                'TAG'           => 'SUNAT'
                            ]);
                    }

                }else{
                    $dataFecha = Carbon::now();
                    Personas::where('NRODOC', $value['NRODOC'])
                        ->update([
                            'FECHA_EVAL'    => $dataFecha,
                            'TAG'           => 'SUNAT'
                        ]);
                }
            }
        }

        return 'success';

    }

    public function recolectDataFalabella(){
        $dataDNI = Personas::Select()
            ->whereNull('FECHA_EVAL')
            ->whereYear('FECHA_NAC', '>', 1948)
            ->limit(1000)
            ->get()
            ->toArray();

        if($dataDNI){
            $searchDataFalabella = new DataFalabella();
            foreach ($dataDNI as $key => $value) {
                $dataSearchDNIVal = Carbon::parse($value['FECHA_EVAL'])->format('M');
                $monthValidate = Carbon::parse($value['FECHA_EVAL'])->addMonths(2)->format('M');
                if(empty($dataSearchDNI['FECHA_EVAL']) && $dataSearchDNIVal != $monthValidate || !empty($dataSearchDNI['FECHA_EVAL']) && $dataSearchDNIVal == $monthValidate) {
                    try {
                        $searchDataFalabella->getDataPersonSearchFalabella($value['NRODOC']);
                        $sunatAPI = new Sunat();

                        $dataSunat = $sunatAPI->search($value['NRODOC'], false);
                        if($dataSunat['success']){
                            $resultSunat = $this->latin_to_utf8($dataSunat['result']);

                            $dataFecha = Carbon::now();

                            SunatDetalle::updateOrCreate([
                                'NRODOC'                    => $value['NRODOC']
                            ], [
                                'NRODOC'                    => $value['NRODOC'],
                                'FECHA_REG'                 => $dataFecha,
                                'RUC'                       => $resultSunat['RUC'],
                                'RazonSocial'               => $resultSunat['RazonSocial'],
                                'Telefono'                  => $resultSunat['Telefono'],
                                'Condicion'                 => $resultSunat['Condicion'],
                                'NombreComercial'           => $resultSunat['NombreComercial'],
                                'Tipo'                      => $resultSunat['Tipo'],
                                'Inscripcion'               => $resultSunat['Inscripcion'],
                                'Estado'                    => $resultSunat['Estado'],
                                'Direccion'                 => $resultSunat['Direccion'],
                                'SistemaEmision'            => $resultSunat['SistemaEmision'],
                                'ActividadExterior'         => $resultSunat['ActividadExterior'],
                                'SistemaContabilidad'       => $resultSunat['SistemaContabilidad'],
                                'Oficio'                    => isset($resultSunat['Oficio']) ? $resultSunat['Oficio'] : '-',
                                'EmisionElectronica'        => $resultSunat['EmisionElectronica'],
                                'PLE'                       => $resultSunat['PLE'],
                                'representantes_legales'    => $resultSunat['representantes_legales'],
                                'cantidad_trabajadores'     => $resultSunat['cantidad_trabajadores'],
                            ]);
                        }

                        Personas::where([
                            'NRODOC'            => $value['NRODOC']
                        ])->update([
                            'NRODOC'            => $value['NRODOC'],
                            'FECHA_EVAL'        => Carbon::now(),
                            'TAG'               => 'FALABELLA'
                        ]);

                    } catch (\Exception $e){
                        $this->recolectDataFalabella();
                    }
                }
            }
        }

        return 'success';
    }

    public function getCoordinateMaps(Request $request){
        $dataAddress = explode('/', $request->dataAddress);

        $address = Str::upper(str_replace('.', ' ', $dataAddress[0]));

        $response = Geocode::make()->address($address);

        if($response){
            return response()->json([
                'latitude'      => $response->latitude(),
                'longitude'     => $response->longitude()
            ]);
        }else{
            $address = Str::upper($dataAddress[1]);

            $response = Geocode::make()->address($address);

            return response()->json([
                'latitude'      => $response->latitude(),
                'longitude'     => $response->longitude()
            ]);
        }
    }

    public function dataFalebellaAPI(Request $request){
        $searchPeruApi = new DataFalabella();
        $dataPerson = $searchPeruApi->getDataPersonSearchFalabella($request->nroDNI);

        return $dataPerson;
    }

    public function syncScore(Request $request){
        ClientesCarteras::updateOrCreate([
            'id_cliente'        => $request->id_cliente,
            'id_cartera'        => $request->id_cartera,
            'cartera'           => $request->nom_cartera,
            'proveedor'         => $request->nom_proveedor
        ], [
            'id_cliente'        => $request->id_cliente,
            'id_cartera'        => $request->id_cartera,
            'cartera'           => $request->nom_cartera,
            'proveedor'         => $request->nom_proveedor
        ]);
        return 'Success';
    }
}