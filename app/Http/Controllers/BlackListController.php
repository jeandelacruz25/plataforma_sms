<?php

namespace App\Http\Controllers;

use App\Http\Requests\formBlackListMasivoRequest;
use App\Models\BlackList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class BlackListController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function black_list(){
        if(searchMenuCliente(auth()->user()->id_cliente, 'black_list') != 0 && searchMenuUsuario(auth()->id(), 'black_list') != 0){
            return view('elements/black_list/index')->with(array(
                'titleModule'           => 'Administrar Lista Negra',
                'iconModule'            => 'fa fa-cogs',
                'selectOptions'         => $this->selectionOption()
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Administrar Lista Negra',
                'menuNombre'            => 'Lista Negra'
            ));
        }
    }

    public function listBlackList(Request $request){
        if ($request->isMethod('get')) {
            if($request->dateFiltro){
                $black_list                 = $this->black_list_query($request->dateFiltro);
            }else{
                $black_list                 = $this->black_list_query();
            }
            $builderview                = $this->builderview($black_list);
            $outgoingcollection         = $this->outgoingcollection($builderview);
            $list_black                 = $this->FormatDatatable($outgoingcollection);
            return $list_black;
        }
    }

    protected function black_list_query($dateRange = false)
    {
        $black_list_query = BlackList::Select()
            ->with(['clientes', 'user_created', 'user_updated']);

        if(!$dateRange){
            $black_list_query->whereMonth('created_at', Carbon::now()->format('m'));
        }else{
            $dateFilter = explode(' / ', $dateRange);
            $black_list_query->whereBetween('created_at', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
        }

        if(!Auth::user()->authorizeRoles(['Administrador'])) {
            $black_list_query->where([
                ['id_cliente', '=', Auth::user()->id_cliente]
            ]);
        }

        return $black_list_query->get()->toArray();
    }

    protected function builderview($black_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($black_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_black_list']        = $query['id'];
            $builderview[$posicion]['id_cliente']           = $query['id_cliente'];
            $builderview[$posicion]['telefono']             = $query['telefono'];
            $builderview[$posicion]['clientes']             = $query['clientes']['razon_social'];
            $builderview[$posicion]['fecha_registro']       = Carbon::parse($query['created_at'])->format('d/m/Y H:i:s a');
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'                => $view['id'],
                'telefono'          => $view['telefono'],
                'clientes'          => $view['clientes'],
                'fecha_registro'    => $view['fecha_registro'],
                'action'            => '<div class="btn-group" role="group">
                                            <button type="button" onclick="responseModal('."'div.dialogScore','formBlackListDelete','".$view['id_black_list']."'".')" data-toggle="modal" data-target="#modalScore" title="Eliminar Número" class="btn btn-rose2 btn-sm"><i class="fa fa-close"></i></button>
                                        </div>'
            ]);
        }
        return $outgoingcollection;
    }

    public function formBlackListIndividual(Request $request){
        return view('elements/black_list/form/form_black_list_individual')->with(array(
            'options'   => $this->selectionOption()
        ));
    }

    public function formBlackListMasivo(Request $request){
        return view('elements/black_list/form/form_black_list_masivo')->with(array(
            'options'   => $this->selectionOption()
        ));
    }

    public function formBlackListDelete(Request $request){
        $dataBlackList = $this->getNumberBlackList($request->valueID);
        return view('elements/black_list/form/form_black_list_delete')->with(array(
            'dataBlackList'   => $dataBlackList
        ));
    }

    public function saveFormBlackList(Request $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $numeroBlackList = explode(",",$request->numeroBlackList);
            $idCliente = $request->clienteBlackList ? $request->clienteBlackList : Auth::user()->id_cliente;
            foreach($numeroBlackList as $key => $val) {
                $rules[$key]    = 'required|numeric';
            }
            $messages = [];
            foreach($numeroBlackList as $key => $val) {
                $messages[$key.'.required']         = 'Debes ingresar por lo menos un número.';
                $messages[$key.'.numeric']          = 'El valor ingresado : '.$val.', no es un número por lo tanto no es valido';
            }
            $validator = Validator::make($numeroBlackList, $rules, $messages);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }else{
                try {
                    if($request->checkDelete){
                        foreach ($numeroBlackList as $key => $value){
                            BlackList::where([
                                ['telefono', '=', $value],
                                ['id_cliente', '=', $idCliente]
                            ])->delete();
                        }
                    }else{
                        foreach ($numeroBlackList as $key => $value){
                            BlackList::updateOrCreate([
                                'telefono'      =>  $value,
                                'id_cliente'    =>  $idCliente
                            ],[
                                'telefono'      =>  $value,
                                'id_cliente'    =>  $idCliente
                            ]);
                        }
                    }
                    return ['message' => 'Success', 'datatable' => 'listBlackList'];
                }catch (\Exception $e){
                    return ['message' => 'Error'];
                }
            }
        }
        return ['message' => 'Error'];
    }

    public function saveFormBlackListMasivo(formBlackListMasivoRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $idCliente = $request->clienteBlackList ? $request->clienteBlackList : Auth::user()->id_cliente;
            $dataSheet = Excel::load($request->file('baseNumeros')->getRealPath(), function($reader) { })->get()->toArray();
            $rules = [];
            foreach($dataSheet as $key => $val) {
                $rules[$key.'.telefono']            = 'required|numeric';
            }
            $messages = [];
            foreach($dataSheet as $key => $val) {
                $messages[$key.'.telefono.required']    = 'Debes ingresar en el campo telefono un número, error fila '.($key + 2).'.';
                $messages[$key.'.telefono.numeric']     = 'El valor :input de la fila '.($key + 2).' en el número usuario, solo debe contener números.';
            }
            $validator = Validator::make($dataSheet, $rules, $messages);

            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }else{
                try {
                    $deleteCheck = $request->checkDelete;
                    Excel::filter('chunk')->load($request->file('baseNumeros')->getRealPath())->chunk(20, function($reader) use($idCliente, $deleteCheck) {
                        foreach ($reader as $key => $value) {
                            if($deleteCheck){
                                BlackList::where([
                                    ['telefono', '=', $value->telefono],
                                    ['id_cliente', '=', $idCliente]
                                ])->delete();
                            }else{
                                BlackList::updateOrCreate([
                                    'telefono'      =>  $value->telefono,
                                    'id_cliente'    =>  $idCliente
                                ],[
                                    'telefono'      =>  $value->telefono,
                                    'id_cliente'    =>  $idCliente
                                ]);
                            }
                        }
                    });
                    return ['message' => 'Success', 'datatable' => 'listBlackList'];
                }catch (\Exception $e){
                    return ['message' => 'Error'];
                }
            }
        }
        return ['message' => 'Error'];
    }

    public function deleteNumberBlackList(Request $request){
        $numberDelete = BlackList::where('id', $request->blackListID)->delete();

        if($numberDelete){
            return ['message' => 'Success', 'datatable' => 'listBlackList'];
        }

        return ['message' => 'Error'];
    }

    public function getNumberBlackList($idBlackList){
        $numberBlackList = BlackList::Select()
            ->where('id', $idBlackList)
            ->get()
            ->toArray();

        return $numberBlackList;
    }
}
