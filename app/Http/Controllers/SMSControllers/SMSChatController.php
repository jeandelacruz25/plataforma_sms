<?php

namespace App\Http\Controllers\SMSControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formChatRequest;
use App\Models\Clientes;
use App\Models\SMSModels\SMSCampanas;
use App\Models\SMSModels\SMSChat;
use App\Models\SMSModels\SMSDetalle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Webpatser\Uuid\Uuid;

class SMSChatController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function connectInfoBip(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_DOSVIAS_USERNAME','Securitec002'), env('INFOBIP_DOSVIAS_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function index(){
        if(searchMenuCliente(auth()->user()->id_cliente, 'chat') != 0 && searchMenuUsuario(auth()->id(), 'chat') != 0){
            return view('elements/sms_chat/index')->with(array(
                'titleModule'       =>  'Chat SMS',
                'selectOptions'     => $this->selectionOption()
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Chat SMS',
                'menuNombre'            => 'Chat SMS'
            ));
        }
    }

    public function listChatCampaign(Request $request){
        $smsCampaign = SMSCampanas::Select()
            ->with(['clientes', 'detalle'])
            ->where([
                ['envio_sms', 1],
                ['id_status', 1]
            ])
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->orderBy('fecha_envio', 'desc');

            if($request->idCliente) {
                $smsCampaign->where([
                    ['id_cliente', '=', $request->idCliente]
                ]);
            }else{
                $smsCampaign->where([
                    ['id_cliente', '=', Auth::user()->id_cliente]
                ]);
            }
        return $smsCampaign->get()->toArray();
    }

    public function chatNumbers(Request $request){
        if($request->idCampana){
            return view('elements/sms_chat/tabs/utils/sms_chat_list')->with(array(
                'idCampana'   =>  $request->idCampana
            ));
        }else{
            return view('elements/sms_chat/tabs/utils/sms_chat_list_not')->with(array(
                'idCampana'   =>  $request->idNotCampana
            ));
        }
    }

    public function chatViewNumbers(Request $request){
        if($request->idCampana){
            return view('elements/sms_chat/tabs/utils/chat_view/sms_chat_list')->with(array(
                'idCampana'   =>  $request->idCampana
            ));
        }else{
            return view('elements/sms_chat/tabs/utils/chat_view/sms_chat_list_not')->with(array(
                'idCampana'   =>  $request->idNotCampana
            ));
        }
    }

    public function listChatNumbers(Request $request){
        $smsNumbers = SMSDetalle::Select()
            ->whereIn('sms_respondido', [1]);

            if(!Auth::user()->authorizeRoles(['Administrador'])) {
                $smsNumbers->where([
                    ['id_cliente', Auth::user()->id_cliente]
                ]);
            }

            if(is_numeric($request->idCampana)){
                $smsNumbers->where([
                    ['tipo_envio', 2],
                    ['id_campana', $request->idCampana]
                ]);
            }else{
                $smsNumbers->where([
                    ['tipo_envio', 1],
                    ['id_bulk_sms', $request->idCampana]
                ]);
            }

        $smsNumbers->whereMonth('fecha_envio', Carbon::now()->format('m'));

        return $smsNumbers->orderBy('fecha_respuesta_sms', 'desc')->get()->toArray();
    }

    public function chatWindow(Request $request){
        if($request->windowNot){
            return view('elements/sms_chat/tabs/utils/sms_chat_window_not')->with(array(
                'remitenteSMS'   => $request->remitenteSMS,
                'telefonoSMS'    => $request->telefonoSMS
            ));
        }else{
            return view('elements/sms_chat/tabs/utils/sms_chat_window')->with(array(
                'remitenteSMS'   => $request->remitenteSMS,
                'telefonoSMS'    => $request->telefonoSMS
            ));
        }
    }

    public function listChatWindow(Request $request){
        $smsWindow = SMSChat::Select()
            ->where([
                ['remitente_sms', $request->remitenteSMS],
                ['telefono_sms', $request->telefonoSMS]
            ])
            ->get()
            ->toArray();

        return $smsWindow;
    }

    public function formChatNumberSeen(){
        return view('elements/sms_chat/tabs/form/form_chat_number_seen')->with(array(
            'notCampaign'   => false
        ));
    }

    public function formChatNumberNotSeen(){
        return view('elements/sms_chat/tabs/form/form_chat_number_seen')->with(array(
            'notCampaign'   => true
        ));
    }

    public function changeSeenChat(Request $request){
        $smsSeenChange = SMSDetalle::where([
            ['id_bulk_sms', $request->remitenteSMS],
            ['telefono_usuario', $request->telefonoSMS]
        ])->update([
            'leido_sms' => 0,
            'visto_sms' => 0,
        ]);

        return $smsSeenChange;
    }

    public function listChatNotCampaign(Request $request){
        $sms_detalle_list_query = SMSDetalle::Select('*', DB::raw("SUM(IF(leido_sms= 1,1,0)) as total_received"))
            ->with(['campana', 'cliente'])
            ->where('tipo_envio',1)
            ->whereIn('sms_respondido', [1])
            ->groupBy('id_bulk_sms')
            ->orderBy('fecha_envio', 'desc');

        $sms_detalle_list_query->whereMonth('fecha_envio', Carbon::now()->format('m'));

        if($request->idCliente) {
            $sms_detalle_list_query->where([
                ['id_cliente', '=', $request->idCliente]
            ]);
        }else{
            $sms_detalle_list_query->where([
                ['id_cliente', '=', Auth::user()->id_cliente]
            ]);
        }

        return $sms_detalle_list_query->get()->toArray();
    }

    public function chatSMSHistory(Request $request){
        return view('elements/sms/tabs/utils/history_chat')->with(array(
            'dataChat'      => $this->getChatHistory($request->numero, $request->remitente),
            'numberChat'    => $request->numero
        ));
    }

    public function sendSMSChat(formChatRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBip());
            $messages = array();

            $arrayBlackList = $this->getBlackList();

            $numerosEnvio = $request->smsIdFrom;

            $arrayIntersect = array_intersect($arrayBlackList,$numerosEnvio);
            if($arrayIntersect){
                $numerosEnvio = array_diff($numerosEnvio, $arrayIntersect);
            }

            if(!empty($numerosEnvio)){
                foreach ($numerosEnvio as $key => $value) {
                    $destination = new Destination();
                    $message = new Message();
                    $destination->setTo($value);
                    $destination->setMessageId($this->getUUID(1, $value));
                    $message->setFrom($request->smsBulkSMS);
                    $message->setDestinations([$destination]);
                    $message->setText($request->smsChat);
                    $message->setSendAt(Carbon::now());
                    $messages[] = $message;
                }

                $queryCampaign = SMSDetalle::Select()->where([
                    ['id_bulk_sms', '=', $request->smsBulkSMS],
                    ['id_campana', '!=', 0],
                ])->get()->toArray();

                $requestBody = new SMSAdvancedTextualRequest();
                $requestBody->setMessages($messages);
                $requestBody->setBulkId($request->smsBulkSMS);

                try {
                    $response = $client->execute($requestBody);
                    $sentMessageInfo = $response->getMessages();
                    foreach ($sentMessageInfo as $key => $value) {
                        SMSDetalle::create([
                            'telefono_usuario'      => $value->getTo(),
                            'mensaje_usuario'       => $request->smsChat,
                            'tipo_envio'            => 3,
                            'flash_sms'             => 0,
                            'id_sms'                => $value->getMessageId(),
                            'id_bulk_sms'           => $request->smsBulkSMS,
                            'estado_sms'            => $value->getStatus()->getGroupId(),
                            'estado_descripcion'    => $value->getStatus()->getName(),
                            'leido_sms'             => 0,
                            'sms_respondido'        => 0,
                            'fecha_envio'           => Carbon::now(),
                            'fecha_recepcion'       => Carbon::now(),
                            'fecha_respuesta_sms'   => null,
                            'visto_sms'             => 0,
                            'numero_mccmnc'         => 0,
                            'id_campana'            => $queryCampaign ? $queryCampaign[0]['id_campana'] : 0,
                            'id_cliente'            => Auth::user()->id_cliente,
                            'async_status'          => 0
                        ]);

                        SMSChat::updateOrCreate([
                            'id_sms'            => $value->getMessageId(),
                            'telefono_sms'      => $value->getTo(),
                            'remitente_sms'     => $request->smsBulkSMS
                        ], [
                            'id_sms'            => $value->getMessageId(),
                            'mensaje_sms'       => $request->smsChat,
                            'telefono_sms'      => $value->getTo(),
                            'envio_sms'         => 0,
                            'remitente_sms'     => $request->smsBulkSMS,
                            'fecha_sms'         => Carbon::now(),
                            'id_cliente'        => Auth::user()->id_cliente
                        ]);
                    }
                    return ['message' => 'Success'];
                } catch (\Exception $exception) {
                    return response()->json($exception->getMessage(), 422);
                }
            }else{
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function saveFormChatNumberSeen(Request $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            foreach ($request->smsDetalleID as $key => $value){
                SMSDetalle::where('id', $value)->update([
                    'leido_sms'     => 0,
                    'visto_sms'     => 0
                ]);
            }
            return ['message' => 'Success', 'notCampaign' => $request->notCampaign];
        }
        return ['message' => 'Error'];
    }

    public function getChatHistory($numero, $remitente){
        $chatHistory = SMSChat::Select()
            ->where([
                ['telefono_sms', $numero],
                ['remitente_sms', $remitente]
            ])
            ->get()
            ->toArray();

        return $chatHistory;
    }

    public function getUUID($ver, $node){
        $uuid = Uuid::generate($ver,$node)->string;
        return $uuid;
    }
}
