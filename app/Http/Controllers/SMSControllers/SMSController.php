<?php

namespace App\Http\Controllers\SMSControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formCampaignRequest;
use App\Http\Requests\formCampaignStatusRequest;
use App\Http\Requests\formCampaignUploadRequest;
use App\Http\Requests\sendSMSIndividualRequest;
use App\Models\Estados;
use App\Models\SMSModels\SMSChat;
use App\Models\SMSModels\SMSDetalle;
use App\Models\SMSModels\SMSCampanas;
use App\Models\SMSModels\SMSKeyword;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Maatwebsite\Excel\Facades\Excel;
use Webpatser\Uuid\Uuid;

class SMSController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function connectInfoBip(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_DOSVIAS_USERNAME','Securitec002'), env('INFOBIP_DOSVIAS_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function connectInfoBipInformativo(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_INFO_USERNAME','Securitec3'), env('INFOBIP_INFO_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function index(){
        if(searchMenuCliente(auth()->user()->id_cliente, 'mensajeria_sms') != 0 && searchMenuUsuario(auth()->id(), 'mensajeria_sms') != 0){
            return view('elements/sms/index')->with(array(
                'titleCampaignModule'           => 'Administrar Campañas',
                'iconCampaignModule'            => 'fa fa-cogs',
                'titleSMSSinCampaignModule'     => 'Administrar Envios Sin Campaña',
                'iconSMSSinCampaignModule'      => 'fa fa-cogs',
                'titleReportSMSModule'          => 'Reportes SMS',
                'iconReportSMSModule'           => 'feather feather-bar-chart',
                'titleKeywordSMSModule'         => 'Administrar Keywords',
                'iconKeywordSMSModule'          => 'feather feather-shield',
                'selectOptions'                 => $this->selectionOption(),
                'selectKeywords'                => $this->keywordsAll()
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Administrar SMS',
                'menuNombre'            => 'SMS'
            ));
        }
    }

    public function paginationSMSCampanas(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->searchCampana){
            $whereCustom = ['nombre_campana', 'LIKE', '%'.$request->searchCampana.'%'];
            array_push($whereArray, $whereCustom);
        }

        $campanas = SMSCampanas::Select('id', 'id_cliente', 'nombre_campana', 'envio_sms', 'total_telefonos', 'fecha_envio', 'id_status', 'tipo_envio', 'created_at')
            ->with(['status', 'clientes'])
            ->withCount('detalle_count')
            ->where($whereArray)
            ->where(function($query) use ($request) {
                if($request->searchDateRange){
                    $dateFilter = explode(' / ', $request->searchDateRange);
                    $query->whereBetween('created_at', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
                }else{
                    $query->whereMonth('created_at', Carbon::now()->format('m'));
                }
            })
            ->orderby('id_status', 'asc')
            ->orderby('created_at', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $campanas->total(),
                'per_page' => $campanas->perPage(),
                'current_page' => $campanas->currentPage(),
                'last_page' => $campanas->lastPage(),
                'from' => $campanas->firstItem(),
                'to' => $campanas->lastItem()
            ],
            'data' => $campanas
        ];

        return response()->json($response);
    }

    public function paginationSMSSinCampanas(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        $sinCampanas = SMSDetalle::Select(DB::raw('count(*) as countTelephone'), 'id_bulk_sms', 'id_cliente', 'tipo_envio', 'fecha_envio', 'id_campana')
            ->with(['campana', 'cliente'])
            ->whereIn('tipo_envio', [1, 6])
            ->where($whereArray)
            ->where(function($query) use ($request) {
                if($request->searchDateRange){
                    $dateFilter = explode(' / ', $request->searchDateRange);
                    $query->whereBetween('fecha_envio', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
                }else{
                    $query->whereMonth('fecha_envio', Carbon::now()->format('m'));
                }
            })
            ->groupBy('id_bulk_sms')
            ->orderBy('fecha_envio', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $sinCampanas->total(),
                'per_page' => $sinCampanas->perPage(),
                'current_page' => $sinCampanas->currentPage(),
                'last_page' => $sinCampanas->lastPage(),
                'from' => $sinCampanas->firstItem(),
                'to' => $sinCampanas->lastItem()
            ],
            'data' => $sinCampanas
        ];

        return response()->json($response);
    }

    public function formCampaign(Request $request){
        if($request->valueID == null){
            return view('elements/sms/tabs/campaign/form/form_campaign')->with(array(
                'dataCampaign'          => '',
                'updateForm'            => false,
                'options'               => $this->selectionOption()
            ));
        }else{
            $getCampaign = $this->getCampaign($request->valueID);
            return view('elements/sms/tabs/campaign/form/form_campaign')->with(array(
                'dataCampaign'          => $getCampaign,
                'updateForm'            => true,
                'options'               => $this->selectionOption()
            ));
        }
    }

    public function formCampaignUpload(Request $request){
        $getCampaign = $this->getCampaign($request->valueID);
        return view('elements/sms/tabs/campaign/form/form_campaign_upload')->with(array(
            'dataCampaign'          => $getCampaign
        ));
    }

    public function formSMSMasivo(Request $request){
        $getCampaing = $this->getCampaign($request->valueID);
        return view('elements/sms/tabs/campaign/form/form_campaign_send')->with(array(
            'dataCampaign'  => $getCampaing
        ));
    }

    public function formCampaignUsers(Request $request){
        $getCampaign = $this->getCampaign($request->valueID);
        return view('elements/sms/tabs/campaign/form/form_campaign_base')->with(array(
            'dataCampaign'      => $getCampaign
        ));
    }

    public function formSMSIndividual(Request $request){
        return view('elements/sms/tabs/send/form/form_sms_individual')->with(array(
            'options'  => $this->selectionOption()
        ));
    }

    public function formBuilkIDUsers(Request $request){
        $getSMSDetalle = $this->getBuilkID($request->valueID);
        return view('elements/sms/tabs/send/form/form_sms_bulk_user')->with(array(
            'dataBulkID'  => $getSMSDetalle
        ));
    }

    public function formCampaignStatus(Request $request){
        $dataCampaign  = $this->getCampaign($request->valueID);
        $statusOptions = $this->getNotCampaignStatus($dataCampaign[0]['status']['id']);
        return view('elements/sms/tabs/campaign/form/form_campaign_status')->with(array(
            'dataCampaign'      => $dataCampaign,
            'options'           => $statusOptions
        ));
    }

    public function saveFormCampaign(formCampaignRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $campaignQuery = SMSCampanas::updateOrCreate([
                'id'                => $request->campaignID
            ], [
                'nombre_campana'    => $request->nameCampaign,
                'id_cliente'        => $request->clienteCampaign ? $request->clienteCampaign : Auth::user()->id_cliente,
                'envio_sms'         => $request->envioSMS ? $request->envioSMS : 0,
                'id_status'         => $request->statusID ? $request->statusID : 1,
                'total_telefonos'   => $request->totalTelefonos ? $request->totalTelefonos : '0',
                'tipo_envio'        => $request->tipoEnvio ? $request->tipoEnvio : 0,
                'fecha_envio'       => $request->fechaEnvio ? $request->fechaEnvio : null,
                'user_created'      => $request->userCreated ? $request->userCreated : Auth::user()->id,
                'user_updated'      => Auth::user()->id
            ]);
            $action = $request->campaignID ? 'update' : 'create';
            if($campaignQuery){
                return ['message' => 'Success', 'action' => $action, 'datatable' => 'listCampaign'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormCampaignUpload(formCampaignUploadRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            return [
                'message' => 'Success',
                'datatable' => 'listCampaign'
            ];
        }
        return ['message' => 'Error'];
    }

    public function executeUploadCampaign(Request $request){
        $idCampana = $request->campaignID;

        $dataCampaign = $this->getCampaign($idCampana);

        SMSCampanas::where('id', $idCampana)->update([
            'envio_sms'   => 2
        ]);

        try {
            $idCliente = $request->clienteID;

            app()->instance('insertCampaignUpload', collect());
            Excel::filter('chunk')->load($request->file('baseClientes')->getRealPath())->chunk(1000, function ($rows) {
                app()->instance('insertCampaignUpload', app('insertCampaignUpload')->merge($rows->toArray()));
            }, $shouldQueue = false);

            $dataSheet = app('insertCampaignUpload')->toArray();

            $arrayBlackList = $this->getBlackList();
            $collectArrayList = collect($arrayBlackList)->values()->toArray();

            $dbInsert = [];
            foreach (collect($dataSheet)->chunk(1000)->toArray() as $chunk) {
                foreach($chunk as $key => $value){
                    if($value['telefono'] || $value['mensaje']){
                        $arrayInsert = [
                            'telefono_usuario'      => $value['telefono'],
                            'mensaje_usuario'       => $value['mensaje'],
                            'filtro_mensaje'        => '-',
                            'mensaje_respuesta'     => '-',
                            'tipo_envio'            => 2,
                            'flash_sms'             => 0,
                            'id_sms'                => '0',
                            'id_bulk_sms'           => '0',
                            'estado_sms'            => array_search($value['telefono'], $collectArrayList) !== false ? '6' : '0',
                            'estado_descripcion'    => array_search($value['telefono'], $collectArrayList) !== false ? 'BLACK_LIST' : 'NO_ENVIADO',
                            'leido_sms'             => 0,
                            'sms_respondido'        => 0,
                            'fecha_envio'           => array_search($value['telefono'], $collectArrayList) !== false ? Carbon::now() : null,
                            'fecha_recepcion'       => null,
                            'fecha_respuesta_sms'   => null,
                            'visto_sms'             => 0,
                            'numero_mccmnc'         => 0,
                            'id_campana'            => $idCampana,
                            'id_keyword'            => 0,
                            'id_cliente'            => $idCliente,
                            'async_status'          => 0
                        ];
                        array_push($dbInsert, $arrayInsert);
                    }
                }
            }

            DB::beginTransaction();

            try {
                $insertDetalle = SMSDetalle::insert($dbInsert);

                if($insertDetalle){
                    $smsDetalleCount = SMSDetalle::where('id_campana', $idCampana)->count();

                    $smsCampanaUpdate = SMSCampanas::where('id', $idCampana)->update([
                        'total_telefonos'   => $smsDetalleCount,
                        'envio_sms'         => 0
                    ]);

                    if($smsCampanaUpdate){
                        $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                        $clientNode->initialize();

                        $clientNode->emit('reloadDatatable', [
                            'nameDatatable' => 'listCampaign',
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave'])
                        ]);

                        $clientNode->emit('notificationCampaign', [
                            'nameCampaign' => $dataCampaign[0]['nombre_campana'],
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                            'tipoSubida' => 0,
                            'licenseExceeded' => ''
                        ]);
                        $clientNode->close();

                        DB::commit();

                        return ['message' => 'Success'];
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                SMSCampanas::where('id', $idCampana)->update([
                    'envio_sms'   => 0
                ]);
                return ['message' => 'Error'];
            }

        }catch (\Exception $e){
            SMSCampanas::where('id', $idCampana)->update([
                'envio_sms'   => 0
            ]);
            return ['message' => 'Error'];
        }
    }

    public function saveFormCampaignStatus(formCampaignStatusRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = SMSCampanas::where('id', $request->campaignID)
                ->update(['id_status' => $request->statusCampaign]);
            if($usersQuery){
                return ['message' => 'Success', 'datatable' => 'listCampaign'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function sendSMSMasivo(Request $request){
        if ($request->isMethod('post')) {

            /*$timeIni = Carbon::now()->micro;
            $timeStart = Carbon::now()->format('H:i:s');*/

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $clienteInformation = $this->getCliente($request->clienteID);

            $informativoSMS = $request->informativoSMS ? true : false;

            $dataCampaign = $this->getCampaign($request->campaignID);

            if($informativoSMS){
                $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBipInformativo());
            }else{
                $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBip());
            }

            SMSCampanas::where('id', $request->campaignID)->update([
                'envio_sms'      => 3
            ]);

            $bulkID = Str::lower($clienteInformation[0]['palabra_clave']).'_campana_'.$request->clienteID.'_'.$request->campaignID;
            $flashSMS = $request->flashSMS ? true : false;
            $dateEnvio = $request->dateEnvio ? Carbon::parse($request->dateEnvio) : Carbon::now();

            $smsDetalle = SMSDetalle::Select()
                ->with('cliente')
                ->where([
                    ['id_campana', '=', $request->campaignID],
                    ['estado_sms', '!=', 6]
                ])
                ->get()
                ->toArray();

            if(!empty($smsDetalle)){
                $messages = array();
                foreach (collect($smsDetalle)->chunk(300) as $chunk) {
                    foreach ($chunk as $key => $value){
                        $destination = new Destination();
                        $message = new Message();
                        $destination->setTo($value['telefono_usuario']);
                        $destination->setMessageId($this->getUUID(1, $value['telefono_usuario'].'_'.$value['id_cliente'].'_'.$value['id_campana']));
                        $message->setFrom($bulkID);
                        $message->setDestinations([$destination]);
                        $message->setText($value['mensaje_usuario']);
                        $message->setSendAt($dateEnvio);
                        $message->setFlash($flashSMS);
                        $messages[] = $message;
                    }
                }

                $requestBody = new SMSAdvancedTextualRequest();
                $requestBody->setMessages($messages);
                $requestBody->setBulkId($bulkID);

                $countMessages = 0;

                try {
                    $response = $client->execute($requestBody);
                    $sentMessageInfo = $response->getMessages();
                    foreach ($sentMessageInfo as $key => $value) {
                        $countMessages++;
                        $smsMessageSend = SMSDetalle::Select()
                            ->with('cliente')
                            ->where([
                                ['telefono_usuario', '=', $value->getTo()],
                                ['id_campana', '=', $request->campaignID],
                                ['estado_sms', '!=', 6]
                            ])
                            ->get()
                            ->toArray();

                        SMSDetalle::where([
                            ['telefono_usuario', $value->getTo()],
                            ['id_campana', $request->campaignID],
                            ['id_cliente', $request->clienteID]
                        ])->update([
                            'tipo_envio'            => $informativoSMS ? 6 : 2,
                            'flash_sms'             => $flashSMS ? 1 : 0,
                            'id_sms'                => $value->getMessageId(),
                            'id_bulk_sms'           => $bulkID,
                            'estado_sms'            => $value->getStatus()->getGroupId(),
                            'estado_descripcion'    => $value->getStatus()->getName(),
                            'fecha_envio'           => $dateEnvio,
                            'fecha_recepcion'       => null
                        ]);

                        SMSChat::updateOrCreate([
                            'id_sms'            => $value->getMessageId(),
                            'telefono_sms'      => $value->getTo(),
                            'remitente_sms'     => $bulkID
                        ], [
                            'id_sms'            => $value->getMessageId(),
                            'mensaje_sms'       => $smsMessageSend[0]['mensaje_usuario'],
                            'telefono_sms'      => $value->getTo(),
                            'envio_sms'         => 0,
                            'remitente_sms'     => $bulkID,
                            'fecha_sms'         => Carbon::now(),
                            'id_cliente'        => $smsMessageSend[0]['id_cliente']
                        ]);
                    }

                    SMSCampanas::where('id', $request->campaignID)->update([
                        'envio_sms'      => 1,
                        'tipo_envio'     => $informativoSMS ? 1 : 0,
                        'fecha_envio'    => $dateEnvio
                    ]);

                    $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                    $clientNode->initialize();

                    $validateSocket = SMSCampanas::where([
                            ['id_cliente', '=', $dataCampaign[0]['id_cliente']],
                            ['envio_sms', '=', 3]
                        ])->count();

                    if($validateSocket == 1){
                        $clientNode->emit('clearTimeOut', [
                            'nameTimeOut' => 'listCampaign',
                            'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                            'userID' => auth()->user()->id
                        ]);
                    }

                    $clientNode->emit('notificationCampaign', [
                        'nameCampaign' => $dataCampaign[0]['nombre_campana'],
                        'nameRoom' => Str::lower($dataCampaign[0]['clientes']['palabra_clave']),
                        'tipoSubida' => 2,
                        'licenseExceeded' => ''
                    ]);
                    $clientNode->close();

                    //$this->createLogSysInformation($timeStart, $timeIni);

                    return ['message' => 'Success', 'datatable' => 'listCampaign'];
                } catch (\Exception $exception) {
                    return response()->json($exception->getMessage(), 422);
                }
            }else{
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function sendSMSIndividual(sendSMSIndividualRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $numerosEnvio = explode(",",$request->numerosEnviar);
            $idCliente = $request->clienteSMSIndividual ? $request->clienteSMSIndividual : Auth::user()->id_cliente;
            $cantidad = SMSDetalle::Select()->where([['id_cliente', $idCliente]])->whereIn('tipo_envio', [1, 6])->get()->count();
            $clienteInformation = $this->getCliente($idCliente);
            $informativoSMS = $request->informativoSMS ? true : false;
            $bulkID = Str::lower($clienteInformation[0]['palabra_clave']).'_sms_individual_'.$idCliente.'_'.($cantidad+1);
            $flashSMS = $request->flashSMS ? true : false;
            $dateEnvio = $request->dateEnvio ? Carbon::parse($request->dateEnvio) : Carbon::now();

            foreach($numerosEnvio as $key => $val) {
                $rules[$key]    = 'required|numeric';
            }
            $messages = [];
            foreach($numerosEnvio as $key => $val) {
                $messages[$key.'.required']    = 'Debes ingresar por lo menos un número.';
                $messages[$key.'.numeric']     = 'El valor ingresado : '.$val.', no es un número por lo tanto no es valido';
            }
            $validator = Validator::make($numerosEnvio, $rules, $messages);

            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }else{
                $paisEnvio = $request->paisEnvio;

                $arrayBlackList = $this->getBlackList();

                $arrayNumeros = [];
                foreach($numerosEnvio as $key => $value){
                    $numeroConcat = $paisEnvio.$value;
                    $arrayNumeros[] = $numeroConcat;
                }

                $arrayIntersect = array_intersect($arrayBlackList,$arrayNumeros);

                if($arrayIntersect){
                    $arrayNumeros = array_diff($arrayNumeros, $arrayIntersect);
                    foreach ($arrayIntersect as $key => $value){
                        SMSDetalle::create([
                            'telefono_usuario'      => $value,
                            'mensaje_usuario'       => $request->mensajeEnviar,
                            'filtro_mensaje'        => '-',
                            'mensaje_respuesta'     => '-',
                            'tipo_envio'            => 1,
                            'flash_sms'             => $flashSMS ? 1 : 0,
                            'id_sms'                => $this->getUUID(1, $value),
                            'id_bulk_sms'           => $bulkID,
                            'estado_sms'            => 6,
                            'estado_descripcion'    => 'BLACK_LIST',
                            'leido_sms'             => 0,
                            'sms_respondido'        => 0,
                            'fecha_envio'           => $dateEnvio,
                            'fecha_recepcion'       => null,
                            'fecha_respuesta_sms'   => null,
                            'visto_sms'             => 0,
                            'numero_mccmnc'         => 0,
                            'id_campana'            => 0,
                            'id_keyword'            => 0,
                            'id_cliente'            => $idCliente
                        ]);

                        SMSChat::updateOrCreate([
                            'id_sms'            => $this->getUUID(1, $value),
                            'telefono_sms'      => $value,
                            'remitente_sms'     => $bulkID
                        ], [
                            'id_sms'            => $this->getUUID(1, $value),
                            'mensaje_sms'       => $request->mensajeEnviar,
                            'telefono_sms'      => $value,
                            'envio_sms'         => 0,
                            'remitente_sms'     => $bulkID,
                            'fecha_sms'         => Carbon::now(),
                            'id_cliente'        => $idCliente
                        ]);
                    }
                }


                if(!empty($arrayNumeros)){

                    if($informativoSMS){
                        $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBipInformativo());
                    }else{
                        $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBip());
                    }

                    $messages = array();
                    foreach ($arrayNumeros as $key => $value) {
                        $destination = new Destination();
                        $message = new Message();
                        $destination->setTo($value);
                        $destination->setMessageId($this->getUUID(1, $value));
                        $message->setFrom($bulkID);
                        $message->setDestinations([$destination]);
                        $message->setText($request->mensajeEnviar);
                        $message->setFlash($flashSMS);
                        $message->setSendAt($dateEnvio);
                        if($request->tiempoValidez){
                            $message->setValidityPeriod($request->tiempoValidez);
                        }
                        $messages[] = $message;
                    }

                    $requestBody = new SMSAdvancedTextualRequest();
                    $requestBody->setMessages($messages);
                    $requestBody->setBulkId($bulkID);

                    try {
                        $response = $client->execute($requestBody);
                        $sentMessageInfo = $response->getMessages();
                        foreach ($sentMessageInfo as $key => $value) {
                            SMSDetalle::create([
                                'telefono_usuario'      => $value->getTo(),
                                'mensaje_usuario'       => $request->mensajeEnviar,
                                'filtro_mensaje'        => '-',
                                'mensaje_respuesta'     => '-',
                                'tipo_envio'            => $informativoSMS ? 6 : 1,
                                'flash_sms'             => $flashSMS ? 1 : 0,
                                'id_sms'                => $value->getMessageId(),
                                'id_bulk_sms'           => $bulkID,
                                'estado_sms'            => $value->getStatus()->getGroupId(),
                                'estado_descripcion'    => $value->getStatus()->getName(),
                                'leido_sms'             => 0,
                                'sms_respondido'        => 0,
                                'fecha_envio'           => $dateEnvio,
                                'fecha_recepcion'       => null,
                                'fecha_respuesta_sms'   => null,
                                'visto_sms'             => 0,
                                'numero_mccmnc'         => 0,
                                'id_campana'            => 0,
                                'id_keyword'            => 0,
                                'id_cliente'            => $idCliente
                            ]);

                            SMSChat::updateOrCreate([
                                'id_sms'            => $value->getMessageId(),
                                'telefono_sms'      => $value->getTo(),
                                'remitente_sms'     => $bulkID
                            ], [
                                'id_sms'            => $value->getMessageId(),
                                'mensaje_sms'       => $request->mensajeEnviar,
                                'telefono_sms'      => $value->getTo(),
                                'envio_sms'         => 0,
                                'remitente_sms'     => $bulkID,
                                'fecha_sms'         => $dateEnvio,
                                'id_cliente'        => $idCliente
                            ]);
                        }
                        return ['message' => 'Success', 'datatable' => 'listSMSIndividual'];
                    } catch (\Exception $exception) {
                        return response()->json($exception->getMessage(), 422);
                    }
                }else{
                    return ['message' => 'Success', 'datatable' => 'listSMSIndividual'];
                }
            }
        }
        return ['message' => 'Error'];
    }

    public function downloadReportCampaign(Request $request){
        $detalleSMS = SMSDetalle::Select()->where('id_campana', $request->idCampana)->get()->toArray();
        if($detalleSMS){
            $campana = SMSCampanas::Select()->where('id', $detalleSMS[0]['id_campana'])->get()->toArray();
            $nameCampana = substr(cleanString(str_replace(' ', '_', $campana[0]['nombre_campana'])).'_'.time(), 0, 31);

            Excel::create($nameCampana, function($excel) use($campana, $nameCampana) {
                $excel->sheet('sms_detalle', function($sheet) use($campana) {
                    $sheet->fromArray($this->builderExportReportCampaign($this->getSMSDetalle($campana[0]['id'])));
                });
            })->store('xlsx', 'exports');


            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameCampana.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    public function downloadReportSMSIndividual(Request $request){
        $detalleSMS = SMSDetalle::Select()->where('id_bulk_sms', $request->idBulkSMS)->get()->toArray();
        if($detalleSMS){
            $nameExport = cleanString(str_replace(' ', '_', $detalleSMS[0]['id_bulk_sms'])).'_'.time();

            Excel::create($nameExport, function($excel) use($detalleSMS) {
                $excel->sheet($detalleSMS[0]['id_bulk_sms'], function($sheet) use($detalleSMS) {
                    $sheet->fromArray($this->builderExportReportSMSIndividual($this->getBuilkID($detalleSMS[0]['id_bulk_sms'])));
                });
            })->store('xlsx', 'exports');

            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameExport.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    protected function builderExportReportCampaign($campana_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($campana_list_query as $query) {
            $numMCCMNC = $query['numero_mccmnc'];

            if($numMCCMNC){
                $getCountryCode = substr($numMCCMNC, 0, 3);
                $getOperadorCode = strlen($numMCCMNC) == 5 ? substr($numMCCMNC, 3, 6) : substr($numMCCMNC, 3, 7);
                $countryMCCNC = $getCountryCode;
                $operadorMCCNC = $getOperadorCode;
            }else{
                $countryMCCNC = 0;
                $operadorMCCNC = 0;
            }

            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['número_celular']           = $query['telefono_usuario'];
            $builderview[$posicion]['mensaje_enviado']          = $query['mensaje_usuario'];
            $builderview[$posicion]['campaña']                  = ucwords(Str::lower($query['campana']['nombre_campana']));
            $builderview[$posicion]['estado_envio']             = strip_tags(getStatusSMS($query['estado_sms']));
            $builderview[$posicion]['pais_envio']               = getCountryMCCMNC($countryMCCNC);
            $builderview[$posicion]['operador_envio']           = getOperatorMCCMNC($operadorMCCNC);
            $builderview[$posicion]['fecha_envio']              = $query['fecha_envio'] ? Carbon::parse($query['fecha_envio'])->format('d/m/Y H:i:s a') : '-';
            $builderview[$posicion]['fecha_recepcion']          = $query['fecha_recepcion'] ? Carbon::parse($query['fecha_recepcion'])->format('d/m/Y H:i:s a') : '-';
            $builderview[$posicion]['sincronizado']             = $query['async_status'] == 0 ? 'no' : 'si';
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function builderExportReportSMSIndividual($sms_individual_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($sms_individual_list_query as $query) {
            $numMCCMNC = $query['numero_mccmnc'];

            if($numMCCMNC){
                $getCountryCode = substr($numMCCMNC, 0, 3);
                $getOperadorCode = strlen($numMCCMNC) == 5 ? substr($numMCCMNC, 3, 6) : substr($numMCCMNC, 3, 7);
                $countryMCCNC = $getCountryCode;
                $operadorMCCNC = $getOperadorCode;
            }else{
                $countryMCCNC = 0;
                $operadorMCCNC = 0;
            }

            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['número_celular']           = $query['telefono_usuario'];
            $builderview[$posicion]['mensaje_enviado']          = $query['mensaje_usuario'];
            $builderview[$posicion]['bulk_id']                  = $query['id_bulk_sms'];
            $builderview[$posicion]['estado_envio']             = strip_tags(getStatusSMS($query['estado_sms']));
            $builderview[$posicion]['pais_envio']               = getCountryMCCMNC($countryMCCNC);
            $builderview[$posicion]['operador_envio']           = getOperatorMCCMNC($operadorMCCNC);
            $builderview[$posicion]['fecha_envio']              = $query['fecha_envio'] ? Carbon::parse($query['fecha_envio'])->format('d/m/Y H:i:s a') : '-';
            $builderview[$posicion]['fecha_recepcion']          = $query['fecha_recepcion'] ? Carbon::parse($query['fecha_recepcion'])->format('d/m/Y H:i:s a') : '-';
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    public function getNotCampaignStatus($idStatus){
        $statusCampaign = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusCampaign;
    }

    public function getCampaign($idCampaign){
        $campaign = SMSCampanas::Select()
            ->with(['status', 'clientes'])
            ->where('id', $idCampaign)
            ->get()
            ->toArray();

        return $campaign;
    }

    public function getSMSDetalle($idCampaing){
        $baseCampaign = SMSDetalle::Select()
            ->with('campana')
            ->where([
                ['id_campana', '=', $idCampaing]
            ])
            ->whereIn('tipo_envio', [2, 6])
            ->get()
            ->toArray();

        return $baseCampaign;
    }

    public function paginationFormCampaignUsers(Request $request){
        $baseCampaign = SMSDetalle::Select('id_campana', 'estado_sms', 'telefono_usuario', 'mensaje_usuario',
            'estado_descripcion', 'fecha_recepcion', 'tipo_envio', 'id_bulk_sms', 'numero_mccmnc')
            ->with('campana')
            ->where([
                ['id_campana', '=', $request->valueID],
                ['telefono_usuario', 'LIKE', '%'.$request->searchTelephone.'%']
            ])
            ->whereIn('tipo_envio', [2, 6])
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $baseCampaign->total(),
                'per_page' => $baseCampaign->perPage(),
                'current_page' => $baseCampaign->currentPage(),
                'last_page' => $baseCampaign->lastPage(),
                'from' => $baseCampaign->firstItem(),
                'to' => $baseCampaign->lastItem()
            ],
            'data' => $baseCampaign
        ];
        return response()->json($response);
    }

    public function paginationFormNotCampaignUsers(Request $request){
        $smsBulkID = SMSDetalle::Select('id_campana', 'estado_sms', 'telefono_usuario', 'mensaje_usuario',
            'estado_descripcion', 'fecha_recepcion', 'tipo_envio', 'id_bulk_sms', 'numero_mccmnc')
            ->with('campana')
            ->where([
                ['id_bulk_sms', '=', $request->valueID],
                ['telefono_usuario', 'LIKE', '%'.$request->searchTelephone.'%']
            ])
            ->whereIn('tipo_envio', [1, 6])
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $smsBulkID->total(),
                'per_page' => $smsBulkID->perPage(),
                'current_page' => $smsBulkID->currentPage(),
                'last_page' => $smsBulkID->lastPage(),
                'from' => $smsBulkID->firstItem(),
                'to' => $smsBulkID->lastItem()
            ],
            'data' => $smsBulkID
        ];

        return response()->json($response);
    }

    public function getBuilkID($idBulkID){
        $smsBulkID = SMSDetalle::Select('id_campana', 'estado_sms', 'telefono_usuario', 'mensaje_usuario',
            'estado_descripcion', 'fecha_recepcion', 'tipo_envio', 'id_bulk_sms', 'numero_mccmnc', 'fecha_envio', 'id_cliente')
            ->where([
                ['id_bulk_sms', $idBulkID]
            ])
            ->whereIn('tipo_envio', [1, 6])
            ->get()
            ->toArray();

        return $smsBulkID;
    }

    public function listReportCampana(Request $request){
        if($request->tipoEnvio == '0'){
            $campana = SMSCampanas::Select()
                ->with('clientes')
                ->where('id_cliente', $request->idCliente)
                ->get()
                ->toArray();
        }elseif($request->tipoEnvio == '1'){
            $campana = SMSDetalle::Select()
                ->with('cliente')
                ->where([
                    ['id_cliente', $request->idCliente]
                ])
                ->whereIn('tipo_envio', [1, 6])
                ->groupBy('id_bulk_sms')
                ->orderBy('fecha_envio', 'desc')
                ->get()
                ->toArray();
        }else{
            $campana = SMSKeyword::Select()
                ->with('clientes')
                ->where([
                    ['id_cliente', $request->idCliente],
                    ['estado_servicio', 1],
                    ['id_estado', 1]
                ])
                ->get()
                ->toArray();
        }

        return $campana;
    }

    public function miniDashboardSMS(Request $request){
        if(isset($request->valueID['idCampana'])){
            $dataCampaign = $this->getCampaign($request->valueID['idCampana']);
        }else{
            $dataCampaign = $this->getBuilkID($request->valueID['idBulkSMS']);
        }

        $dataCliente = $this->getCliente($dataCampaign[0]['id_cliente']);

        return view('elements/sms/tabs/campaign/chart/mini_dashboard')->with(array(
            'dataCampaign'      => $dataCampaign,
            'dataCliente'       => $dataCliente,
            'scriptCampaign'    => isset($request->valueID['idCampana']) ? true : false
        ));
    }

    public function getDataMiniDashboardSMS(Request $request){
        $whereChart = array();

        if($request->campaignID){
            $whereCustom = ['id_campana', $request->campaignID];
            array_push($whereChart, $whereCustom);
        }else{
            $whereCustom = ['id_bulk_sms', $request->bulkID];
            array_push($whereChart, $whereCustom);
        }

        $smsDetalle = SMSDetalle::with(['campana', 'cliente'])
            ->where($whereChart)
            ->orderBy('estado_sms', 'asc')
            ->get()
            ->toArray();

        $smsDetalleAsync = SMSDetalle::Select(DB::raw('COUNT(CASE WHEN async_status = 1 THEN 1 ELSE NULL END) as sincronizado, COUNT(CASE WHEN async_status = 0 THEN 0 ELSE NULL END) as no_sincronizado, id_campana, id_cliente'))->with(['campana', 'cliente'])
            ->where($whereChart)
            ->orderBy('estado_sms', 'asc')
            ->groupBy('estado_sms')
            ->get()
            ->toArray();

        $dataLabel = collect($smsDetalle)->map(function ($item, $key) {
            return getStatus($item['estado_sms']);
        })->unique()->toArray();

        $dataLabelTable = collect($smsDetalle)->map(function ($item, $key) {
            return $item['estado_sms'];
        })->unique()->toArray();

        $backgrounLabel = collect($smsDetalle)->map(function ($item, $key) {
            return getColorStatus($item['estado_sms']);
        })->unique()->toArray();

        $valuesArray = collect($smsDetalle)->mapToGroups(function ($item) {
            return [$item['estado_sms'] => $item['estado_sms']];
        })->toArray();

        $out = array();
        foreach ($valuesArray as $key => $value){
            foreach ($value as $keyValue => $valueValue){
                $index = $valueValue;
                if (array_key_exists($index, $out)){
                    $out[$index]++;
                } else {
                    $out[$index] = 1;
                }
            }
        }
        $dataValues = collect($out)->values();

        $chart = Charts::create('pie', 'chartjs')
            ->title('')
            ->colors($backgrounLabel)
            ->labels($dataLabel)
            ->values($dataValues)
            ->responsive(false)
            ->dimensions(0, 280)
            ->legend(false)
            ->credits(false);

        $getCliente = $this->getCliente($smsDetalle[0]['id_cliente']);
        $asyncScore = $getCliente[0]['score_async'] === '1' ? true : false;

        $dataTable = $this->chartBarTable($dataLabelTable, $dataValues, $asyncScore, $smsDetalleAsync);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function chartSend(Request $request){
        $whereChart = array();

        if($request->selectClientes){
            $whereCustom = ['id_cliente', $request->selectClientes];
            array_push($whereChart, $whereCustom);
        }else{
            $whereCustom = ['id_cliente', Auth::user()->id_cliente];
            array_push($whereChart, $whereCustom);
        }

        $dateRange = explode(' / ', $request->fechaReporte);

        if($request->tipoEnvio == '0'){
            $whereCustom = ['id_campana', $request->selectListEnvio];
            array_push($whereChart, $whereCustom);
        }elseif ($request->tipoEnvio == '1') {
            if($request->selectListEnvio){
                $whereCustom = ['id_bulk_sms', $request->selectListEnvio];
                array_push($whereChart, $whereCustom);
            }
        }else{
            if($request->selectListEnvio){
                $whereCustom = ['id_keyword', $request->selectListEnvio];
                array_push($whereChart, $whereCustom);

                if(empty($request->filtroEnvio)){
                    $whereCustom2 = ['tipo_envio', 4];
                    array_push($whereChart, $whereCustom2);
                }
            }
        }

        if($request->filtroEnvio){
            $whereCustom = ['tipo_envio', $request->filtroEnvio];
            array_push($whereChart, $whereCustom);
        }
        
        $smsDetalle = SMSDetalle::with(['campana', 'cliente'])
            ->where($whereChart);

        if(empty($request->filtroEnvio)){
            $smsDetalle->whereIn('tipo_envio', [1, 2, 6]);
        }

        if($dateRange[0] == $dateRange[1]){
            $smsDetalle->whereDate('fecha_envio', '=', $dateRange[0]);
        }else{
            $smsDetalle->whereBetween('fecha_envio', array($dateRange[0]." 00:00:00", $dateRange[1]." 23:59:59"));
        }

        if(!empty($request->estadoSMS)){
            $estadoSMS = collect($request->estadoSMS)->flatten();
            $smsDetalle->whereIn('estado_sms', $estadoSMS);
        }

        $dataChart = $smsDetalle->orderBy('fecha_envio', 'asc')->get()->toArray();

        $dataLabel = collect($dataChart)->map(function ($item, $key) {
            return getStatus($item['estado_sms']);
        })->unique()->toArray();

        $dataLabelTable = collect($dataChart)->map(function ($item, $key) {
            return $item['estado_sms'];
        })->unique()->toArray();

        $backgrounLabel = collect($dataChart)->map(function ($item, $key) {
            return getColorStatus($item['estado_sms']);
        })->unique()->toArray();

        $valuesArray = collect($dataChart)->mapToGroups(function ($item) {
            return [$item['estado_sms'] => $item['estado_sms']];
        })->toArray();

        $out = array();
        foreach ($valuesArray as $key => $value){
            foreach ($value as $keyValue => $valueValue){
                $index = $valueValue;
                if (array_key_exists($index, $out)){
                    $out[$index]++;
                } else {
                    $out[$index] = 1;
                }
            }
        }
        $dataValues = collect($out)->values();

        $chart = Charts::create('pie', 'chartjs')
            ->title('')
            ->colors($backgrounLabel)
            ->labels($dataLabel)
            ->values($dataValues)
            ->responsive(false)
            ->dimensions(0, 280)
            ->legend(false)
            ->credits(false);

        $dataTable = $this->chartBarTable($dataLabelTable, $dataValues);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function chartReceived(Request $request)
    {
        $whereChart = array();

        $dateRange = explode(' / ', $request->fechaReporte);

        if($request->selectClientes){
            $idCliente = $request->selectClientes;
        }else{
            $idCliente = Auth::user()->id_cliente;
        }

        if($request->tipoEnvio == '0' || $request->tipoEnvio == '1' || $request->tipoEnvio == '2'){
            if($request->selectListEnvio){
                $whereCustom = ['remitente_sms', $request->selectListEnvio];
                array_push($whereChart, $whereCustom);
            }
        }

        if($request->filtroRecibido){
            $whereCustom = ['envio_sms', $request->filtroRecibido];
            array_push($whereChart, $whereCustom);
        }else{
            $whereCustom = ['envio_sms', 1];
            array_push($whereChart, $whereCustom);
        }

        $smsChat = SMSChat::where($whereChart);

        if($request->tipoEnvio == '1'){
            $smsRemitente = SMSDetalle::Select('id_bulk_sms')->where('id_cliente', $idCliente)->groupBy('id_bulk_sms')->get()->toArray();
            $colletion = collect($smsRemitente)->map(function ($item, $key) {
               return $item['id_bulk_sms'];
            })->values()->toArray();

            $smsChat->whereIn('remitente_sms', $colletion);
        }

        if($request->tipoEnvio == '2'){
            if($request->selectListEnvio){
                $whereCustom = ['remitente_sms', $request->selectListEnvio];
                array_push($whereChart, $whereCustom);
            }
        }

        if($dateRange[0] == $dateRange[1]){
            $smsChat->whereDate('fecha_sms', '=', $dateRange[0]);
        }else{
            $smsChat->whereBetween('fecha_sms', array($dateRange[0]." 00:00:00", $dateRange[1]." 23:59:59"));
        }

        $dataChart = $smsChat->orderBy('fecha_sms', 'asc')->get()->toArray();

        $dataLabel = collect($dataChart)->map(function ($item, $key) {
            return Carbon::parse($item['fecha_sms'])->format('Y-m-d');
        })->unique()->values()->toArray();

        $grouped = collect($dataChart)->groupBy(function ($item, $key) {
            return Carbon::parse($item['fecha_sms'])->format('Y-m-d');
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        })->values();

        $chart = Charts::create('bar', 'chartjs')
            ->title('')
            ->elementLabel('Mensajes Recibidos')
            ->colors(['#1abc9c'])
            ->labels($dataLabel)
            ->values($groupCount)
            ->dimensions(0, 280)
            ->legend(false)
            ->credits(false);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script()
        ]);
    }

    public function getNotificationSMS(){
        $notificationSMS = SMSDetalle::with(['cliente', 'campana'])
            ->where([
                ['id_cliente', '=', Auth::user()->id_cliente],
                ['leido_sms', '=', 1],
                ['visto_sms', '=', 1]
            ])->orderBy('fecha_respuesta_sms', 'desc')
            ->get()->toArray();

        $collectNotification = collect($notificationSMS)->unique(function ($item) {
            return $item['id_bulk_sms'].$item['campana']['nombre_campana'];
        })->toArray();

        $notificationFix = array_values($collectNotification);

        return $notificationFix;
    }

    public function setViewSMSDetalle(Request $request){
        if($request->tipoCampana == 0){
            $updateSMSDetalle = SMSDetalle::where([
                ['id_campana', $request->idBulk]
            ]);
        }else{
            $updateSMSDetalle = SMSDetalle::where([
                ['id_bulk_sms', $request->idBulk]
            ]);
        }

        $updateSMSDetalle->update([
            'visto_sms' => 0
        ]);

        return 'Bulk SMS Visteado';
    }

    public function getUUID($ver, $node){
        $uuid = Uuid::generate($ver,$node)->string;
        return $uuid;
    }
}
