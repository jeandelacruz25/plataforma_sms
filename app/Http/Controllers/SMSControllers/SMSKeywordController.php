<?php

namespace App\Http\Controllers\SMSControllers;

use App\Http\Controllers\SecuritecController;
use App\Http\Requests\formKeywordConfirmationRequest;
use App\Http\Requests\formKeywordRequest;
use App\Http\Requests\formKeywordStatusRequest;
use App\Http\Requests\formSendKeywordRequest;
use App\Models\Clientes;
use App\Models\Estados;
use App\Models\SMSModels\SMSChat;
use App\Models\SMSModels\SMSDetalle;
use App\Models\SMSModels\SMSKeyword;
use Carbon\Carbon;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Maatwebsite\Excel\Facades\Excel;
use Webpatser\Uuid\Uuid;

class SMSKeywordController extends SecuritecController
{
    public function connectInfoBip(){
        $connect = new BasicAuthConfiguration(env('INFOBIP_KEYWORD_USERNAME','Securitec001'), env('INFOBIP_KEYWORD_PASSWORD', '/.S3cuu3rtC3%'));
        return $connect;
    }

    public function paginationSMSKeyword(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->searchKeyword){
            $whereCustom = ['nombre_keyword', 'LIKE', '%'.$request->searchKeyword.'%'];
            array_push($whereArray, $whereCustom);
        }

        $keywords = SMSKeyword::Select()
            ->with(['status', 'clientes'])
            ->where($whereArray)
            ->orderby('id_estado', 'asc')
            ->orderby('created_at', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $keywords->total(),
                'per_page' => $keywords->perPage(),
                'current_page' => $keywords->currentPage(),
                'last_page' => $keywords->lastPage(),
                'from' => $keywords->firstItem(),
                'to' => $keywords->lastItem()
            ],
            'data' => $keywords
        ];

        return response()->json($response);
    }

    public function paginationSMSKeywordSend(Request $request){

        $whereArray = [];
        if(!Auth::user()->authorizeRoles(['Administrador'])){
            $whereCustom = ['id_cliente', '=', Auth::user()->id_cliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectCliente){
            $whereCustom = ['id_cliente', '=', $request->selectCliente];
            array_push($whereArray, $whereCustom);
        }

        if($request->selectKeyword){
            $whereCustom = ['id_keyword', '=', $request->selectKeyword];
            array_push($whereArray, $whereCustom);
        }

        $keywordsSend = SMSDetalle::Select()
            ->with(['keyword', 'cliente'])
            ->whereIn('tipo_envio', [4])
            ->where($whereArray)
            ->where(function($query) use ($request) {
                if($request->searchDateRange){
                    $dateFilter = explode(' / ', $request->searchDateRange);
                    $query->whereBetween('fecha_envio', array($dateFilter[0]." 00:00:00", $dateFilter[1]." 23:59:59"));
                }else{
                    $query->whereMonth('fecha_envio', Carbon::now()->format('m'));
                }
            })
            ->groupBy('id_bulk_sms')
            ->orderBy('fecha_envio', 'desc')
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $keywordsSend->total(),
                'per_page' => $keywordsSend->perPage(),
                'current_page' => $keywordsSend->currentPage(),
                'last_page' => $keywordsSend->lastPage(),
                'from' => $keywordsSend->firstItem(),
                'to' => $keywordsSend->lastItem()
            ],
            'data' => $keywordsSend
        ];

        return response()->json($response);
    }

    public function formSMSKeyword(Request $request){
        if($request->valueID == null){
            return view('elements/sms/tabs/keyword/form/form_sms_keyword')->with(array(
                'dataKeyword'           => '',
                'updateForm'            => false,
                'options'               => $this->selectionOption()
            ));
        }else{
            $getKeyword = $this->getKeyword($request->valueID);
            return view('elements/sms/tabs/keyword/form/form_sms_keyword')->with(array(
                'dataKeyword'           => $getKeyword,
                'updateForm'            => true,
                'options'               => $this->selectionOption()
            ));
        }
    }

    public function formKeywordUsers(Request $request){
        $getSMSDetalle = $this->getSMSDetalleBulk($request->valueID);
        return view('elements/sms/tabs/keyword/form/form_sms_keyword_base')->with(array(
            'dataBaseKeyword'   => $getSMSDetalle
        ));
    }

    public function formKeywordStatus(Request $request){
        $dataKeyword  = $this->getKeyword($request->valueID);
        $statusOptions = $this->getNotKeywordStatus($dataKeyword[0]['status']['id']);
        return view('elements/sms/tabs/keyword/form/form_sms_keyword_status')->with(array(
            'dataKeyword'       => $dataKeyword,
            'options'           => $statusOptions
        ));
    }

    public function formSendKeyword(){
        $getKeyword = $this->keywordsAll();
        return view('elements/sms/tabs/keyword/form/form_sms_keyword_send')->with(array(
            'dataKeyword'  => $getKeyword
        ));
    }

    public function formKeywordConfirmation(Request $request){
        $dataKeyword  = $this->getKeyword($request->valueID);
        return view('elements/sms/tabs/keyword/form/form_sms_keyword_confirmation')->with(array(
            'dataKeyword'       => $dataKeyword
        ));
    }

    public function saveFormSMSKeyword(formKeywordRequest $request){
        if ($request->isMethod('post')) {

            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            if($request->keywordID){
                $idCliente = $request->clienteID;
                $nameKeyword = $request->nameKeyword;
            }else{
                $idCliente = $request->clienteKeyword ? $request->clienteKeyword : Auth::user()->id_cliente;
                $clientePalabraClave = Clientes::Select()->where('id', $idCliente)->get()->toArray();
                $nameKeyword = $clientePalabraClave[0]['palabra_clave'].$request->nameKeyword;

                $request->merge(array('nameKeywordFix' => $nameKeyword));

                $rules = [];
                $rules['nameKeywordFix'] =  'required|alpha_num|unique:sms_keyword,nombre_keyword,'.$request->keywordID.',id,id_cliente,'.$idCliente.'';
                $messages = [];
                $messages['nameKeywordFix.required'] = 'Debes ingresar un nombre de Keyword';
                $messages['nameKeywordFix.alpha_num'] = 'El keyword solo debe contener letras con o sin números';
                $messages['nameKeywordFix.unique'] = 'El keyword ya se encuentra creado';

                $validator = Validator::make($request->all(), $rules, $messages);

                if($validator->fails()){
                    return response()->json($validator->errors(), 422);
                }
            }

            $keywordQuery = SMSKeyword::updateOrCreate([
                'id'                => $request->keywordID
            ], [
                'nombre_keyword'    => Str::upper($nameKeyword),
                'estado_servicio'   => $request->statusService ? $request->statusService : 0,
                'mensaje_error'     => $request->mensajeError ? $request->mensajeError : null,
                'id_cliente'        => $idCliente,
                'id_estado'         => $request->statusID ? $request->statusID : 1,
                'estado_error'      => $request->statusError ? 1 : 0,
                'user_created'      => $request->userCreated ? $request->userCreated : Auth::user()->id,
                'user_updated'      => $request->userUpdated ? $request->userUpdated : Auth::user()->id
            ]);
            if($keywordQuery){
                if(!$request->keywordID){
                    $queryCliente = Clientes::Select()->where('id', $idCliente)->get()->toArray();
                    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
                    $beautymail->send('emails.test', [
                        'nameKeyword'    => Str::upper($nameKeyword),
                        'nameCliente'    => $queryCliente[0]['razon_social']
                    ], function($message) use($queryCliente)
                    {
                        $message
                            ->from('keyword@securitec.pe', 'Keyword Securitec')
                            ->to('support@securitec.pe', 'Support Securitec')
                            ->subject('[KEYWORD] Nuevo Keyword - '.$queryCliente[0]['razon_social']);
                    });
                }
                $action = $request->keywordID ? 'update' : 'create';
                return ['message' => 'Success', 'action' => $action, 'datatable' => 'listKeyword'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormKeywordStatus(formKeywordStatusRequest $request){
        if ($request->isMethod('post')) {
            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = SMSKeyword::where('id', $request->keywordID)
                ->update(['id_estado' => $request->statusKeyword]);
            if($usersQuery){
                return ['message' => 'Success', 'datatable' => 'listKeyword'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormKeywordConfirmation(formKeywordConfirmationRequest $request){
        if ($request->isMethod('post')) {
            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            $usersQuery = SMSKeyword::where('id', $request->keywordID)
                ->update(['estado_servicio' => $request->confirmationKeyword]);
            if($usersQuery){
                return ['message' => 'Success', 'datatable' => 'listKeyword'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function sendSMSKeyword(formSendKeywordRequest $request){
        if ($request->isMethod('post')) {
            $userInformation = $this->getUserInformation();
            if($userInformation[0]['clientes']['id_estado'] == 2) return ['message' => 'Error'];
            if($userInformation[0]['id_status'] == 2) return ['message' => 'Error'];

            return [
                'message' => 'Success',
                'datatable' => 'listSendKeyword'
            ];
        }
        return ['message' => 'Error'];
    }

    public function executeUploadKeyword(Request $request){
        $idKeyword = $request->selectKeyword;
        $dataKeyword = SMSKeyword::Select()->with('clientes')->where('id', $idKeyword)->get()->toArray();

        $cantidad = SMSDetalle::Select()->where([['tipo_envio', 4], ['id_cliente', $dataKeyword[0]['id_cliente']]])->get()->count();
        $clienteInformation = $this->getCliente($dataKeyword[0]['id_cliente']);
        $bulkID = Str::lower($clienteInformation[0]['palabra_clave']).'_keyword_'.$dataKeyword[0]['id_cliente'].'_'.$dataKeyword[0]['id'].'_'.($cantidad+1);

        $dateEnvio = $request->dateEnvio ? Carbon::parse($request->dateEnvio) : Carbon::now();

        try {
            app()->instance('insertKeywordUpload', collect());
            Excel::filter('chunk')->load($request->file('baseClientes')->getRealPath())->chunk(1000, function ($rows) {
                app()->instance('insertKeywordUpload', app('insertKeywordUpload')->merge($rows->toArray()));
            }, $shouldQueue = false);

            $dataSheet = app('insertKeywordUpload')->toArray();

            $arrayBlackList = $this->getBlackList();
            $collectArrayList = collect($arrayBlackList)->values()->toArray();

            $dbInsert = [];
            foreach (collect($dataSheet)->chunk(1000)->toArray() as $chunk) {
                foreach($chunk as $key => $value){
                    if($value['telefono'] || $value['mensaje'] || $value['filtro'] || $value['respuesta']){
                        $arrayInsert = [
                            'telefono_usuario'      => $value['telefono'],
                            'mensaje_usuario'       => $value['mensaje'],
                            'filtro_mensaje'        => $value['filtro'],
                            'mensaje_respuesta'     => $value['respuesta'],
                            'tipo_envio'            => 4,
                            'flash_sms'             => 0,
                            'id_sms'                => '0',
                            'id_bulk_sms'           => $bulkID,
                            'estado_sms'            => array_search($value['telefono'], $collectArrayList) !== false ? '6' : '0',
                            'estado_descripcion'    => array_search($value['telefono'], $collectArrayList) !== false ? 'BLACK_LIST' : 'NO_ENVIADO',
                            'leido_sms'             => 0,
                            'sms_respondido'        => 0,
                            'fecha_envio'           => $dateEnvio,
                            'fecha_recepcion'       => null,
                            'fecha_respuesta_sms'   => null,
                            'visto_sms'             => 0,
                            'numero_mccmnc'         => 0,
                            'id_campana'            => 0,
                            'id_keyword'            => $idKeyword,
                            'id_cliente'            => $dataKeyword[0]['id_cliente']
                        ];
                        array_push($dbInsert, $arrayInsert);
                    }
                }
            }

            $insertDetalle = SMSDetalle::insert($dbInsert);

            if($insertDetalle){
                $client = new SendMultipleTextualSmsAdvanced($this->connectInfoBip());
                $flashSMS = $request->flashSMS ? true : false;

                $smsDetalle = SMSDetalle::Select()
                    ->where([
                        ['id_bulk_sms', '=', $bulkID],
                        ['estado_sms', '!=', 6]
                    ])
                    ->get()
                    ->toArray();

                $messages = array();
                foreach ($smsDetalle as $key => $value) {
                    $destination = new Destination();
                    $message = new Message();
                    $destination->setTo($value['telefono_usuario']);
                    $destination->setMessageId($this->getUUID(1, $value['telefono_usuario'].'_'.$value['id_cliente'].'_'.$value['id_bulk_sms']));
                    $message->setFrom(76333);
                    $message->setDestinations([$destination]);
                    $message->setText($value['mensaje_usuario']);
                    $message->setSendAt($dateEnvio);
                    $message->setFlash($flashSMS);
                    $messages[] = $message;
                }

                $requestBody = new SMSAdvancedTextualRequest();
                $requestBody->setMessages($messages);
                $requestBody->setBulkId($bulkID);

                try {
                    $response = $client->execute($requestBody);
                    $sentMessageInfo = $response->getMessages();
                    foreach ($sentMessageInfo as $key => $value) {
                        $smsMessageSend = SMSDetalle::Select()
                            ->with('cliente')
                            ->where([
                                ['telefono_usuario', '=', $value->getTo()],
                                ['id_bulk_sms', '=', $bulkID],
                                ['estado_sms', '!=', 6]
                            ])
                            ->get()
                            ->toArray();

                        SMSDetalle::where([
                            ['telefono_usuario', $value->getTo()],
                            ['id_bulk_sms', $bulkID],
                            ['id_keyword', $idKeyword],
                            ['id_cliente', $dataKeyword[0]['id_cliente']]
                        ])->update([
                            'flash_sms'             => $flashSMS ? 1 : 0,
                            'id_sms'                => $value->getMessageId(),
                            'estado_sms'            => $value->getStatus()->getGroupId(),
                            'estado_descripcion'    => $value->getStatus()->getName()
                        ]);

                        SMSChat::updateOrCreate([
                            'id_sms'            => $value->getMessageId(),
                            'telefono_sms'      => $value->getTo(),
                            'remitente_sms'     => $bulkID
                        ], [
                            'id_sms'            => $value->getMessageId(),
                            'mensaje_sms'       => $smsMessageSend[0]['mensaje_usuario'],
                            'telefono_sms'      => $value->getTo(),
                            'envio_sms'         => 0,
                            'remitente_sms'     => $bulkID,
                            'fecha_sms'         => Carbon::now()
                        ]);
                    }

                    $clientNode = new \ElephantIO\Client(new Version2X(env('NODEJS_SERVER','https://nodejs.securitec.pe')));
                    $clientNode->initialize();
                    $clientNode->emit('notificationCampaign', [
                        'nameCampaign' => $dataKeyword[0]['nombre_keyword'],
                        'nameRoom' => Str::lower($dataKeyword[0]['clientes']['palabra_clave']),
                        'tipoSubida' => 1,
                        'licenseExceeded' => ''
                    ]);
                    $clientNode->close();

                    return ['message' => 'Success'];
                } catch (\Exception $exception) {
                    return response()->json($exception->getMessage(), 422);
                }
            }
        }catch (\Exception $e){
            return ['message' => 'Error'];
        }
    }

    public function paginationFormKeywordUsers(Request $request){
        $baseKeyword = SMSDetalle::Select('id_campana', 'id_keyword', 'estado_sms', 'telefono_usuario', 'mensaje_usuario',
            'estado_descripcion', 'fecha_recepcion', 'tipo_envio', 'id_bulk_sms', 'numero_mccmnc')
            ->where([
                ['id_bulk_sms', '=', $request->valueID],
                ['telefono_usuario', 'LIKE', '%'.$request->searchTelephone.'%']
            ])
            ->whereIn('tipo_envio', [4])
            ->paginate(10);

        $response = [
            'pagination' => [
                'total' => $baseKeyword->total(),
                'per_page' => $baseKeyword->perPage(),
                'current_page' => $baseKeyword->currentPage(),
                'last_page' => $baseKeyword->lastPage(),
                'from' => $baseKeyword->firstItem(),
                'to' => $baseKeyword->lastItem()
            ],
            'data' => $baseKeyword
        ];
        return response()->json($response);
    }

    public function downloadReportSMSKeyword(Request $request){
        $detalleSMS = SMSDetalle::Select()->where('id_bulk_sms', $request->idBulkSMS)->get()->toArray();
        if($detalleSMS){
            $nameExport = cleanString(str_replace(' ', '_', $detalleSMS[0]['id_bulk_sms'])).'_'.time();

            Excel::create($nameExport, function($excel) use($detalleSMS) {
                $excel->sheet($detalleSMS[0]['id_bulk_sms'], function($sheet) use($detalleSMS) {
                    $sheet->fromArray($this->builderExportReportSMSKeyword($this->getBuilkID($detalleSMS[0]['id_bulk_sms'])));
                });
            })->store('xlsx', 'exports');

            return [
                'success'   => true,
                'path'      => asset('exports/'.$nameExport.'.xlsx')
            ];
        }
        return response()->json([], 422);
    }

    protected function builderExportReportSMSKeyword($keyword_list_query)
    {
        $posicion = 0;
        $idList = 0;
        foreach ($keyword_list_query as $query) {
            $numMCCMNC = $query['numero_mccmnc'];

            if($numMCCMNC){
                $getCountryCode = substr($numMCCMNC, 0, 3);
                $getOperadorCode = strlen($numMCCMNC) == 5 ? substr($numMCCMNC, 3, 6) : substr($numMCCMNC, 3, 7);
                $countryMCCNC = $getCountryCode;
                $operadorMCCNC = $getOperadorCode;
            }else{
                $countryMCCNC = 0;
                $operadorMCCNC = 0;
            }

            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['número_celular']           = $query['telefono_usuario'];
            $builderview[$posicion]['mensaje_enviado']          = $query['mensaje_usuario'];
            $builderview[$posicion]['keyword']                  = $query['keyword']['nombre_keyword'];
            $builderview[$posicion]['estado_envio']             = strip_tags(getStatusSMS($query['estado_sms']));
            $builderview[$posicion]['pais_envio']               = getCountryMCCMNC($countryMCCNC);
            $builderview[$posicion]['operador_envio']           = getOperatorMCCMNC($operadorMCCNC);
            $builderview[$posicion]['fecha_envio']              = $query['fecha_envio'] ? Carbon::parse($query['fecha_envio'])->format('d/m/Y H:i:s a') : '-';
            $builderview[$posicion]['fecha_recepcion']          = $query['fecha_recepcion'] ? Carbon::parse($query['fecha_recepcion'])->format('d/m/Y H:i:s a') : '-';
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    public function getKeyword($idKeyword){
        $keyword = SMSKeyword::Select()
            ->with(['status', 'clientes'])
            ->where('id', $idKeyword)
            ->get()
            ->toArray();

        return $keyword;
    }

    public function getSMSDetalle($idKeyword){
        $baseCampaign = SMSDetalle::Select()
            ->where('id_keyword', $idKeyword)
            ->get()
            ->toArray();

        return $baseCampaign;
    }

    public function getBuilkID($idBulkID){
        $smsBulkID = SMSDetalle::Select()
            ->with('keyword')
            ->where([
                ['tipo_envio', 4],
                ['id_bulk_sms', $idBulkID]
            ])
            ->get()
            ->toArray();

        return $smsBulkID;
    }

    public function getSMSDetalleBulk($idBulkKeyword){
        $baseCampaign = SMSDetalle::Select()
            ->where([
                ['id_bulk_sms', '=', $idBulkKeyword],
                ['tipo_envio', '=', 4]
            ])
            ->get()
            ->toArray();

        return $baseCampaign;
    }

    public function getNotKeywordStatus($idStatus){
        $statusKeyword = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusKeyword;
    }

    public function getCliente($idCliente){
        $cliente = Clientes::Select()
            ->with('status')
            ->where('id', $idCliente)
            ->get()
            ->toArray();

        return $cliente;
    }

    public function getUUID($ver, $node){
        $uuid = Uuid::generate($ver,$node)->string;
        return $uuid;
    }
}
