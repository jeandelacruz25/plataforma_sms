<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserRoleController extends SecuritecController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if(searchMenuCliente(auth()->user()->id_cliente, 'usersRole') != 0 && searchMenuUsuario(auth()->id(), 'usersRole') != 0){
            return view('elements/users_roles/index')->with(array(
                'titleUsersModule'       => 'Administrar Usuarios',
                'iconUsersModule'        => 'fa fa-cogs',
                'nameRouteUsers'         => 'administrar usuarios',
                'titleRolesModule'       => 'Administrar Perfiles',
                'iconRolesModule'        => 'fa fa-cogs',
                'nameRouteRoles'         => 'administrar perfiles',
                'selectOptions'          => $this->selectionOption()
            ));
        }else{
            return view('elements/menu_errors')->with(array(
                'titleModule'           => 'Administrar Usuarios | Administrar Perfiles',
                'menuNombre'            => 'Usuarios | Perfiles'
            ));
        }
    }
}
