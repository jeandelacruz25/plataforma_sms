<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\Clientes;
use App\Models\Estados;
use App\Models\MenuClientes;
use App\Models\MenuUsers;
use App\Models\Role;
use App\Models\SMSModels\SMSKeyword;
use App\Models\TipoIdentidad;
use App\Models\User;
use App\Models\ValidatePhoneModels\TelefonosCampanas;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;

class SecuritecController extends Controller
{
    public function getUserInformation(){
        $user = User::Select()
            ->with('clientes')
            ->with('menu')
            ->where('id', Auth::id())
            ->get()
            ->toArray();

        return $user;
    }

    public function getClienteMenu(){
        $menu = MenuClientes::Select()
            ->where('cliente_id', Auth::user()->id_cliente)
            ->get()
            ->toArray();

        return $menu;
    }

    public function getUserMenu(){
        $menu = MenuUsers::Select()
            ->where('user_id', Auth::id())
            ->get()
            ->toArray();

        return $menu;
    }

    public function getUtilitiesUser () {
        $total = User::Select()
            ->get()
            ->count();

        $total_month = User::Select()
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->get()
            ->count();

        $last = User::latest()->first();

        $userUtilities['total'] = $total;
        $userUtilities['total_month'] = $total_month;
        $userUtilities['last'] = $last;

        return $userUtilities;
    }

    public function getBlackList() {
        $blackList = BlackList::Select()
            ->where('id_cliente', Auth::user()->id_cliente)
            ->get()
            ->toArray();

        $arrayBlackList = array_column($blackList, 'telefono');

        return $arrayBlackList;
    }

    protected function FormatDatatable($collection)
    {
        return DataTables::of($collection)
                    ->escapeColumns([])
                    ->make(true);
    }

    public function cookieAuthRemove(){
        Cookie::queue(Cookie::forget('cookie_username'));
        return redirect('/');
    }

    protected function makeDirectory($ubicacion){
        return File::makeDirectory($ubicacion, 0777, true);
    }

    protected function resetCookie($nameCookie, $setValueCookie){
        Cookie::queue(Cookie::forget($nameCookie));
        return Cookie::queue($nameCookie, $setValueCookie, 2147483647);
    }

    protected function arrayColors(){
        $arrayColors = [
            '#1abc9c','#2ecc71','#3498db','#9b59b6','#34495e','#f1c40f','#e67e22','#e74c3c','#ecf0f1','#95a5a6'
        ];

        return $arrayColors;
    }

    protected function chartBarTable($labelChart, $dataChart, $asyncScore = false, $dataAsync = []){
        $tableData = "<table class='table table-bordered fs-14'>";
        if($asyncScore){
            $tableData .= "<tr class='bg-facebook'><td class='fw-600'>Estado</td><td class='fw-600'>Cantidad</td><td class='fw-600'>Sinc.</td><td class='fw-600'>No Sinc.</td><td class='fw-600'>Acción</td></tr>";
        }else{
            $tableData .= "<tr class='bg-facebook'><td class='fw-600'>Estado</td><td class='fw-600'>Cantidad</td></tr>";
        }

        $key = 0;
        foreach ($labelChart as $keyLabel => $valueLabel){
            $menuAccion = "<a class='dropdown-item' href='javascript:void(0)' onclick='vmMiniDashboardSMS.formGenerarGestiones(".$valueLabel.")'> Generar Gestiones </a>";
            $tableData .= "<tr>";
            $tableData .= "<td><i class='material-icons fs-13' style='color: ".getColorStatus($valueLabel)." !important;'>fiber_manual_record</i> <span class='d-sm-inline'>".getStatus($valueLabel)."</span></td>";
            $tableData .= "<td>".$dataChart[$key]."</td>";
            if($asyncScore){
                $tableData .= "<td>".$dataAsync[$key]['sincronizado']."</td>";
                $tableData .= "<td>".$dataAsync[$key]['no_sincronizado']."</td>";
                if($dataAsync[$key]['sincronizado'] - $dataAsync[$key]['no_sincronizado'] != 0 && $valueLabel == 3 || $dataAsync[$key]['sincronizado'] == 0 && $dataAsync[$key]['no_sincronizado'] == 0 && $valueLabel == 3){
                    $tableData .= "<td class='text-center'>
                                        <div class='btn-group dropdown m-r-10'>
                                            <button aria-haspopup='true' aria-expanded='false' data-toggle='dropdown' class='btn btn-orange2 btn-sm dropdown-toggle' type='button'>
                                                <i class='fa fa-bars'></i>
                                            </button>
                                            <div role='menu' class='dropdown-menu dropdown-menu-right fs-13'>".$menuAccion."</div>
                                        </div>
                                   </td>";
                }else{
                    $tableData .= "<td class='text-center'> - </td>";
                }
            }
            $tableData .= "</tr>";
            $key++;
        }

        $tableData .= "</table>";
        return $tableData;
    }

    protected function chartBarTableValidacionTelephone($labelChart, $dataChart, $asyncScore = false, $dataAsync = []){
        $tableData = "<table class='table table-bordered fs-14'>";
        if($asyncScore){
            $tableData .= "<tr class='bg-facebook'><td class='fw-600'>Estado</td><td class='fw-600'>Cantidad</td><td class='fw-600'>Sinc.</td><td class='fw-600'>No Sinc.</td><td class='fw-600'>Acción</td></tr>";
        }else{
            $tableData .= "<tr class='bg-facebook'><td class='fw-600'>Estado</td><td class='fw-600'>Cantidad</td></tr>";
        }

        $key = 0;
        foreach ($labelChart as $keyLabel => $valueLabel){
            if($valueLabel == 9){
                $menuAccion = "<a class='dropdown-item' href='javascript:void(0)' onclick='vmMiniDashboardTelefono.formGenerarMarcadores()'> Generar Marcadores </a>";
            }else if($valueLabel == 7){
                $menuAccion = "<a class='dropdown-item' href='javascript:void(0)' onclick='vmMiniDashboardTelefono.formGenerarInactivos(".$valueLabel.")'> Inactivar Telefonos </a>";
                $menuAccion .= "<a class='dropdown-item' href='javascript:void(0)' onclick='vmMiniDashboardTelefono.formGenerarGestiones(".$valueLabel.")'> Generar Gestiones NC </a>";
            }else if($valueLabel == 8){
                $menuAccion = "<a class='dropdown-item' href='javascript:void(0)' onclick='vmMiniDashboardTelefono.formGenerarGestiones(".$valueLabel.")'> Generar Gestiones NC </a>";
            }else{
            	$menuAccion = "<a class='dropdown-item'> - </a>";
            }

            $tableData .= "<tr>";
            $tableData .= "<td><i class='material-icons fs-13' style='color: ".getColorValidacionStatus($valueLabel)." !important;'>fiber_manual_record</i> <span class='d-sm-inline'>".getStatusValidacion($valueLabel)."</span></td>";
            $tableData .= "<td>".$dataChart[$key]."</td>";
            if($asyncScore){
                $tableData .= "<td>".$dataAsync[$key]['sincronizado']."</td>";
                $tableData .= "<td>".$dataAsync[$key]['no_sincronizado']."</td>";
                if($dataAsync[$key]['sincronizado'] - $dataAsync[$key]['no_sincronizado'] != 0 || $dataAsync[$key]['sincronizado'] == 0 && $dataAsync[$key]['no_sincronizado'] == 0){
                    $tableData .= "<td class='text-center'>
                                        <div class='btn-group dropdown m-r-10'>
                                            <button aria-haspopup='true' aria-expanded='false' data-toggle='dropdown' class='btn btn-orange2 btn-sm dropdown-toggle' type='button'>
                                                <i class='fa fa-bars'></i>
                                            </button>
                                            <div role='menu' class='dropdown-menu dropdown-menu-right fs-13'>".$menuAccion."</div>
                                        </div>
                                   </td>";
                }else{
                    $tableData .= "<td class='text-center'> - </td>";
                }
            }
            $tableData .= "</tr>";
            $key++;
        }

        $tableData .= "</table>";
        return $tableData;
    }

    protected function createLogSysInformation($time_start, $time_ini){
        $time_fin = Carbon::now()->format('H:i:s');
        $time_end = Carbon::now()->micro;
        $bd_time = $time_ini - $time_end;

        $folderReports = '../reportes/logs/';

        $existFolder = File::exists($folderReports);
        if (!$existFolder) {
            $this->makeDirectory($folderReports);
        }

        //$larinfo = (new Larinfo())->getUptime();

        $time_message = 'El Tiempo de procesamiento fue: '.sprintf('%.4f',$bd_time)." Segundos";
        //$load = sys_getloadavg();
        $peak_memory_message = ' Memoria  usado por el proceso: ' . (memory_get_peak_usage(true) / 1024 / 1024) . " MB  -  Load Average: ";

        $date = date("YmdHis");

        $filename = $folderReports.'log_report_cliente'.$date.'.txt';
        $existsFile = File::exists($filename);
        if ($existsFile) {
            File::delete($filename);
        }

        $messageLog = 'Inicio: '.$time_start.' - Fin: '.$time_fin.' | '.$time_message.' | '.$peak_memory_message;
        $fp = fopen('log_report_cliente'.$date.'.txt', 'w');
        fwrite($fp, $messageLog);
        fclose($fp);
    }

    public static function latin_to_utf8($dat)
    {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[ $i ] = self::latin_to_utf8($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::latin_to_utf8($d);

            return $dat;
        } else {
            return $dat;
        }
    }

    /* Funciones Repetidas */

    protected function selectionOption(){
        $typeIdentity = TipoIdentidad::Select()
            ->get()
            ->toArray();

        if(!Auth::user()->authorizeRoles(['Administrador'])) {
            $roles = Role::Select()->where([
                ['id', '!=', '1']
            ])->get()->toArray();
        }else{
            $roles = Role::Select()
                ->get()
                ->toArray();
        }

        $status = Estados::Select()
            ->get()
            ->toArray();

        $clientes = Clientes::Select()
            ->get()
            ->toArray();

        $select['tipoIdentidad'] = $typeIdentity;
        $select['roles'] = $roles;
        $select['clientes'] = $clientes;
        $select['estado'] = $status;

        return $select;
    }

    public function keywordsAll(){
        $keyword = SMSKeyword::Select()
            ->with(['status', 'clientes'])
            ->where([
                ['estado_servicio', '=', 1],
                ['id_estado', '=', 1]
            ]);

        if(!Auth::user()->authorizeRoles(['Administrador'])) {
            $keyword->where([
                ['id_cliente', '=', Auth::user()->id_cliente]
            ]);
        }

        return $keyword->get()->toArray();
    }

    protected function getMenuCliente($idCliente) {
        $cliente_menus = MenuClientes::Select('vue_name')
            ->where('cliente_id', $idCliente)
            ->get()
            ->toArray();

        return $cliente_menus;
    }

    protected function getCliente($id){
        $cliente = Clientes::Select()
            ->with(['status', 'rules'])
            ->where('id', $id)
            ->get()
            ->toArray();

        return $cliente;
    }

    public function getCountCampaign($idCliente){
        $campaign = TelefonosCampanas::Select()
            ->where([
                ['id_cliente', '=', $idCliente]
            ])
            ->whereIn('estado_envio', [3])
            ->count();

        return $campaign;
    }
}