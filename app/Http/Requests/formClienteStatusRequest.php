<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formClienteStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'clienteID'          => 'required',
            'statusCliente'      => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'clienteID.required'          => 'El clienteID donde se actualizara los datos no existe',
            'statusCliente.required'      => 'Debes seleccionar un estado'
        ];

        return $rules;
    }
}
