<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formKeywordConfirmationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $rules['confirmationKeyword'] = 'required';

        return $rules;
    }

    public function messages()
    {

        $messages = [];

        $messages['confirmationKeyword.required'] = 'Necesitas escoger un estado para poder actualizar';

        return $messages;
    }
}
