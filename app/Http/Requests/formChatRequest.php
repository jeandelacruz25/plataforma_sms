<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'smsChat'   => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'smsChat.required'   => 'No puedes enviar un mensaje vacio'
        ];

        return $rules;
    }
}
