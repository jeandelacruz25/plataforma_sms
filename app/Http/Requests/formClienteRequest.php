<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if(empty($this->avatarOriginalCliente)){
            $rules = [
                'avatarCliente'             => 'required|image',
                'rucCliente'                => 'required|numeric',
                'nombreCliente'             => 'required',
                'emailContacto'             => 'required|email',
                'palabraClave'              => 'required|max:3',
                'telefonoCliente'           => 'required|numeric',
                'direccionCliente'          => 'required'
            ];
        }else{
            $rules = [
                'avatarOriginalCliente'     => 'required',
                'rucCliente'                => 'required|numeric',
                'nombreCliente'             => 'required',
                'emailContacto'             => 'required|email',
                'palabraClave'              => 'required|max:3',
                'telefonoCliente'           => 'required|numeric',
                'direccionCliente'          => 'required',
                'clienteID'                 => 'required'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        if(empty($this->avatarOriginalCliente)){
            $rules = [
                'avatarCliente.required'            => 'Debes ingresar una imagen',
                'avatarCliente.image'               => 'El archivo a subir debe ser una imagen',
                'rucCliente.required'               => 'Debes ingresar un numero de ruc',
                'rucCliente.numeric'                => 'El RUC solo debe contener números',
                'nombreCliente.required'            => 'Debes ingresar un nombre para el Cliente',
                'emailContacto.required'            => 'Debes ingresar un email de contacto',
                'emailContacto.email'               => 'El email de contacto no es valido',
                'palabraClave.required'             => 'Debes ingresar una palabra clave, para poder distinguir usos en la plataforma',
                'palabraClave.max'                  => 'La palabra clave debe tener maximo 3 caracteres',
                'telefonoCliente.required'          => 'Debes ingresar un número de telefono',
                'telefonoCliente.numeric'           => 'El telefono solo debe ser numerico',
                'direccionCliente.required'         => 'Debes ingresar una dirección del cliente',
            ];
        }else{
            $rules = [
                'avatarOriginalCliente.required'    => 'No hay ningun avatar anterior al actual',
                'rucCliente.required'               => 'Debes ingresar un numero de ruc',
                'rucCliente.numeric'                => 'El RUC solo debe contener números',
                'nombreCliente.required'            => 'Debes ingresar un nombre para el Cliente',
                'emailContacto.required'            => 'Debes ingresar un email de contacto',
                'emailContacto.email'               => 'El email de contacto no es valido',
                'palabraClave.required'             => 'Debes ingresar una palabra clave, para poder distinguir usos en la plataforma',
                'palabraClave.max'                  => 'La palabra clave debe tener maximo 3 caracteres',
                'telefonoCliente.required'          => 'Debes ingresar un número de telefono',
                'telefonoCliente.numeric'           => 'El telefono solo debe ser numerico',
                'direccionCliente.required'         => 'Debes ingresar una dirección del cliente',
                'clienteID.required'                => 'El clienteID donde se actualizara los datos no existe'
            ];
        }

        return $rules;
    }
}
