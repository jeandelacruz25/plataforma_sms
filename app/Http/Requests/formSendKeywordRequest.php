<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formSendKeywordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'baseClientes'  => 'required|file|mimes:csv,txt,xlsx,xls',
            'selectKeyword' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'baseClientes.required'     => 'Debes subir un archivo de excel, como tambien sea csv',
            'baseClientes.file'         => 'El input del formulario no es un tipo file',
            'baseClientes.mimes'        => 'El archivo a subir no es un excel o csv',
            'selectKeyword.required'    => 'Debes elegir un keyword'
        ];

        return $rules;
    }
}
