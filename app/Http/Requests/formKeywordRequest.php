<?php

namespace App\Http\Requests;

use App\Models\Clientes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class formKeywordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if($this->keywordID){
            $rules['nameKeyword'] = 'required|alpha_num|unique:sms_keyword,nombre_keyword,'.$this->keywordID;
        }

        if(Auth::user()->authorizeRoles(['Administrador']) && !$this->clienteID){
            $rules['clienteKeyword'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $rules = [];

        if($this->keywordID) {
            $rules['nameKeyword.required'] = 'Debes ingresar un nombre de Keyword';
            $rules['nameKeyword.alpha_num'] = 'El keyword solo debe contener letras con o sin números';
            $rules['nameKeyword.unique'] = 'El keyword ya se encuentra creado';
        }

        $rules['clienteKeyword.required'] = 'Debes escoger a que cliente se asignara a este nuevo keyword';

        return $rules;
    }
}
