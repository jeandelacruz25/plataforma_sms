<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class sendSMSIndividualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $rules['paisEnvio']     = 'required';
        $rules['mensajeEnviar'] = 'required|min:10|max:160';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $rules['clienteSMSIndividual'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $rules = [];

        $rules['paisEnvio.required'] = 'Debes escoger un país para concatenar su codigo';

        $rules['mensajeEnviar.required'] = 'Debes ingresar por lo menos un mensaje';
        $rules['mensajeEnviar.min'] = 'El mensaje debe tener minimo 10 caracteres';
        $rules['mensajeEnviar.max'] = 'El mensaje debe tener maximo 160 caracteres';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $rules['clienteSMSIndividual.required'] = 'Debes escoger a que cliente se asociara este envio';
        }

        return $rules;
    }
}
