<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formBlackListMasivoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'baseNumeros'  => 'required|file|mimes:csv,txt,xlsx,xls'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'baseNumeros.required'     => 'Debes subir un archivo de excel, como tambien sea csv',
            'baseNumeros.file'         => 'El input del formulario no es un tipo file',
            'baseNumeros.mimes'        => 'El archivo a subir no es un excel o csv'
        ];

        return $rules;
    }
}
