<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formUserStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'userID'        => 'required',
            'statusUser'    => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'userID.required'        => 'El userID donde se actualizara los datos no existe',
            'statusUser.required'    => 'Debes seleccionar un estado'
        ];

        return $rules;
    }
}
