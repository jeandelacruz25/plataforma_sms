<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(empty($this->avatarOriginal)){
            $rules = [
                'avatarUser'        => 'required|image',
            ];
        }else{
            $rules = [
                'avatarOriginal'    => 'required',
            ];
        }
        return $rules;
    }

    public function messages()
    {
        if(empty($this->avatarOriginal)){
            $rules = [
                'avatarUser.required'       => 'Debes ingresar una imagen',
                'avatarUser.image'          => 'El archivo a subir debe ser una imagen'
            ];
        }else{
            $rules = [
                'avatarOriginal.required'   => 'No hay ningun avatar anterior al actual',
            ];
        }
        return $rules;
    }
}
