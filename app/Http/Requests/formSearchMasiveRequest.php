<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class formSearchMasiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $idCliente = $this->clienteCampaign ? $this->clienteCampaign : Auth::user()->id_cliente;

        $rules['nameCampaign'] = 'required|unique:base_campanas,nombre_campana,'.$this->campaignID.',id,id_cliente,'.$idCliente.'';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $rules['clienteCampaign'] = 'required';
        }

        if(!$this->campaignID){
            $rules['checkCriterio.0'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        $messages['nameCampaign.required'] = 'Debes ingresar un nombre para la nueva campaña';
        $messages['nameCampaign.unique'] = 'El nombre de campaña ingresado ya se encuentra en uso';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $messages['clienteCampaign.required'] = 'Debes escoger a que cliente se asignara esta nueva campaña';
        }

        if(!$this->campaignID) {
            $messages['checkCriterio.0.required'] = 'Debes escoger por lo menos un criterio';
        }

        return $messages;
    }
}
