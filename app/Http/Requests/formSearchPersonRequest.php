<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formSearchPersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(empty($this->dniPersona) && empty($this->apePatPersona) && empty($this->apeMatPersona) && empty($this->nombrePersona) && empty($this->numPlacaAuto)){
            $rules = [
                'dniPersona'          => 'required|numeric',
                'apePatPersona'       => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u',
                'apeMatPersona'       => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u',
                'nombrePersona'       => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u',
                'numPlacaAuto'        => 'required|alpha_num'
            ];
        }else if(empty($this->dniPersona) && empty($this->numPlacaAuto)) {
            if(!empty($this->nombrePersona) && !empty($this->apePatPersona) && !empty($this->apeMatPersona)){
                $rules = [
                    'apePatPersona' => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u',
                    'apeMatPersona' => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u',
                    'nombrePersona' => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u'
                ];
            }else if(!empty($this->apePatPersona) && empty($this->nombrePersona) && empty($this->apeMatPersona)){
                $rules = [
                    'nombrePersona' => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u'
                ];
            }else if(!empty($this->apeMatPersona) && empty($this->nombrePersona) && empty($this->apePatPersona)){
                $rules = [
                    'nombrePersona' => 'required|regex:/^[\pL\pM\p{Zs}.-]+$/u'
                ];
            }else{
                $rules = [];
            }
        }else if(empty($this->dniPersona) && empty($this->apePatPersona) && empty($this->apeMatPersona) && empty($this->nombrePersona)){
            $rules = [
                'numPlacaAuto'        => 'required|alpha_num'
            ];
        }else{
            $rules = [
                'dniPersona'          => 'required|numeric'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        if(empty($this->dniPersona) && empty($this->apePatPersona) && empty($this->apeMatPersona) && empty($this->nombrePersona) && empty($this->numPlacaAuto)){
            $messages = [
                'dniPersona.required'                => 'Debes ingresar por lo menos un DNI ó',
                'dniPersona.numeric'                 => 'El DNI solo puede ser números',
                'apePatPersona.required'             => 'Debes ingresar un apellido paterno ó',
                'apePatPersona.regex'                => 'El apellido paterno solo puede ser letras con espacios',
                'apeMatPersona.required'             => 'Debes ingresar un apellido materno ó',
                'apeMatPersona.regex'                => 'El apellido materno solo puede ser letras con espacios',
                'nombrePersona.required'             => 'Debes ingresar un nombre ó',
                'nombrePersona.regex'                => 'El nombre solo puede ser letras con espacios',
                'numPlacaAuto.required'              => 'Debes ingresar ua placa de autos',
                'numPlacaAuto.alpha_num'             => 'La placa solo puede ser letras y numeros'
            ];
        }else if(empty($this->dniPersona) && empty($this->numPlacaAuto)){
            if(!empty($this->nombrePersona) && !empty($this->apePatPersona) && !empty($this->apeMatPersona)){
                $messages = [
                    'apePatPersona.required'                => 'Debes ingresar un apellido paterno',
                    'apePatPersona.regex'                   => 'El apellido paterno es requerido, recuerda que solo puede ser letras con espacios',
                    'apeMatPersona.required'                => 'Debes ingresar un apellido materno',
                    'apeMatPersona.regex'                   => 'El apellido materno es requerido, recuerda que solo puede ser letras con espacios',
                    'nombrePersona.required'                => 'Debes ingresar un nombre',
                    'nombrePersona.regex'                   => 'El nombre es requerido, recuerda que solo puede ser letras con espacios'
                ];
            }else if(!empty($this->apePatPersona) && empty($this->nombrePersona) && empty($this->apeMatPersona)){
                $messages = [
                    'nombrePersona.required'                => 'Debes ingresar un nombre',
                    'nombrePersona.regex'                   => 'El nombre es requerido, recuerda que solo puede ser letras con espacios'
                ];
            }else if(!empty($this->apeMatPersona) && empty($this->nombrePersona) && empty($this->apePatPersona)){
                $messages = [
                    'nombrePersona.required'                => 'Debes ingresar un nombre',
                    'nombrePersona.regex'                   => 'El nombre es requerido, recuerda que solo puede ser letras con espacios'
                ];
            }else{
                $messages = [];
            }
        }else if(empty($this->nombrePersona) && !empty($this->apePatPersona) && !empty($this->apeMatPersona)){
            $messages = [
                'apePatPersona.required'                => 'Debes ingresar un apellido paterno',
                'apePatPersona.regex'                   => 'El apellido paterno es requerido, recuerda que solo puede ser letras con espacios',
                'apeMatPersona.required'                => 'Debes ingresar un apellido materno',
                'apeMatPersona.regex'                   => 'El apellido materno es requerido, recuerda que solo puede ser letras con espacios'
            ];
        }else if(empty($this->dniPersona) && empty($this->apePatPersona) && empty($this->apeMatPersona) && empty($this->nombrePersona)){
            $messages = [
                'numPlacaAuto.required'             => 'Debe ingresar una placa de autos',
                'numPlacaAuto.alpha_num'            => 'La placa solo puede ser letras y numeros'
            ];
        }else{
            $messages = [
                'dniPersona.required'               => 'Debe ingresar un número de DNI',
                'dniPersona.numeric'                => 'El DNI solo puede ser números'
            ];
        }

        return $messages;
    }
}
