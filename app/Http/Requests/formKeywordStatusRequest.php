<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formKeywordStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $rules['statusKeyword'] = 'required';

        return $rules;
    }

    public function messages()
    {

        $messages = [];

        $messages['statusKeyword.required'] = 'Necesitas escoger un estado para poder actualizar';

        return $messages;
    }
}
