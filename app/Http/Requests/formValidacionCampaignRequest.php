<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class formValidacionCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $idCliente = $this->clienteCampaign ? $this->clienteCampaign : Auth::user()->id_cliente;

        $rules['nameCampaign'] = 'required|unique:telefonos_campanas,nombre_campana,'.$this->campaignID.',id,id_cliente,'.$idCliente.'';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $rules['clienteCampaign'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $rules = [];

        $rules['nameCampaign.required'] = 'Debes ingresar un nombre para la nueva campaña';
        $rules['nameCampaign.unique'] = 'El nombre de campaña ingresado ya se encuentra en uso';

        if(Auth::user()->authorizeRoles(['Administrador'])){
            $rules['clienteCampaign.required'] = 'Debes escoger a que cliente se asignara esta nueva campaña';
        }

        return $rules;
    }
}
