<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'roles';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'name', 'description', 'id_status',
    ];

    public function users()
	{
	    return $this
	        ->belongsToMany('App\Models\User')
	        ->withTimestamps();
	}

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }
}
