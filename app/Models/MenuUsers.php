<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuUsers extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'menus_users';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'user_id', 'vue_name',
    ];

    public function user(){
        return $this->hasOne('App\Models\User', 'id','user_id');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}