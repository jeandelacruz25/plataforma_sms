<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class BaseDetalle extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'base_detalle';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'id_campania', 'nrodoc', 'fecha_subida',
    ];

    public function campana(){
        return $this->hasOne('App\Models\SearchModels\BaseCampanas', 'id','id_campania');
    }
}
