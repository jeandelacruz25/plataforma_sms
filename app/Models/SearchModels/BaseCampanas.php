<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class BaseCampanas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'base_campanas';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'nombre_campana', 'total_documentos', 'usuario_envio', 'fecha_envio', 'fecha_fin', 'estado_base', 'id_status', 'id_cliente', 'user_created', 'created_at', 'user_updated', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }

    public function criterios(){
        return $this->hasMany('App\Models\SearchModels\BaseCriterio', 'id_campana','id');
    }

    public function criterios_finish(){
        return $this->hasMany('App\Models\SearchModels\BaseCriterio', 'id_campana','id')->where('base_criterios.id_estado' , 1);
    }

    public function criterios_count(){
        return $this->hasMany('App\Models\SearchModels\BaseCriterio', 'id_campana','id')->whereIn('base_criterios.id_estado', [1]);
    }

    public function criterio_phone(){
        return $this->hasOne('App\Models\SearchModels\BaseCriterio', 'id_campana','id')->whereIn('base_criterios.id_criterio', [2])->where('base_criterios.id_estado', '=', 1);
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id','usuario_envio');
    }
}
