<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Autos extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'AUTOS';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'TITULAR', 'TITULAR2', 'PLACA', 'NROTRANSFERENCIA', 'FABRICACION', 'COMPRA', 'MARCA', 'TIPO', 'MODELO', 'CLASE', 'COLOR', 'CARROCERIA', 'COMBUSTIBLE', 'SERIE', 'MOTOR', 'TIPODEPROPIEDAD', 'FECHAREG', 'FECHAACT', 'TAG',
    ];

    public function personas(){
        return $this->hasOne('App\Models\SearchModels\Personas', 'NRODOC','NRODOC');
    }
}
