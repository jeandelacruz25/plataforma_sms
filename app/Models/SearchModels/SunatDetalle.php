<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class SunatDetalle extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'SUNAT';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;

    protected $casts = [
        'ID' => 'int',
        'representantes_legales' => 'array',
        'cantidad_trabajadores' => 'array',
    ];

    protected $fillable = [
        'NRODOC', 'FECHA_REG', 'RUC', 'RazonSocial', 'Telefono', 'Condicion', 'NombreComercial', 'Tipo', 'Inscripcion', 'Estado', 'Direccion', 'SistemaEmision', 'ActividadExterior', 'SistemaContabilidad', 'Oficio', 'EmisionElectronica', 'PLE', 'representantes_legales', 'cantidad_trabajadores',
    ];
}
