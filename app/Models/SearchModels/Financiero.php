<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Financiero extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'FINANCIERO';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'CODIGOSBS', 'FECHAREPORTE', 'CANTIDADEMPRESAS', 'CALIFICACION_0', 'CALIFICACION_1', 'CALIFICACION_2', 'CALIFICACION_3', 'CALIFICACION_4',
    ];

    public function detalle(){
        return $this->hasOne('App\Models\SearchModels\FinancieroDetalle', 'CODIGOSBS','CODIGOSBS');
    }
}
