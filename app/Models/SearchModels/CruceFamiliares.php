<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceFamiliares extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_familiares';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
