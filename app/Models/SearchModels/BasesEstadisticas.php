<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class BasesEstadisticas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'bases_estadisticas';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'id_cliente', 'nrodoc', 'fecha', 'personas', 'telefonos', 'direcciones', 'familiares', 'sueldos', 'sunat', 'financiero', 'autos', 'user_id', 'id_campania',
    ];
}
