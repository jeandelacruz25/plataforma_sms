<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceSunat extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_sunat';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
