<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CrucePersonas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_personas';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
