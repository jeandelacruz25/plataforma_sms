<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class BaseIndividual extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'base_individual';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'nrodoc', 'fecha_validacion',
    ];
}
