<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Telefonos extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'TELEFONOS';
    protected $primaryKey   = 'ID_TELEFONO';
    public $timestamps      = false;
}
