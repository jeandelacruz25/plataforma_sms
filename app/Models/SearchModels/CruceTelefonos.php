<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceTelefonos extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_telefonos';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
