<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'DIRECCIONES';
    protected $primaryKey   = 'ID_DIRECCION';
    public $incrementing    = true;
    public $timestamps      = false;

    protected $fillable = [
        'ID_DIRECCION', 'NRO_DOC', 'PERSONA', 'DEPARTAMENTO', 'PROVINCIA', 'DISTRITO', 'DIRECCION', 'REFERENCIA', 'UBIGEO', 'TAG', 'FECHAREG', 'FECHAACT', 'FECHAEVAL', 'ESTADO',
    ];

    public function personas(){
        return $this->hasOne('App\Models\SearchModels\Personas', 'NRODOC','NRO_DOC');
    }
}