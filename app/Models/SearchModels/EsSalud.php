<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class EsSalud extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'ESSALUD';
    protected $primaryKey   = 'ID';
    public $incrementing    = true;
    public $timestamps      = false;

    protected $fillable = [
        'ID', 'NRODOC', 'PERIODO', 'TITULAR', 'AUTOGENERADO', 'INGRESOS', 'SITUACION', 'E_NRODOC', 'E_RAZONSOCIAL', 'E_DIRECCION', 'E_DEPARTAMENTO', 'E_PROVINCIA', 'E_DISTRITO', 'E_GIRO', 'E_TELEFONO1', 'E_TELEFONO2', 'TAG', 'FECHAREG', 'FECHAACT',
    ];

    public function personas(){
        return $this->hasOne('App\Models\SearchModels\Personas', 'NRODOC','NRO_DOC');
    }
}
