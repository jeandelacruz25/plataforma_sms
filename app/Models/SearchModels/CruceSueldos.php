<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceSueldos extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_sueldos';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
