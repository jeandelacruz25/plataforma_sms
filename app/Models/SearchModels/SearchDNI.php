<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class SearchDNI extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'SEARCH_DNI';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'FECHA_REG', 'FECHA_TELEFONO', 'FECHA_DIRECCION', 'FECHA_RESUMENLABORAL', 'FECHA_FAMILIARES', 'FECHA_AUTOS',
    ];
}
