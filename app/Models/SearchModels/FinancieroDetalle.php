<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class FinancieroDetalle extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'FINANCIERO_DETALLE';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'CODIGOSBS', 'CODIGOEMPRESA', 'EMPRESA', 'FECHAREPORTE', 'TIPOCREDITO', 'CODIGOCUENTA', 'CONDICION', 'SALDO', 'CLASIFICACION', 'TIPOREPORTE', 'ALINEAMIENTO',
    ];
}
