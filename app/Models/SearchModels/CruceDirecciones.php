<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceDirecciones extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_direcciones';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
