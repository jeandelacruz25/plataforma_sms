<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceAutos extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_autos';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
