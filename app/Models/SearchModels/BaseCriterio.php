<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class BaseCriterio extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'base_criterios';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id_criterio', 'id_campana', 'fecha_reg', 'fecha_proc', 'id_estado',
    ];

    public function campana(){
        return $this->hasOne('App\Models\SearchModels\BaseCampanas', 'id','id_campana');
    }
}
