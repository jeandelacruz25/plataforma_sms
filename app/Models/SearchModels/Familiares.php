<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Familiares extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'FAMILIARES';
    protected $primaryKey   = 'ID';
    public $incrementing    = true;
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'PARENTESCO', 'NRODOC_F', 'AUTOGENERADO_F', 'PATERNO_F', 'MATERNO_F', 'NOMBRES_F', 'FECHANAC_F', 'SEXO_F', 'TAG', 'FECHAREG', 'FECHAACT',
    ];

    public function personas(){
        return $this->hasOne('App\Models\SearchModels\Personas', 'NRODOC','NRODOC');
    }
}
