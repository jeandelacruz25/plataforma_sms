<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class TipoCriterio extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_criterio';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;
}
