<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class SunatDNI extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'SUNAT_DNI';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'FECHA_REG', 'FECHA_PROCESO', 'ID_ESTADO',
    ];
}
