<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    protected $connection   = 'richard';
    protected $table        = 'PERSONAS';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'NRODOC', 'TIPODOC', 'APELLIDO_PAT', 'APELLIDO_MAT', 'NOMBRES', 'FECHA_NAC', 'SEXO', 'ESTADO_CIVIL', 'DIRECCION', 'UBIGEO', 'FECHA_REG', 'FECHA_ACT', 'TAG', 'FECHA_EVAL',
    ];

    public function telefonos(){
        return $this->hasMany('App\Models\SearchModels\Telefonos', 'NRODOC','NRODOC')->distinct();
    }

    public function direccion(){
        return $this->hasOne('App\Models\SearchModels\Direcciones', 'NRO_DOC','NRODOC');
    }

    public function direcciones(){
        return $this->hasMany('App\Models\SearchModels\Direcciones', 'NRO_DOC','NRODOC');
    }

    public function familiares(){
        return $this->hasMany('App\Models\SearchModels\Familiares', 'NRODOC','NRODOC');
    }

    public function autos(){
        return $this->hasMany('App\Models\SearchModels\Autos', 'NRODOC','NRODOC');
    }

    public function laboral(){
        return $this->hasMany('App\Models\SearchModels\EsSalud', 'NRODOC','NRODOC')->orderBy('ESSALUD.PERIODO', 'desc');
    }

}