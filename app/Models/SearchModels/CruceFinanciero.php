<?php

namespace App\Models\SearchModels;

use Illuminate\Database\Eloquent\Model;

class CruceFinanciero extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'cruce_financiero';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;
}
