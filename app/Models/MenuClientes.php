<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuClientes extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'menus_clientes';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'cliente_id', 'vue_name',
    ];

    public function clientes(){
        return $this->belongsToMany('App\Models\Clientes');
    }
}
