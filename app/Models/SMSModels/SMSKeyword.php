<?php

namespace App\Models\SMSModels;

use Illuminate\Database\Eloquent\Model;

class SMSKeyword extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'sms_keyword';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'id', 'nombre_keyword', 'estado_servicio', 'mensaje_error', 'id_cliente', 'id_estado', 'estado_error', 'user_created', 'user_updated', 'created_at', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_estado');
    }

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }
}
