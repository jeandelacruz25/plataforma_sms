<?php

namespace App\Models\SMSModels;

use Illuminate\Database\Eloquent\Model;

class SMSChat extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'sms_chat';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id_sms', 'mensaje_sms', 'telefono_sms', 'envio_sms', 'remitente_sms', 'fecha_sms', 'id_cliente',
    ];

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }
}
