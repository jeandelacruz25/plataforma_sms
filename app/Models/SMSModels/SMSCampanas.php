<?php

namespace App\Models\SMSModels;

use Illuminate\Database\Eloquent\Model;

class SMSCampanas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'sms_campanas';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'nombre_campana', 'id_cliente', 'id_status', 'envio_sms', 'total_telefonos', 'tipo_envio', 'fecha_envio', 'user_created', 'created_at', 'user_updated', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }

    public function detalle(){
        return $this->hasMany('App\Models\SMSModels\SMSDetalle', 'id_campana','id')->where([['sms_detalle.leido_sms', '=', 1], ['sms_detalle.sms_respondido', '=', 1]]);
    }

    public function detalle_count()
    {
        return $this->hasMany('App\Models\SMSModels\SMSDetalle', 'id_campana','id')->whereIn('sms_detalle.tipo_envio', [2, 6])->whereNotIn('sms_detalle.estado_sms', [0]);
    }
}