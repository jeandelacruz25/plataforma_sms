<?php

namespace App\Models\SMSModels;

use Illuminate\Database\Eloquent\Model;

class SMSDetalle extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'sms_detalle';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'telefono_usuario', 'mensaje_usuario', 'filtro_mensaje', 'mensaje_respuesta', 'tipo_envio', 'flash_sms', 'id_sms', 'id_bulk_sms', 'estado_sms', 'estado_descripcion', 'leido_sms', 'sms_respondido', 'fecha_envio', 'fecha_recepcion', 'fecha_respuesta_sms', 'visto_sms', 'numero_mccmnc', 'id_campana', 'id_keyword', 'id_cliente', 'async_status',
    ];

    public function campana(){
        return $this->hasOne('App\Models\SMSModels\SMSCampanas', 'id','id_campana');
    }

    public function cliente(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }

    public function keyword(){
        return $this->hasOne('App\Models\SMSModels\SMSKeyword', 'id','id_keyword');
    }
}
