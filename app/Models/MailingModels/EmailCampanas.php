<?php

namespace App\Models\MailingModels;

use Illuminate\Database\Eloquent\Model;

class EmailCampanas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'email_campanas';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'nombre_campana', 'asunto_campana', 'email_remitente', 'nombre_remitente', 'email_respuesta', 'nombre_respuesta', 'total_correos', 'fecha_envio', 'id_cliente', 'id_status', 'user_created', 'created_at', 'user_updated', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }
}
