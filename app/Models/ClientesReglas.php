<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientesReglas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'clientes_reglas';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $casts = [
        'reglas' => 'array'
    ];

    protected $fillable = [
        'id', 'id_cliente', 'reglas',
    ];
}
