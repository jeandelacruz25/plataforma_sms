<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'clientes';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'id', 'avatar', 'ruc', 'razon_social', 'correo_contacto', 'palabra_clave', 'telefono', 'direccion_cliente', 'score_async', 'id_estado', 'user_cre', 'user_upd', 'created_at', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_estado');
    }

    public function rules(){
        return $this->hasOne('App\Models\ClientesReglas', 'id_cliente','id');
    }
}
