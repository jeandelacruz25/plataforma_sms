<?php

namespace App\Models\ScoreModels;

use Illuminate\Database\Eloquent\Model;

class ClientesCarteras extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'clientes_carteras';
    protected $primaryKey   = ['id_cliente', 'id_cartera'];
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'id_cliente', 'id_cartera', 'cartera', 'proveedor',
    ];
}
