<?php

namespace App\Models\ValidatePhoneModels;

use Illuminate\Database\Eloquent\Model;

class TelefonosCanales extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'telefonos_canales';
    protected $primaryKey   = null;
    public $incrementing    = false;
    public $timestamps      = false;

    protected $fillable = [
        'id_cliente', 'num_canales', 'fecha_actualizacion',
    ];
}
