<?php

namespace App\Models\ValidatePhoneModels;

use Illuminate\Database\Eloquent\Model;

class TelefonosDetalle extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'telefonos_detalle';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'telefono', 'num_identidad', 'fecha_validacion', 'estado_validacion', 'fecha_subida', 'id_campana',
    ];

    public function campana(){
        return $this->hasOne('App\Models\ValidatePhoneModels\TelefonosCampanas', 'id','id_campana');
    }
}
