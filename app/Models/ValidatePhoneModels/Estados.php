<?php

namespace App\Models\ValidatePhoneModels;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'estados_validaciones';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;
}
