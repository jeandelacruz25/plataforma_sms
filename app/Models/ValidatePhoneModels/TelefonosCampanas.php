<?php

namespace App\Models\ValidatePhoneModels;

use Illuminate\Database\Eloquent\Model;

class TelefonosCampanas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'telefonos_campanas';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'nombre_campana', 'total_telefonos', 'user_validacion', 'inicio_validacion', 'fin_validacion', 'id_cliente', 'estado_envio', 'id_estado', 'user_created', 'created_at', 'user_updated', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_estado');
    }

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }

    public function total()
    {
        return $this->hasMany('App\Models\ValidatePhoneModels\TelefonosDetalle', 'id_campana','id');
    }

    public function detalle_count()
    {
        return $this->hasMany('App\Models\ValidatePhoneModels\TelefonosDetalle', 'id_campana','id')->whereIn('telefonos_detalle.estado_validacion', [6, 7, 8, 9])->whereNotIn('telefonos_detalle.estado_validacion', [0]);
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id','user_validacion');
    }
}
