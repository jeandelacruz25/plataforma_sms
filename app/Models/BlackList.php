<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'black_list';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'telefono', 'id_cliente', 'user_created', 'created_at', 'user_updated', 'updated_at',
    ];

    public function clientes(){
        return $this->hasOne('App\Models\Clientes', 'id','id_cliente');
    }

    public function user_created(){
        return $this->hasOne('App\Models\User', 'id','user_created');
    }

    public function user_updated(){
        return $this->hasOne('App\Models\User', 'id','user_updated');
    }
}
