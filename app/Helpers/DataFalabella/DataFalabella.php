<?php

namespace DataFalabella;
use App\Models\SearchModels\Autos;
use App\Models\SearchModels\Direcciones;
use App\Models\SearchModels\EsSalud;
use App\Models\SearchModels\Familiares;
use App\Models\SearchModels\Financiero;
use App\Models\SearchModels\FinancieroDetalle;
use App\Models\SearchModels\Personas;
use Carbon\Carbon;
use duzun\hQuery;

class DataFalabella{
    var $cc;

    function __construct()
    {
        $this->cc = new \DataFalabella\cURL();
        $this->cc->setReferer( "http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona" );
        $this->cc->useCookie( true );
        $this->cc->setCookiFileLocation( __DIR__ . "/cookie.txt" );
    }

    function getDataPersonTelefonos($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:first > tbody > tr');

        $dataTelefonos = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataTelefonos[] = explode(',', $replaceArray);
            }

            if($dataTelefonos[0][0] != "NO EXISTEN REGISTROS"){
                $arrayMerge = array_merge($rtn, $dataTelefonos);
                return [
                    'success' => true,
                    'result' => $arrayMerge
                ];
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonDirecciones($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:eq(1) > tbody > tr');


        $dataDirecciones = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataDirecciones[] = explode(',', $replaceArray);
            }

            if($dataDirecciones[0][0] != "NO EXISTEN REGISTROS"){
                if($dataDirecciones[0][1] != "RENIEC EN LINEA"){
                    $arrayMerge = array_merge($rtn, $dataDirecciones);
                    return [
                        'success' => true,
                        'result' => $arrayMerge
                    ];
                }else{
                    return [
                        'success' => false,
                        'result' => $rtn
                    ];
                }
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonResumenLaboral($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:eq(2) > tbody > tr');

        $dataResumenLaboral = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataResumenLaboral[] = explode(',', $replaceArray);
            }

            if($dataResumenLaboral[0][0] != "NO EXISTEN REGISTROS"){
                $arrayMerge = array_merge($rtn, $dataResumenLaboral);
                return [
                    'success' => true,
                    'result' => $arrayMerge
                ];
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonFamiliares($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:eq(3) > tbody > tr');

        $dataFamiliares = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataFamiliares[] = explode(',', $replaceArray);
            }

            if($dataFamiliares[0][0] != "NO EXISTEN REGISTROS"){
                $arrayMerge = array_merge($rtn, $dataFamiliares);
                return [
                    'success' => true,
                    'result' => $arrayMerge
                ];
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonAutos($dni){
        $rtn = array();

        $dataBody = [
            "ruc" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_sunarp', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table#base_personal > tbody');

        if($getData){
            foreach ($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $arrayExplode = explode(',', $replaceArray);

                $removeArray = [
                    '',
                    'PLACA :',
                    'MARCA :',
                    'MODELO :',
                    'SERIE :',
                    'FECHA :',
                    'CLASE :',
                    'CARROCERIA :',
                    'COMBUSTIBLE :',
                    'COLOR :',
                    'MOTOR :'
                ];

                $arrayDiff = array_diff($arrayExplode, $removeArray);

                $rtn[] = array_values($arrayDiff);
            }

            return [
                'success' => true,
                'result' => $rtn
            ];
        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonReporteSBS($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:eq(-3) > tbody > tr');

        $dataReporteSBS = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataReporteSBS[] = explode(',', $replaceArray);
            }

            if($dataReporteSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayMerge = array_merge($rtn, $dataReporteSBS);
                return [
                    'success' => true,
                    'result' => $arrayMerge
                ];
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonDetalleSBS($dni){
        $rtn = array();

        $dataBody = [
            "dni" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $getData = $doc->find_html('table:id=base_personal:last > tbody > tr');

        $dataDetalleSBS = array();
        if($getData){
            foreach($getData as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataDetalleSBS[] = explode(',', $replaceArray);
            }

            if($dataDetalleSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayMerge = array_merge($rtn, $dataDetalleSBS);
                return [
                    'success' => true,
                    'result' => $arrayMerge
                ];
            }else{
                return [
                    'success' => false,
                    'result' => $rtn
                ];
            }

        }else{
            return [
                'success' => false,
                'result' => $rtn
            ];
        }
    }

    function getDataPersonSearchFalabella($dni){
        $arrayDireccion = array();
        $arrayResumenLaboral = array();
        $arrayFamiliares = array();
        $arrayAutos = array();
        $arrayReporteSBS = array();
        $arrayDetalleSBS = array();

        $dataBody = [
            "dni" => $dni
        ];

        $dataBodySunarp = [
            "ruc" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $docSunarp = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_sunarp', // url
            [],
            $dataBodySunarp,
            ['method' => 'POST']
        );

        //$getDataDirecciones = $doc->find_html('table#base_personal:eq(1) > tbody > tr');
        $getDataResumenLaboral = $doc->find_html('table#base_personal:eq(2) > tbody > tr');
        $getDataFamiliares = $doc->find_html('table#base_personal:eq(3) > tbody > tr');
        $getDataAuto = $docSunarp->find_html('table#base_personal > tbody');
        $getDataReporteSBS = $doc->find_html('table:id=base_personal:eq(-3) > tbody > tr');
        $getDataDetalleSBS = $doc->find_html('table#base_personal:last > tbody > tr');

        /*if($getDataDirecciones){
            $dataDirecciones = array();
            foreach($getDataDirecciones as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $removeArray = [
                    'RENIEC EN LINEA'
                ];

                $direcciones = explode(',', $replaceArray);
                $arrayDiff = array_diff($direcciones, $removeArray);
                $listDirecciones = array_values($arrayDiff);

                $result = array_filter($listDirecciones,
                    function($arrayEntry) {
                        return !is_numeric($arrayEntry);
                    }
                );

                $dataDirecciones[] = array_values($result);
            }

            if($dataDirecciones[0][0] != "NO EXISTEN REGISTROS"){
                $arrayDireccion = $dataDirecciones;
            }
        }*/

        if($getDataResumenLaboral){
            $dataResumenLaboral = array();
            foreach($getDataResumenLaboral as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataResumenLaboral[] = explode(',', $replaceArray);
            }

            if($dataResumenLaboral[0][0] != "NO EXISTEN REGISTROS"){
                $arrayResumenLaboral = $dataResumenLaboral;
            }
        }

        if($getDataFamiliares){
            $dataFamiliares = array();
            foreach($getDataFamiliares as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataFamiliares[] = explode(',', $replaceArray);
            }

            if($dataFamiliares[0][0] != "NO EXISTEN REGISTROS"){
                $arrayFamiliares = $dataFamiliares;
            }
        }

        if($getDataAuto){
            foreach ($getDataAuto as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $arrayExplode = explode(',', $replaceArray);

                $removeArray = [
                    '',
                    'PLACA :',
                    'MARCA :',
                    'MODELO :',
                    'SERIE :',
                    'FECHA :',
                    'CLASE :',
                    'CARROCERIA :',
                    'COMBUSTIBLE :',
                    'COLOR :',
                    'MOTOR :'
                ];

                $arrayDiff = array_diff($arrayExplode, $removeArray);

                $arrayAutos[] = array_values($arrayDiff);
            }
        }

        if($getDataReporteSBS){
            foreach($getDataReporteSBS as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataReporteSBS[] = explode(',', $replaceArray);
            }

            if($dataReporteSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayReporteSBS = $dataReporteSBS;
            }
        }

        if($getDataDetalleSBS){
            foreach($getDataDetalleSBS as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataDetalleSBS[] = explode(',', $replaceArray);
            }

            if($dataDetalleSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayDetalleSBS = $dataDetalleSBS;
            }
        }

        /*if(!empty($arrayDireccion)){
            $resultSearchPeru = $arrayDireccion;

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {

                if(isset($valueSearch[2]) && isset($valueSearch[3])){
                    $distrito = $valueSearch[2] === 'CALLAO' ? 'CALLAO' : utf8_decode($valueSearch[3]);
                    Direcciones::updateOrCreate([
                        'NRO_DOC'                    => $dni,
                        'DIRECCION'                 => utf8_decode($valueSearch[0]),
                    ], [
                        'NRODOC'                    => $dni,
                        'PERSONA'                   => '-',
                        'DEPARTAMENTO'              => $valueSearch[2] === 'CALLAO' ? 'CALLAO' : utf8_decode($valueSearch[2]),
                        'PROVINCIA'                 => $valueSearch[2] === 'CALLAO' ? utf8_decode($valueSearch[2]) : utf8_decode($valueSearch[3]),
                        'DISTRITO'                  => $distrito,
                        'DIRECCION'                 => utf8_decode($valueSearch[0]),
                        'REFERENCIA'                => '-',
                        'UBIGEO'                    => '-',
                        'TAG'                       => 'FALABELLA',
                        'FECHAREG'                  => Carbon::now(),
                        'FECHAACT'                  => Carbon::now(),
                        'FECHAEVAL'                 => Carbon::now(),
                        'ESTADO'                    => 999
                    ]);
                }
            }
        }*/

        if(!empty($arrayResumenLaboral)){
            $resultSearchPeru = $arrayResumenLaboral;

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {
                EsSalud::updateOrCreate([
                    'NRODOC'                    => $dni,
                    'E_NRODOC'                  => $valueSearch[0],
                    'PERIODO'                   => $valueSearch[2],
                ], [
                    'NRODOC'                    => $dni,
                    'PERIODO'                   => $valueSearch[2],
                    'TITULAR'                   => '-',
                    'AUTOGENERADO'              => '-',
                    'INGRESOS'                  => $valueSearch[4],
                    'SITUACION'                 => $valueSearch[3],
                    'E_NRODOC'                  => $valueSearch[0],
                    'E_RAZONSOCIAL'             => utf8_decode($valueSearch[1]),
                    'E_DIRECCION'               => '-',
                    'E_DEPARTAMENTO'            => '-',
                    'E_PROVINCIA'               => '-',
                    'E_DISTRITO'                => '-',
                    'E_GIRO'                    => '-',
                    'E_TELEFONO1'               => '-',
                    'E_TELEFONO2'               => '-',
                    'TAG'                       => 'FALABELLA',
                    'FECHAREG'                  => Carbon::now(),
                    'FECHAACT'                  => Carbon::now()
                ]);
            }
        }

        if(!empty($arrayFamiliares)){
            $resultSearchPeru = $arrayFamiliares;

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {
                Familiares::updateOrCreate([
                    'NRODOC'                => $dni,
                    'NRODOC_F'              => $valueSearch[0],
                ], [
                    'NRODOC'                => $dni,
                    'PARENTESCO'            => $valueSearch[6] ? $valueSearch[6] : $valueSearch[5],
                    'NRODOC_F'              => $valueSearch[0],
                    'AUTOGENERADO_F'        => '-',
                    'PATERNO_F'             => utf8_decode($valueSearch[1]),
                    'MATERNO_F'             => utf8_decode($valueSearch[2]),
                    'NOMBRES_F'             => $valueSearch[6] ? utf8_decode($valueSearch[3].' '.$valueSearch[4]) : utf8_decode($valueSearch[3]),
                    'FECHANAC_F'            => $valueSearch[6] ? $valueSearch[5] : $valueSearch[4],
                    'SEXO_F'                => '-',
                    'TAG'                       => 'FALABELLA',
                    'FECHAREG'                  => Carbon::now(),
                    'FECHAACT'                  => Carbon::now()
                ]);
            }
        }

        if(!empty($arrayAutos)){
            $resultSearchPeru = $arrayAutos;

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {
                $searchData = Autos::where([
                    ['NRODOC', '=', $dni],
                    ['PLACA', '=', isset($valueSearch[0]) ? $valueSearch[0] : '-'],
                ])->count();

                if($searchData > 0){
                    Autos::where([
                        ['NRODOC', '=', $dni],
                        ['PLACA', '=', isset($valueSearch[0]) ? $valueSearch[0] : '-'],
                    ])->update([
                        'NRODOC'                => $dni,
                        'TITULAR'               => '-',
                        'TITULAR2'              => '-',
                        'PLACA'                 => isset($valueSearch[0]) ? $valueSearch[0] : '-',
                        'NROTRANSFERENCIA'      => '-',
                        'FABRICACION'           => '-',
                        'COMPRA'                => '-',
                        'MARCA'                 => isset($valueSearch[1]) ? $valueSearch[1] : '-',
                        'TIPO'                  => '-',
                        'MODELO'                => isset($valueSearch[2]) ? $valueSearch[2] : '-',
                        'CLASE'                 => isset($valueSearch[5]) ? $valueSearch[5] : '-',
                        'COLOR'                 => isset($valueSearch[8]) ? utf8_decode($valueSearch[8]) : '-',
                        'CARROCERIA'            => isset($valueSearch[6]) ? $valueSearch[6] : '-',
                        'COMBUSTIBLE'           => isset($valueSearch[7]) ? $valueSearch[7] : '-',
                        'SERIE'                 => isset($valueSearch[3]) ? $valueSearch[3] : '-',
                        'MOTOR'                 => isset($valueSearch[9]) ? $valueSearch[9] : '-',
                        'TIPODEPROPIEDAD'       => '-',
                        'FECHAREG'              => Carbon::now(),
                        'FECHAACT'              => Carbon::now(),
                        'TAG'                   => 'FALABELLA'
                    ]);
                }else{
                    Autos::insert([
                        'NRODOC'                => $dni,
                        'TITULAR'               => '-',
                        'TITULAR2'              => '-',
                        'PLACA'                 => isset($valueSearch[0]) ? $valueSearch[0] : '-',
                        'NROTRANSFERENCIA'      => '-',
                        'FABRICACION'           => '-',
                        'COMPRA'                => '-',
                        'MARCA'                 => isset($valueSearch[1]) ? $valueSearch[1] : '-',
                        'TIPO'                  => '-',
                        'MODELO'                => isset($valueSearch[2]) ? $valueSearch[2] : '-',
                        'CLASE'                 => isset($valueSearch[5]) ? $valueSearch[5] : '-',
                        'COLOR'                 => isset($valueSearch[8]) ? utf8_decode($valueSearch[8]) : '-',
                        'CARROCERIA'            => isset($valueSearch[6]) ? $valueSearch[6] : '-',
                        'COMBUSTIBLE'           => isset($valueSearch[7]) ? $valueSearch[7] : '-',
                        'SERIE'                 => isset($valueSearch[3]) ? $valueSearch[3] : '-',
                        'MOTOR'                 => isset($valueSearch[9]) ? $valueSearch[9] : '-',
                        'TIPODEPROPIEDAD'       => '-',
                        'FECHAREG'              => Carbon::now(),
                        'FECHAACT'              => Carbon::now(),
                        'TAG'                   => 'FALABELLA'
                    ]);
                }
            }
        }

        if(!empty($arrayReporteSBS)){
            $resultSearchPeru = $arrayReporteSBS;

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {
                $codigoSBS = isset($valueSearch[0]) ? str_replace('-', '', $valueSearch[0]) : 0;
                $searchData = Financiero::where([
                    ['NRODOC', '=', $dni],
                    ['FECHAREPORTE', '=', $codigoSBS],
                ])->count();

                if($searchData > 0){
                    Financiero::where([
                        ['NRODOC', '=', $dni],
                        ['FECHAREPORTE', '=', $codigoSBS],
                    ])->update([
                        'NRODOC'                => $dni,
                        'CODIGOSBS'             => $codigoSBS.$dni,
                        'FECHAREPORTE'          => $codigoSBS,
                        'CANTIDADEMPRESAS'      => isset($valueSearch[1]) ? $valueSearch[1] : 0,
                        'CALIFICACION_0'        => isset($valueSearch[2]) ? $valueSearch[2] : 0,
                        'CALIFICACION_1'        => isset($valueSearch[3]) ? $valueSearch[3] : 0,
                        'CALIFICACION_2'        => isset($valueSearch[4]) ? $valueSearch[4] : 0,
                        'CALIFICACION_3'        => isset($valueSearch[5]) ? $valueSearch[5] : 0,
                        'CALIFICACION_4'        => isset($valueSearch[6]) ? $valueSearch[6] : 0,
                    ]);
                }else{
                    Financiero::insert([
                        'NRODOC'                => $dni,
                        'CODIGOSBS'             => $codigoSBS.$dni,
                        'FECHAREPORTE'          => $codigoSBS,
                        'CANTIDADEMPRESAS'      => isset($valueSearch[1]) ? $valueSearch[1] : 0,
                        'CALIFICACION_0'        => isset($valueSearch[2]) ? $valueSearch[2] : 0,
                        'CALIFICACION_1'        => isset($valueSearch[3]) ? $valueSearch[3] : 0,
                        'CALIFICACION_2'        => isset($valueSearch[4]) ? $valueSearch[4] : 0,
                        'CALIFICACION_3'        => isset($valueSearch[5]) ? $valueSearch[5] : 0,
                        'CALIFICACION_4'        => isset($valueSearch[6]) ? $valueSearch[6] : 0,
                    ]);
                }
            }
        }

        if(!empty($arrayDetalleSBS)){
            $resultSearchPeru = $arrayDetalleSBS;

            $detalleSBS = Financiero::where([
                ['NRODOC', '=', $dni],
            ])->orderBy('FECHAREPORTE', 'desc')->first();

            foreach ($resultSearchPeru as $keySearch => $valueSearch) {
                $tipoProducto = '-';

                if(isset($valueSearch[1])){
                    $producto = preg_replace('/[^a-zA-Z]/', '', $valueSearch[1]);;
                    if(empty($producto)){
                        $tipoProducto = '-';
                        $monto = $valueSearch[1];
                        $calificacion = intClasificacionSBS($valueSearch[2]);
                        $diasAtraso = $valueSearch[3];
                    }else{
                        $tipoProducto = $valueSearch[1];
                        $monto = $valueSearch[2];
                        $calificacion = intClasificacionSBS($valueSearch[3]);
                        $diasAtraso = $valueSearch[4];
                    }
                }

                FinancieroDetalle::insert([
                    'CODIGOSBS'             => $detalleSBS['CODIGOSBS'],
                    'CODIGOEMPRESA'         => 0,
                    'EMPRESA'               => isset($valueSearch[0]) ? $valueSearch[0] : '-',
                    'FECHAREPORTE'          => $detalleSBS['FECHAREPORTE'],
                    'TIPOCREDITO'           => $tipoProducto,
                    'CODIGOCUENTA'          => 0,
                    'CONDICION'             => $calificacion,
                    'SALDO'                 => $monto,
                    'CLASIFICACION'         => $calificacion,
                    'TIPOREPORTE'           => 0,
                    'ALINEAMIENTO'          => 0
                ]);
            }
        }

        return [
            'Direccion'         => $arrayDireccion,
            'ResumenLaboral'    => $arrayResumenLaboral,
            'Familia'           => $arrayFamiliares,
            'Auto'              => $arrayAutos,
            'ReporteSBS'        => $arrayReporteSBS,
            'DetalleSBS'        => $arrayDetalleSBS
        ];
    }

    function getDataPersonSearchFalabellaFirst($dni){
        $arrayDireccion = array();
        $arrayResumenLaboral = array();
        $arrayFamiliares = array();
        $arrayAutos = array();
        $arrayReporteSBS = array();
        $arrayDetalleSBS = array();

        $dataBody = [
            "dni" => $dni
        ];

        $dataBodySunarp = [
            "ruc" => $dni
        ];

        $doc = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_persona', // url
            [],
            $dataBody,
            ['method' => 'POST']
        );

        $docSunarp = hQuery::fromUrl(
            'http://www.searchperu.pe/index.php/Busqueda_Personas/busca_sunarp', // url
            [],
            $dataBodySunarp,
            ['method' => 'POST']
        );

        $getDataDirecciones = $doc->find_html('table#base_personal:eq(1) > tbody > tr');
        $getDataResumenLaboral = $doc->find_html('table#base_personal:eq(2) > tbody > tr');
        $getDataFamiliares = $doc->find_html('table#base_personal:eq(3) > tbody > tr');
        $getDataAuto = $docSunarp->find_html('table#base_personal > tbody');
        $getDataReporteSBS = $doc->find_html('table:id=base_personal:eq(-3) > tbody > tr');
        $getDataDetalleSBS = $doc->find_html('table#base_personal:last > tbody > tr');

        if($getDataDirecciones){
            $dataDirecciones = array();
            foreach($getDataDirecciones as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $removeArray = [
                    'RENIEC EN LINEA'
                ];

                $direcciones = explode(',', $replaceArray);
                $arrayDiff = array_diff($direcciones, $removeArray);
                $listDirecciones = array_values($arrayDiff);

                $result = array_filter($listDirecciones,
                    function($arrayEntry) {
                        return !is_numeric($arrayEntry);
                    }
                );

                $dataDirecciones[] = array_values($result);
            }

            if($dataDirecciones[0][0] != "NO EXISTEN REGISTROS"){
                $arrayDireccion = $dataDirecciones;
            }
        }

        if($getDataResumenLaboral){
            $dataResumenLaboral = array();
            foreach($getDataResumenLaboral as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataResumenLaboral[] = explode(',', $replaceArray);
            }

            if($dataResumenLaboral[0][0] != "NO EXISTEN REGISTROS"){
                $arrayResumenLaboral = $dataResumenLaboral;
            }
        }

        if($getDataFamiliares){
            $dataFamiliares = array();
            foreach($getDataFamiliares as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataFamiliares[] = explode(',', $replaceArray);
            }

            if($dataFamiliares[0][0] != "NO EXISTEN REGISTROS"){
                $arrayFamiliares = $dataFamiliares;
            }
        }

        if($getDataAuto){
            foreach ($getDataAuto as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $arrayExplode = explode(',', $replaceArray);

                $removeArray = [
                    '',
                    'PLACA :',
                    'MARCA :',
                    'MODELO :',
                    'SERIE :',
                    'FECHA :',
                    'CLASE :',
                    'CARROCERIA :',
                    'COMBUSTIBLE :',
                    'COLOR :',
                    'MOTOR :'
                ];

                $arrayDiff = array_diff($arrayExplode, $removeArray);

                $arrayAutos[] = array_values($arrayDiff);
            }
        }

        if($getDataReporteSBS){
            foreach($getDataReporteSBS as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataReporteSBS[] = explode(',', $replaceArray);
            }

            if($dataReporteSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayReporteSBS = $dataReporteSBS;
            }
        }

        if($getDataDetalleSBS){
            foreach($getDataDetalleSBS as $key => $data){
                $dataArray = strip_tags(trim($data));
                $replaceArray = preg_replace('/\s{2,}/', ',', $dataArray);
                $dataDetalleSBS[] = explode(',', $replaceArray);
            }

            if($dataDetalleSBS[0][0] != "NO EXISTEN REGISTROS"){
                $arrayDetalleSBS = $dataDetalleSBS;
            }
        }

        return [
            'Direccion'         => $arrayDireccion,
            'ResumenLaboral'    => $arrayResumenLaboral,
            'Familia'           => $arrayFamiliares,
            'Auto'              => $arrayAutos,
            'ReporteSBS'        => $arrayReporteSBS,
            'DetalleSBS'        => $arrayDetalleSBS
        ];
    }
}
