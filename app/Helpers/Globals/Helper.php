<?php

function getStatusSMS($idStatus){
    switch ($idStatus){
        case 0:
            return "<span class='px-3 bg-span btn-cian'>Aún sin Enviar</span>";
            break;
        case 1:
            return "<span class='px-3 bg-span btn-yellow2'>Pendiente</span>";
            break;
        case 2:
            return "<span class='px-3 bg-span btn-purple2'>No Entregado</span>";
            break;
        case 3:
            return "<span class='px-3 bg-span btn-green2'>Entregado</span>";
            break;
        case 4:
            return "<span class='px-3 bg-span btn-orange2'>Expirado</span>";
            break;
        case 5:
            return "<span class='px-3 bg-span btn-rose2'>Rechazado</span>";
            break;
        case 6:
            return "<span class='px-3 bg-span btn-gray2'>Lista Negra</span>";
            break;
    }
}

function getStatus($idStatus){
    switch ($idStatus){
        case 0:
            return "Aún sin Enviar";
            break;
        case 1:
            return "Pendiente";
            break;
        case 2:
            return "No Entregado";
            break;
        case 3:
            return "Entregado";
            break;
        case 4:
            return "Expirado";
            break;
        case 5:
            return "Rechazado";
            break;
        case 6:
            return "Lista Negra";
            break;
    }
}

function getStatusResponse($idStatus){
    switch ($idStatus){
        case 0:
            return "aun_sin_enviar";
            break;
        case 1:
            return "pendiente";
            break;
        case 2:
            return "no_entregado";
            break;
        case 3:
            return "entregado";
            break;
        case 4:
            return "expirado";
            break;
        case 5:
            return "rechazado";
            break;
        case 6:
            return "lista_negra";
            break;
    }
}

function getColorStatus($idStatus){
    switch ($idStatus){
        case 0:
            return "#74CEFA";
            break;
        case 1:
            return "#FAEC72";
            break;
        case 2:
            return "#FA7EB3";
            break;
        case 3:
            return "#84F962";
            break;
        case 4:
            return "#F9BD59";
            break;
        case 5:
            return "#F56767";
            break;
        case 6:
            return "#9B9B9B";
            break;
    }
}

function getConceptStatusSMS($idStatus){
    switch ($idStatus){
        case 0:
            return "<p>El mensaje aún no es enviado</p>";
            break;
        case 1:
            return "<p>El mensaje ha sido procesado y enviado a la próxima instancia, es decir, operador de telefonía móvil.</p>";
            break;
        case 2:
            return "<p>El mensaje no ha sido entregado.</p>";
            break;
        case 3:
            return "<p>El mensaje ha sido procesado y entregado exitosamente.</p>";
            break;
        case 4:
            return "<p>El mensaje se ha enviado y ha caducado debido a la espera más allá de su período de validez (nuestra plataforma predeterminada es 48 horas), o el informe de entrega del operador ha revertido el vencido como estado final.</p>";
            break;
        case 5:
            return "<p>El mensaje ha sido recibido pero ha sido rechazado por el sistema, o el operador ha revertido <span class='badge px-3 heading-font-family bg-danger'>Rechazado</span> como estado final.</p>";
            break;
        case 6:
            return "<p>El mensaje no ha sido enviado, ya que este número pertenece a su lista negra.</p>";
            break;
    }
}

function getDescriptionStatusSMS($nameStatus){
    switch ($nameStatus) {
        case 'NO_ENVIADO':
            return "<p>El mensaje aún no ha sido procesado ni enviado</p>";
            break;

        case 'PENDING_WAITING_DELIVERY':
            return "<p>El mensaje ha sido procesado y enviado a la próxima instancia, es decir, operador de telefonía móvil con acuse de recibo de solicitud desde su plataforma. El informe de entrega aún no se ha recibido y se espera, por lo que el estado aún está pendiente.</p>";
            break;

        case 'PENDING_ENROUTE':
            return "<p>El mensaje se ha procesado y enviado a la siguiente instancia, es decir, operador de telefonía móvil.</p>";
            break;

        case 'PENDING_ACCEPTED':
            return "<p>El mensaje ha sido aceptado y procesado, y está listo para ser enviado a la siguiente instancia, es decir, operador.</p>";
            break;

        case 'UNDELIVERABLE_REJECTED_OPERATOR':
            return "<p>El mensaje se ha enviado al operador, mientras que la solicitud se rechazó o se revertió un informe de entrega con el estado <span class='badge px-3 heading-font-family bg-danger'>Rechazado</span>.</p>";
            break;

        case 'UNDELIVERABLE_NOT_DELIVERED':
            return "<p>Se ha enviado un mensaje al operador, pero no se ha podido entregar, ya que un informe de entrega con el estado <span class='badge px-3 heading-font-family bg-warning'>No Entregado</span> se revertió del operador.</p>";
            break;

        case 'DELIVERED_TO_OPERATOR':
            return "<p>El mensaje ha sido enviado y entregado exitosamente al operador.</p>";
            break;

        case 'DELIVERED_TO_HANDSET':
            return "<p>El mensaje ha sido procesado y entregado exitosamente al destinatario.</p>";
            break;

        case 'EXPIRED_EXPIRED':
            return "<p>El mensaje fue recibido y enviado al operador. Sin embargo, ha estado pendiente hasta que expire el período de validez, o el operador devolvió el estado <span class='badge px-3 heading-font-family bg-dribbble'>Expirado</span> mientras tanto.</p>";
            break;

        case 'EXPIRED_DLR_UNKNOWN':
            return "<p>El mensaje ha sido recibido y enviado al operador para su entrega. Sin embargo, el informe de entrega del operador no se ha formateado correctamente o no se ha reconocido como válido.</p>";
            break;

        case 'REJECTED_NETWORK':
            return "<p>Se recibió un mensaje, pero la red está fuera de nuestra cobertura o no está configurada en su cuenta. Su administrador de cuenta puede informarle sobre el estado de la cobertura o configurar la red en cuestión.</p>";
            break;

        case 'REJECTED_PREFIX_MISSING':
            return "<p>El mensaje ha sido recibido pero ha sido rechazado ya que el número no es reconocido debido a un número incorrecto de prefijo o número de longitud. Esta información es diferente para cada red y se actualiza regularmente.</p>";
            break;

        case 'REJECTED_DND':
            return "<p>El mensaje se ha recibido y rechazado debido a que el usuario está suscrito a los servicios DND (No molestar), lo que deshabilita el tráfico de servicio a su número.</p>";
            break;

        case 'REJECTED_SOURCE':
            return "<p>Su cuenta está configurada para aceptar solo ID de remitente registrados, mientras que la ID de remitente definida en la solicitud no se ha registrado en su cuenta.</p>";
            break;

        case 'REJECTED_NOT_ENOUGH_CREDITS':
            return "<p>Comunicarse con nuestra área de soporte : support@securitec.pe, por un problema de envio, con el asunto de Corte de Red.</p>";
            break;

        case 'REJECTED_SENDER':
            return "<p>La identificación del remitente ha sido incluida en la lista negra en su cuenta comuníquese con el Soporte para obtener más ayuda.</p>";
            break;

        case 'REJECTED_DESTINATION':
            return "<p>El número de destino ha sido incluido en la lista negra ya sea en la solicitud del operador o en su cuenta póngase en contacto con el Soporte para obtener más información.</p>";
            break;

        case 'REJECTED_PREPAID_PACKAGE_EXPIRED':
            return "<p>Comunicarse con nuestra área de soporte : support@securitec.pe, por un problema de envio, con el asunto de Expirado.</p>";
            break;

        case 'REJECTED_DESTINATION_NOT_REGISTERED':
            return "<p>Su cuenta ha sido configurada para enviarse solo a un número único con fines de prueba. Póngase en contacto con su Administrador de cuentas para eliminar la limitación.</p>";
            break;

        case 'REJECTED_ROUTE_NOT_AVAILABLE':
            return "<p>Se ha recibido un mensaje en el sistema, sin embargo, su cuenta no se ha configurado para enviar mensajes, es decir, no hay rutas en su cuenta disponibles para su envío posterior. Su administrador de cuenta podrá configurar su cuenta según su preferencia.</p>";
            break;

        case 'REJECTED_FLOODING_FILTER':
            return "<p>El mensaje ha sido rechazado debido a un mecanismo anti-flooding. De forma predeterminada, un solo número solo puede recibir 20 mensajes variados y 6 mensajes idénticos por hora. Si hay un requisito, la limitación se puede extender por cuenta a pedido a su Administrador de cuenta.</p>";
            break;

        case 'REJECTED_SYSTEM_ERROR':
            return "<p>La solicitud ha sido rechazada debido a un error esperado del sistema. Póngase en contacto con nuestro equipo de soporte técnico para obtener más detalles.</p>";
            break;

        case 'REJECTED_DUPLICATE_MESSAGE_ID':
            return "<p>La solicitud ha sido rechazada debido a un ID de mensaje duplicado especificado en la solicitud de envío, mientras que los ID de mensaje deberían ser un valor único.</p>";
            break;

        case 'REJECTED_INVALID_UDH':
            return "<p>Se recibió un mensaje y nuestro sistema detectó que el mensaje se formateó incorrectamente debido a un parámetro de clase de ESM no válido o a una cantidad inexacta de caracteres al usar esmclass: 64.</p>";
            break;

        case 'REJECTED_MESSAGE_TOO_LONG':
            return "<p>Se ha recibido un mensaje, pero la longitud total del mensaje es de más de 25 partes o texto de mensaje que supera los 4000 bytes según nuestra limitación del sistema.</p>";
            break;

        case 'MISSING_TO':
            return "<p>La solicitud se ha recibido, sin embargo, el parámetro 'to' no se ha establecido o está vacío, es decir, debe haber destinatarios válidos para enviar el mensaje.</p>";
            break;

        case 'REJECTED_INVALID_DESTINATION':
            return "<p>Se ha recibido la solicitud, sin embargo, el destino no es válido: el prefijo del número no es correcto, ya que no coincide con un prefijo numérico válido de ningún operador de telefonía móvil. La longitud del número también se toma en consideración para verificar la validez del número.</p>";
            break;

        case 'BLACK_LIST':
            return "<p>La solicitud de envio ha sido cancelada para este número, ya que se encuentra en la lista negra.</p>";
            break;
    }
}

function getCountryMCCMNC ($numberCountry) {
    switch ($numberCountry){
        case 716:
            return "Perú";
            break;
        default:
            return "Desconocido";
            break;
    }
}

function getOperatorMCCMNC ($numberOperator) {
    switch ($numberOperator){
        case 06:
            return "Movistar Perú";
            break;
        case 07:
            return "Nextel Perú";
            break;
        case 10:
            return "Claro Perú";
            break;
        case 15:
            return "Bitel Perú";
            break;
        case 17:
            return "Entel Perú";
            break;
        default:
            return "Desconocido";
            break;
    }
}

function letterMin($text){
    return ucwords(\Illuminate\Support\Str::lower($text));
}

function formatDatetime($datetime){
    return \Carbon\Carbon::parse($datetime)->format('d/m/Y H:i:s a');
}

function limitString($text, $limit){
    return str_limit($text, $limit).'...';
}

function convertTime($dateTime){
    $response = '';
    $dateTimeChat = \Carbon\Carbon::parse($dateTime);
    $dateTimeActually = \Carbon\Carbon::now();
    $diffYearDateTime = $dateTimeActually->diffInYears($dateTimeChat);
    $diffMonthDateTime = $dateTimeActually->diffInMonths($dateTimeChat);
    $diffDaysDateTime = $dateTimeActually->diffInDays($dateTimeChat);
    $diffSecondsDateTime = $dateTimeActually->diffInSeconds($dateTimeChat);

    if($diffSecondsDateTime > 0){
        $hours = floor( $diffSecondsDateTime / 3600 );
        $minutes = floor( ($diffSecondsDateTime % 3600) / 60 );
        $seconds = $diffSecondsDateTime % 60;

        if($diffYearDateTime > 0){
            $response .= $diffYearDateTime;
            $response .= $diffYearDateTime === 1 ? " año " : " años ";
        } else if($diffMonthDateTime > 0) {
            $response .= $diffMonthDateTime;
            $response .= $diffMonthDateTime === 1 ? " mes " : " meses ";
        } else if($diffDaysDateTime > 0) {
            $response .= $diffDaysDateTime;
            $response .= $diffDaysDateTime === 1 ? " dia " : " dias ";
        } else {
            if($hours > 0){
                $response .= $hours;
                $response .= $hours === 1 ? " hora " : " horas ";
            } else if($minutes > 0) {
                $response .= $minutes;
                $response .= $minutes === 1 ? " minuto " : " minutos ";
            } else if($seconds > 0){
                $response .= $seconds;
                $response .= $seconds === 1 ? " segundo " : " segundos ";
            }
        }
    }

    return 'Hace '.$response;
}

function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-',
        '/[’‘‹›‚]/u'    =>   ' ',
        '/[“”«»„]/u'    =>   ' ',
        '/ /'           =>   ' ',
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}

function monthActual() {
    return letterMin(\Jenssegers\Date\Date::now()->format('F'));
}

function colorPercentage($number){
    switch ($number){
        case ($number > 75):
            return "success";
            break;
        case ($number > 50 && $number <= 75):
            return "primary-light";
            break;
        case ($number > 25 && $number <= 50):
            return "info";
            break;
        case ($number > 10 && $number <= 25):
            return "warning";
            break;
        case ($number > 0 && $number <= 10):
            return "danger";
            break;
        default:
            return "danger";
            break;
    }
}

function getColorValidacionStatus($idStatus){
    switch ($idStatus){
        case 0:
            return "#ceeefd";
            break;
        case 5:
            return "#fcf3aa";
            break;
        case 7:
            return "#fdcee8";
            break;
        case 8:
            return "#fddfdf";
            break;
        case 9:
            return "#d9fdce";
            break;
        default:
            return "#ceeefd";
            break;
    }
}

function getStatusValidacion($idStatus){
    switch ($idStatus){
        case 0:
            return "Aún sin Validar";
            break;
        case 5:
            return "Validando";
            break;
        case 7:
            return "Inexistente";
            break;
        case 8:
            return "Buzón de Voz";
            break;
        case 9:
            return "Prendido";
            break;
        default:
            return "Sin Validar";
            break;
    }
}

function getStatusValidacionText($idStatus){
    switch ($idStatus){
        case 0:
            return "sin_validar";
            break;
        case 5:
            return "validando";
            break;
        case 7:
            return "no_contesta";
            break;
        case 8:
            return "buzon_voz";
            break;
        case 9:
            return "prendido";
            break;
        default:
            return "estado_invalido";
            break;
    }
}

function search($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }

    }

    return $results;
}

function searchMenuCliente($idCliente, $nameMenu){
    $dataCliente = \App\Models\MenuClientes::where([
        ['cliente_id', '=', $idCliente],
        ['vue_name', '=', $nameMenu]
    ])->count();

    return $dataCliente;
}

function searchMenuUsuario($idUser, $nameMenu){
    $dataCliente = \App\Models\MenuUsers::where([
        ['user_id', '=', $idUser],
        ['vue_name', '=', $nameMenu]
    ])->count();

    return $dataCliente;
}

function nameCruce($typeCruce){

    switch ($typeCruce){
        case 1:
            return 'Datos Personales';
            break;
        case 2:
            return 'Telefonos';
            break;
        case 3:
            return 'Direcciones';
            break;
        case 4:
            return 'Familiares';
            break;
        case 5:
            return 'Sueldos';
            break;
        case 6:
            return 'Sunat';
            break;
        case 7:
            return 'Financiero';
            break;
        case 8:
            return 'Autos';
            break;
    }
}

function intClasificacionSBS($nameClasificacion){
    switch ($nameClasificacion){
        case 'NOR':
            return 0;
            break;
        case 'CPP':
            return 1;
            break;
        case 'DEF':
            return 2;
            break;
        case 'DUD':
            return 3;
            break;
        case 'PER':
            return 4;
            break;
        default:
            return 5;
            break;
    }
}