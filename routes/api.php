<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getSMSLogs',            'WebServiceController@getSMSLogs')->name('getSMSLogs');
Route::get('/getSMSReceivedLogs',    'WebServiceController@getSMSReceivedLogs')->name('getSMSReceivedLogs');
Route::post('/getSendSMS',           'WebServiceController@getSendSMS')->name('getSendSMS');
Route::post('/getSendSMSReport',     'WebServiceController@getSendSMSReport')->name('getSendSMSReport');
Route::get('/getSMSReceived',        'WebServiceController@getSMSReceived')->name('getSMSReceived');
Route::get('/getNotificationSMS',    'WebServiceController@getNotificationSMS')->name('getNotificationSMS');
Route::get('/getKeywordSMS',         'WebServiceController@getKeywordSMS')->name('getKeywordSMS');

/* Rutas de Envio */
Route::get('/getSendSMSDosVias',      'WebServiceController@getSendSMSDosVias')->name('getSendSMSDosVias');
Route::get('/getSendSMSInformativo',  'WebServiceController@getSendSMSInformativo')->name('getSendSMSInformativo');
Route::get('/getSendSMSKeyword',      'WebServiceController@getSendSMSKeyword')->name('getSendSMSKeyword');

/* Rutas Recolectar Data desde Webservices */
Route::get('/recolectDataSunat',        'WebServiceController@recolectDataSunat')->name('recolectDataSunat');
Route::get('/recolectDataFalabella',    'WebServiceController@recolectDataFalabella')->name('recolectDataFalabella');

Route::get('/searchFalabellaAPI',       'WebServiceController@dataFalebellaAPI')->name('searchFalabellaAPI');

Route::get('/getCoordinateMaps',        'WebServiceController@getCoordinateMaps')->name('getCoordinateMaps');

Route::post('/syncScore',               'WebServiceController@syncScore')->name('syncScore');

