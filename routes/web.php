<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {

    /* Routes Auth */
    Auth::routes();

    /* Routes Login */
    Route::post('/loginSession',                       'Auth\LoginController@authenticate')->name('loginSession');
    Route::post('/validateSession',                    'Auth\LoginController@validateSession')->name('validateSession');
    Route::post('/emitValidateSession',                'Auth\LoginController@emitValidateSession')->name('emitValidateSession');

    /* Routes Default */
    Route::get('/',                                     'HomeController@index')->name('home');
    Route::get('/home',                                 'HomeController@index')->name('home');
    Route::get('/getUserInformation',                   'SecuritecController@getUserInformation')->name('getUserInformation');
    Route::get('/getClienteMenu',                       'SecuritecController@getClienteMenu')->name('getClienteMenu');
    Route::get('/getUserMenu',                          'SecuritecController@getUserMenu')->name('getUserMenu');
    Route::get('/getUtilitiesUser',                     'SecuritecController@getUtilitiesUser')->name('getUtilitiesUser');

    /* RemoveAuthCookie */
    Route::get('/removeAuthCookie',                     'SecuritecController@cookieAuthRemove')->name('removeAuthCookie');

    /* Route Avatar */
    Route::get('/formAvatar',                           'UserController@formAvatar')->name('formAvatar');
    Route::post('/saveFormAvatar',                      'UserController@saveFormAvatar')->name('saveFormAvatar');

    /* Route Users Roles */
    Route::get('/usersRole',                            'UserRoleController@index')->name('usersRole');

    /* Route User */
    Route::get('/paginationUsuarios',                   'UserController@paginationUsuarios')->name('paginationUsuarios');
    Route::match(['get', 'post'],   '/formUsers',       'UserController@formUsers')->name('formUsers');
    Route::post('/formUsersMenu',                       'UserController@formUsersMenu')->name('formUsersMenu');
    Route::post('/formUsersStatus',                     'UserController@formUsersStatus')->name('formUsersStatus');
    Route::post('/saveFormUsers',                       'UserController@saveFormUsers')->name('saveFormUsers');
    Route::post('/saveFormUsersMenu',                   'UserController@saveFormUsersMenu')->name('saveFormUsersMenu');
    Route::post('/saveFormUsersStatus',                 'UserController@saveFormUsersStatus')->name('saveFormUsersStatus');
    Route::match(['get', 'post'],   '/logoutSession',   'Auth\LoginController@logout')->name('logoutSession');
    Route::match(['get', 'post'],   '/keepSession',     'UserController@keepSession')->name('keepSession');

    /* Route Role */
    Route::get('/paginationPerfiles',                   'RoleController@paginationPerfiles')->name('paginationPerfiles');
    Route::match(['get', 'post'],   '/formRoles',       'RoleController@formRoles')->name('formRoles');
    Route::post('/formRolesAssing',                     'RoleController@formRolesAssing')->name('formRolesAssing');
    Route::post('/formRolesMenu',                       'RoleController@formRolesMenu')->name('formRolesMenu');
    Route::post('/saveFormRoles',                       'RoleController@saveFormRoles')->name('saveFormRoles');
    Route::post('/saveFormRolesAssing',                 'RoleController@saveFormRolesAssing')->name('saveFormRolesAssing');
    Route::post('/saveFormRolesMenu',                   'RoleController@saveFormRolesMenu')->name('saveFormRolesMenu');

    /* Route Chat */
    /*Route::post('/chat',                                'ChatController@index')->name('chat');*/
    Route::post('/getChat',                             'ChatController@getChat')->name('getChat');
    Route::post('/saveChatMessage',                     'ChatController@saveChatMessage')->name('saveChatMessage');

    /* Route Clientes */
    Route::get('/clientes',                             'ClientesController@index')->name('clientes');
    Route::get('/paginationClientes',                   'ClientesController@paginationClientes')->name('paginationClientes');
    Route::match(['get', 'post'],   '/formClientes',    'ClientesController@formClientes')->name('formClientes');
    Route::post('/formClientesStatus',                  'ClientesController@formClientesStatus')->name('formClientesStatus');
    Route::post('/formClientesMenu',                    'ClientesController@formClientesMenu')->name('formClientesMenu');
    Route::post('/formClientesRules',                   'ClientesController@formClientesRules')->name('formClientesRules');
    Route::post('/saveFormClientes',                    'ClientesController@saveFormClientes')->name('saveFormClientes');
    Route::post('/saveFormClientesStatus',              'ClientesController@saveFormClientesStatus')->name('saveFormClientesStatus');
    Route::post('/saveFormClientesMenu',                'ClientesController@saveFormClientesMenu')->name('saveFormClientesMenu');
    Route::post('/saveFormClientesRules',               'ClientesController@saveFormClientesRules')->name('saveFormClientesRules');

    /* Route SMS */
    Route::get('/sms',                                  'SMSControllers\SMSController@index')->name('sms');
    Route::post('/sendSMS',                             'SMSControllers\SMSController@sendSMS')->name('sendSMS');

    /* Route SMS Campaign */
    Route::get('/paginationSMSCampanas',                    'SMSControllers\SMSController@paginationSMSCampanas')->name('paginationSMSCampanas');
    Route::match(['get', 'post'],   '/formCampaign',        'SMSControllers\SMSController@formCampaign')->name('formCampaign');
    Route::post('/saveFormCampaign',                        'SMSControllers\SMSController@saveFormCampaign')->name('saveFormCampaign');
    Route::post('/formCampaignUpload',                      'SMSControllers\SMSController@formCampaignUpload')->name('formCampaignUpload');
    Route::post('/saveFormCampaignUpload',                  'SMSControllers\SMSController@saveFormCampaignUpload')->name('saveFormCampaignUpload');
    Route::post('/executeUploadCampaign',                   'SMSControllers\SMSController@executeUploadCampaign')->name('executeUploadCampaign');
    Route::post('/formCampaignUsers',                       'SMSControllers\SMSController@formCampaignUsers')->name('formCampaignUsers');
    Route::get('/paginationFormCampaignUsers',              'SMSControllers\SMSController@paginationFormCampaignUsers')->name('paginationFormCampaignUsers');
    Route::post('/formSMSMasivo',                           'SMSControllers\SMSController@formSMSMasivo')->name('formSMSMasivo');
    Route::post('/sendSMSMasivo',                           'SMSControllers\SMSController@sendSMSMasivo')->name('sendSMSMasivo');
    Route::post('/formCampaignStatus',                      'SMSControllers\SMSController@formCampaignStatus')->name('formCampaignStatus');
    Route::post('/saveFormCampaignStatus',                  'SMSControllers\SMSController@saveFormCampaignStatus')->name('saveformCampaignStatus');
    Route::post('/downloadReportCampaign',                  'SMSControllers\SMSController@downloadReportCampaign')->name('downloadReportCampaign');
    Route::post('/miniDashboardSMS',                        'SMSControllers\SMSController@miniDashboardSMS')->name('miniDashboardSMS');
    Route::post('/getDataMiniDashboardSMS',                 'SMSControllers\SMSController@getDataMiniDashboardSMS')->name('getDataMiniDashboardSMS');

    /* Route SMS Not Campaign */
    Route::get('/paginationSMSSinCampanas',                 'SMSControllers\SMSController@paginationSMSSinCampanas')->name('paginationSMSSinCampanas');
    Route::match(['get', 'post'],   '/formSMSIndividual',   'SMSControllers\SMSController@formSMSIndividual')->name('formSMSIndividual');
    Route::post('/sendSMSIndividual',                       'SMSControllers\SMSController@sendSMSIndividual')->name('sendSMSIndividual');
    Route::post('/formBuilkIDUsers',                        'SMSControllers\SMSController@formBuilkIDUsers')->name('formBuilkIDUsers');
    Route::get('/paginationFormNotCampaignUsers',           'SMSControllers\SMSController@paginationFormNotCampaignUsers')->name('paginationFormNotCampaignUsers');
    Route::post('/downloadReportSMSIndividual',             'SMSControllers\SMSController@downloadReportSMSIndividual')->name('downloadReportSMSIndividual');

    /* SMS Report */
    Route::post('/listReportCampana',                       'SMSControllers\SMSController@listReportCampana')->name('listReportCampana');
    Route::post('/chartSend',                               'SMSControllers\SMSController@chartSend')->name('chartSend');
    Route::post('/chartReceived',                           'SMSControllers\SMSController@chartReceived')->name('chartReceived');

    /* Route SMS Chat */
    Route::get('/chat',                                     'SMSControllers\SMSChatController@index')->name('chat');
    Route::post('/chatSMSHistory',                          'SMSControllers\SMSChatController@chatSMSHistory')->name('chatSMSHistory');

    /* Route SMS Chat Campaign */
    Route::get('/listChatCampaign',                         'SMSControllers\SMSChatController@listChatCampaign')->name('listChatCampaign');
    Route::post('/chatNumbers',                             'SMSControllers\SMSChatController@chatNumbers')->name('chatNumbers');
    Route::post('/listChatNumbers',                         'SMSControllers\SMSChatController@listChatNumbers')->name('listChatNumbers');
    Route::get('/formChatNumberSeen',                       'SMSControllers\SMSChatController@formChatNumberSeen')->name('formChatNumberSeen');
    Route::get('/formChatNumberNotSeen',                    'SMSControllers\SMSChatController@formChatNumberNotSeen')->name('formChatNumberNotSeen');
    Route::post('/saveFormChatNumberSeen',                  'SMSControllers\SMSChatController@saveFormChatNumberSeen')->name('saveFormChatNumberSeen');
    Route::post('/chatWindow',                              'SMSControllers\SMSChatController@chatWindow')->name('chatWindow');
    Route::post('/listChatWindow',                          'SMSControllers\SMSChatController@listChatWindow')->name('listChatWindow');
    Route::post('/changeSeenChat',                          'SMSControllers\SMSChatController@changeSeenChat')->name('changeSeenChat');
    Route::post('/sendSMSChat',                             'SMSControllers\SMSChatController@sendSMSChat')->name('sendSMSChat');

    /* Route SMS Chat Not Campaign */
    Route::get('/listChatNotCampaign',                  'SMSControllers\SMSChatController@listChatNotCampaign')->name('listChatNotCampaign');

    /* Route SMS Chat View */
    Route::get('/chatViewNumbers',                      'SMSControllers\SMSChatController@chatViewNumbers')->name('chatViewNumbers');

    /* Route SMS Keyword */
    Route::get('/paginationSMSKeyword',                 'SMSControllers\SMSKeywordController@paginationSMSKeyword')->name('paginationSMSKeyword');
    Route::get('/paginationSMSKeywordSend',             'SMSControllers\SMSKeywordController@paginationSMSKeywordSend')->name('paginationSMSKeywordSend');
    Route::match(['get', 'post'],   '/formSMSKeyword',  'SMSControllers\SMSKeywordController@formSMSKeyword')->name('formSMSKeyword');
    Route::post('/saveFormSMSKeyword',                  'SMSControllers\SMSKeywordController@saveFormSMSKeyword')->name('saveFormSMSKeyword');
    Route::post('/formBaseKeywordUpload',               'SMSControllers\SMSKeywordController@formBaseKeywordUpload')->name('formBaseKeywordUpload');
    Route::post('/saveFormBaseKeywordUpload',           'SMSControllers\SMSKeywordController@saveFormBaseKeywordUpload')->name('saveFormBaseKeywordUpload');
    Route::post('/formKeywordUsers',                    'SMSControllers\SMSKeywordController@formKeywordUsers')->name('formCampaignUsers');
    Route::get('/paginationFormKeywordUsers',           'SMSControllers\SMSKeywordController@paginationFormKeywordUsers')->name('paginationFormKeywordUsers');
    Route::post('/formKeywordStatus',                   'SMSControllers\SMSKeywordController@formKeywordStatus')->name('formKeywordStatus');
    Route::post('/saveFormKeywordStatus',               'SMSControllers\SMSKeywordController@saveFormKeywordStatus')->name('saveFormKeywordStatus');
    Route::get('/formSendKeyword',                      'SMSControllers\SMSKeywordController@formSendKeyword')->name('formSendKeyword');
    Route::post('/sendSMSKeyword',                      'SMSControllers\SMSKeywordController@sendSMSKeyword')->name('sendSMSKeyword');
    Route::post('/formKeywordConfirmation',             'SMSControllers\SMSKeywordController@formKeywordConfirmation')->name('formKeywordConfirmation');
    Route::post('/saveFormKeywordConfirmation',         'SMSControllers\SMSKeywordController@saveFormKeywordConfirmation')->name('saveFormKeywordConfirmation');
    Route::post('/downloadReportSMSKeyword',            'SMSControllers\SMSKeywordController@downloadReportSMSKeyword')->name('downloadReportSMSKeyword');
    Route::post('/executeUploadKeyword',                'SMSControllers\SMSKeywordController@executeUploadKeyword')->name('executeUploadKeyword');

    /* Route BlackList */
    Route::get('/black_list',                           'BlackListController@black_list')->name('black_list');
    Route::get('/listBlackList',                        'BlackListController@listBlackList')->name('listBlackList');
    Route::get('/formBlackListIndividual',              'BlackListController@formBlackListIndividual')->name('formBlackListIndividual');
    Route::post('/saveFormBlackList',                   'BlackListController@saveFormBlackList')->name('saveFormBlackList');
    Route::get('/formBlackListMasivo',                  'BlackListController@formBlackListMasivo')->name('formBlackListMasivo');
    Route::post('/saveFormBlackListMasivo',             'BlackListController@saveFormBlackListMasivo')->name('saveFormBlackListMasivo');
    Route::post('/formBlackListDelete',                 'BlackListController@formBlackListDelete')->name('formBlackListDelete');
    Route::post('/deleteNumberBlackList',               'BlackListController@deleteNumberBlackList')->name('deleteNumberBlackList');

    /* Route Mailing */
    Route::get('mailing',                               'MailingControllers\MailController@index')->name('mailing');

    /* Route Notification SMS */
    Route::get('/getNotificationSMS',                   'SMSControllers\SMSController@getNotificationSMS')->name('getNotificationSMS');
    Route::post('/setViewSMSDetalle',                   'SMSControllers\SMSController@setViewSMSDetalle')->name('setViewSMSDetalle');

    /* Dashboard */
    Route::get('/dashboard',                            'DashboardController@index')->name('Dashboard');
    Route::post('/dashboardSMSSend',                    'DashboardController@dashboardSMSSend')->name('dashboardSMSSend');
    Route::post('/dashboardSMSReceived',                'DashboardController@dashboardSMSReceived')->name('dashboardSMSReceived');
    Route::post('/dashboardSMSMonth',                   'DashboardController@dashboardSMSMonth')->name('dashboardSMSMonth');
    Route::post('/listEstadosSMS',                      'DashboardController@listEstadosSMS')->name('listEstadosSMS');
    Route::post('/listEstadosValidatePhone',            'DashboardController@listEstadosValidatePhone')->name('listEstadosValidatePhone');
    Route::post('/dashboardValidateTelephoneDay',       'DashboardController@dashboardValidateTelephoneDay')->name('dashboardValidateTelephoneDay');
    Route::post('/dashboardNumbersValidate',            'DashboardController@dashboardNumbersValidate')->name('dashboardNumbersValidate');
    Route::post('/dashboardValidatePhoneMonth',         'DashboardController@dashboardValidatePhoneMonth')->name('dashboardValidatePhoneMonth');
    Route::post('/listBoxBases',                        'DashboardController@listBoxBases')->name('listBoxBases');
    Route::post('/dashboardTotalEncontrados',           'DashboardController@dashboardTotalEncontrados')->name('dashboardTotalEncontrados');
    Route::post('/dashboardBuscadosEncontrados',        'DashboardController@dashboardBuscadosEncontrados')->name('dashboardBuscadosEncontrados');
    Route::post('/dashboardBasesMonth',                 'DashboardController@dashboardBasesMonth')->name('dashboardBasesMonth');

    /* Route Person */
    Route::get('/search_person',                                'PersonControllers\SearchController@index')->name('search_person');
    Route::post('/getDataFormIndividualSearch',                 'PersonControllers\SearchController@getDataFormIndividualSearch')->name('getDataFormIndividualSearch');
    Route::get('/getPersonDNI',                                 'PersonControllers\SearchController@getPersonDNI')->name('getPersonDNI');
    Route::get('/paginationDataPersona',                        'PersonControllers\SearchController@paginationDataPersona')->name('paginationDataPersona');
    Route::get('/paginationDataAuto',                           'PersonControllers\SearchController@paginationDataAuto')->name('paginationDataAuto');
    Route::post('/personDetails',                               'PersonControllers\SearchController@personDetails')->name('personDetails');
    Route::get('/getDataPersona',                               'PersonControllers\SearchController@getDataPersona')->name('getDataPersona');
    Route::get('/getDataPersonSunat',                           'PersonControllers\SearchController@getDataPersonSunat')->name('getDataPersonSunat');
    Route::get('/getDataTelefonos',                             'PersonControllers\SearchController@getDataTelefonos')->name('getDataTelefonos');
    Route::get('/getDataPersonSunatGraph',                      'PersonControllers\SearchController@getDataPersonSunatGraph')->name('getDataPersonSunatGraph');
    Route::get('/getDataPersonReporteSBS',                      'PersonControllers\SearchController@getDataPersonReporteSBS')->name('getDataPersonReporteSBS');
    Route::get('/getDataPersonDetalleSBS',                      'PersonControllers\SearchController@getDataPersonDetalleSBS')->name('getDataPersonDetalleSBS');
    Route::get('/getSearchFalabella',                           'PersonControllers\SearchController@getSearchFalabella')->name('getSearchFalabella');
    Route::get('/getSearchFalabellaFirst',                      'PersonControllers\SearchController@getSearchFalabellaFirst')->name('getSearchFalabellaFirst');
    Route::post('/formPersonValidation',                        'PersonControllers\SearchController@formPersonValidation')->name('formPersonValidation');
    Route::post('/executePersonValidation',                     'PersonControllers\SearchController@executePersonValidation')->name('executePersonValidation');
    Route::post('/executePersonValidationStart',                'PersonControllers\SearchController@executePersonValidationStart')->name('executePersonValidationStart');
    Route::post('/baseEstadistica',                             'PersonControllers\SearchController@baseEstadistica')->name('baseEstadistica');

    /* Route Search Masivo */
    Route::get('/paginationBaseCampanas',                       'PersonControllers\SearchMasivoController@paginationBaseCampanas')->name('paginationBaseCampanas');
    Route::match(['get', 'post'],   '/formCampaignBase',        'PersonControllers\SearchMasivoController@formCampaignBase')->name('formCampaignBase');
    Route::post('/saveFormCampaignBase',                        'PersonControllers\SearchMasivoController@saveFormCampaignBase')->name('saveFormCampaignBase');
    Route::post('/formCampaignBaseUpload',                      'PersonControllers\SearchMasivoController@formCampaignBaseUpload')->name('formCampaignBaseUpload');
    Route::post('/saveFormCampaignBaseUpload',                  'PersonControllers\SearchMasivoController@saveFormCampaignBaseUpload')->name('saveFormCampaignBaseUpload');
    Route::post('/executeUploadCampaignBase',                   'PersonControllers\SearchMasivoController@executeUploadCampaignBase')->name('executeUploadCampaignBase');
    Route::post('/formBaseSMS',                                 'PersonControllers\SearchMasivoController@formBaseSMS')->name('formBaseSMS');
    Route::post('/formBaseCruce',                               'PersonControllers\SearchMasivoController@formBaseCruce')->name('formBaseCruce');
    Route::post('/formCampaignBaseStatus',                       'PersonControllers\SearchMasivoController@formCampaignBaseStatus')->name('formCampaignBaseStatus');
    Route::post('/saveFormCampaignBaseStatus',                  'PersonControllers\SearchMasivoController@saveFormCampaignBaseStatus')->name('saveFormCampaignBaseStatus');
    Route::post('/startCruceBase',                              'PersonControllers\SearchMasivoController@startCruceBase')->name('startCruceBase');
    Route::post('/finishCruceBase',                             'PersonControllers\SearchMasivoController@finishCruceBase')->name('finishCruceBase');
    Route::post('/downloadReportBase',                          'PersonControllers\SearchMasivoController@downloadReportBase')->name('downloadReportBase');
    Route::post('/asyncBaseValidation',                         'PersonControllers\SearchMasivoController@asyncBaseValidation')->name('asyncBaseValidation');


    /* Route Validate Phone */
    Route::get('/validacion_telefonos',                             'ValidatePhoneControllers\ValidatePhoneController@index')->name('validacion_telefonos');
    Route::get('/paginationValidacionCampanas',                     'ValidatePhoneControllers\ValidatePhoneController@paginationValidacionCampanas')->name('paginationValidacionCampanas');
    Route::match(['get', 'post'],   '/formCampaignValidacion',      'ValidatePhoneControllers\ValidatePhoneController@formCampaignValidacion')->name('formCampaignValidacion');
    Route::post('/saveFormCampaignValidacion',                      'ValidatePhoneControllers\ValidatePhoneController@saveFormCampaignValidacion')->name('saveFormCampaignValidacion');
    Route::post('/formCampaignValidacionUpload',                    'ValidatePhoneControllers\ValidatePhoneController@formCampaignValidacionUpload')->name('formCampaignValidacionUpload');
    Route::post('/saveFormCampaignValidacionUpload',                'ValidatePhoneControllers\ValidatePhoneController@saveFormCampaignValidacionUpload')->name('saveFormCampaignValidacionUpload');
    Route::post('/executeUploadCampaignValidacion',                 'ValidatePhoneControllers\ValidatePhoneController@executeUploadCampaignValidacion')->name('executeUploadCampaignValidacion');
    Route::post('/formCampaignValidacionUsers',                     'ValidatePhoneControllers\ValidatePhoneController@formCampaignValidacionUsers')->name('formCampaignValidacionUsers');
    Route::get('/paginationFormCampaignValidationTelefonos',        'ValidatePhoneControllers\ValidatePhoneController@paginationFormCampaignValidationTelefonos')->name('paginationFormCampaignValidationTelefonos');
    Route::post('/formCampaignValidacionStatus',                    'ValidatePhoneControllers\ValidatePhoneController@formCampaignValidacionStatus')->name('formCampaignValidacionStatus');
    Route::post('/saveFormCampaignValidacionStatus',                'ValidatePhoneControllers\ValidatePhoneController@saveFormCampaignValidacionStatus')->name('saveFormCampaignValidacionStatus');
    Route::post('/downloadReportCampaignValidacion',                'ValidatePhoneControllers\ValidatePhoneController@downloadReportCampaignValidacion')->name('downloadReportCampaignValidacion');
    Route::post('/formValidacionStart',                             'ValidatePhoneControllers\ValidatePhoneController@formValidacionStart')->name('formValidacionStart');
    Route::post('/formValidacionPause',                             'ValidatePhoneControllers\ValidatePhoneController@formValidacionPause')->name('formValidacionPause');
    Route::post('/executeValidacionStart',                          'ValidatePhoneControllers\ValidatePhoneController@executeValidacionStart')->name('executeValidacionStart');
    Route::post('/executeValidacionPause',                          'ValidatePhoneControllers\ValidatePhoneController@executeValidacionPause')->name('executeValidacionPause');
    Route::post('/finishValidationCampaign',                        'ValidatePhoneControllers\ValidatePhoneController@finishValidationCampaign')->name('finishValidationCampaign');
    Route::post('/miniDashboardValidacionTelefono',                 'ValidatePhoneControllers\ValidatePhoneController@miniDashboardValidacionTelefono')->name('miniDashboardValidacionTelefono');
    Route::post('/getDataMiniDashboardTelefono',                    'ValidatePhoneControllers\ValidatePhoneController@getDataMiniDashboardTelefono')->name('getDataMiniDashboardTelefono');

    /* Sincronización Score */
    Route::get('/getCarteras',                                      'ScoreController@getCarteras')->name('getCarteras');
    Route::get('/getProveedores',                                   'ScoreController@getProveedores')->name('getProveedores');
    Route::post('/generarMarcador',                                 'ScoreController@generarMarcador')->name('generarMarcador');
    Route::post('/generarGestion',                                  'ScoreController@generarGestion')->name('generarGestion');
    Route::post('/generarInactivos',                                'ScoreController@generarInactivos')->name('generarInactivos');
});